ARCH_NAME = $(shell uname -m)
ifeq (${ARCH_NAME}, x86_64)
ARCH = 64
else
ARCH = 32
endif

ifeq (${ARCH}, 64)
else
endif

ifeq (${ARCH}, 64)
GSLINCLUDE = -I /usr/include/gsl
GSLLIBS = -L /usr/lib/gsl -lgsl -lgslcblas
else
GSLINCLUDE = -I /usr/include/gsl
GSLLIBS = -L /usr/lib/gsl -lgsl -lgslcblas
endif
GSLINCLUDE := ${GSLINCLUDE} -DHAVE_INLINE -DGSL_RANGE_CHECK_OFF

ifeq (${ARCH}, 64)
LIB = lib64
BIN = bin64
else
LIB = lib32
BIN = bin32
endif

BOOST_LIBS = -lboost_iostreams -lboost_program_options

WARNINGS = -Woverloaded-virtual -Wall -Wno-sign-compare

CFLAGS = $(WARNINGS) -Iinclude $(GSLINCLUDE)
LIBFLAGS = $(GSLLIBS) $(BOOST_LIBS) -L$(LIB)
#OPTFLAGS = -g
OPTFLAGS = -O3 # -pg

vpath %.h include
vpath %.cc src
#vpath %.o lib

LIBS = \
	$(LIB)/common.o \
	$(LIB)/SymbolString.o \
	$(LIB)/Generator.o \
	$(LIB)/CRP.o \
	$(LIB)/Monkey.o \
	$(LIB)/NGramMonkey.o \
	$(LIB)/CRPLM.o \
	$(LIB)/SegSampler.o \
	$(LIB)/State.o \
	$(LIB)/MonkeyState.o \
	$(LIB)/SuffixTree.o \
	$(LIB)/Channel.o \
	$(LIB)/Phones.o \
	$(LIB)/SyllableChannel.o \
	$(LIB)/FeatureSet.o \
	$(LIB)/MaxEntSelection.o \
	$(LIB)/OWLQN.o \
	$(LIB)/TerminationCriterion.o \
	$(LIB)/FiniteDist.o \
	$(LIB)/BigramMonkey.o \
	$(LIB)/NGramMonkey.o \
	$(LIB)/CollocationMonkey.o \
	$(LIB)/SyllableMonkey.o \
	$(LIB)/LazySuffixTree.o

.SUFFIXES : 
.PRECIOUS : $(LIB)/%.o

# something :
# 	@echo "So, what should I make now?"

everything: BeamSample OracleTransducer PrintChannel InitialTransducer

$(LIB)/%.o : %.cc %.h
	g++ $(CFLAGS) $(OPTFLAGS) -c -o $@ $<

$(LIB)/%.o : %.cc
	g++ $(CFLAGS) $(OPTFLAGS) -c -o $@ $<

% : $(LIB)/%.o $(LIBS)
	g++ $(OPTFLAGS) -o $(BIN)/$@ $^ $(CFLAGS) $(LIBFLAGS)

.PHONY : clean

clean:
	-rm $(LIB)/*.o
	-rm $(BIN)/*
	-rm script/*.pyc
