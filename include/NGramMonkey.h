class NGramMonkey;

#ifndef NGRAM_MONKEY_H
#define NGRAM_MONKEY_H

#include "common.h"
#include "CRP.h"
#include "BigramMonkey.h"

class NGramMonkey : public BigramMonkey
{
public:
	NGramMonkey(int ngrams, doubles& alpha, doubles& disc, 
				bool allowEmpty, Prob pEnd, StrVocab& alphabet);
	virtual ~NGramMonkey();

	virtual int sample();
	virtual Prob update(int sample, double temp=1);
	virtual Prob remove(int sample, double temp=1);

	virtual Prob prob(int sample);
	virtual Prob biPr(int pr, int curr);

	virtual void print(ostream& os);
	virtual void printTree(ostream& os, 
						   CRP* node, SymbolString& context);

	virtual CRP* getProcess(intLst& context);
	virtual void rmProcess(intLst& context);
	virtual CRP* getProcess(CRP* curr, 
							intLst::reverse_iterator& context,
							intLst::reverse_iterator& rend,
							bool get, int level);

	int _ngrams;
	doubles _alpha;
	doubles _discount;
	CRP* _root;
	Uniform _g0;
	intProbMap _cache;
	int _updates;
};

#endif
