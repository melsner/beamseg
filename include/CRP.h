class CRP;
struct Table;

#ifndef CRP_H
#define CRP_H

#include "Generator.h"

#include <gsl_sf_gamma.h>

typedef std::list<Table*> TableLst;
typedef std::map<int, TableLst> intToTableLst;
typedef std::tr1::unordered_map<int, CRP*> intCRPMap;
typedef std::vector<CRP*> CRPs;

class CRP : public Generator
{
public:
	CRP(double& alpha, Generator* labels, double& discount);
	virtual ~CRP();

	virtual int sample();
	virtual Prob update(int sample, double temp=1);
	virtual Prob update(int sample, Table*& tab, double temp=1);
	virtual Prob remove(int sample, Table* tab);
	virtual Prob remove(int sample, double temp=1);
	virtual void addPriorCount(int sample, int count);

	virtual Prob prob(int sample);
	virtual Prob probNoG0(int sample);
	virtual Prob newTableProb();

	virtual bool canRemove(int sample);
	virtual void noNewTables();

	virtual intIntMap& labels(); //access to label -> count

	virtual void print(ostream& os);

	virtual LogProb ll(bool verbose=false);
	virtual int total();

	virtual intProbMap& beta();
	virtual void sampleBeta();
	virtual intProbMap& meanBeta(bool unigram, double posteriorCont);
	virtual void getMeanBeta(bool unigram, double posteriorCont);

	double& _alpha;
	int _total;
	int _nTables;
	int _fakeCounts;
	Generator* _prior;
	double& _discount;

	intProbMap _beta;

	intToTableLst _tables;
	intIntMap _counts;
	intIntMap _nTablesFor;

	intCRPMap _children;
	int* _betaDate;
	int _myBetaDate;
};

struct Table
{
	Table(int lab):
		count(1),
		label(lab)
	{}

	void print(ostream& os);

	int count;
	int label;
	Table* parTab;
};

ostream& operator<<(ostream& os, CRP&);
ostream& operator<<(ostream& os, Table&);

#endif
