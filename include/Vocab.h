template <class T> class Vocab;

#include <boost/functional/hash.hpp>

#ifndef VOCAB_H
#define VOCAB_H

#include "common.h"

typedef Vocab<string> StrVocab;
typedef Vocab<int> IntVocab;

template <class T>
class Vocab
{
public:
	Vocab():
		_next(0)
	{}

	Vocab(Vocab<T>& other):
		_vocab(other._vocab),
		_invVocab(other._invVocab),
		_free(other._free),
		_next(other._next)
	{
	}

	void clear()
	{
		_vocab.clear();
		_invVocab.clear();
		_next = 0;
		_free.clear();
	}

	void write(ostream& os)
	{
		writeHashMap(_vocab, os);
	}

	void read(istream& is)
	{
		readHashMap(_vocab, is);
	}

	int size()
	{
		return _vocab.size();
	}

	int get(const T& str, bool add)
	{
		typename std::tr1::unordered_map<T, int>::iterator sv = 
			_vocab.find(str);
		if(sv != _vocab.end())
		{
			return sv->second;
		}
		else
		{
			if(add)
			{
				int newKey = -1;
				if(!_free.empty())
				{
					newKey = _free.back();
					_free.pop_back();
				}
				else
				{
					newKey = _next;
					++_next;
				}

				_vocab[str] = newKey;
				_invVocab[newKey] = &_vocab.find(str)->first;
				return newKey;
			}
			else
			{
				return -1;
			}
		}
	}

	T& inv(int vi)
	{
		if(!inMap(_invVocab, vi))
		{
			cerr<<"can't find "<<vi<<"\n";
		}
		assert(inMap(_invVocab, vi));
		//cast away constancy to avoid having to type const..
		//but don't actually alter it!
		return *(const_cast<T*>(_invVocab[vi]));
	}

	void translate(Vocab& other, intIntMap& res)
	{
		for(typename std::tr1::unordered_map<T, int>::iterator word = 
				other._vocab.begin;
			word != other._vocab.end();
			++word)
		{
			int key = get(word->first, true);
			res[word->second] = key;
		}
	}

	void reap(intIntMap& keep)
	{
		ints remove;
		for(typename std::tr1::unordered_map<int, const T*>::iterator 
				word = _invVocab.begin();
			word != _invVocab.end();
			++word)
		{
			if(!inMap(keep, word->first))
			{
				// cerr<<"reaping "<<word->first<<" "<<
				// 	*const_cast<T*>(word->second)<<"\n";
				remove.push_back(word->first);

				_vocab.erase(*word->second);
			}
		}

		foreach(ints, rm, remove)
		{
			_free.push_back(*rm);
			_invVocab.erase(*rm);
		}
	}

	std::tr1::unordered_map<T, int, boost::hash<T> > _vocab;
	std::tr1::unordered_map<int, const T*> _invVocab;

	ints _free;
	int _next;
};

#endif
