#ifndef SUFFIX_TREE_H
#define SUFFIX_TREE_H

#include "common.h"
#include "SymbolString.h"

class SuffixTree;
typedef std::tr1::unordered_map<int, SuffixTree*> intSuffixMap;

struct SuffixTree
{
	SuffixTree();
	~SuffixTree();

	void deleteChild(int ch);

	void insert(SymbolString& str, Prob pr);
	void insert1(SymbolString& str, int pos, Prob pr);

	intSuffixMap& successors();

	SuffixTree* successor(int chr);

	Prob bestString();
	Prob final();

	void print(ostream& os, StrVocab& alphabet);
	void print1(ostream& os, int depth, StrVocab& alphabet);

	intSuffixMap _successors;
	Prob _bestString;
	Prob _final;
};

#endif
