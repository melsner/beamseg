class BigramMonkey;

#ifndef BIGRAM_MONKEY_H
#define BIGRAM_MONKEY_H

#include "Monkey.h"

class BigramMonkey : public Monkey
{
public:
	BigramMonkey(double alpha, Prob pBreak, Prob pEnd, StrVocab& alphabet);
	virtual ~BigramMonkey();

	virtual int sample();
	virtual Prob update(int sample, double temp=1);
	virtual Prob remove(int sample, double temp=1);

	virtual Prob prob(int sample);
	virtual Prob biPr(int pr, int curr);

	virtual void addStr(SymbolString& surf);
	virtual void addCount(int prev, int curr);

	intIntIntMap _counts;
	intIntMap _totals;
	int _total;
	double _alpha;
};

#endif
