class DPEnumerator;

#ifndef DP_ENUMERATOR_H
#define DP_ENUMERATOR_H

#include "common.h"
#include "CRP.h"

typedef std::pair<int, Prob> IntProb;

class DPEnumerator
{
public:
	struct iterator
	{
		DPEnumerator::iterator(DPEnumerator* di, Prob stop);
		DPEnumerator::iterator(DPEnumerator* di, Prob stop,
							   Prob weight);

		void next();
		bool done();
		intProb value();

		DPEnumerator* dpen;
		Prob stop;
		int cached;
		DPEnumerator::iterator* prior;
		Prob total;
	};

	DPEnumerator(CRP& distr);
	~DPEnumerator();

	DPEnumerator::iterator iter(Prob pr);

	bool next();

	CRP& distr;

	IntProbs _cached;
	int _countPtr;
	Prob _remaining;
	IntPriVec _sortedCounts;
};

typedef DPEnumerator::iterator DPIterator;

#endif
