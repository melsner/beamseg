class SyllableMonkey;

#ifndef SYLLABLE_MONKEY_H
#define SYLLABLE_MONKEY_H

#include "Monkey.h"

class SyllableMonkey : public Monkey
{
public:
	SyllableMonkey(Prob stop, bool allowEmpty, Prob pEnd, StrVocab& alphabet);
	virtual ~SyllableMonkey();

	virtual int sample();
	virtual Prob update(int sample, double temp=1);
	virtual Prob remove(int sample, double temp=1);

	virtual Prob prob(int sample);

	virtual bool onsetOk(SymbolString& str, int begin, int end);
	virtual bool codaOk(SymbolString& str, int begin, int end);

	virtual bool invalid(SymbolString& str);

	int sChar;
	intSet nuclei;
	intSet liquids;
	intSet fricatives;
	intSet stops;
	intSet nasals;
};

#endif
