class EditProposal;
class RecorderProposal;

#ifndef EDIT_PROPOSAL_H
#define EDIT_PROPOSAL_H

#include "common.h"
#include "CRPLM.h"

typedef std::map<SymbolString, Prob> symStrToProb;
typedef std::map<SymbolString, symStrToProb> symStrToSymStrToProb;
typedef std::map<SymbolString, SymbolString> SymStrToSymStr;

class EditProposal
{
public:
	EditProposal(CRPLM& model, intIntMap* surf);
	virtual ~EditProposal();

	virtual int justEd(int word, Prob& proposal, Prob& reverse, 
					   double temp, bool verbose);

	virtual int proposeMerge(int word, 
							 Prob& proposal, Prob& reverse, bool verbose);
	virtual int proposeSplit(int word, 
							 Prob& proposal, bool verbose);
	virtual Prob splitReverse(int mergeFrom, int mergeTo, bool verbose);

	Prob editDist(SymbolString& in, SymbolString& out);
	int sampleEdit(SymbolString& in, Prob& proposal);
	int randomSym(StrVocab& alphabet);
	Prob pSymbol(int sym, StrVocab& alphabet);

	CRPLM* _model;
	intIntMap* _surf;
	Prob _pIns;
	Prob _pDel;
	Prob _pSub;

	intIntMap _charCt;
	int _totalChars;

	symStrToSymStrToProb _editCache;
};

class SurfaceProposal : public EditProposal
{
public:
	SurfaceProposal(CRPLM& model, intIntIntMap* real);
	virtual ~SurfaceProposal();

	virtual int proposeMerge(int word, 
					 Prob& proposal, Prob& reverse, bool verbose);
	virtual int proposeSplit(int word, 
							 Prob& proposal, bool verbose);
	virtual Prob splitReverse(int mergeFrom, int mergeTo, bool verbose);

	intIntIntMap* _real;
};

class RecorderProposal : public EditProposal
{
public:
	RecorderProposal(CRPLM& model);
	virtual ~RecorderProposal();

	virtual int proposeMerge(int word, 
							 Prob& proposal, Prob& reverse, bool verbose);
	virtual int proposeSplit(int word, 
							 Prob& proposal, bool verbose);
	virtual Prob splitReverse(int mergeFrom, int mergeTo, bool verbose);

	void addMerge(int word, int newWord);
	void addSplit(int word, int newWord);

	SymStrToSymStr _merges;
	SymStrToSymStr _splits;
};

#endif
