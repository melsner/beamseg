class MarkovSampler;

#ifndef MARKOV_SAMPLER_H
#define MARKOV_SAMPLER_H

#include "CRPLM.h"
#include "SegSampler.h"
#include "FSTKit.h"

#include <fst/compose.h>
#include <fst/randgen.h>
#include <fst/project.h>

const int INDICES = 1;
const int SAMPLES = 2;
const int FSTS = 4;
const int TRANSITIONS = 8;
const int CUTOFF = 16;
const int PROPOSAL = 32;
const int MHR = 64;

class MarkovSampler
{
public:
	MarkovSampler(Analysis& ana, CRPLM& model, FST& misspeller,
				  LogProbArcSelector<FSTArc> selector, int verbose);
	virtual ~MarkovSampler();

	virtual void sampleIntended(int jj, int kk,
								SymbolString* targ, intProbMap& intended);
	virtual int sampleFrom(int jj, int kk);
	virtual SymbolString* getString(int jj, int kk);
	virtual LogProb transition(int jj, int kk, int tt);
	virtual LogProb endTransition(int kk, int tt);
	virtual int sampleNext(ProbMat& table, int last);
	virtual void proposeBreaks(ints& breakpoints);
	virtual LogProb pPropose(ints& breakpoints, ints& lex);
	virtual void sample(double temp);
	virtual void sampleIntendedForms(ints& breakpoints, ints& newLex);
	virtual int getCutoff();
	virtual LogProb transducerLexProb(ints& intended, ints& breakpoints);
	virtual LogProb transducerProb(int in, int out);
	virtual LogProb proposalTransProb(int prevToken, int nextToken,
									  SymbolString& targ);


	Analysis& _ana;
	CRPLM& _model;
	FST& _misspeller;
	int _nSamples;
	ProbMat _table;
	intIntIntIntProbMap _samples; //t : k : j : strid : pr
	LogProbArcSelector<FSTArc> _selector;
	int _verbose;
	int _cutoff;
};

#endif
