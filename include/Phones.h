struct Phone;
class PhoneChannel;

#ifndef PHONES_H
#define PHONES_H

#include "common.h"
#include "Vocab.h"
#include "Channel.h"
#include "SegSampler.h"
#include "MaxEntSelection.h"
#include "FeatureSet.h"

struct Phone;
typedef std::tr1::unordered_map<int, Phone*> intPhoneMap;

struct Phone
{
	Phone(intPhoneMap& table, StrVocab& alphabet,
		  string nm, string feats);

	string name;
	bool vowel;
	bool epsilon;
	int voice;
	int height;
	int sonority;
	int position;
	bool nasal;
	bool rhotic;
	bool glide;
};

void initPhones(StrVocab& alphabet, intPhoneMap& table);

class PhoneChannel : public Channel
{
public:
	PhoneChannel(StrVocab& alphabet);
	virtual ~PhoneChannel();
	virtual Channel* copy();

	virtual void print(ostream& os);
	virtual void write(ostream& os);
	virtual void read(istream& is);

	virtual Prob emitCh(int given, int produced);
	virtual Prob epsilonAfter(int prevCh);
	virtual Prob greatestEpsilonAfter();
	virtual Prob emit(State* st, int ch);

	virtual void computeProbs(int phGiven);
	virtual void computeEps(int phGiven);
	virtual void pointFeats(int given, int prod, Feats& res);
	virtual Phone* getPhone(int ph);

	virtual void learnFrom(Analysis* ana);
	virtual void estimate();
	virtual void sgd(double rate);
	virtual void mlEstimate();

	virtual int statePrevChar(State* st);

	virtual LogProb ll();

	FeatureSet fset;
	int FAITH;
	int MANNER;
	int VOICE;
	int PLACE;
	int GIVMANNER;
	int GIVVOICE;
	int GIVPLACE;
	int GLIDE;
	int GIVGLIDE;
	int OUT;

	Prob greatestEpsAfter;
	Phone* epsPh;
	intPhoneMap table;
	StrVocab& alphabet;
	MaxEntSelection learner;
	MaxEntSelection epsLearner;
	intIntProbMap prGiven;
	intIntIntMap counts;
	intProbMap prEps;
	intIntIntMap epsCounts;
};

#endif
