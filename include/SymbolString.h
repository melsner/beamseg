class SymbolString;

#ifndef SYMBOL_STRING_H
#define SYMBOL_STRING_H

#include "common.h"
#include "Vocab.h"

typedef Vocab<SymbolString> SymVocab;
typedef std::vector<SymbolString*> SymbolStrings;

class SymbolString : public ints
{
public:
	SymbolString();
	SymbolString(StrVocab& alphabet);
	SymbolString(string& readFrom, StrVocab& alphabet);
	SymbolString(istream& is, StrVocab& alphabet);

	virtual void resize(int sz);

	void appendRange(SymbolString& other, int begin, int end);

	void print(ostream& os);
	void write(ostream& os, int begin=-1, int end=-1);
	void read(istream& is);

	void append(SymbolString& other);

	bool is(const string& str);

	StrVocab& alphabet();

	StrVocab* _alphabet;
};

ostream& operator<<(ostream& os, SymbolString&);

#endif
