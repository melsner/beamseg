class Alignment;

#ifndef ALIGNMENT_H
#define ALIGNMENT_H

#include "common.h"

class Alignment : public ints
{
public:
	Alignment();
	Alignment(int begin, int size);
	Alignment(const Alignment& other);

	void makeOneToOne();
	void checkOneToOne();
	void checkMonotonic();

	int split(int targPos, Alignment& onRight);
	void merge(Alignment& onRight);

	bool valid();
	bool cleared();
	void setValid(int state);

	int _begin;
	int _srcSize;
	int _valid;
};

#endif
