#ifndef ANNEAL_RATE_H
#define ANNEAL_RATE_H

#include "common.h"

struct AnnealRate;

typedef std::vector<AnnealRate*> AnnealRates;

struct AnnealRate
{
	AnnealRate(double orr):
		orig(orr)
	{}

	virtual ~AnnealRate()
	{}

	virtual double next()
	{
		return orig;
	}

	virtual double channelTemp()
	{
		return 1.0 / orig;
	}

	virtual bool done()
	{
		return false;
	}

	double orig;
};

struct LinearRate : public AnnealRate
{
	LinearRate(double orr, double fin, int its):
		AnnealRate(orr),
		final(fin),
		iters(its),
		rate(0),
		curr(orr),
		ct(0)
	{
		if(iters == 0)
		{
			++iters;
		}
		rate = (final - orig) / iters;
	}

	virtual ~LinearRate()
	{}

	virtual double next()
	{
		double temp = curr;
		curr += rate;
		++ct;
		return temp;
	}

	virtual bool done()
	{
		return ct == iters;
	}

	double final;
	int iters;
	double rate;
	double curr;
	int ct;
};

struct InvRate : public AnnealRate
{
	InvRate(AnnealRate* sb):
		AnnealRate(0),
		sub(sb)
	{}

	virtual ~InvRate()
	{
		delete sub;
	}

	virtual double next()
	{
		double sb = sub->next();
		if(sb == 0)
		{
			return 1e5;
		}
		return 1 / sb;
	}

	virtual bool done()
	{
		return sub->done();
	}

	AnnealRate* sub;
};

struct NegRate : public AnnealRate
{
	NegRate(double c0, AnnealRate* sb):
		AnnealRate(0),
		center(c0),
		sub(sb)
	{}

	virtual ~NegRate()
	{
		delete sub;
	}

	virtual double next()
	{
		return center - sub->next();
	}

	virtual bool done()
	{
		return sub->done();
	}

	double center;
	AnnealRate* sub;
};

struct ExponentialRate : public LinearRate
{
	ExponentialRate(double orr, double fin, int its):
		LinearRate(orr, fin, its)
	{
		rate = expl(logl(final / orig) / iters);
	}

	virtual ~ExponentialRate()
	{}

	virtual double next()
	{
		double temp = curr;
		curr *= rate;
		++ct;
		return temp;
	}
};

struct PiecewiseRate : public AnnealRate
{
	PiecewiseRate(AnnealRate* one, AnnealRate* two):
		AnnealRate(0),
		first(one),
		second(two)
	{}

	virtual ~PiecewiseRate()
	{
		delete first;
		delete second;
	}

	virtual double next()
	{
		if(first->done())
		{
			return second->next();
		}
		return first->next();
	}

	virtual bool done()
	{
		return second->done();
	}

	AnnealRate* first;
	AnnealRate* second;
};

struct StepwiseRate : public AnnealRate
{
	StepwiseRate():
		AnnealRate(0),
		currRate(0)
	{}

	virtual ~StepwiseRate()
	{
		foreach(AnnealRates, rate, rates)
		{
			delete *rate;
		}
	}

	virtual double next()
	{
		double temp = rates[currRate]->next();
		if(rates[currRate]->done())
		{
			++currRate;
		}
		return temp;
	}

	virtual bool done()
	{
		return currRate == rates.size();
	}

	virtual void add(AnnealRate* rate)
	{
		rates.push_back(rate);
	}

	AnnealRates rates;
	int currRate;
};

#endif
