#ifndef FST_KIT_H
#define FST_KIT_H

#include "SymbolString.h"
#include "Vocab.h"
#include "CRP.h"

const int MAX_WORD_LEN = 200;

class FSTKit
{
public:
	FSTKit(StrVocab& alphabet);

	void stringToFst(SymbolString& str, FST& out);
	SymbolString* fstToString(FST& out, bool input=true);
	LogProb fstProb(FST& out);
	void monkeyFst(Prob stop, bool allowEmpty, FST& out);
	void vocabFst(CRP* distr, FST& g0, SymVocab& vocab, intIntMap& endStates,
				  FST& out);
	void updateFst(CRP* distr, SymVocab& vocab, intIntMap& endStates,
				   FST& out);
	void bigramVocabFst(CRP* distr, FST& g0, SymVocab& vocab, FST& out);
	void identityMisspell(FST& out);
	void xMisspell(FST& out, Prob error);
	void probUnion(FST& out, FST& other, LogProb weight);

	StateId sampleState(Probs& prevProbs, StateId currState,
						FST& fst, const FSTArc*& arc);
	LogProb sampleSeq(FST& fst, FST& out, int verbose);

	int depth(FST& fst);
	int depth1(FST& fst, StateId state);

	StateId insertIntoTrie(SymbolString& str, int ctr, FST& out, int state,
						   LogProb pr);
	void scaleWeights(FST& out, LogProb wt);

	void composeWithString(FST& in, SymbolString& str, FST& out);
	StateId compose1(FST& in, StateId state, FST& out,
					 SymbolString& str, int ptr);


	LogProb sampleComposedWithString(FST& fst, FST& out, 
									 SymbolString& str,
									 int verbose);
	StateId sampleState(intProbMap& prevProbs, StateId currState,
						intIntProbMap& transitions, int chr,
						FST& fst, const FSTArc*& arc, bool verbose);

	StrVocab& _alphabet;
};

#endif
