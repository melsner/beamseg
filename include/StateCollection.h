#ifndef STATE_COLLECTION_H
#define STATE_COLLECTION_H

class StateCollection;
struct StateCollection::iterator
{
	State* operator*();
	

typedef std::vector<StateSet> StateTiers;

class StateCollection
{
public:
	StateCollection();

	void clear();
	int size();
	bool empty();

	SCIter begin();
	SCIter end();

	int tiers();
	StateSet& tier(int ti, Prob threshold);
	State* origState();

	void add(State* st);

	void clearTier(int tier, Prob threshold);
	void mergeTier(int tier);

	States origStates;
	StateTiers allStates;
	StateSet currTier;
	State* orig;
};

#endif
