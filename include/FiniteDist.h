class FiniteDist;

#ifndef FINITE_DIST_H
#define FINITE_DIST_H

#include "CRP.h"
#include "Monkey.h"

class FiniteDist : public CRP, public Monkey
{
public:
	FiniteDist(double& dummy, Prob pEnd, StrVocab& alphabet);
	virtual ~FiniteDist();

	virtual Prob update(int sample, double temp=1);
	virtual Prob remove(int sample, double temp=1);
	virtual Prob prob(int sample);
	virtual Prob probNoG0(int sample);
	virtual Prob newTableProb();
};

#endif
