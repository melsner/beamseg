struct State;
struct DeleteState;
struct InsertState;
class Analysis;

#include "common.h"

typedef std::pair<State*, Prob> Transition;
typedef std::vector<Transition> Transitions;
typedef std::map<State*, Prob> StateToProb;
typedef std::vector<State*> States;

#ifndef STATE_H
#define STATE_H

#include "LazySuffixTree.h"
#include "SuffixTree.h"
#include "CRP.h"

enum {SORT_EMIT, SORT_DELETE, SORT_REC, SORT_POSTDELETE, SORT_MONKEY};

struct State
{
	State(int ind, int pv, Analysis&);
	virtual ~State();

	virtual void wipe();

	virtual State* canonicalize(int prev, SymbolString& currStr);

	virtual bool less(const State& other) const;
	virtual int sortType() const;

	virtual void linkTo(State* st, Prob pr, Prob ucorr);

	virtual void print(ostream& os);
	virtual void printLattice(ostream& os);
	virtual void printLattice1(ostream& os, int depth);

	virtual void setU(Prob uLimit, CRP* distr);

	virtual Prob emit();
	virtual int emitted();
	virtual int lastChar();

	virtual bool newWord() const;
	virtual bool end();

	virtual void expand();
	virtual State* sample();

	virtual LogProb pathProb(State* prev);
	virtual LogProb channelCost();

	int index;
	int prevWord;
	SymbolString chars;

	Prob uu;

	LogProb prob;
	LogProb corr;

	LazySuffixTree* suffix;
//	SuffixTree* suffix;

	StateToProb predecessors;
	StateToProb successors;

	State* path;

	Analysis& ana;
};

struct DeleteState : public State
{
	DeleteState(int ind, int pv, bool isPost, Analysis&);
	virtual ~DeleteState();
	virtual int sortType() const;
	virtual void print(ostream& os);
	virtual int emitted();
	virtual State* canonicalize(int prev, SymbolString& currStr);

	bool postDelete;
};

struct InsertState : public State
{
	InsertState(int ind, int pv, Analysis&);
	virtual ~InsertState();
	virtual void print(ostream& os);
	virtual int lastChar();
	virtual bool newWord() const;
	virtual State* canonicalize(int prev, SymbolString& currStr);
};

ostream& operator<<(ostream& os, State& st);
bool operator<(State& aa, State& bb);
bool operator<(const State& aa, const State& bb);
bool deleteType(int xx);

#endif
