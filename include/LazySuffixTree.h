#ifndef LAZY_SUFFIX_TREE_H
#define LAZY_SUFFIX_TREE_H

#include "common.h"
#include "SymbolString.h"
#include "Vocab.h"

typedef std::pair<Prob, Prob> ProbCorr;
typedef ProbCorr Payload;

struct LazySuffixTree;
//typedef std::tr1::unordered_map<int, LazySuffixTree*> intLazySuffixMap;
typedef std::vector<LazySuffixTree*> LazySuffixTrees;
typedef std::pair<int, Payload> intPayload;
typedef std::vector<intPayload> intPayloads;

struct LazySuffixTree
{
	LazySuffixTree(int nchars, int depth, SymVocab* lexicon);
	LazySuffixTree(int nchars, int depth, bool wipeCache, SymVocab* lexicon);
	~LazySuffixTree();

	void deleteChild(int ch);

//	intLazySuffixMap& successors();
	LazySuffixTrees& successors();

	void insert(int sym, Prob pr, Prob corr);
	void insert(SymbolString& str, Prob pr, Prob corr);
	void insert1(int sym, SymbolString& str, int pos, Payload pr);
	void insert2(int sym, Payload pr);

	LazySuffixTree* successor(int chr);

	intPayloads& cache();

	Prob bestString();
	Prob final();
	Prob finalCorr();

	void print(ostream& os, StrVocab& alphabet);
	void print1(ostream& os, int depth, StrVocab& alphabet);

//	intLazySuffixMap _successors;
	LazySuffixTrees _successors;
	Prob _bestString;
	Payload _final;
	int _depth;
	bool _wipeCache;
	bool _forwarded;
	intPayloads _toInsert;
	SymVocab* _vocab;
};

#endif
