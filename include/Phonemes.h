class Phonemes;
class Phoneme;

#ifndef PHONEMES_H
#define PHONEMES_H

#include "common.h"

//possibly worth using a more modern systematization?
enum {F_MANNER, F_VOICE, F_PLACE, F_NASAL, F_RHOTIC, F_HEIGHT, F_OFFGLIDE,
	  F_LAST};
enum {M_STOP, M_FRICATIVE, M_VOWEL};
enum {V_VOICED, V_VOICELESS};
enum {P_LABIAL, P_ALVEOLAR, P_PALATAL, P_VELAR, P_GLOTTAL};
enum {N_NONNASAL, N_NASAL};
enum {R_NONRHOTIC, R_RHOTIC};
enum {H_LOW, H_MIDLOW, H_MID, H_MIDHIGH, H_HIGH};
enum {OFFGLIDE_NONE, OFFGLIDE_Y, OFFGLIDE_W};

typedef std::tr1::unordered_map<string, Phoneme> strPhonemeMap;

class Phoneme : public ints
{
public:
	Phoneme(string s):
		ints(F_LAST),
		name(s)
	{}

	Phoneme():
		name("error")
	{}

	void print(ostream& os);
	string valToString(int feat);

	string name;		
};

class Phonemes
{
public:
	Phonemes();

	double sim(Phoneme& f1, Phoneme& f2);
	double fSim(Phoneme& f1, Phoneme& f2);
	Phoneme& get(string& sym);
	Phoneme& get(const char* sym);

	void addVowel(string sym, int ht, int place);
	void addNasal(string sym, int plc);
	void addCons(string sym, int vc, int plc, int mann);

	strPhonemeMap& contents();

	static Phonemes brent();
	static Phonemes buckeye();
	static Phonemes buckeyePure();

	strPhonemeMap _contents;
};

#endif
