#include "common.h"

class Generator;
class Counter;

#ifndef GENERATOR_H
#define GENERATOR_H

class Generator
{
public:
	Generator();
	virtual ~Generator();
	virtual int sample();
	virtual Prob update(int sample, double temp=1);
	virtual Prob remove(int sample, double temp=1);

	virtual int draw();

	virtual void print(ostream& os);

	virtual Prob prob(int sample);

	virtual bool canRemove(int sample);

	virtual LogProb ll(bool verbose=false);
};

class Counter : public Generator
{
public:
	Counter(int start=0);
	virtual ~Counter();

	virtual int sample();
	virtual Prob update(int sample, double temp=1);
	virtual Prob remove(int sample, double temp=1);

	virtual Prob prob(int sample);

	int _next;
};

class Uniform : public Generator
{
public:
	Uniform(int start, int end);
	virtual ~Uniform();

	virtual int sample();
	virtual Prob update(int sample, double temp=1);
	virtual Prob remove(int sample, double temp=1);

	virtual Prob prob(int sample);

	int _start;
	int _end;
	int _n;
};

ostream& operator<<(ostream& os, Generator& gen);

#endif
