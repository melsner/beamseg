class SegSampler;
struct Segment;
struct Analysis;

#ifndef SEG_SAMPLER_H
#define SEG_SAMPLER_H

#include "CRPLM.h"
#include "SymbolString.h"
#include "State.h"
#include "StateSet.h"
#include "Channel.h"
#include "Phones.h"

typedef std::map<int, Segment*> intToSeg;
typedef std::vector<Segment> Segments;
typedef std::vector<Analysis*> Analyses;

typedef std::pair<Analysis*, Segment*> Site;
typedef std::vector<Site> Sites;
typedef std::pair<int, int> SiteDesc;
typedef std::tr1::unordered_map<SiteDesc, Site, PairHash> SiteMap;
typedef std::tr1::unordered_map<int, SiteMap> WordSiteMap;

const int DBG_STATES = 1;
const int DBG_MHR = 2;
const int DBG_U = 4;
const int DBG_FILTER = 8;
const int DBG_WORDS = 16;

class SegSampler
{
public:
	SegSampler(CRPLM& model);
	virtual ~SegSampler();

	virtual void add(SymbolString& seq);
	virtual void add(Analysis* ana);

	virtual void split(int seq, int pos);
	virtual bool split(Analysis* seq, int pos);
	virtual void merge(Analysis* seq, int pos);

	virtual void sampleHypers(bool verbose, double temp);
	virtual void sampleHyper(double& hyper, double lower, double upper,
							 double sigma, double temp, bool verbose);

	virtual LogProb sgwStyleResample(double temp);

	virtual Analysis& sequence(int seq);
	virtual int numSequences();

	virtual void print(ostream& os);
	virtual void summarize(ostream& os);
	virtual void writeSeqs(ostream& os);
	virtual void printLexicon(ostream& os);
	virtual void printPhoneDist(ostream& os);

	virtual void writeSurface(ostream& os);
	virtual void writeUnderlying(ostream& os);

	virtual void reapVocab();

	int randomChar(StrVocab& alphabet);

	Analyses _seqs;
	CRPLM* _model;
};

struct Segment
{
	Segment(int i, int j, SymbolString& targ, CRPLM* model);
	Segment(int i, int j, SymbolString& targ, 
			SymbolString& underlying, int read, 
			CRPLM* model);

	Segment* copy(CRPLM* model);

	Segment* split(int pos, CRPLM* model);
	Segment* merge(Segment& other, CRPLM* model);
	bool isStartOrEnd();
	SymbolString* targetSubstr();

	void computeTargetSubstring();

	int begin;
	int end;
	Table* tab;
	SymbolString* target;
	SymbolString str;
	int strSym;
	SymbolString targetSubstring;
};

struct Analysis
{
	Analysis(int index, SymbolString& surf, CRPLM* mod);
	~Analysis();

	Analysis* copy(int ind, CRPLM* mod);

	int nSyms();
	int nSegments();

	bool posIsBreak(int pos);

	Segment* segAt(int pos);
	Segment* nextSegment(Segment& seg);
	Segment* prevSegment(Segment& seg);
	bool split(int pos);
	void merge(int pos);

	void ensureSplit(int pos);
	void ensureMerge(int pos);

	void print(ostream& os);
	void write(ostream& os);

	void writeSurface(ostream& os);
	void writeUnderlying(ostream& os);

	State* primeStates();
	void computeUs();

	State* getState(int ind, int prev, SymbolString& read, int readTo);
	State* getState(int ind, int prev, SymbolString& read, int readTo,
					int addCh);
	State* getDeleteState(int ind, int prev, SymbolString& read,
						  int readTo, int addCh, bool isPost);
	State* getInsertState(int ind, int prev, SymbolString& read,
						  int readTo);
	State* getMonkey(int ind, int prevCh, int addCh);
	State* registerState(State* st);

	void forward();
	void backward();
	void trace();

	Prob getULimit(State* st, CRP* proc);
	Prob getLocalU(State* st);
	Prob getLocalU(int ind);
	Prob pLocalU(int ind, Prob emit);
	LogProb pWordU(int ind, Prob trans);
	Prob sampleLocalU(Prob trans);
	Prob sampleWordU(Prob trans);

	LazySuffixTree* unkTree(int pos);

	void mhrSweep();
	void decanonicalize();
	void handleLengthAlterations(intSet& added);
	void handleCollocations(intSet& added);

	void onlyResample();

	bool wordAllowed(int word, int pos);
	bool checkWordAllowed(int word, int pos);
	void makeEmitProbTable();
	void makeGeneralSuffixTree();

	void checkWordsAtPos(int pos);
	void checkWordsAtPos1(int pos, SymbolString& checkStr,
						  LazySuffixTree* suffix, int depth);

	bool boundaryAt(int pos);

	intToSeg segments; //*end* pos of segment -> segment obj
	SymbolString surface;
	CRPLM* model;
	int index;

	intSet allowedBounds;
	bool lockBounds;

	State* root;
	StateSet states;
	bool noU;
	bool allowDeletions;
	Prob nextU;
	Prob currHpr;
	Prob prevPr;
	intToProb localUs;
	intToProb localTrans;
	intToProb wordUs;
	intToProb wordTrans;
	int verbose;

	intMat3 emitAllowed;
	intSetVec wordsAllowed;

	bool integrateOut;
	bool bigramMonkeys;
	Prob invTemp;
	Prob channelInvTemp;
	LazySuffixTrees unkTrees;
	LazySuffixTree* allVocab;
};

ostream& operator<<(ostream& os, Analysis&);
ostream& operator<<(ostream& os, SegSampler&);

Prob forcedAlign(SymbolString& in, SymbolString& out, PhoneChannel* ch,
				 bool learn);

#endif
