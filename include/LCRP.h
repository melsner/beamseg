class LCRP;

#ifndef LCRP_H
#define LCRP_H

#include "CRP.h"

class LCRP : public CRP
{
public:
	LCRP(double& alpha, Generator* labels, double& discount);
	virtual ~LCRP();

	virtual int sample();
	virtual Prob update(int sample, double temp=1);
	virtual Prob remove(int sample, double temp=1);
	virtual void addPriorCount(int sample, int count);

	virtual Prob prob(int sample);

	virtual LogProb ll(bool verbose=false);
	virtual void print(ostream& os);

	double _discountFactor;
	intIntMap _initial;
};

#endif
