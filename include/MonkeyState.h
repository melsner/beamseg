struct MonkeyState;
struct BiMonkeyState;

#ifndef MONKEY_STATE_H
#define MONKEY_STATE_H

#include "State.h"

struct MonkeyState : public State
{
	MonkeyState(int ind, Prob stop, Analysis& ana);
	virtual ~MonkeyState();

	virtual int sortType() const;

	virtual State* canonicalize(int prev, SymbolString& str);

	virtual void print(ostream& os);
	virtual bool newWord() const;
	virtual Prob transProb(int ch);
	virtual Prob stop();

	virtual void expand();

	Prob _stop;
};

struct BiMonkeyState : public MonkeyState
{
	BiMonkeyState(int ind, int prevCh, int addCh, Analysis& ana);
	virtual ~BiMonkeyState();

	virtual void print(ostream& os);

	virtual Prob transProb(int ch);
	virtual Prob stop();
};

#endif
