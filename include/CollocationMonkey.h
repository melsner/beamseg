class CollocationMonkey;

#ifndef COLLOCATION_MONKEY_H
#define COLLOCATION_MONKEY_H

#include "Monkey.h"
#include "CRP.h"

class CollocationMonkey : public Monkey
{
public:
	CollocationMonkey(double alpha, Prob pBreak, Prob pEnd,
					  StrVocab& alphabet);

	virtual ~CollocationMonkey();

	virtual int sample();
	virtual Prob update(int sample, double temp=1);
	virtual Prob remove(int sample, double temp=1);

	virtual Prob prob(int sample);

	virtual Prob pEmptyConjunct();

	virtual void printMetaLexicon(ostream& os);

	virtual int intern(SymbolString& sym);
	virtual SymbolString& repr(int sample);

	int _empty;
	int _total;
	double _aEmpty;

	double _a1;
	double _dummy;

	CRP* _freqs;
	Monkey _base;
};

#endif
