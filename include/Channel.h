class Channel;

#ifndef CHANNEL_H
#define CHANNEL_H

#include "common.h"
#include "State.h"

class Channel
{
public:
	Channel();
	virtual ~Channel();
	virtual Channel* copy();

	virtual Prob emitCh(int given, int produced);
	virtual Prob epsilonAfter(int prevCh);

	virtual Prob emit(State* st, int ch);

	virtual LogProb ll();

	intIntIntMap _phoneCounts;
	intIntMap _epsCounts;
};

class XChannel : public Channel
{
public:
	XChannel(Prob misspell, int xx);
	virtual ~XChannel();
	virtual Channel* copy();

	virtual Prob emitCh(int given, int produced);
	virtual Prob emit(State* st, int ch);

	Prob _misspell;
	int _xx;
};	

#endif
