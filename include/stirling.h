
intIntIntMap STIRLING;

inline int stirling(int nn, int mm)
{
	if(nn == 0 && mm == 0)
	{
		return 1;
	}
	else if(mm == 0)
	{
		return 0;
	}
	else if(nn == mm)
	{
		return 1;
	}
	else if(mm > nn)
	{
		return 0;
	}

	intIntMap& nmap = STIRLING[nn];
	intIntMap::iterator entry = nmap.find(mm);
	if(entry != nmap.end())
	{
		return entry->second;
	}

	int res = stirling(nn - 1, mm - 1) + n * stirling(nn - 1, mm);
	entry->second = res;
	return res;
}
