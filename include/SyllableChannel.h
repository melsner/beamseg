struct SyllableChannel;

#ifndef SYLLABLE_CHANNEL_H
#define SYLLABLE_CHANNEL_H

#include "common.h"
#include "Vocab.h"
#include "Channel.h"
#include "Phones.h"

class SyllableChannel : public PhoneChannel
{
public:
	SyllableChannel(StrVocab& alphabet, StrVocab& internalAlphabet);
	virtual ~SyllableChannel();

	virtual void print(ostream& os);

	virtual void learnFrom(Analysis* ana);

	virtual void computeProbs(int phGiven);
	virtual void computeEps(int phGiven);

	virtual void splitPhones(int sym, SymbolString& res);

	virtual void read(istream& is);
	virtual void write(ostream& os);

	int editDist(SymbolString& si, SymbolString& sj);

	StrVocab& sylAlphabet;
	StrVocab& innerAlph;
	PhoneChannel* base;
};

#endif
