#ifndef STATE_SET_H
#define STATE_SET_H

#include "State.h"

struct comp_state : public std::binary_function<State*, State*, bool>
{
	bool operator()(const State* aa, const State* bb)
	{
		return aa->less(*bb);
	}
};

typedef std::set<State*, comp_state> StateSet;

#endif
