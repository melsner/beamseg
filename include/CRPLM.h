class CRPLM;

#ifndef CRPLM_H
#define CRPLM_H

#include "common.h"
#include "CRP.h"
#include "SegSampler.h"
#include "Monkey.h"
#include "Channel.h"

#include <boost/functional/hash.hpp>

class CRPLM
{
public:
	CRPLM(int ngrams, doubles& alpha, 
		  doubles& discount, Monkey* makeWords, Channel* ch);
	virtual ~CRPLM();

	virtual CRPLM* copy();

	virtual void sampleSeq(ints& seq);
	virtual void resampleTables(ints& seq, TableLst& tabs,
								double temp, bool verbose);
	virtual LogProb updateSeq(ints& seq, double temp, bool verbose);
	virtual LogProb updateSeq(ints& seq, TableLst& tabs, 
							  double temp, bool verbose);
	virtual void drawSeq(ints& seq);
	virtual LogProb removeSeq(ints& seq, TableLst& tabs, 
							  double temp, bool verbose);
	virtual LogProb removeSeq(ints& seq, double temp, bool verbose);

	virtual LogProb probWithBreak(Analysis* ana, int pos);
	virtual LogProb probNoBreak(Analysis* ana, int pos);

	virtual Prob posteriorPCont();

	virtual Prob segProb(Analysis* ana, Segment* seg, bool verbose);
	virtual Prob segProbFull(Analysis* ana, Segment* seg, bool verbose);
	virtual Prob updateSeg(Analysis* ana, Segment* seg,
						   bool verbose, double temp);
	virtual Prob updateSegProduction(Analysis* ana, Segment* seg, 
									 intLst& context, bool verbose, 
									 double temp);
	virtual Prob removeSeg(Analysis* ana, Segment* seg,
						   bool verbose, double temp);
	virtual Prob removeSegProduction(Analysis* ana, Segment* seg, 
									 intLst& context, bool verbose, 
									 double temp);

	virtual intIntMap& lexicon();

	virtual SymbolString& repr(int word);
	virtual int intern(SymbolString& word);

	virtual CRP* getProcess(intLst& context);
	virtual CRP* getProcessOrEmpty(intLst& context);
	virtual void rmProcess(intLst& context);
	virtual CRP* getProcess(CRP* curr, 
							intLst::reverse_iterator& context,
							intLst::reverse_iterator& rend,
							bool get, int level, bool empty);

	virtual LogProb ll(bool verbose=false);
	virtual LogProb treeLL(CRP* node);
	virtual int totalTables(CRP* node);

	int _ngrams;
	doubles _alpha;
	doubles _discount;
	Monkey* _prior;
	Channel* _channel;

	CRP* _root;
	CRPs _empty;

	int _nWords;
	int _nUtts;
	intIntMap _uttLengths;
	int _betaDate;

	intIntMap _rawCounts;
	strIntMap stats;
	Probs statsgap;
};

#endif
