class Monkey;

#ifndef MONKEY_H
#define MONKEY_H

#include "common.h"

#include "Generator.h"
#include "Vocab.h"
#include "SymbolString.h"

class Monkey : public Generator
{
public:
	Monkey(Prob stop, bool allowEmpty, Prob pEnd, StrVocab& alphabet);
	virtual ~Monkey();

	virtual Monkey* copy();

	virtual int intern(SymbolString& sym);

	virtual int sample();
	virtual Prob update(int sample, double temp=1);
	virtual Prob remove(int sample, double temp=1);

	virtual Prob prob(int sample);

	virtual SymbolString& repr(int sample);

	virtual int randomSym();

	Prob _stop;
	bool _allowEmpty;
	Prob _pEnd;
	StrVocab* _alphabet;
	SymVocab _lexicon;
};

#endif
