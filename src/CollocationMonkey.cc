#include "CollocationMonkey.h"

CollocationMonkey::CollocationMonkey(double alpha, Prob pBreak,
									 Prob pEnd, StrVocab& alphabet):
	Monkey(pBreak, false, pEnd, alphabet),
	_empty(0),
	_total(0),
	_aEmpty(alpha),
	_a1(alpha),
	_dummy(0),
	_base(pBreak, false, pEnd, alphabet)
{
	_freqs = new CRP(_a1, &_base, _dummy);
}

CollocationMonkey::~CollocationMonkey()
{
	delete _freqs;
}

Prob CollocationMonkey::prob(int sample)
{
	SymbolString& unintern = repr(sample);
	int nSyms = unintern.size();
	if(nSyms == 0) //the empty string
	{
		if(_allowEmpty) //as an actual string
		{
			return _stop;
		}
		else //as start-end (no! even as end token this causes issues)
		{
			return _pEnd;
		}
	}

	Prob total = 0;
	for(int split = 0; split < nSyms; ++split)
	{
		SymbolString left(*_alphabet);
		left.appendRange(unintern, 0, split);
		SymbolString right(*_alphabet);
		right.appendRange(unintern, split, nSyms);

		Prob pLeft;
		if(left.empty())
		{
			pLeft = pEmptyConjunct();
		}
		else
		{
			pLeft = _freqs->prob(intern(left));
		}

		Prob pRight;
		if(right.empty())
		{
			pRight = pEmptyConjunct();
		}
		else
		{
			pRight = _freqs->prob(intern(right));
		}

		total += pLeft * pRight;
	}

	assertProb(total);
	return total;
}

Prob CollocationMonkey::update(int sample, double temp)
{
	SymbolString& unintern = repr(sample);
	int nSyms = unintern.size();
	if(nSyms == 0) //the empty string
	{
		return _pEnd;
	}

	Probs terms;
	Prob total = 0;
	typedef std::pair<SymbolString*, SymbolString*> stPair;
	std::vector< stPair > strs;
	for(int split = 0; split < nSyms; ++split)
	{
		SymbolString* left = new SymbolString(*_alphabet);
		left->appendRange(unintern, 0, split);
		SymbolString* right = new SymbolString(*_alphabet);
		right->appendRange(unintern, split, nSyms);

		stPair curr(left, right);
		strs.push_back(curr);

		Prob pLeft;
		if(left->empty())
		{
			pLeft = pEmptyConjunct();
		}
		else
		{
			pLeft = _freqs->prob(intern(*left));
		}

		Prob pRight;
		if(right->empty())
		{
			pRight = pEmptyConjunct();
		}
		else
		{
			pRight = _freqs->prob(intern(*right));
		}

		total += pLeft * pRight;
		terms.push_back(pLeft * pRight);
	}

	Prob sm = gsl_rng_uniform(RNG);
	Prob soFar = 0;
	for(int ii = 0; ii < nSyms; ++ii)
	{
		soFar += terms[ii] / total;
		if(sm <= soFar)
		{
			SymbolString* left = strs[ii].first;
			SymbolString* right = strs[ii].second;

			if(!left->empty())
			{
				_freqs->update(intern(*left));
			}
			else
			{
				_empty += 1;
			}

			if(!right->empty())
			{
				_freqs->update(intern(*right));
			}
			else
			{
				_empty += 1;
			}
			_total += 1;

			break;
		}
	}

	for(int ii = 0; ii < nSyms; ++ii)
	{
		delete strs[ii].first;
		delete strs[ii].second;
	}

	return total;
}

Prob CollocationMonkey::remove(int sample, double temp)
{
	SymbolString& unintern = repr(sample);
	int nSyms = unintern.size();
	if(nSyms == 0) //the empty string
	{
		return _pEnd;
	}

	Probs terms;
	Prob total = 0;
	typedef std::pair<SymbolString*, SymbolString*> stPair;
	std::vector< stPair > strs;
	for(int split = 0; split < nSyms; ++split)
	{
		SymbolString* left = new SymbolString(*_alphabet);
		left->appendRange(unintern, 0, split);
		SymbolString* right = new SymbolString(*_alphabet);
		right->appendRange(unintern, split, nSyms);

		stPair curr(left, right);
		strs.push_back(curr);

		Prob pLeft;
		if(left->empty())
		{
			pLeft = _empty / (Prob)_total;
		}
		else
		{
			pLeft = _freqs->probNoG0(intern(*left));
		}

		Prob pRight;
		if(right->empty())
		{
			pRight = _empty / (Prob)_total;
		}
		else
		{
			pRight = _freqs->probNoG0(intern(*right));
		}

		total += pLeft * pRight;
		terms.push_back(pLeft * pRight);
	}

	Prob sm = gsl_rng_uniform(RNG);
	Prob soFar = 0;
	for(int ii = 0; ii < nSyms; ++ii)
	{
		soFar += terms[ii] / total;
		if(sm <= soFar)
		{
			SymbolString* left = strs[ii].first;
			SymbolString* right = strs[ii].second;

			if(!left->empty())
			{
				_freqs->remove(intern(*left));
			}
			else
			{
				_empty -= 1;
			}

			if(!right->empty())
			{
				_freqs->remove(intern(*right));
			}
			else
			{
				_empty -= 1;
			}
			_total -= 1;

			assert(_empty >= 0);
			assert(_total >= 0);

			break;
		}
	}

	for(int ii = 0; ii < nSyms; ++ii)
	{
		delete strs[ii].first;
		delete strs[ii].second;
	}

	return total;
}

int CollocationMonkey::sample()
{
	assert(0);
	return 0;
}

Prob CollocationMonkey::pEmptyConjunct()
{
	Prob res = (_empty + _aEmpty) / (_total + 2 * _aEmpty);
	assertProb(res);
	return res;
}

void CollocationMonkey::printMetaLexicon(ostream& os)
{
	IntPriVec sorter;
	foreach(intIntMap, word, _freqs->_counts)
	{
		sorter.push_back(IntPriNode(word->second, word->first));
	}
	sort(sorter.begin(), sorter.end(), IntGreater());

	os<<"--- meta lexicon ("<<
		_freqs->_counts.size()<<" words) --- \n";

	foreach(IntPriVec, node, sorter)
	{
		os<<repr(node->data)<<"\t"<<node->pri<<"\n";
	}

	os<<"Prob of empty conjunct "<<pEmptyConjunct()<<"\n";
}

int CollocationMonkey::intern(SymbolString& ss)
{
	Monkey::intern(ss);
	return _base.intern(ss);
}

SymbolString& CollocationMonkey::repr(int sample)
{
	return _base.repr(sample);
}

