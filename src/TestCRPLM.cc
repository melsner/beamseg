#include "common.h"
#include "SegSampler.h"
#include "Monkey.h"
#include "State.h"

#include <boost/program_options.hpp>

int main(int argc, char* argv[])
{
	appInit(DATA_PATH);

	namespace po = boost::program_options;

	po::options_description desc("Beam sampling for segments/intended forms");
	desc.add_options()
		("help", "show options")
		("input-file", po::value<string>(),
		 "file to read input from")
		("init", po::value<string>(),
		 "initial breakpoints (true,random,none,underlying file)")
		("iters", po::value<int>(), "number of iterations")
		("grams", po::value<int>(), "ngram order")
		("alpha", po::value< std::vector<double> >(), "CRP alphas")
		("discount", po::value< std::vector<double> >(), "CRP discount")
		("output", po::value<string>(), 
		 "file stem to output to (creates output.learned.underlying, output.learned.surface)")
		("channel", po::value<string>(),
		 "channel type: noiseless, xsub")
		("brent-reader",
		 "reader for Brent format (one char per phone, space-delimited")
		;

	po::positional_options_description p;
	p.add("input-file", -1);

	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv).
			  options(desc).positional(p).run(), vm);
	po::notify(vm);

	if(vm.count("help"))
	{
		cout<<desc<<"\n";
		return 1;
	}

	int grams = 2;

	if(vm.count("grams"))
	{
		grams = vm["grams"].as<int>();
		assert(grams > 0);
		cerr<<"Selected "<<grams<<"-grams\n";
	}

	bool normalReader = true;
	double breakProb = .2;
	doubles crpAlpha(grams, 0);
	crpAlpha[0] = 3000;
	crpAlpha[1] = 300;
	doubles crpDiscount(grams, 0);
	string output = "output";
	string baseType = "monkey";
	string channelType = "noiseless";
	bool cheat = false;
	bool randomBreaks = true;
	string initUnderlying = "";
	int iters = 1000;

	if(vm.count("brent-reader"))
	{
		normalReader = false;
	}

	string input;
	if(!vm.count("input-file"))
	{
		cerr<<"Assuming input is "<<output<<".surface\n";
		input = output + ".surface";
	}
	else
	{
		input = vm["input-file"].as<string>();
	}

	if(vm.count("iters"))
	{
		iters = vm["iters"].as<int>();
	}

	if(vm.count("alpha"))
	{
		crpAlpha = vm["alpha"].as< std::vector<double> >();
	}

	for(int ii = 0; ii < crpAlpha.size(); ++ii)
	{
		cerr<<"Alpha: "<<ii<<" = "<<crpAlpha[ii]<<"\n";
	}		

	if(vm.count("discount"))
	{
		crpDiscount = vm["discount"].as< std::vector<double> >();
	}

	for(int ii = 0; ii < crpDiscount.size(); ++ii)
	{
		cerr<<"Discount: "<<ii<<" = "<<crpDiscount[ii]<<"\n";
	}

	if(vm.count("output"))
	{
		output = vm["output"].as<string>();
	}

	if(vm.count("channel"))
	{
		channelType = vm["channel"].as<string>();
	}

	if(vm.count("init"))
	{
		string init = vm["init"].as<string>();
		cerr<<"Initialization: "<<init<<"\n";
		if(init == "true")
		{
			cheat = true;
		}
		else if(init == "random")
		{
			randomBreaks = true;
		}
		else if(init == "none")
		{
			randomBreaks = false;
		}
		else
		{
			randomBreaks = false;
			initUnderlying = init;
			input = initUnderlying + ".surface";
			cerr<<"Using "<<input<<" as input file.\n";

			initUnderlying = initUnderlying + ".underlying";
			cerr<<"Using "<<initUnderlying<<" as initialization.\n";

			cheat = true;
		}
	}

	cerr<<"Break prob: "<<breakProb<<"\n";

	cerr<<"Base distribution: "<<baseType<<"\n";

	StrVocab alpha;

	Monkey* simian = NULL;
	if(baseType == "monkey")
	{
		simian = new Monkey(breakProb, false, alpha);
	}
	else
	{
		cerr<<"Unknown base distribution "<<baseType<<"\n";
		return 1;
	}

	cerr<<"Channel type: "<<channelType<<"\n";

	Channel* channel = NULL;
	if(channelType == "noiseless")
	{
		channel = new Channel();
	}
	else if(channelType == "xsub")
	{
		channel = new XChannel(.1, alpha.get("x", true));
	}
	else
	{
		cerr<<"Unknown channel type "<<channelType<<"\n";
		return 1;
	}

	string empty("");
	SymbolString emptySym(empty, alpha);
	int start = simian->intern(emptySym);
	assert(start == 0);

	CRPLM model(grams, crpAlpha, crpDiscount, simian, channel);
	SegSampler sampler(model);

	ifstream ff(input.c_str());
	if(!ff.is_open())
	{
		cerr<<"Can't open input file '"<<input<<"'\n";
		return 1;
	}

	while(ff)
	{
		string line;
		getline(ff, line);
		if(line.empty())
		{
			break;
		}

		std::istringstream split(line);

		SymbolString si(alpha);
		ints trueSplits;
		int curr = 0;

		while(split)
		{
			SymbolString* word;
			if(normalReader)
			{
				word = new SymbolString(split, alpha);
			}
			else
			{
				string key;
				split>>key;
				if(key == "||")
				{
					cerr<<"*WARNING* Is your reader setting wrong?\n";
					return 1;
				}
				word = new SymbolString(key, alpha);
			}

			if(word->empty())
			{
				break;
			}

			curr += word->size();
			trueSplits.push_back(curr);
			si.append(*word);
			delete word;
		}

		cerr<<"Read "<<si<<"\n";
		sampler.add(si);

		trueSplits.pop_back(); //last 'split' point is end of last word

		//when active, initializes at true segmentations
		if(cheat)
		{
			foreach(ints, splitPt, trueSplits)
			{
				sampler.split(sampler.numSequences() - 1, *splitPt);
			}
		}
		else if(randomBreaks)
		{
			int seqSize = 
				sampler.sequence(sampler.numSequences() - 1).nSyms();
			for(int sym = 1; sym < seqSize - 1; ++sym)
			{
				if(gsl_rng_uniform(RNG) < breakProb)
				{
					sampler.split(sampler.numSequences() - 1, sym);
				}
			}
		}
	}

	int nSeqs = sampler.numSequences();
	for(int seq = 0; seq < nSeqs; ++seq)
	{
		sampler.sequence(seq).primeStates();
	}

	foreach(strIntMap, ch, alpha._vocab)
	{
		if(ch->first.size() > 3)
		{
			cerr<<"Very long symbol detected: "<<ch->first<<"\n";
			cerr<<"*WARNING* Did you get your reader setting wrong?\n";
			return 1;
		}
	}

	cerr<<"Alphabet size: "<<alpha.size()<<"\n";
	cerr<<"Base distribution:\n"<<*simian<<"\n";

	// double origTemp = .1;
	double origTemp = 1;
	double invTemp = origTemp;

	for(int ii = 0; ii < iters; ++ii)
	{
		if(invTemp < 1)
		{
			invTemp += (1 - origTemp) / iters;
		}

		cerr<<"... Iter "<<ii<<", inverse temp "<<invTemp<<
			" ll "<<model.ll()<<"\n";
		sampler.summarize(cerr);

		for(int seq = 0; seq < nSeqs; ++seq)
		{
			Analysis& ana = sampler.sequence(seq);
			{
				ints oldSeq;
				Segment* curr = ana.segAt(0);
				while(!curr->isStartOrEnd())
				{
					oldSeq.push_back(curr->strSym);
					curr = ana.nextSegment(*curr);
				}

				model.removeSeq(oldSeq, invTemp, false);
				model.updateSeq(oldSeq, invTemp, false);
				// int word1 = oldSeq[0];
				// int word2 = 0;
				// if(oldSeq.size() > 1)
				// {
				// 	word2 = oldSeq[1];
				// }
				// intLst ctxt;
				// ctxt.push_back(word1);
				// CRP* proc = model.getProcess(ctxt);

				// // cerr<<"\n";
				// // cerr<<"proc : "<<proc<<" root "<<model._root<<"\n";
				// proc->remove(word2);
				// proc->update(word2);
				// // cerr<<"\n";
			}

		}

	}
}
