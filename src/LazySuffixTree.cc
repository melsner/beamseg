#include "LazySuffixTree.h"
#include "common.h"

LazySuffixTree::LazySuffixTree(int nchars, int dp, SymVocab* lex):
	_successors(nchars, NULL),
	_bestString(0),
	_final(ProbCorr(0, 0)),
	_depth(dp),
	_wipeCache(true),
	_forwarded(false),
	_vocab(lex)
{
}

LazySuffixTree::LazySuffixTree(int nchars, int dp, bool wipe, SymVocab* lex):
	_successors(nchars, NULL),
	_bestString(0),
	_final(ProbCorr(0, 0)),
	_depth(dp),
	_wipeCache(wipe),
	_forwarded(false),
	_vocab(lex)
{
}

LazySuffixTree::~LazySuffixTree()
{
//	foreach(intLazySuffixMap, nxt, _successors)
	foreach(LazySuffixTrees, nxt, _successors)
	{
		//delete nxt->second;
		delete *nxt;
	}
}

void LazySuffixTree::deleteChild(int ch)
{
	delete _successors[ch];
//	_successors.erase(ch);
	_successors[ch] = NULL;
}

//intLazySuffixMap& LazySuffixTree::successors()
LazySuffixTrees& LazySuffixTree::successors()
{
	if(!_forwarded)
	{
		_forwarded = true;
		foreach(intPayloads, item, _toInsert)
		{
			SymbolString& str = _vocab->inv((*item).first);
			insert1((*item).first, str, _depth, (*item).second);
		}
		if(_wipeCache)
		{
			_toInsert.clear();
		}
	}

	return _successors;
}

intPayloads& LazySuffixTree::cache()
{
	return _toInsert;
}

void LazySuffixTree::insert(int sym, Prob pr, Prob corr)
{
	SymbolString& str = _vocab->inv(sym);
	insert1(sym, str, 0, ProbCorr(pr, corr));
}

void LazySuffixTree::insert(SymbolString& str, Prob pr, Prob corr)
{
	int sym = _vocab->get(str, true);
	insert1(sym, str, 0, ProbCorr(pr, corr));
}

void LazySuffixTree::insert1(int sym, SymbolString& str, int pos, Payload pl)
{
	Prob pr = pl.first;
	assert(pr > 0);
	if(pr > _bestString)
	{
		_bestString = pr;
	}

	if(pos == str.size())
	{
		_final = pl;
		return;
	}

	int chr = str[pos];
//	if(!inMap(_successors, chr))
	if(_successors[chr] == NULL)
	{
		_successors[chr] = new LazySuffixTree(_successors.size(),
											  _depth + 1, _wipeCache,
											  _vocab);
	}
	_successors[chr]->insert2(sym, pl);
}

void LazySuffixTree::insert2(int sym, Payload pr)
{
//	if(_successors.empty())
	if(_bestString == 0)
	{
		_toInsert.push_back(intPayload(sym, pr));
	}
}

LazySuffixTree* LazySuffixTree::successor(int chr)
{
//	return ifExists(successors(), chr, (LazySuffixTree*)NULL);
	return _successors[chr];
}

Prob LazySuffixTree::bestString()
{
	successors(); //trigger expansion
	return _bestString;
}

Prob LazySuffixTree::final()
{
	successors(); //trigger expansion
	return _final.first;
}

Prob LazySuffixTree::finalCorr()
{
	successors(); //trigger expansion
	return _final.second;
}

void LazySuffixTree::print(ostream& os, StrVocab& alphabet)
{
	print1(os, 0, alphabet);
}

void LazySuffixTree::print1(ostream& os, int depth, StrVocab& alphabet)
{
	if(_final.first > 0)
	{
		os<<"*"<<_final.first<<"* ";
	}

    //for full expansion, use successors()
//	foreach(intLazySuffixMap, succ, _successors)
	for(int ii = 0; ii < _successors.size(); ++ii)
	{
		if(_successors[ii] == NULL)
		{
			continue;
		}

		os<<"("<<alphabet.inv(ii)<<" ";
		_successors[ii]->print1(os, 1 + depth, alphabet);
		os<<")";
	}
}
