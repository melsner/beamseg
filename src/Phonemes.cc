#include "Phonemes.h"

string OFFGLIDES[OFFGLIDE_W + 1] = {"-", "y", "w"};
string RHOTICS[R_RHOTIC + 1] = {"-", "rhotic"};
string NASALS[N_NASAL + 1] = {"-", "nasal"};
string PLACES[P_GLOTTAL + 1] = {"labial/front", "alveolar/mid", 
								"palatal/back", "velar", "glottal"};
string VOICES[V_VOICELESS + 1] = {"voiced", "voiceless"};
string MANNERS[M_VOWEL + 1] = {"stop", "fricative", "vowel"};
string HEIGHTS[H_HIGH + 1] = {"low", "mid-low", "mid", "midhigh", "high"};

void Phoneme::print(ostream& os)
{
	os<<"/"<<name<<"/: ";
	os<<valToString(F_VOICE)<<" "<<
		valToString(F_PLACE)<<" "<<
		valToString(F_NASAL)<<" "<<
		valToString(F_RHOTIC)<<" "<<
		valToString(F_MANNER);
}

string Phoneme::valToString(int feat)
{
	assert(feat >= 0 && feat < F_LAST);
	int val = (*this)[feat];

	switch(feat)
	{
		case F_MANNER:
			return MANNERS[val];
		case F_VOICE:
			return VOICES[val];
		case F_PLACE:
			return PLACES[val];
		case F_NASAL:
			return NASALS[val];
		case F_RHOTIC:
			return RHOTICS[val];
		default:
			assert(0);
	}
}


Phonemes::Phonemes()
{
}

Phoneme& Phonemes::get(string& targ)
{
	if(_contents.find(targ) == _contents.end())
	{
		cerr<<"Can't find "<<targ<<"\n";
	}
	assert(_contents.find(targ) != _contents.end());
	return _contents[targ];
}

Phoneme& Phonemes::get(const char* st)
{
	string stS(st);
	return get(stS);
}

double Phonemes::fSim(Phoneme& one, Phoneme& two)
{
	if((one[F_MANNER] != two[F_MANNER] && (one[F_MANNER] == M_VOWEL ||
										   two[F_MANNER] == M_VOWEL))
	   || one[F_VOICE] != two[F_VOICE]
	   || abs(one[F_PLACE] - two[F_PLACE]) > 1)
	{
		return 2;
	}
	return 0;
}

double Phonemes::sim(Phoneme& one, Phoneme& two)
{
	double diffs = 0;
	for(int ctr = 0; ctr < F_LAST; ++ctr)
	{
		if(one[ctr] != two[ctr])
		{
			diffs += abs(one[ctr] - two[ctr]);
		}
	}
	return diffs;
}

void Phonemes::addVowel(string name, int ht, int place)
{
	Phoneme pi(name);
	pi[F_VOICE] = V_VOICED;
	pi[F_MANNER] = M_VOWEL;
	pi[F_HEIGHT] = ht;
	pi[F_PLACE] = place;
	_contents[name] = pi;
}

void Phonemes::addNasal(string name, int place)
{
	Phoneme pi(name);
	pi[F_VOICE] = V_VOICED;
	pi[F_MANNER] = M_STOP;
	pi[F_PLACE] = place;
	pi[F_NASAL] = N_NASAL;
	_contents[name] = pi;
}

void Phonemes::addCons(string name, int vc, int place, int mann)
{
	Phoneme pi(name);
	pi[F_PLACE] = place;
	pi[F_MANNER] = mann;
	pi[F_VOICE] = vc;
	_contents[name] = pi;
}

strPhonemeMap& Phonemes::contents()
{
	return _contents;
}

Phonemes Phonemes::brent()
{
	Phonemes brent;

	brent.addVowel("a", H_MIDLOW, P_PALATAL);
	brent.addVowel("&", H_MIDLOW, P_LABIAL);
	brent.addVowel("9", H_LOW, P_LABIAL);
	brent.get("9")[F_OFFGLIDE] = OFFGLIDE_Y;
	brent.addVowel("Q", H_LOW, P_LABIAL);
	brent.get("Q")[F_OFFGLIDE] = OFFGLIDE_W;
	//'thought' vowel merged with the 'lot' vowel in most AmE dialects
	brent.addVowel("O", H_MIDLOW, P_PALATAL);
	brent.addVowel("7", H_MID, P_PALATAL);
	brent.get("7")[F_OFFGLIDE] = OFFGLIDE_Y;
	brent.addVowel("o", H_LOW, P_PALATAL);
	brent.addVowel("E", H_MID, P_LABIAL);
	brent.addVowel("e", H_MIDHIGH, P_LABIAL);
	brent.addVowel("(", H_HIGH, P_LABIAL);
	brent.get("(")[F_RHOTIC] = R_RHOTIC;
	brent.addVowel(")", H_MID, P_PALATAL);
	brent.get(")")[F_RHOTIC] = R_RHOTIC;
	brent.addVowel("*", H_MIDHIGH, P_LABIAL);
	brent.get("*")[F_RHOTIC] = R_RHOTIC;
	brent.addVowel("3", H_HIGH, P_LABIAL);
	brent.get("3")[F_RHOTIC] = R_RHOTIC;
	brent.addVowel("#", H_MIDLOW, P_LABIAL);
	brent.get("#")[F_RHOTIC] = R_RHOTIC;
	brent.addVowel("%", H_MIDLOW, P_PALATAL);
	brent.get("%")[F_RHOTIC] = R_RHOTIC;
	brent.addVowel("y", H_HIGH, P_PALATAL);
	brent.get("y")[F_OFFGLIDE] = OFFGLIDE_Y;
	brent.addVowel("w", H_HIGH, P_LABIAL);
	brent.get("w")[F_OFFGLIDE] = OFFGLIDE_W;
	brent.addVowel("W", H_HIGH, P_LABIAL);
	brent.get("W")[F_OFFGLIDE] = OFFGLIDE_W;
	//add a feature to distinguish @ from ^?
	brent.addVowel("A", H_MID, P_ALVEOLAR);
	brent.addVowel("6", H_MID, P_ALVEOLAR);
	brent.addVowel("u", H_HIGH, P_PALATAL);
	brent.addVowel("U", H_MIDHIGH, P_PALATAL);
	brent.addVowel("I", H_MIDHIGH, P_ALVEOLAR);
	brent.addVowel("i", H_HIGH, P_LABIAL);
	brent.addVowel("~", H_MID, P_ALVEOLAR);
	brent.get("~")[F_NASAL] = N_NASAL;
	brent.addNasal("N", P_VELAR);
	brent.addNasal("n", P_ALVEOLAR);
	brent.addNasal("m", P_LABIAL);
	brent.addNasal("M", P_LABIAL);
	brent.get("M")[F_MANNER] = M_VOWEL;
	brent.addCons("p", V_VOICELESS, P_LABIAL, M_STOP);
	brent.addCons("b", V_VOICED, P_LABIAL, M_STOP);
	brent.addCons("f", V_VOICELESS, P_LABIAL, M_FRICATIVE);
	brent.addCons("v", V_VOICED, P_LABIAL, M_FRICATIVE);
	brent.addCons("d", V_VOICED, P_ALVEOLAR, M_STOP);
	brent.addCons("D", V_VOICED, P_ALVEOLAR, M_FRICATIVE);
	brent.addCons("z", V_VOICED, P_ALVEOLAR, M_FRICATIVE);
	brent.addCons("Z", V_VOICED, P_ALVEOLAR, M_FRICATIVE);
	brent.addCons("G", V_VOICED, P_ALVEOLAR, M_FRICATIVE);
	brent.addCons("t", V_VOICELESS, P_ALVEOLAR, M_STOP);
	brent.addCons("s", V_VOICELESS, P_ALVEOLAR, M_FRICATIVE);
	brent.addCons("T", V_VOICELESS, P_ALVEOLAR, M_FRICATIVE);
	brent.addCons("S", V_VOICELESS, P_ALVEOLAR, M_FRICATIVE);
	brent.addCons("c", V_VOICELESS, P_ALVEOLAR, M_FRICATIVE);
	brent.addCons("l", V_VOICED, P_PALATAL, M_FRICATIVE);
	brent.addCons("r", V_VOICED, P_PALATAL, M_FRICATIVE);
	brent.addCons("R", V_VOICED, P_PALATAL, M_FRICATIVE);
	brent.get("R")[F_MANNER] = M_VOWEL;
	brent.addCons("L", V_VOICED, P_PALATAL, M_FRICATIVE);
	brent.get("L")[F_MANNER] = M_VOWEL;
	brent.addCons("k", V_VOICELESS, P_VELAR, M_STOP);
	brent.addCons("g", V_VOICED, P_VELAR, M_STOP);
	brent.addCons("h", V_VOICELESS, P_VELAR, M_FRICATIVE);

	return brent;
}

Phonemes Phonemes::buckeye()
{
	Phonemes buckeye;

	buckeye.addVowel("aa", H_MIDLOW, P_PALATAL);
	buckeye.addVowel("ae", H_MIDLOW, P_LABIAL);
	buckeye.addVowel("ay", H_LOW, P_LABIAL);
	buckeye.get("ay")[F_OFFGLIDE] = OFFGLIDE_Y;
	buckeye.addVowel("aw", H_LOW, P_LABIAL);
	buckeye.get("aw")[F_OFFGLIDE] = OFFGLIDE_W;
	//'thought' vowel merged with the 'lot' vowel in most AmE dialects
	buckeye.addVowel("ao", H_MIDLOW, P_PALATAL);
	buckeye.addVowel("oy", H_MID, P_PALATAL);
	buckeye.get("oy")[F_OFFGLIDE] = OFFGLIDE_Y;
	buckeye.addVowel("ow", H_LOW, P_PALATAL);
	buckeye.addVowel("eh", H_MID, P_LABIAL);
	buckeye.addVowel("ey", H_MIDHIGH, P_LABIAL);
	buckeye.addVowel("er", H_MIDHIGH, P_LABIAL);
	buckeye.get("er")[F_RHOTIC] = R_RHOTIC;
	buckeye.addVowel("y", H_HIGH, P_PALATAL);
	buckeye.get("y")[F_OFFGLIDE] = OFFGLIDE_Y;
	buckeye.addVowel("w", H_HIGH, P_LABIAL);
	buckeye.get("w")[F_OFFGLIDE] = OFFGLIDE_W;
	buckeye.addVowel("ah", H_MID, P_ALVEOLAR);
	buckeye.addVowel("uw", H_HIGH, P_PALATAL);
	buckeye.addVowel("uh", H_MIDHIGH, P_PALATAL);
	buckeye.addVowel("ih", H_MIDHIGH, P_ALVEOLAR);
	buckeye.addVowel("iy", H_HIGH, P_LABIAL);
	buckeye.addVowel("en", H_MID, P_ALVEOLAR);
	buckeye.get("en")[F_NASAL] = N_NASAL;
	buckeye.addVowel("eng", H_MID, P_VELAR);
	buckeye.get("eng")[F_NASAL] = N_NASAL;
	buckeye.addNasal("ng", P_VELAR);
	buckeye.addNasal("n", P_ALVEOLAR);
	buckeye.addNasal("m", P_LABIAL);
	buckeye.addNasal("em", P_LABIAL);
	buckeye.get("em")[F_MANNER] = M_VOWEL;
	buckeye.addCons("p", V_VOICELESS, P_LABIAL, M_STOP);
	buckeye.addCons("b", V_VOICED, P_LABIAL, M_STOP);
	buckeye.addCons("f", V_VOICELESS, P_LABIAL, M_FRICATIVE);
	buckeye.addCons("v", V_VOICED, P_LABIAL, M_FRICATIVE);
	buckeye.addCons("d", V_VOICED, P_ALVEOLAR, M_STOP);
	buckeye.addCons("dh", V_VOICED, P_ALVEOLAR, M_FRICATIVE);
	buckeye.addCons("z", V_VOICED, P_ALVEOLAR, M_FRICATIVE);
	buckeye.addCons("zh", V_VOICED, P_ALVEOLAR, M_FRICATIVE);
	buckeye.addCons("jh", V_VOICED, P_ALVEOLAR, M_FRICATIVE);
	buckeye.addCons("t", V_VOICELESS, P_ALVEOLAR, M_STOP);
	buckeye.addCons("s", V_VOICELESS, P_ALVEOLAR, M_FRICATIVE);
	buckeye.addCons("th", V_VOICELESS, P_ALVEOLAR, M_FRICATIVE);
	buckeye.addCons("sh", V_VOICELESS, P_ALVEOLAR, M_FRICATIVE);
	buckeye.addCons("ch", V_VOICELESS, P_ALVEOLAR, M_FRICATIVE);
	buckeye.addCons("l", V_VOICED, P_PALATAL, M_FRICATIVE);
	buckeye.addCons("r", V_VOICED, P_PALATAL, M_FRICATIVE);
	buckeye.addCons("el", V_VOICED, P_PALATAL, M_FRICATIVE);
	buckeye.get("el")[F_MANNER] = M_VOWEL;
	buckeye.addCons("k", V_VOICELESS, P_VELAR, M_STOP);
	buckeye.addCons("g", V_VOICED, P_VELAR, M_STOP);
	buckeye.addCons("hh", V_VOICELESS, P_VELAR, M_FRICATIVE);

	return buckeye;
}

Phonemes Phonemes::buckeyePure()
{
	Phonemes buckeye;

	buckeye.addVowel("aa", H_MIDLOW, P_PALATAL);
	buckeye.addVowel("aan", H_MIDLOW, P_PALATAL);
	buckeye.get("aan")[F_NASAL] = N_NASAL;
	buckeye.addVowel("ae", H_MIDLOW, P_LABIAL);
	buckeye.addVowel("aen", H_MIDLOW, P_LABIAL);
	buckeye.get("aen")[F_NASAL] = N_NASAL;
	buckeye.addVowel("ay", H_LOW, P_LABIAL);
	buckeye.get("ay")[F_OFFGLIDE] = OFFGLIDE_Y;
	buckeye.addVowel("ayn", H_LOW, P_LABIAL);
	buckeye.get("ayn")[F_OFFGLIDE] = OFFGLIDE_Y;
	buckeye.get("ayn")[F_NASAL] = N_NASAL;
	buckeye.addVowel("aw", H_LOW, P_LABIAL);
	buckeye.get("aw")[F_OFFGLIDE] = OFFGLIDE_W;
	buckeye.addVowel("awn", H_LOW, P_LABIAL);
	buckeye.get("awn")[F_OFFGLIDE] = OFFGLIDE_W;
	buckeye.get("awn")[F_NASAL] = N_NASAL;
	//'thought' vowel merged with the 'lot' vowel in most AmE dialects
	buckeye.addVowel("ao", H_MIDLOW, P_PALATAL);
	buckeye.addVowel("aon", H_MIDLOW, P_PALATAL);
	buckeye.get("aon")[F_NASAL] = N_NASAL;
	buckeye.addVowel("oy", H_MID, P_PALATAL);
	buckeye.get("oy")[F_OFFGLIDE] = OFFGLIDE_Y;
	buckeye.addVowel("oyn", H_MID, P_PALATAL);
	buckeye.get("oyn")[F_OFFGLIDE] = OFFGLIDE_Y;
	buckeye.get("oyn")[F_NASAL] = N_NASAL;
	buckeye.addVowel("ow", H_LOW, P_PALATAL);
	buckeye.addVowel("own", H_LOW, P_PALATAL);
	buckeye.get("own")[F_NASAL] = N_NASAL;
	buckeye.addVowel("eh", H_MID, P_LABIAL);
	buckeye.addVowel("ehn", H_MID, P_LABIAL);
	buckeye.get("ehn")[F_NASAL] = N_NASAL;
	buckeye.addVowel("ey", H_MIDHIGH, P_LABIAL);
	buckeye.addVowel("eyn", H_MIDHIGH, P_LABIAL);
	buckeye.get("eyn")[F_NASAL] = N_NASAL;
	buckeye.addVowel("er", H_MIDHIGH, P_LABIAL);
	buckeye.get("er")[F_RHOTIC] = R_RHOTIC;
	buckeye.addVowel("ern", H_MIDHIGH, P_LABIAL);
	buckeye.get("ern")[F_RHOTIC] = R_RHOTIC;
	buckeye.get("ern")[F_NASAL] = N_NASAL;
	buckeye.addVowel("y", H_HIGH, P_PALATAL);
	buckeye.get("y")[F_OFFGLIDE] = OFFGLIDE_Y;
	buckeye.addVowel("w", H_HIGH, P_LABIAL);
	buckeye.get("w")[F_OFFGLIDE] = OFFGLIDE_W;
	buckeye.addVowel("ah", H_MID, P_ALVEOLAR);
	buckeye.addVowel("ahr", H_MID, P_ALVEOLAR);
	buckeye.get("ahr")[F_RHOTIC] = R_RHOTIC;
	buckeye.addVowel("ahn", H_MID, P_ALVEOLAR);
	buckeye.get("ahn")[F_NASAL] = N_NASAL;
	buckeye.addVowel("uw", H_HIGH, P_PALATAL);
	buckeye.addVowel("uwn", H_HIGH, P_PALATAL);
	buckeye.get("uwn")[F_NASAL] = N_NASAL;
	buckeye.addVowel("uh", H_MIDHIGH, P_PALATAL);
	buckeye.addVowel("uhn", H_MIDHIGH, P_PALATAL);
	buckeye.get("uhn")[F_NASAL] = N_NASAL;
	buckeye.addVowel("ih", H_MIDHIGH, P_ALVEOLAR);
	buckeye.addVowel("ihn", H_MIDHIGH, P_ALVEOLAR);
	buckeye.get("ihn")[F_NASAL] = N_NASAL;
	buckeye.addVowel("iy", H_HIGH, P_LABIAL);
	buckeye.addVowel("iyn", H_HIGH, P_LABIAL);
	buckeye.get("iyn")[F_NASAL] = N_NASAL;
	buckeye.addVowel("en", H_MID, P_ALVEOLAR);
	buckeye.get("en")[F_NASAL] = N_NASAL;
	buckeye.addVowel("eng", H_MID, P_VELAR);
	buckeye.get("eng")[F_NASAL] = N_NASAL;
	buckeye.addNasal("ng", P_VELAR);
	buckeye.addNasal("n", P_ALVEOLAR);
	buckeye.addNasal("nx", P_PALATAL);
	buckeye.addNasal("m", P_LABIAL);
	buckeye.addNasal("em", P_LABIAL);
	buckeye.get("em")[F_MANNER] = M_VOWEL;
	buckeye.addCons("p", V_VOICELESS, P_LABIAL, M_STOP);
	buckeye.addCons("b", V_VOICED, P_LABIAL, M_STOP);
	buckeye.addCons("f", V_VOICELESS, P_LABIAL, M_FRICATIVE);
	buckeye.addCons("v", V_VOICED, P_LABIAL, M_FRICATIVE);
	buckeye.addCons("d", V_VOICED, P_ALVEOLAR, M_STOP);
	buckeye.addCons("dx", V_VOICED, P_PALATAL, M_STOP);
	buckeye.addCons("dh", V_VOICED, P_ALVEOLAR, M_FRICATIVE);
	buckeye.addCons("z", V_VOICED, P_ALVEOLAR, M_FRICATIVE);
	buckeye.addCons("zh", V_VOICED, P_ALVEOLAR, M_FRICATIVE);
	buckeye.addCons("jh", V_VOICED, P_ALVEOLAR, M_FRICATIVE);
	buckeye.addCons("t", V_VOICELESS, P_ALVEOLAR, M_STOP);
	buckeye.addCons("tq", V_VOICELESS, P_PALATAL, M_STOP);
	buckeye.addCons("s", V_VOICELESS, P_ALVEOLAR, M_FRICATIVE);
	buckeye.addCons("th", V_VOICELESS, P_ALVEOLAR, M_FRICATIVE);
	buckeye.addCons("sh", V_VOICELESS, P_ALVEOLAR, M_FRICATIVE);
	buckeye.addCons("ch", V_VOICELESS, P_ALVEOLAR, M_FRICATIVE);
	buckeye.addCons("l", V_VOICED, P_PALATAL, M_FRICATIVE);
	buckeye.addCons("r", V_VOICED, P_PALATAL, M_FRICATIVE);
	buckeye.addCons("el", V_VOICED, P_PALATAL, M_FRICATIVE);
	buckeye.get("el")[F_MANNER] = M_VOWEL;
	buckeye.addCons("k", V_VOICELESS, P_VELAR, M_STOP);
	buckeye.addCons("g", V_VOICED, P_VELAR, M_STOP);
	buckeye.addCons("hh", V_VOICELESS, P_VELAR, M_FRICATIVE);

	return buckeye;
}
