#include "FiniteDist.h"

FiniteDist::FiniteDist(double& dummy, Prob pEnd, StrVocab& alphabet):
	CRP(dummy, NULL, dummy),
	Monkey(.5, false, pEnd, alphabet)
{
	_alpha = 0;
	_discount = 0;
}

FiniteDist::~FiniteDist()
{
}

Prob FiniteDist::update(int sample, double temp)
{
	return prob(sample);
}

Prob FiniteDist::remove(int sample, double temp)
{
	return prob(sample);
}

Prob FiniteDist::prob(int sample)
{
	int count = ifExists(_counts, sample, 0);
	Prob res = count / (Prob)_total;
	assert(_total > 0);
	assertProb(res);

	const Prob EPS = 1e-6;
	res *= 1 - EPS;
	res += EPS * Monkey::prob(sample);

	return res;
}

Prob FiniteDist::probNoG0(int sample)
{
	// cerr<<"check prob of "<<sample<<" "<<prob(sample)<<" "<<
	// 	repr(sample)<<"\n";
	int count = ifExists(_counts, sample, 0);
	Prob res = count / (Prob)_total;
	assert(_total > 0);
	assertProb(res);
	return res;
}

Prob FiniteDist::newTableProb()
{
	return 1e-6;
}
