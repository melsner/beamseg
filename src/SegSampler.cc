#include "SegSampler.h"
#include <math.h>
#include "gsl_cdf.h"
#include "MonkeyState.h"
#include "BigramMonkey.h"
#include "NGramMonkey.h"
#include "CollocationMonkey.h"

SegSampler::SegSampler(CRPLM& model):
	_model(&model)
{
}

SegSampler::~SegSampler()
{
	foreach(Analyses, ana, _seqs)
	{
		Analysis* anaPtr = *ana;
		delete anaPtr;
	}
}

void SegSampler::add(SymbolString& seq)
{
	Analysis* ana = new Analysis(numSequences(), seq, _model);
	add(ana);
}

void SegSampler::add(Analysis* ana)
{
	_seqs.push_back(ana);

	Segment* curr = ana->segAt(0);
	ints anaLex;
	SymbolStrings surf;
	while(!curr->isStartOrEnd())
	{
		anaLex.push_back(curr->strSym);
		_model->_rawCounts[curr->strSym] += 1;

		surf.push_back(&curr->targetSubstring);

		curr = ana->nextSegment(*curr);
	}

	TableLst tabs;
	_model->updateSeq(anaLex, tabs, 1.0, false);
	curr = ana->segAt(0);
	TableLst::iterator ctr = tabs.begin();
	while(!curr->isStartOrEnd())
	{
		curr->tab = *ctr;
		++ctr;
		curr = ana->nextSegment(*curr);
	}
	curr->tab = *ctr;
}

Analysis& SegSampler::sequence(int seq)
{
	return *_seqs[seq];
}

int SegSampler::numSequences()
{
	return _seqs.size();
}

void SegSampler::merge(Analysis* seq, int pos)
{
	// cerr<<"targ1 source size "<<targ1->alignment._srcSize<<" targ 2 "<<
	// 	targ2->alignment._srcSize<<"\n";
	// cerr<<"targ1 str "<<targ1->str<<" targ2 str "<<targ2->str<<"\n";

	seq->merge(pos);
}

bool SegSampler::split(Analysis* seq, int pos)
{
	// cerr<<"going to split "<<targ->str<<" and "<<targ->targetSubstring<<"\n";
	// foreach(ints, ii, targ->alignment)
	// {
	// 	cerr<<*ii<<" ";
	// }
	// cerr<<"\n";

	bool res = seq->split(pos);
	// cerr<<"got "<<produced1->str<<" and "<<produced1->targetSubstring<<"\n";
	// foreach(ints, ii, produced1->alignment)
	// {
	// 	cerr<<*ii<<" ";
	// }
	// cerr<<"\n";

	// cerr<<"and "<<produced2->str<<" and "<<produced2->targetSubstring<<"\n";
	// foreach(ints, ii, produced2->alignment)
	// {
	// 	cerr<<*ii<<" ";
	// }
	// cerr<<"\n";

	return res;
}

void SegSampler::split(int seqNum, int pos)
{
//	cerr<<"Breaking at "<<pos<<"\n";
	Analysis* seq = _seqs[seqNum];
	Segment* targ = seq->segAt(pos);
	_model->removeSeg(seq, targ, false, 1.0);

	_model->_rawCounts[targ->strSym] -= 1;
	if(_model->_rawCounts[targ->strSym] == 0)
	{
		_model->_rawCounts.erase(targ->strSym);
	}

	seq->split(pos);
	Segment* produced1 = seq->segAt(pos);
	Segment* produced2 = seq->segAt(pos - 1);

	_model->updateSeg(seq, produced1, false, 1.0);
	intLst dummy;
	_model->updateSegProduction(seq, produced2, dummy, false, 1.0);

	_model->_rawCounts[produced1->strSym] += 1;
	_model->_rawCounts[produced2->strSym] += 1;
}

void SegSampler::print(ostream& os)
{
	foreach(Analyses, ana, _seqs)
	{
		(*ana)->print(os);
	}

	os<<"--- generated lexicon --- \n";
	foreach(intIntMap, word, _model->lexicon())
	{
		os<<_model->repr(word->first)<<
			"\t ("<<_model->_root->_counts[word->first]<<")\n";
	}

	// os<<"--- channel substitutions --- \n";
	// _model->_channel->print(os);

	summarize(os);
}

void SegSampler::summarize(ostream& os)
{
	int underlyingChars = 0;
	int surfChars = 0;
	foreach(Analyses, ana, _seqs)
	{
		surfChars += (*ana)->surface.size();
		Segment* curr = (*ana)->segAt(0);
		while(!curr->isStartOrEnd())
		{
			underlyingChars += curr->str.size();
			curr = (*ana)->nextSegment(*curr);
		}
	}

	os<<"--- ll --- \n";
	Prob ll = _model->ll(true);
	Prob chanT = 0;
	foreach(Analyses, ana, _seqs)
	{
		chanT += (*ana)->root->channelCost();
	}
	os<<"Channel "<<chanT<<"\n";
	os<<"Total "<<(ll + chanT)<<"\n";
	os<<"Surface symbols: "<<surfChars<<" underlying symbols: "<<
		underlyingChars<<"\n";
	double avgWord = ((double)underlyingChars / _model->_nWords);
	os<<"Utterances: "<<_model->_nUtts<<" words: "<<_model->_nWords<<
		" average word length: "<<avgWord<<"\n";
	Prob posteriorPCont = (_model->_nWords - _model->_nUtts + 2.0) / 
		(_model->_nWords + 3.0);
	os<<"PCont: "<<posteriorPCont<<"\n";
	os<<"Word types: "<<_model->lexicon().size()<<
		" root tables: "<<_model->_root->_nTables<<" total tables: "<<
		_model->totalTables(_model->_root)<<"\n";

	// os<<"Root total counts "<<_model->_root->_total<<"\n";
	// int tables = 0;
	// int cts = 0;
	// foreach(intCRPMap, tab, _model->_root->_children)
	// {
	// 	cts += tab->second->_total;
	// 	tables += tab->second->_nTables;
	// }
	// os<<"Total child counts "<<cts<<" total child tables "<<tables<<"\n";
	// os<<"Total child restaurants "<<_model->_root->_children.size()<<"\n";

	for(int gram = 0; gram < _model->_ngrams; ++gram)
	{
		os<<"Alpha "<<gram<<" "<<_model->_alpha[gram]<<" "<<
			"discount "<<_model->_discount[gram]<<"\n";
	}
}

void SegSampler::printLexicon(ostream& os)
{
	// intIntMap wordCounts;

	// foreach(Analyses, ana, _seqs)
	// {
	// 	Segment* curr = (*ana)->segAt(0);
	// 	while(!curr->isStartOrEnd())
	// 	{
	// 		wordCounts[curr->strSym] += 1;
	// 		curr = (*ana)->nextSegment(*curr);
	// 	}
	// }
	intIntMap& wordCounts = _model->_rawCounts;

	IntPriVec sorter;
	foreach(intIntMap, word, wordCounts)
	{
		sorter.push_back(IntPriNode(word->second, word->first));
	}
	sort(sorter.begin(), sorter.end(), IntGreater());

	os<<"--- generated lexicon ("<<
		wordCounts.size()<<" words) --- \n";

	foreach(IntPriVec, node, sorter)
	{
		os<<_model->repr(node->data)<<"\t"<<node->pri<<"\n";
	}
}

void SegSampler::printPhoneDist(ostream& os)
{
	os<<"--- empirical phone-to-phone counts ---\n";

	StrVocab& alphabet = *_model->_prior->_alphabet;
	int nonzeros = 0;

	foreach(intIntIntMap, ph, _model->_channel->_phoneCounts)
	{
		IntPriVec sorter;
		foreach(intIntMap, prod, ph->second)
		{
			if(prod->second > 0)
			{
				if(prod->first != ph->first)
				{
					nonzeros += prod->second;
				}

				sorter.push_back(IntPriNode(prod->second, prod->first));
			}
		}
		sort(sorter.begin(), sorter.end(), IntGreater());

		if(ph->first != -1)
		{
			os<<alphabet.inv(ph->first)<<"\n";
		}
		else
		{
			os<<"-1\n";
		}

		foreach(IntPriVec, node, sorter)
		{
			if(node->data != -1)
			{
				os<<"\t"<<alphabet.inv(node->data);
			}
			else
			{
				os<<"\t-1";
			}

			os<<"\t"<<node->pri<<"\n";
		}
		os<<"\n";
	}

	os<<"Total "<<nonzeros<<" changes\n";
}

void SegSampler::writeSeqs(ostream& os)
{
	foreach(Analyses, ana, _seqs)
	{
		(*ana)->write(os);
	}
}

void SegSampler::writeUnderlying(ostream& os)
{
	foreach(Analyses, ana, _seqs)
	{
		(*ana)->writeUnderlying(os);
	}
}

void SegSampler::writeSurface(ostream& os)
{
	foreach(Analyses, ana, _seqs)
	{
		(*ana)->writeSurface(os);
	}
}

int SegSampler::randomChar(StrVocab& alphabet)
{
	Prob sm = gsl_rng_uniform(RNG);
	Prob total = alphabet.size();

	int progress = 0;
	//breaks abstraction... whatever
	foreach(strIntMap, ch, alphabet._vocab)
	{
		progress += 1;

		if(sm <= progress / total)
		{
			return ch->second;
		}
	}

	assert(0);
}

void SegSampler::reapVocab()
{
	// cerr<<_model->_root->labels().size()<<" words known\n";
	// cerr<<_model->_root->_children.size()<<" processes tracked\n";
	// cerr<<"REAPING\n";
	_model->_root->labels()[0] += 0;
	_model->_prior->_lexicon.reap(_model->_root->labels());
	if(_model->_root->labels()[0] == 0)
	{
		_model->_root->labels().erase(0);
	}
	// cerr<<_model->_root->labels().size()<<" words known\n";
	// cerr<<_model->_root->_children.size()<<" processes tracked\n";
	_model->_root->_beta.clear();
	foreach(intCRPMap, ch, _model->_root->_children)
	{
		ch->second->_beta.clear();
	}
}

void SegSampler::sampleHypers(bool verbose, double temp)
{
	for(int step = 0; step < 20; ++step)
	{
		for(int gram = 0; gram < _model->_ngrams; ++gram)
		{
			sampleHyper(_model->_alpha[gram], 0, 1e4, .1 + (gram > 1) * 10,
						temp, verbose);
		}

		for(int gram = 0; gram < _model->_ngrams; ++gram)
		{
			sampleHyper(_model->_discount[gram], 0, 1, .01,
						temp, verbose);
		}
	}

	if(verbose)
	{
		for(int gram = 0; gram < _model->_ngrams; ++gram)
		{
			cerr<<"Alpha "<<gram<<" "<<_model->_alpha[gram]<<" "<<
				"discount "<<_model->_discount[gram]<<"\n";
		}
	}
}

void SegSampler::sampleHyper(double& current, double lower, double upper,
							 double sigma, double temp, bool verbose)
{
	double proposal;

	while(true)
	{
		proposal = gsl_ran_gaussian(RNG, sigma) + current;
		if(proposal > lower && proposal < upper)
		{
			break;
		}
	}

	if(verbose)
	{
		cerr<<"Current: "<<current<<" proposing "<<proposal<<"\n";
	}

	double excludedBelow = gsl_cdf_gaussian_P(lower - current, sigma);
	double willBeExcludedBelow = gsl_cdf_gaussian_P(lower - proposal, sigma);
	double excludedAbove = gsl_cdf_gaussian_Q(upper - current, sigma);
	double willBeExcludedAbove = gsl_cdf_gaussian_Q(upper - proposal, sigma);

	//factor cancels
	//gsl_ran_gaussian_pdf(proposal - current, sigma);
	double propose = 1.0 / (1 - (excludedBelow + excludedAbove));
	double proposeBack = 1.0 / (1 - 
								(willBeExcludedBelow + willBeExcludedAbove));

	LogProb orig = _model->ll();
	double currVal = current;
	current = proposal;
	LogProb change = _model->ll();

	Prob mhr = expl( (change - orig) ) * (proposeBack / propose);
	mhr = powl(mhr, temp);

	if(verbose)
	{
		cerr<<"LL "<<orig<<" to "<<change<<" "<<(orig - change)<<
			" proposal rev "<<proposeBack<<" fwd "<<propose<<" ratio "<<
			(proposeBack / propose)<<"; mhr: "<<mhr<<"\n";
	}

	double sm = gsl_rng_uniform(RNG);
	if(sm > mhr)
	{
		current = currVal;

		if(verbose)
		{
			cerr<<"Reject\n";
		}
	}
	else if(verbose)
	{
		cerr<<"Accept\n";
	}
}

LogProb SegSampler::sgwStyleResample(double temp)
{
	// foreach(Analyses, ana, _seqs)
	// {
	// 	{
	// 		ints seq;
	// 		TableLst tabs;
	// 		{
	// 			Segment* curr = (*ana)->segAt(0);
	// 			while(!curr->isStartOrEnd())
	// 			{
	// 				tabs.push_back(curr->tab);
	// 				seq.push_back(curr->strSym);
	// 				curr = (*ana)->nextSegment(*curr);
	// 			}
	// 			tabs.push_back(curr->tab);
	// 		}
	// 		_model->removeSeq(seq, tabs, temp, false);
	// 	}
	// }

	// // cerr<<"Just to confirm, ll of empty model is\n";
	// // LogProb ll = _model->ll();
	// // cerr<<"ll "<<ll<<"\n";

	// LogProb res = 0;

	// foreach(Analyses, ana, _seqs)
	// {
	// 	{
	// 		ints seq;
	// 		{
	// 			Segment* curr = (*ana)->segAt(0);
	// 			while(!curr->isStartOrEnd())
	// 			{
	// 				seq.push_back(curr->strSym);
	// 				curr = (*ana)->nextSegment(*curr);
	// 			}
	// 		}
	// 		TableLst tabs;
	// 		LogProb term = _model->updateSeq(seq, tabs, temp, false);
	// 		res += term;

	// 		// cerr<<"Prob of "<<**ana<<"\n";
	// 		// cerr<<term<<"\n";

	// 		TableLst::iterator ctr = tabs.begin();
	// 		{
	// 			Segment* curr = (*ana)->segAt(0);
	// 			while(!curr->isStartOrEnd())
	// 			{
	// 				curr->tab = *ctr;
	// 				++ctr;
	// 				curr = (*ana)->nextSegment(*curr);
	// 			}
	// 			curr->tab = *ctr;
	// 		}
	// 	}
	// }

	// return res;

	LogProb res = 0;
	foreach(intCRPMap, child, _model->_root->_children)
	{
		res += child->second->ll();
	}

	double dummy = 0;
	CRP fakeRoot(_model->_root->_alpha, _model->_root->_prior, dummy);
	foreach(intIntMap, item, _model->_root->_counts)
	{
		for(int ii = 0; ii < item->second; ++ii)
		{
			res += logl(fakeRoot.prob(item->first));
			fakeRoot.update(item->first);
		}
	}

	return res;
}

Segment::Segment(int i, int j, SymbolString& targ, CRPLM* model):
	begin(i),
	end(j),
	tab(NULL),
	target(&targ),
	str(target->alphabet()),
	targetSubstring(target->alphabet())
{
	assert(begin < end);

	if(!isStartOrEnd())
	{
		int size = target->size();
		assert(begin >= 0 && end <= size);
		int mySize = end - begin;

		str.resize(mySize);

		for(int ii = 0; ii < mySize; ++ii)
		{
			str[ii] = targ[ii + begin];
		}
	}

	strSym = model->intern(str);
	computeTargetSubstring();

	assert(&target->alphabet() != NULL);
	assert(&targetSubstring.alphabet() != NULL);
}

Segment::Segment(int i, int j, SymbolString& targ, 
				 SymbolString& underlying, int read,
				 CRPLM* model):
	begin(i),
	end(j),
	tab(NULL),
	target(&targ),
	str(target->alphabet()),
	targetSubstring(target->alphabet())
{
	int mySize = underlying.size() - read;
//true only for 1x1
//	assert(mySize == end - begin);
	assert(end > begin);
	assert(mySize > 0);

	str.resize(mySize);

	for(int ii = 0; ii < mySize; ++ii)
	{
		str[ii] = underlying[ii + read];
	}
	strSym = model->intern(str);
	computeTargetSubstring();

	assert(&target->alphabet() != NULL);
	assert(&targetSubstring.alphabet() != NULL);
}

Segment* Segment::copy(CRPLM* model)
{
	Segment* copy;
	if(isStartOrEnd())
	{
		copy = new Segment(begin, end, *target, model);
	}
	else
	{
		copy = new Segment(begin, end, *target, 
						   str, 0, model);
	}

	return copy;
}

bool Segment::isStartOrEnd()
{
	int size = target->size();
	return (end <= 0 || begin >= size);
}

Segment* Segment::split(int pos, CRPLM* model)
{
	assert(pos > begin && pos < end);

	int splitStarts = pos - begin;

	// cerr<<"split starts at "<<splitStarts<<" str size "<<str.size()<<"\n";

	//cast to (int) b/c the stupid fn returns a size_t
	if(splitStarts == 0 || (int)str.size() - splitStarts <= 0)
	{
		// cerr<<"can't\n";
		//splitting here would result in an empty string
		return NULL;
	}

	Segment* afterMe = new Segment(pos,
								   end, *target, str, splitStarts, 
								   model);
	end = pos;

	str.resize(splitStarts); //cut off later material

	int newSize = end - begin;
	assert(newSize > 0);
	strSym = model->intern(str);
	targetSubstring.resize(newSize); //cut off later material

	// cerr<<str<<" and "<<afterMe->str<<"\n";
	// cerr<<targetSubstring<<" "<<afterMe->targetSubstring<<"\n";

	return afterMe;
}

Segment* Segment::merge(Segment& other, CRPLM* model)
{
	if(other.end == begin)
	{
		return other.merge(*this, model);
	}

	assert(other.begin == end); //argument is to the right

	SymbolString mergedStr(str);
	mergedStr.append(other.str);

//	cerr<<"merging "<<str<<" and "<<other.str<<" to create "<<mergedStr<<"\n";

	Segment* newSeg = new Segment(begin, other.end, *target, mergedStr, 
								  0, model);
//	cerr<<" and output is "<<newSeg->str<<"\n";
	return newSeg;
}

void Segment::computeTargetSubstring()
{
	if(isStartOrEnd())
	{
		return;
	}

//	int size = end - begin;
//	targetSubstring.resize(size);
	assert(target->size() >= end);

	targetSubstring.appendRange(*target, begin, end);
	// cerr<<"fabricated the string "<<strHere<<"\n";
	// cerr<<"comparing it to "<<seg->str<<"\n";
}

SymbolString* Segment::targetSubstr()
{
	return &targetSubstring;
}

Analysis::Analysis(int ind, SymbolString& surf, CRPLM* mod):
	surface(surf),
	model(mod),
	index(ind),
	lockBounds(false),
	noU(false),
	allowDeletions(false),
	verbose(0),
	integrateOut(false),
	bigramMonkeys(false),
	invTemp(1.0),
	channelInvTemp(1.0),
	allVocab(NULL)
{
	int size = surface.size();

	Segment* start = new Segment(-1, 0, surface, model);
	segments[start->end] = start;

	Segment* all = new Segment(0, size, surface,  model);
	segments[all->end] = all;

	Segment* end = new Segment(size, size + 1, surface, model);
	segments[end->end] = end;

	if(dynamic_cast<BigramMonkey*>(model->_prior) != NULL ||
	   dynamic_cast<NGramMonkey*>(model->_prior) != NULL)
	{
		bigramMonkeys = true;
	}
}

Analysis::~Analysis()
{
	foreach(intToSeg, seg, segments)
	{
		delete seg->second;
	}
}

Analysis* Analysis::copy(int ind, CRPLM* mod)
{
	Analysis* copy = new Analysis(ind, surface, mod);

	//copy gets some placeholder segments, which we must wipe
	foreach(intToSeg, seg, copy->segments)
	{
		delete seg->second;
	}

	foreach(intToSeg, seg, segments)
	{
		copy->segments[seg->first] = seg->second->copy(mod);
	}

	return copy;
}

int Analysis::nSyms()
{
	return surface.size();
}

int Analysis::nSegments()
{
	assert(segments.size() > 2);
	return segments.size() - 2;
}

bool Analysis::posIsBreak(int pos)
{
	Segment* atPos = segAt(pos);
	return (atPos->begin == pos);
}

Segment* Analysis::segAt(int pos)
{
	//key is greater than pos-- ends later than this char
	intToSeg::iterator entry = segments.upper_bound(pos);
	return entry->second;
}

Segment* Analysis::nextSegment(Segment& seg)
{
	if(seg.end > surface.size())
	{
		return &seg;
	}
	else
	{
		intToSeg::iterator entry = segments.upper_bound(seg.end);
		return entry->second;
	}
}

Segment* Analysis::prevSegment(Segment& seg)
{
	if(seg.begin < 0)
	{
		return &seg;
	}
	else
	{
		return segments[seg.begin];
	}
}

bool Analysis::split(int pos)
{
	Segment* atPos = segAt(pos);
	// cerr<<"split command at "<<atPos->str<<" "<<pos<<"\n";

	assert(pos != atPos->begin);

	Segment* splitSeg = atPos->split(pos, model);

	if(splitSeg == NULL)
	{
		return false;
	}

	segments[atPos->end] = atPos;
	segments[splitSeg->end] = splitSeg;

	return true;
}

void Analysis::merge(int pos)
{
	Segment* atPos = segAt(pos);
	assert(pos == atPos->begin);
	assert(pos != 0); //can't merge the start of string
	Segment* beforePos = segAt(pos - 1);
	assert(beforePos->end == pos);

	Segment* mergeSeg = beforePos->merge(*atPos, model);

	segments.erase(beforePos->end);
	segments.erase(atPos->end);

	delete beforePos;
	delete atPos;

	segments[mergeSeg->end] = mergeSeg;
}

void Analysis::ensureSplit(int pos)
{
	Segment* atPos = segAt(pos);
	if(pos != atPos->begin)
	{
		split(pos);
	}
}

void Analysis::ensureMerge(int pos)
{
	Segment* atPos = segAt(pos);
	if(pos == atPos->begin)
	{
		merge(pos);
	}
}

void Analysis::print(ostream& os)
{
	os<<"|| ";
	foreach(intToSeg, seg, segments)
	{
		os<<seg->second->str<<" || ";
	}
	os<<"\n";
	os<<"-- ";
	foreach(intToSeg, seg, segments)
	{
		os<<seg->second->targetSubstring<<" || ";
	}
	os<<"\n";
}

void Analysis::write(ostream& os)
{
	foreach(intToSeg, seg, segments)
	{
		os<<seg->second->str<<" ";
	}
	os<<"\n";
}

void Analysis::writeSurface(ostream& os)
{
	foreach(intToSeg, seg, segments)
	{
		if(!seg->second->isStartOrEnd())
		{
			seg->second->targetSubstring.write(os);
		}
	}
	os<<"\n";
}

void Analysis::writeUnderlying(ostream& os)
{
	foreach(intToSeg, segPtr, segments)
	{
		Segment* seg = segPtr->second;
		if(!seg->isStartOrEnd())
		{
			seg->str.write(os, seg->begin, seg->end);
		}
	}
	os<<"\n";
}

State* Analysis::primeStates()
{
	assert(model->_ngrams <= 2);

	root = getState(0, 0, surface, 0);
	registerState(root);
	root->prob = 0;
	root->corr = 0;
	State* curr = root;

	Segment* seg = segAt(0);
	int chr = 0;
	int prevWord = 0;
	while(!seg->isStartOrEnd())
	{
		SymbolString& str = seg->str;
		int sz = str.size();
		for(int ii = 0; ii < sz; ++ii,++chr)
		{
			State* nxt = getState(1 + chr, prevWord, str, 1 + ii);
			registerState(nxt);
			curr->path = nxt;
			curr = nxt;

			model->_channel->_phoneCounts[str[ii]][str[ii]] += 1;
		}

		prevWord = seg->strSym;
		State* nxt = getState(chr, prevWord, str, 0);
		registerState(nxt);
		curr->path = nxt;
		curr = nxt;
		seg = nextSegment(*seg);
	}

	{
		State* sink = getState(1 + chr, 0, surface, 0);
		registerState(sink);
		curr->path = sink;
	}

	return root;
}

void Analysis::computeUs()
{
	State* curr = root;
	localUs[curr->index] = 1.0;
	wordUs[curr->index] = 0.0;

	while(!curr->end())
	{
		// cerr<<"state "<<*curr<<" "<<curr<<"\n";
		curr = curr->path;
		if(localUs.find(curr->index) == localUs.end())
		{
			localUs[curr->index] = 1.0;
			localTrans[curr->index] = 1.0;
			wordUs[curr->index] = 0.0;
		}
	}

	int prevWord = 0;
	int prevTrans = 0;
	curr = root;
	while(!curr->end())
	{
		curr = curr->path;

		if(curr->newWord())
		{
			intLst ctxt;
			if(model->_ngrams == 2)
			{
				ctxt.push_back(prevWord);
			}
			CRP* proc = model->getProcessOrEmpty(ctxt);
			intProbMap& beta = (integrateOut ? 
								proc->meanBeta(model->_ngrams == 1,
											   model->posteriorPCont()) :
								proc->beta());
			Prob ptrans = beta[curr->prevWord];

			if(verbose & DBG_U)
			{
				cerr<<"Transition at "<<*curr<<" from ";
				if(prevWord == -1){ cerr<<"-1"; }
				else { cerr<<model->repr(prevWord); }
				cerr<<" is "<<ptrans<<" exp is "<<powl(ptrans, invTemp)<<"\n";
			}
			// if(invTemp > 1)
			// {
			// 	ptrans = powl(ptrans, invTemp);
			// }

			// ptrans = powl(ptrans, 1.0 / invTemp);

			wordTrans[prevTrans] = ptrans;
			prevWord = curr->prevWord;
			prevTrans = curr->index;
		}

		if((!curr->chars.empty() ||
				 dynamic_cast<InsertState*>(curr))
				&& curr->index <= surface.size())
		{
			Prob eProb = curr->emit();

			if(verbose & DBG_U)
			{
				cerr<<"Emission at "<<*curr<<" is "<<eProb<<" exp is "<<
					powl(eProb, channelInvTemp)<<"\n";
			}

			// if(channelInvTemp > 1)
			// {
			// 	eProb = powl(eProb, channelInvTemp);
			// }

			// eProb = powl(eProb, channelInvTemp);

			localTrans[curr->index] *= eProb;

			// if(verbose & DBG_U)
			// {
			// 	cerr<<"Setting local u for "<<*curr<<" to "<<
			// 		localUs[curr->index]<<"\n";
			// }
		}
	}

	//note: this block does not do anything (b/c sampleWordU returns 0)
	//attempting to make it do anything is mathematically invalid and
	//probably dangerous
	foreach(intToProb, wi, wordUs)
	{
		intToProb::iterator entry = wordTrans.upper_bound(wi->first);
		--entry;
		Prob trans = entry->second;
		Prob uu = sampleWordU(trans);
		wi->second = uu;

		if(verbose & DBG_U)
		{
			cerr<<"word u at "<<wi->first<<" (trans "<<trans<<") "<<
				wi->second<<"\n";
		}
	}

	foreach(intToProb, wi, localTrans)	
	{
		//hard sampler
		// wi->second = wi->second - 1e-10;

		//no local control
		// wi->second = 0;

		localUs[wi->first] = sampleLocalU(wi->second);

		if(localUs[wi->first] < 0)
		{
			localUs[wi->first] = 0;
		}

		if(verbose & (DBG_U | DBG_WORDS))
		{
			cerr<<"original local "<<wi->first<<" "<<localTrans[wi->first]<<
				" set to "<<localUs[wi->first]<<"\n";
		}
	}

	makeEmitProbTable();
	makeGeneralSuffixTree();
	wordsAllowed.resize(surface.size() + 1);
	for(int pos = 0; pos < surface.size() + 1; ++pos)
	{
		wordsAllowed[pos].clear();
		checkWordsAtPos(pos);
	}
}

Prob Analysis::sampleLocalU(Prob trans)
{
	double ceiling = .05;
	double low = gsl_ran_flat(RNG, 0, min(ceiling, trans - 1e-10));
	double high = (trans - 1e-10) * gsl_ran_beta(RNG, 5, 1e-5);
	Prob res = 0;
	if(gsl_ran_flat(RNG, 0, 1) < channelInvTemp)
	{
		res = low;
	}
	else
	{
		res = high;
	}
	assert(res < trans);
	return res;
}

Prob Analysis::sampleWordU(Prob trans)
{
	return 0;
}

Prob Analysis::pLocalU(int ind, Prob emit)
{
	if(noU)
	{
		return 1;
	}

	if(emit == 0)
	{
		return 1; //doesn't matter what we return; it gets multiplied by 0
	}

	Prob uAt = getLocalU(ind);

	if(emit < uAt)
	{
		cerr<<"Emission prob "<<emit<<" should be cut by u "<<uAt<<
			" at "<<ind<<"\n";
		assert(0);
	}

	// if(channelInvTemp < 1)
	// {
	// 	emit = powl(emit, 1.0/channelInvTemp);
	// }
	Prob ceiling = .05;
//	ceiling = powl(ceiling, channelInvTemp);
	Prob low = min(ceiling, emit - 1e-10);
	// Prob high = 1.0 / (emit - 1e-10);

	Prob scaled = smoothExtremes(uAt / emit);
	Prob high = gsl_ran_beta_pdf(scaled, 5, 1e-5);

	double chance = channelInvTemp;
	Prob pU;
	if(uAt < ceiling)
	{
		pU = chance * (1.0 / low) + (1 - chance) * high;
	}
	else
	{
		pU = (1 - chance) * high;
	}

	Prob res = pU;
	if(res <= 0 && approxEQ(res, 0))
	{
		res = 1e-10;
	}
	if(!isfinite(res) || res <= 0)
	{
		cerr<<"Given u "<<uAt<<" and emit "<<emit<<" pu is "<<res<<"\n";
	}
	assert(isfinite(res) && res > 0);

	// cerr<<"Emit prob is "<<emit<<" u is "<<uAt<<" prob is "<<pU<<
	// 	" product is "<<(emit * res)<<"\n";

	return res;
}

LogProb Analysis::pWordU(int ind, Prob emit)
{
	//note that corresponding sample function returns 0
 	return 1;
}

State* Analysis::getState(int ind, int prev, SymbolString& read, int readTo)
{
	return getState(ind, prev, read, readTo, -1);
}

State* Analysis::getState(int ind, int prev, SymbolString& read, int readTo,
						  int addCh)
{
	State* newSt = new State(ind, prev, *this);
	newSt->chars.appendRange(read, 0, readTo);
	if(addCh != -1)
	{
		newSt->chars.push_back(addCh);
	}
	if(verbose & DBG_STATES){ cerr<<"getting state: "<<*newSt<<"\n"; }
	return newSt;
}

State* Analysis::getDeleteState(int ind, int prev,
								SymbolString& read, int readTo, int addCh,
								bool isPost)
{
	State* newSt = new DeleteState(ind, prev, isPost, *this);
	newSt->chars.appendRange(read, 0, readTo);
	if(addCh != -1)
	{
		newSt->chars.push_back(addCh);
	}
	if(verbose & DBG_STATES){ cerr<<"getting state: "<<*newSt<<"\n"; }
	return newSt;
}

State* Analysis::getInsertState(int ind, int prev,
								SymbolString& read, int readTo)
{
	State* newSt = new InsertState(ind, prev, *this);
	newSt->chars.appendRange(read, 0, readTo);
	if(verbose & DBG_STATES){ cerr<<"getting state: "<<*newSt<<"\n"; }
	return newSt;
}

State* Analysis::getMonkey(int ind, int prevCh, int addCh)
{
	State* newSt = NULL;
	if(bigramMonkeys)
	{
		newSt = new BiMonkeyState(ind, prevCh, addCh, *this);
	}
	else
	{
		Prob stop = model->_prior->_stop;
		newSt = new MonkeyState(ind, stop, *this);

		if(addCh != -1)
		{
			newSt->chars.push_back(addCh);
		}
	}
	
	if(verbose & DBG_STATES){ cerr<<"getting state: "<<*newSt<<"\n"; }
	return newSt;
}

State* Analysis::registerState(State* st)
{
	std::pair<StateSet::iterator, bool> res = states.insert(st);
	if(!res.second)
	{
		delete st;
		st = NULL;
	}

	if(states.size() % 10000 == 0)
	{
		StateSet::const_iterator select = res.first;
		select--;
		cerr<<states.size()<<" states known, last state "<<**select<<
			" current u "<<nextU<<"\n";
		// if(st)
		// {
		// 	cerr<<"\t\t: known states\n";
		// 	foreach(StateSet, known, states)
		// 	{
		// 		cerr<<"\t\t"<<**known<<"\n";
		// 	}
		// }
	}

	return *(res.first);
}

Prob Analysis::getLocalU(State* st)
{
	if(noU)
	{
		return 0;
	}

	Prob uu = localUs[st->index];

	//can be 0 when doing forced alt.
	if(uu < 0)
	{
		cerr<<*st<<"\n";
		assert(0);
	}

	return uu;
}

Prob Analysis::getLocalU(int index)
{
	if(noU)
	{
		return 0;
	}

	Prob uu = localUs[index];

	//can be 0 when doing forced alt.
	if(uu < 0)
	{
		cerr<<index<<" local u calculated as "<<uu<<"\n";
		assert(0);
	}

	return uu;
}

Prob Analysis::getULimit(State* st, CRP* proc)
{
	if(noU)
	{
		return -INFINITY;
	}
//	return -INFINITY;
	return nextU;
}

void Analysis::forward()
{
	nextU = -INFINITY;
	prevPr = 0;
	currHpr = 0;

	if(verbose & DBG_STATES)
	{
		cerr<<"\t\t: known states\n";
		foreach(StateSet, known, states)
		{
			cerr<<"\t\t"<<**known<<"\n";
		}
	}

	States origStates;
	{
		State* curr = root;
		while(!curr->end())
		{
			origStates.push_back(curr);
			curr = curr->path;
		}
		origStates.push_back(curr);
	}

	int nextOrig = 0;
	int nOrigStates = origStates.size();

	foreach(StateSet, curr, states)
	{
		if((*curr)->end())
		{
			if(verbose & DBG_STATES) { cerr<<"end state "<<**curr<<"\n"; }
			break;
		}
		if(verbose & DBG_STATES) { cerr<<"next state "<<**curr<<"\n"; }

		State* origS = origStates[nextOrig];

		if(!noU &&
		   (origS->index < (*curr)->index ||
			(origS->index == (*curr)->index &&
			 (
				 origS->sortType() < (*curr)->sortType() ||
				 //hack for deletions
				 (
					 deleteType((*curr)->sortType()) &&
					 origS->sortType() == (*curr)->sortType() &&
					 (*curr)->chars.size() > origS->chars.size()
					 )))))
		{
			while(nextOrig + 1 < nOrigStates)
			{
				State* upcoming = origStates[nextOrig + 1];
				if((*curr)->index < upcoming->index ||
				   (*curr)->sortType() < upcoming->sortType() ||
				   (
					   deleteType((*curr)->sortType()) &&
					   (*curr)->chars.size() < upcoming->chars.size()))
				{
					if(verbose & (DBG_U | DBG_STATES))
					{
						cerr<<"New tier at "<<**curr<<
							" but not ready to advance to "<<*upcoming<<"\n";
					}
					break;
				}
				else
				{
					if(verbose & (DBG_U | DBG_STATES))
					{
						cerr<<"Advance orig state to "<<*upcoming<<"\n";
					}
					origS = upcoming;
					++nextOrig;
				}
			}

			Prob suffPr = origS->suffix ?
				origS->suffix->bestString() : 1.0;
			assert(suffPr > 0);
			//subtract a small constant to prevent choosing u ~= hpr
			Prob heurPr = origS->prob + logl(suffPr) - 1e-5;
			currHpr = origS->corr - 1e-5;

			//turn off global beam
			nextU = -INFINITY;

			if(verbose & (DBG_U | DBG_STATES))
			{
				cerr<<"States in this tier (starting with "<<**curr<<")"<<
					" will be scored relative to "<<*origS<<
					" suff "<<suffPr<<" heur "<<heurPr<<" with u "<<
					nextU<<"\n";
			}

			if(!isfinite(origS->prob))
			{
				State* dbg = root;
				while(!dbg->end())
				{
					cerr<<*dbg<<"\n";
					dbg = dbg->path;
				}
			}
			assert(isfinite(origS->prob));
		}

		(*curr)->expand();

		//some state counting for efficiency analysis
		// if((*curr)->sortType() == SORT_DELETE || (*curr)->sortType() == SORT_POSTDELETE)
		// {
		// 	model->stats[string("deletestates")] += 1;
		// }
		// else if(dynamic_cast<InsertState*>((*curr)))
		// {
		// 	model->stats[string("insertstates")] += 1;
		// }
		// else if((*curr)->sortType() == SORT_EMIT)
		// {
		// 	model->stats[string("states")] += 1;
		// }
		// else if((*curr)->sortType() == SORT_REC)
		// {
		// 	model->stats[string("recstates")] += 1;
		// }
		// else if((*curr)->sortType() == SORT_MONKEY)
		// {
		// 	model->stats[string("monkeystates")] += 1;
		// }
		
		// if((*curr)->successors.empty())
		// {
		// if((*curr)->sortType() == SORT_DELETE || (*curr)->sortType() == SORT_POSTDELETE)
		// {
		// 	model->stats[string("faildeletestates")] += 1;
		// }
		// else if(dynamic_cast<InsertState*>((*curr)))
		// {
		// 	model->stats[string("failinsertstates")] += 1;
		// }
		// else if((*curr)->sortType() == SORT_EMIT)
		// {
		// 	model->stats[string("failstates")] += 1;
		// }
		// else if((*curr)->sortType() == SORT_REC)
		// {
		// 	model->stats[string("failrecstates")] += 1;
		// }
		// else if((*curr)->sortType() == SORT_MONKEY)
		// {
		// 	model->stats[string("failmonkeystates")] += 1;
		// }

		// }

		// if(verbose & DBG_STATES)
		// {
		// 	cerr<<"\t\t: known states\n";
		// 	foreach(StateSet, known, states)
		// 	{
		// 		cerr<<"\t\t"<<**known<<"\n";
		// 	}
		// }
	}
}

void Analysis::backward()
{
	foreach(StateSet, known, states)
	{
		(*known)->path = NULL;
	}

	// cerr<<"\t\t: known states\n";
	// foreach(StateSet, known, states)
	// {
	// 	cerr<<"\t\t"<<**known<<"\n";
	// }

	State* curr = (*states.rbegin());
	// cerr<<"final state "<<*curr<<"\n";

	while(curr != root)
	{
		curr = curr->sample();
		// cerr<<"next state "<<*curr<<"\n";
	}
}

void Analysis::trace()
{
	// State* curr = root;
	// while(curr != NULL)
	// {
	// 	cerr<<*curr<<"\n";
	// 	curr = curr->path;
	// }

	SymbolString emptyStr(*model->_prior->_alphabet);
	root->canonicalize(0, emptyStr);

	// {
	// 	cerr<<"Canonical seq:\n";
	// 	State* curr = root;
	// 	while(curr != NULL)
	// 	{
	// 		cerr<<*curr<<"\n";
	// 		curr = curr->path;
	// 	}
	// }

	//record and wipe the old word sequence
	{
		ints oldSeq;
		Segment* curr = segAt(0);
		while(!curr->isStartOrEnd())
		{
			oldSeq.push_back(curr->strSym);
			curr = nextSegment(*curr);
		}

		model->removeSeq(oldSeq, invTemp, false);
	}

	foreach(intToSeg, seg, segments)
	{
		delete seg->second;
	}
	segments.clear();

	//record and update the new word sequence
	{
		ints newSeq;
		int size = surface.size();

		Segment* start = new Segment(-1, 0, surface, model);
		segments[start->end] = start;

		int prevChar = 0;

		State* curr = root->path;
		while(!curr->end())
		{
			if(curr->newWord())
			{
				// cerr<<"just recognized "<<model->repr(curr->prevWord)<<"\n";

				newSeq.push_back(curr->prevWord);

				Segment* newSeg = new Segment(prevChar, curr->index,
											  surface, 
											  model->repr(curr->prevWord),
											  0, model);
				prevChar = curr->index;
				segments[newSeg->end] = newSeg;
			}

			curr = curr->path;
		}

		Segment* end = new Segment(size, size + 1, surface, model);
		segments[end->end] = end;

		model->updateSeq(newSeq, invTemp, false);
	}

	//wipe all unneeded memory and reassign to default values
	{
		StateSet keep;
		foreach(StateSet, state, states)
		{
			(*state)->wipe();

			if((*state)->path != NULL || (*state)->end())
			{
				assert(dynamic_cast<MonkeyState*>(*state) == NULL);
				keep.insert(*state);
			}
			else
			{
				delete *state;
			}
		}

		states = keep;

		// cerr<<"\t\t: known states\n";
		// foreach(StateSet, known, states)
		// {
		// 	cerr<<"\t\t"<<**known<<"\n";
		// }

		root->prob = 0;
		root->corr = 0;
	}
}

void Analysis::handleLengthAlterations(intSet& added)
{
	//an obnoxious workaround for how our G0 machine doesn't ever
	//produce underlying strings longer or shorter than their surf
	//equivalents... if such a word exists a previous state
	//we must keep it in the beta table explicitly so that the previous
	//path can have a probability...
	//so just stick it in, kludge it into the lex table so that
	//it isn't decanonicalized into G0 monkey states
	State* curr = root;
	intSet addToTable;
	// intSet wordsInSeq;
	// wordsInSeq.insert(-1);

	bool sawAlter = false;

	while(!curr->end())
	{
		curr = curr->path;
		if(dynamic_cast<DeleteState*>(curr) != NULL ||
		   dynamic_cast<InsertState*>(curr) != NULL)
		{
			sawAlter = true;
		}

		if(curr->newWord())
		{
			if(sawAlter)
			{
				addToTable.insert(curr->prevWord);
			}
			// wordsInSeq.insert(curr->prevWord);
			sawAlter = false;
		}
	}

	if(dynamic_cast<CollocationMonkey*>(model->_prior))
	{
		CollocationMonkey* cm = 
			dynamic_cast<CollocationMonkey*>(model->_prior);
		CRP* fr = cm->_freqs;
		foreach(intIntMap, w1, fr->_counts)
		{
			addToTable.insert(w1->first);
		}
	}

	foreach(intSet, add, addToTable)
	{
		if(!inMap(model->lexicon(), *add))
		{
			model->_root->labels()[*add] = 0;
			added.insert(*add);

			if(verbose & DBG_U)
			{
				cerr<<"putting "<<*add<<" "<<model->repr(*add)<<
					" in btable (prob "<<model->_root->prob(*add)<<")\n";
			}

			// foreach(intSet, context, wordsInSeq)
			// {
			// 	intLst ctxt;
			// 	ctxt.push_back(*context);
			// 	CRP* proc = model->getProcessOrEmpty(ctxt);

			// 	intProbMap& beta = proc->meanBeta();
			// 	Prob fakePr = beta[-1] * model->_prior->prob(*add);
			// 	assert(fakePr > 0);
			// 	beta[*add] = fakePr;

			// 	if(verbose & DBG_U)
			// 	{
			// 		cerr<<"putting "<<*add<<" "<<
			// 			model->repr(*add)<<
			// 			" in btable for prevword (canon as ";
			// 		if(*context == -1)
			// 		{
			// 			cerr<<"UNK";
			// 		}
			// 		else
			// 		{
			// 			cerr<<model->repr(*context);
			// 		}
			// 		cerr<<") with p= "<<fakePr<<"\n";
			// 	}
			// }
		}
	}
}

void Analysis::handleCollocations(intSet& added)
{
	if(!dynamic_cast<CollocationMonkey*>(model->_prior))
	{
		return;
	}

	CollocationMonkey* cm = 
		dynamic_cast<CollocationMonkey*>(model->_prior);
	CRP* fr = cm->_freqs;
	intSet allAllowed;
	//sloppy overgeneralization of set of allowed collocs
	//set of allowed words ^ set of meta words
	foreach(intSetVec, pos, wordsAllowed)
	{
		foreach(intSet, wi, *pos)
		{
			allAllowed.insert(*wi);
		}
	}

	intSet allowedMetas;
	foreach(intSet, allowed, allAllowed)
	{
		if(inMap(fr->_counts, *allowed))
		{
			allowedMetas.insert(*allowed);
		}
	}

	intSet addToTable;

	foreach(intSet, meta, allowedMetas)
	{
		SymbolString& m1 = model->repr(*meta);
		foreach(intSet, meta2, allowedMetas)
		{
			SymbolString& m2 = model->repr(*meta2);
			SymbolString si(*model->_prior->_alphabet);
			si.appendRange(m1, 0, m1.size());
			si.appendRange(m2, 0, m2.size());

			addToTable.insert(model->intern(si));
		}
	}

	LazySuffixTree* newVocab =
		new LazySuffixTree(model->_prior->_alphabet->size(),
						   0, false, &model->_prior->_lexicon);

	foreach(intSet, add, addToTable)
	{
		if(!inMap(model->lexicon(), *add))
		{
			model->_root->labels()[*add] = 0;
			added.insert(*add);

			//make sure word goes in generic suff tree
			SymbolString& word = model->repr(*add);
			newVocab->insert(word, 1, 1);

			if(verbose & DBG_U)
			{
				cerr<<"putting "<<*add<<" "<<model->repr(*add)<<
					" in btable (prob "<<model->_root->prob(*add)<<")\n";
			}

		}
	}

	for(int pos = 0; pos < surface.size() + 1; ++pos)
	{
		SymbolString checkStr(surface.alphabet());
		checkStr.push_back(0);
		checkWordsAtPos1(pos, checkStr, newVocab, 0);
	}

	delete newVocab;
	model->_betaDate++;
}

void Analysis::decanonicalize()
{
	if(verbose & DBG_STATES)
	{
		cerr<<"\t\t: before decanonicalize known states\n";
		foreach(StateSet, known, states)
		{
			cerr<<"\t\t"<<**known<<"\n";
		}
	}

	State* prevRecog = root;
	State* curr = root;

	while(!curr->end())
	{
		if(curr->prevWord == -1 && !curr->path->newWord())
		{
			curr->path->prevWord = -1;
		}

		curr = curr->path;

		if(curr->newWord())
		{
			if(!inMap(model->lexicon(), curr->prevWord) &&
			   !(curr->prevWord == 0 && model->_ngrams == 1))
			{
				SymbolString& str = model->repr(curr->prevWord);

				curr->prevWord = -1;

				// cerr<<"no entry for "<<str<<"\n";
				// cerr<<"prev recog from "<<*prevRecog<<"\n";
				State* monkey = getMonkey(prevRecog->index, -1, -1);
				monkey = registerState(monkey);
				prevRecog->path = monkey;

				int prevCh = -1;
				for(int ii = 0; ii < str.size(); ++ii)
				{
					State* nxt = getMonkey(prevRecog->index + ii + 1,
										   prevCh, str[ii]);
					nxt = registerState(nxt);
					monkey->path = nxt;
					monkey = nxt;
					prevCh = str[ii];
				}

				monkey->path = curr;
			}

			prevRecog = curr;
		}
	}

	{
		StateSet keep;
		State* curr = root;
		while(curr != NULL)
		{
			keep.insert(curr);
			curr = curr->path;
		}

		foreach(StateSet, state, states)
		{
			if(keep.find(*state) == keep.end())
			{
				(*state)->wipe();
				delete *state;
			}
		}

		states = keep;

		root->prob = 0;
		root->corr = 0;
	}

	// cerr<<"\t\t: decanonicalize known states\n";
	// foreach(StateSet, known, states)
	// {
	// 	cerr<<"\t\t"<<**known<<"\n";
	// }
}

LazySuffixTree* Analysis::unkTree(int pos)
{
	if(unkTrees[pos] != NULL)
	{
		return unkTrees[pos];
	}

	intLst ctxt;
	if(model->_ngrams == 2)
	{
		ctxt.push_back(-1);
	}
	CRP* proc = model->getProcessOrEmpty(ctxt);

	LazySuffixTree* ut = new LazySuffixTree(model->_prior->_alphabet->size(),
								 0, &model->_prior->_lexicon);

	intProbMap& beta = (integrateOut ? 
						proc->meanBeta(model->_ngrams == 1,
									   model->posteriorPCont()) :
						proc->beta());

	intSet& allowed = wordsAllowed[pos];
	Prob wordU = wordUs[pos];
	foreach(intSet, wi, allowed)
	{
		Prob pr = beta[*wi];
		if(pr > wordU)
		{
			SymbolString& word = model->repr(*wi);
			pr = powl(pr, invTemp);
			if(verbose & DBG_STATES)
			{
				cerr<<"inserting "<<word<<" pr "<<pr<<"\n";
			}
			Prob corr = pWordU(pos, pr);
			ut->insert(word, pr, corr);
		}
	}

	unkTrees[pos] = ut;
	return ut;
}

void Analysis::onlyResample()
{
	ints seq;
	TableLst tabs;
	{
		Segment* curr = segAt(0);
		while(!curr->isStartOrEnd())
		{
			tabs.push_back(curr->tab);
			seq.push_back(curr->strSym);
			curr = nextSegment(*curr);
		}
		tabs.push_back(curr->tab);
	}
	model->resampleTables(seq, tabs, invTemp, false);
	TableLst::iterator ctr = tabs.begin();
	{
		Segment* curr = segAt(0);
		while(!curr->isStartOrEnd())
		{
			curr->tab = *ctr;
			++ctr;
			curr = nextSegment(*curr);
		}
		curr->tab = *ctr;
	}
}

void Analysis::mhrSweep()
{
	integrateOut = true;
	Channel* ch = model->_channel;

	Prob postOld;
	ints oldSeq;
	TableLst tabs;
	{
		Segment* curr = segAt(0);
		while(!curr->isStartOrEnd())
		{
			tabs.push_back(curr->tab);
			oldSeq.push_back(curr->strSym);
			model->_rawCounts[curr->strSym] += 10;
			curr = nextSegment(*curr);
		}
		tabs.push_back(curr->tab);

		postOld = model->removeSeq(oldSeq, tabs, invTemp, verbose & DBG_MHR);

		//table ass'ts annealed but probability returned is not affected
		//so have to correct it here
		postOld *= invTemp;

		//compute the channel costs
		postOld += root->channelCost();
	}
	model->_rawCounts[0] += 10;

	intSet betasWithNoCounts;
	handleLengthAlterations(betasWithNoCounts);

	decanonicalize();

	//record old state seq in case we need it
	States ptrs;
	{
		State* curr = root;
		while(!curr->end())
		{
			ptrs.push_back(curr->path);
			curr = curr->path;
		}
	}

	unkTrees.resize(surface.size() + 1);

	computeUs();

	handleCollocations(betasWithNoCounts);

	forward();

	LogProb oldUTerm = 0;

	{
		State* curr = root;
		while(!curr->end())
		{
			//also record p(u|q)
			if((!curr->chars.empty() ||
				dynamic_cast<InsertState*>(curr))
			   && curr->index <= surface.size())
			{
				// cerr<<"check "<<*curr<<"\n";
				Prob emitPr = curr->emit();
				// cerr<<"emit at "<<*curr<<" "<<curr->emit()<<"\n";
				oldUTerm += logl(pLocalU(curr->index, emitPr));

				ch->_phoneCounts[curr->lastChar()][curr->emitted()] -= 1;
				if(curr->lastChar() == -1)
				{
					int prevCh = curr->chars.empty() ? 
						-1 : curr->chars[curr->chars.size() - 1];
					ch->_epsCounts[prevCh] -= 1;
				}
			}

			curr = curr->path;
		}
		// postOld += channelInvTemp * oldUTerm;
		postOld += oldUTerm;
	}

	Prob propOld = 0;
	{
		State* curr = root;
		do
		{
			State* nxt = curr->path;
			if(verbose & DBG_MHR)
			{
				LogProb ppr = nxt->pathProb(curr);
				cerr<<"Old state "<<*curr<<" connects to "<<*nxt<<"\n";
				cerr<<"\tprob is "<<expl(ppr)<<" log "<<ppr<<"\n";
			}
			propOld += nxt->pathProb(curr);
			curr = nxt;
			assert(curr != NULL);
		} while(!curr->end());
	}
//	assertLogProb(propOld);
	assert(isfinite(propOld));

	if(verbose & DBG_MHR)
	{
		State* curr = root;
		Prob pathPr = 1;
		do
		{
			State* nxt = curr->path;
			cerr<<"Uncorrected: old "<<*curr<<" connects to "<<*nxt<<"\n";
			cerr<<"\tprob is "<<nxt->predecessors[curr]<<" log "<<
				logl(nxt->predecessors[curr])<<"\n";
			pathPr *= nxt->predecessors[curr];
			curr = nxt;
		} while(!curr->end());
		cerr<<"Verified path prob: "<<pathPr<<" log "<<logl(pathPr)<<"\n";
	}		

	backward();

	Prob propNew = 0;
	{
		State* curr = root;
		do
		{
			State* nxt = curr->path;
			if(verbose & DBG_MHR)
			{
				LogProb ppr = nxt->pathProb(curr);
				cerr<<"New state "<<*curr<<" connects to "<<*nxt<<"\n";
				cerr<<"\tprob is "<<expl(ppr)<<" log "<<ppr<<"\n";
				foreach(StateToProb, si, nxt->predecessors)
				{
					cerr<<"\t"<<*(si->first)<<" "<<
						(expl((si->first)->prob) * si->second)<<"\n";
				}
			}
			propNew += nxt->pathProb(curr);
			curr = nxt;
			assert(curr != NULL);
		} while(!curr->end());
	}

	if(verbose & DBG_MHR)
	{
		State* curr = root;
		Prob pathPr = 1;
		do
		{
			State* nxt = curr->path;
			cerr<<"Uncorrected: new "<<*curr<<" connects to "<<*nxt<<"\n";
			cerr<<"\tprob is "<<nxt->predecessors[curr]<<" log "<<
				logl(nxt->predecessors[curr])<<"\n";
			pathPr *= nxt->predecessors[curr];
			curr = nxt;
		} while(!curr->end());
		cerr<<"Verified path prob: "<<pathPr<<" log "<<logl(pathPr)<<"\n";
	}

	// if(verbose & DBG_STATES)
	// {
	// 	cerr<<"Pre-canonical seq\n";
	// 	State* curr = root;
	// 	while(curr != NULL)
	// 	{
	// 		cerr<<*curr<<"\n";
	// 		curr = curr->path;
	// 	}
	// }

	//compute u term before canonicalization
	LogProb uTerm = 0;
	{
		State* curr = root->path;
		while(!curr->end())
		{
			if((!curr->chars.empty() ||
				dynamic_cast<InsertState*>(curr))
			   && curr->index <= surface.size())
			{
				Prob emitPr = curr->emit();
				// cerr<<"emit at "<<*curr<<" "<<curr->emit()<<"\n";
				uTerm += logl(pLocalU(curr->index, emitPr));
			}
			curr = curr->path;
		}
	}

	SymbolString emptyStr(*model->_prior->_alphabet);
	root->canonicalize(0, emptyStr);

	// if(verbose & DBG_STATES)
	// {
	// 	cerr<<"Canonical seq\n";
	// 	State* curr = root;
	// 	while(curr != NULL)
	// 	{
	// 		cerr<<*curr<<"\n";
	// 		curr = curr->path;
	// 	}
	// }

	cerr<<"proposed solution ";

	Prob postNew;
	ints newSeq;
	TableLst newTabs;
	{
		State* curr = root->path;
		while(!curr->end())
		{
			if(curr->newWord())
			{
				cerr<<model->repr(curr->prevWord)<<" | ";
				// cerr<<"just recognized "<<model->repr(curr->prevWord)<<"\n";
				newSeq.push_back(curr->prevWord);
				assert(curr->prevWord != -1);
			}

			curr = curr->path;
		}

		postNew = model->updateSeq(newSeq, newTabs, 
								   invTemp, verbose & DBG_MHR);
		postNew *= invTemp;
		postNew += root->channelCost();

		// postNew += channelInvTemp * uTerm;
	}

	postNew += uTerm;
	cerr<<"\n";

	LogProb mhr = (propOld - propNew) + (postNew - postOld);
	Prob mhrE = expl(mhr);
	Prob sm = gsl_rng_uniform(RNG);

	cerr<<"u term for old "<<oldUTerm<<" u term for new "<<uTerm<<"\n";

	{
		cerr<<"Propose (old "<<propOld<<" "<<propNew<<") "<<
			(propOld - propNew)<<"\n";
		cerr<<"Post (old "<<postOld<<" "<<postNew<<") "<<
			(postOld - postNew)<<"\n";
		cerr<<"MHR "<<mhr<<" "<<mhrE<<"\n";
	}

	if(sm > mhrE)
	{
		cerr<<"Reject\n";
		model->removeSeq(newSeq, newTabs, invTemp, false);
		newTabs.clear();
		model->updateSeq(oldSeq, newTabs, invTemp, false);
		newSeq = oldSeq;

		foreach(StateSet, known, states)
		{
			(*known)->path = NULL;
		}

		State* curr = root;
		foreach(States, nxt, ptrs)
		{
			curr->path = *nxt;
			curr = *nxt;
		}

		SymbolString emptyStr(*model->_prior->_alphabet);
		root->canonicalize(0, emptyStr);
	}
	else
	{
		cerr<<"Accept\n";
	}

	model->resampleTables(newSeq, newTabs, invTemp, false);

	// {
	// 	cerr<<"Post rejection seq\n";
	// 	State* curr = root;
	// 	while(curr != NULL)
	// 	{
	// 		cerr<<*curr<<"\n";
	// 		curr = curr->path;
	// 	}
	// }

	//clean up
	foreach(intToSeg, seg, segments)
	{
		delete seg->second;
	}
	segments.clear();

	//record and update the new word sequence
	{
		int size = surface.size();

		Segment* start = new Segment(-1, 0, surface, model);
		segments[start->end] = start;

		int prevChar = 0;

		State* curr = root->path;
		TableLst::iterator ctr = newTabs.begin();
		while(!curr->end())
		{
			if(curr->newWord())
			{
				// cerr<<"just recognized "<<model->repr(curr->prevWord)<<"\n";
				Segment* newSeg = new Segment(prevChar, curr->index,
											  surface, 
											  model->repr(curr->prevWord),
											  0, model);
				newSeg->tab = *ctr;
				++ctr;
				prevChar = curr->index;
				segments[newSeg->end] = newSeg;
			}

			if((!curr->chars.empty() ||
				dynamic_cast<InsertState*>(curr))
			   && curr->index <= surface.size())
			{
				ch->_phoneCounts[curr->lastChar()][curr->emitted()] += 1;
				if(curr->lastChar() == -1)
				{
					int prevCh = curr->chars.empty() ? 
						-1 : curr->chars[curr->chars.size() - 1];
					ch->_epsCounts[prevCh] += 1;
				}
			}

			if(curr->path && !curr->path->newWord() &&
			   curr->index == curr->path->index &&
			   !dynamic_cast<DeleteState*>(curr) &&
			   !dynamic_cast<DeleteState*>(curr->path))
			{
				cerr<<"Non-delete state implementing deletion "<<
					*curr<<" "<<*curr->path<<"\n";
				assert(0);
			}

			curr = curr->path;
		}

		Segment* end = new Segment(size, size + 1, surface, model);
		segments[end->end] = end;
		end->tab = *ctr;
	}

	//update raw counts
	{
		foreach(ints, wi, oldSeq)
		{
			model->_rawCounts[*wi] -= 11;
			if(model->_rawCounts[*wi] == 0)
			{
				model->_rawCounts.erase(*wi);
			}
		}
		model->_rawCounts[0] -= 10;

		Segment* curr = segAt(0);
		while(!curr->isStartOrEnd())
		{
			model->_rawCounts[curr->strSym] += 1;
			curr = nextSegment(*curr);
		}
	}

	//wipe all unneeded memory and reassign to default values
	{
		StateSet keep;
		foreach(StateSet, state, states)
		{
			(*state)->wipe();

			if((*state)->path != NULL || (*state)->end())
			{
				assert(dynamic_cast<MonkeyState*>(*state) == NULL);
				keep.insert(*state);
			}
			else
			{
				delete *state;
			}
		}

		states = keep;

		// cerr<<"\t\t: known states\n";
		// foreach(StateSet, known, states)
		// {
		// 	cerr<<"\t\t"<<**known<<"\n";
		// }

		root->prob = 0;
		root->corr = 0;

		foreach(intSet, bi, betasWithNoCounts)
		{
			if(model->_root->labels()[*bi] == 0)
			{
				model->_root->labels().erase(*bi);
			}
		}
	}

	foreach(LazySuffixTrees, ut, unkTrees)
	{
		delete *ut;
	}

	unkTrees.clear();
	localUs.clear();
	localTrans.clear();
	wordUs.clear();
	wordTrans.clear();
	emitAllowed.clear();
	if(allVocab)
	{
		delete allVocab;
	}
	for(int pos = 0; pos < surface.size() + 1; ++pos)
	{
		wordsAllowed[pos].clear();
	}
}

void Analysis::makeEmitProbTable()
{
	int sz = surface.size() + 1;
	int alphaSize = surface.alphabet().size();

	emitAllowed.resize(sz + 1);

	//pos 0
	{
		intMat& emitByPrev = emitAllowed[0];
		emitByPrev.resize(alphaSize + 1);
		Prob localU = localUs[0];

		for(int prevCh = 0; prevCh < alphaSize + 1; ++prevCh)
		{
			ints& emits = emitByPrev[prevCh];
			emits.resize(2 * alphaSize + 1);
		}

		for(int ch = 0; ch < alphaSize; ++ch)
		{
			Prob emitCh = model->_channel->emitCh(ch, -1);
			if(emitCh > localU)
			{
				// cerr<<"At pos 0 we can emit a "<<
				// 	surface.alphabet().inv(ch)<<"\n";

				for(int prevCh = 0; prevCh < alphaSize + 1; ++prevCh)
				{
					Prob noEps = 1 - model->_channel->epsilonAfter(prevCh - 1);
					// cerr<<"At pos 0 after "<<(prevCh - 1)<<"\n";
					Prob emit = noEps * emitCh;

					if(emit > localU)
					{
						// cerr<<"Allow\n";
						emitByPrev[prevCh][alphaSize + ch] = 1;
					}
				}
			}
		}
	}

	//only for bigram channel model looking left
	for(int pos = 1; pos < sz; ++pos)
	{
		intMat& emitByPrev = emitAllowed[pos];
		emitByPrev.resize(alphaSize + 1);
		int surfChar = surface[pos - 1];
		Prob localU = localUs[pos];

		for(int prevCh = 0; prevCh < alphaSize + 1; ++prevCh)
		{
			ints& emits = emitByPrev[prevCh];
			emits.resize(2 * alphaSize + 1);
		}

		for(int ch = 0; ch < alphaSize; ++ch)
		{
			Prob emitCh = model->_channel->emitCh(ch, surfChar);

			if(emitCh > localU)
			{
				for(int prevCh = 0; prevCh < alphaSize + 1; ++prevCh)
				{
					Prob noEps = 1 -
						model->_channel->epsilonAfter(prevCh - 1);

					Prob emit = noEps * emitCh;

					if(emit > localU)
					{
						// cerr<<"At pos "<<pos<<" emit "<<
						// 	surface.alphabet().inv(ch)<<" = "<<emit<<"\n";
						emitByPrev[prevCh][ch] = 1;
					}
				}
			}

			Prob emitNone = model->_channel->emitCh(ch, -1);

			if(emitNone > localU)
			{
				for(int prevCh = 0; prevCh < alphaSize + 1; ++prevCh)
				{
					Prob noEps = 1 -
						model->_channel->epsilonAfter(prevCh - 1);
					Prob emit = emitNone * noEps;

					if(emit > localU)
					{
						// cerr<<"At pos "<<pos<<" delete "<<
						// 	surface.alphabet().inv(ch)<<" = "<<emit<<"\n";
						emitByPrev[prevCh][alphaSize + ch] = 1;
					}
				}
			}
		}

		{
			Prob insEmit = model->_channel->emitCh(-1, surfChar);
			if(insEmit > localU)
			{
				for(int prevCh = 0; prevCh < alphaSize + 1; ++prevCh)
				{
					Prob eps = model->_channel->epsilonAfter(prevCh - 1);
					Prob emit = eps * insEmit;

					if(emit > localU)
					{
						// cerr<<"At pos "<<pos<<" insert next\n";
						emitByPrev[prevCh][2 * alphaSize] = 1;
					}
				}
			}
		}
	}
}

void Analysis::makeGeneralSuffixTree()
{
	allVocab = new LazySuffixTree(model->_prior->_alphabet->size(),
								  0, false, &model->_prior->_lexicon);

	intLst ctxt;
	if(model->_ngrams == 2)
	{
		ctxt.push_back(-1);
	}
	CRP* proc = model->getProcessOrEmpty(ctxt);

	intProbMap& beta = (integrateOut ? 
						proc->meanBeta(model->_ngrams == 1,
									   model->posteriorPCont())
						: proc->beta());

	foreach(intProbMap, bi, beta)
	{
		if(bi->first != -1)
		{
			SymbolString& word = model->repr(bi->first);
			// cerr<<"Adding "<<word<<" "<<bi->second<<"\n";
			Prob pr = 1;
			allVocab->insert(word, pr, pr);
		}
	}
}

void Analysis::checkWordsAtPos(int pos)
{
	if(verbose & DBG_WORDS)
	{
		cerr<<"Checking words for "<<pos<<": ";
	}
	SymbolString checkStr(surface.alphabet());
	checkStr.push_back(0);
	checkWordsAtPos1(pos, checkStr, allVocab, 0);
	wordsAllowed[pos].insert(0);
	if(verbose & DBG_WORDS)
	{
		cerr<<wordsAllowed[pos].size()<<" words pass\n";
	}
}

void Analysis::checkWordsAtPos1(int pos, SymbolString& checkStr,
								LazySuffixTree* suffix, int depth)
{
	// cerr<<"Checking words at pos "<<pos<<"\n";

	LazySuffixTrees& succs = suffix->successors();
	int sz = succs.size();
	for(int nxt = 0; nxt < sz; ++nxt)
	{
		if(succs[nxt] == NULL)
		{
			continue;
		}

		checkStr[depth] = nxt;
		int word = model->intern(checkStr);

		if(checkWordAllowed(word, pos))
		{
			// cerr<<"Ok, aligned "<<checkStr<<"\n";

			if(succs[nxt]->final())
			{
				// cerr<<"Insert "<<checkStr<<"\n";
				wordsAllowed[pos].insert(word);
			}

			if(depth < 2)
			{
				checkStr.push_back(0);
				checkWordsAtPos1(pos, checkStr, succs[nxt], depth + 1);
				checkStr.pop_back();
			}
			else
			{
				foreach(intPayloads, wi, succs[nxt]->cache())
				{
					if(checkWordAllowed(wi->first, pos))
					{
						// cerr<<"Insert-on-cache "<<
						// 	model->repr(wi->first)<<"\n";
						wordsAllowed[pos].insert(wi->first);
					}
					// else
					// {
					// 	cerr<<"checked cached "<<model->repr(wi->first)<<
					// 		" and couldn't align\n";
					// }
				}
			}
		}
		// else
		// {
		// 	cerr<<"Can't align "<<checkStr<<"\n";
		// }
	}
}

bool Analysis::checkWordAllowed(int sym, int pos)
{
	if(noU)
	{
		return true;
	}

	SymbolString& word = model->repr(sym);
	int wordSize = word.size();
	int strSize = surface.size();
	int charsLeft = strSize - pos;
	int alphaSize = surface.alphabet().size();

	if(wordSize == 0)
	{
		return true;
	}

	bool row1[charsLeft + 1];
	bool row2[charsLeft + 1];
	for(int jj = 0; jj < charsLeft + 1; ++jj)
	{
		row1[jj] = 0;
		row2[jj] = 0;
	}
	bool* thisRow = row1;
	bool* nextRow = row2;
	thisRow[0] = 1;

	int prevCh = 0;
	int beginRow = 0;
	int endRow = 1;
	int nextRowBegin = strSize;
	int nextRowEnd = -1;

	if(verbose & DBG_FILTER)
	{
		cerr<<"checking "<<word<<" at pos "<<pos<<" in "<<surface<<"\n";
		if(pos < strSize)
		{
			cerr<<"ch "<<surface.alphabet().inv(surface[pos])<<"\n";
		}
	}

	for(int ii = 0; ii < wordSize; ++ii)
	{
		bool markedAny = false;

		for(int jj = beginRow; jj < endRow; ++jj)
		{
			if(verbose & DBG_FILTER)
			{
				cerr<<"filling in cell "<<ii<<" "<<jj<<"\n";
			}

			if(thisRow[jj])
			{
				//insert
				if(pos + jj < strSize &&
				   emitAllowed[pos + jj + 1][prevCh][2 * alphaSize])
				{
					if(verbose & DBG_FILTER)
					{
						cerr<<"insert "<<
							word.alphabet().inv(surface[pos + jj])<<"\n";
					}
					thisRow[jj + 1] = 1;
					if(jj + 1 == endRow)
					{
						endRow++;
					}
				}

				//emit
				if(pos + jj < strSize &&
				   emitAllowed[pos + jj + 1][prevCh][word[ii]])
				{
					if(verbose & DBG_FILTER)
					{
						cerr<<"emit "<<word.alphabet().inv(word[ii])<<" -> "<<
							word.alphabet().inv(surface[pos + jj])<<"\n";
					}
					nextRow[jj + 1] = 1;
					markedAny = true;
					nextRowBegin = min(nextRowBegin, jj + 1);
					nextRowEnd = max(nextRowEnd, jj + 2);
				}

				//delete
				if(verbose & DBG_FILTER)
				{
					cerr<<"my prev char is "<<(
						prevCh == 0 ? "-1" : word.alphabet().inv(prevCh - 1))<<
						" pos "<<(pos + jj)<<" can I delete?\n";
				}

				if(pos + jj <= strSize && allowDeletions &&
				   emitAllowed[pos + jj][prevCh][alphaSize + word[ii]])
				{
					if(verbose & DBG_FILTER)
					{
						cerr<<"delete "<<word.alphabet().inv(word[ii])<<"\n";
					}
					nextRow[jj] = 1;
					markedAny = true;
					nextRowBegin = min(nextRowBegin, jj);
					nextRowEnd = max(nextRowEnd, jj + 1);
				}
			}
		}

		if(!markedAny)
		{
			//if you fail to print word[ii] just quit
			if(verbose & DBG_FILTER)
			{
				cerr<<"No good, see you later!\n";
			}
			return false;
		}

		bool* temp = thisRow;
		thisRow = nextRow;
		nextRow = temp;
		for(int jj = beginRow; jj < endRow; ++jj)
		{
			nextRow[jj] = 0;
		}

		prevCh = word[ii] + 1; //plus 1 b/c -1 is coded as 0
		beginRow = nextRowBegin;
		endRow = nextRowEnd;
		nextRowBegin = strSize;
		nextRowEnd = -1;
	}

	//if you haven't returned false yet you must have printed it somehow
	if(verbose & DBG_FILTER)
	{
		cerr<<"Ok!\n";
	}
	return true;
}

bool Analysis::wordAllowed(int word, int pos)
{
	if(noU)
	{
		return true;
	}
	intSet& wordsAllowedPos = wordsAllowed[pos];
	return contains(wordsAllowedPos, word);
}

bool Analysis::boundaryAt(int pos)
{
	return contains(allowedBounds, pos);
}

Prob forcedAlign(SymbolString& in, SymbolString& out, PhoneChannel* ch,
				 bool learn)
{
	bool debug = false;

	//use previously existing code to align a pair of fixed strings

	//first, build a tiny simulated model
	doubles alpha(2, 1);
	doubles discount(2);
	Monkey* simian = new Monkey(.5, true, .001, in.alphabet());

	string empty("");
	SymbolString emptySym(empty, in.alphabet());
	int start = simian->intern(emptySym);
	assert(start == 0);

	CRPLM model(2, alpha, discount, simian, ch);

	//now, make the model think the two strings exist
	int inStr = model.intern(in);
	int outStr = model.intern(out);
	intLst ctxt;
	ctxt.push_back(0);
	CRP* proc0 = model.getProcess(ctxt);
	proc0->update(inStr);
	Table* rtab;
	proc0->update(outStr, rtab);

	ctxt.clear();
	ctxt.push_back(inStr);
	CRP* proc1 = model.getProcess(ctxt);
	proc1->update(0);

	//with alpha > 0, set up some sane-looking us
	model._betaDate = 1;

	// cerr<<"in str is "<<inStr<<"\n";
	// cerr<<"out str is "<<outStr<<"\n";

	Analysis ana(0, out, &model);
	ana.integrateOut = true;

	if(debug)
	{
		cerr<<ana<<"\n";
	}

	ana.primeStates();

	//remove old empiricals
	{
		State* curr = ana.root;
		while(!curr->end())
		{
			if((!curr->chars.empty() ||
				dynamic_cast<InsertState*>(curr))
			   && curr->index <= ana.surface.size())
			{
				ch->_phoneCounts[curr->lastChar()][curr->emitted()] -= 1;
				if(curr->lastChar() == -1)
				{
					int prevCh = curr->chars.empty() ? 
						-1 : curr->chars[curr->chars.size() - 1];
					ch->_epsCounts[prevCh] -= 1;
				}
			}

			curr = curr->path;
		}
	}

	// cerr<<"\t\t: known states\n";
	// foreach(StateSet, known, ana.states)
	// {
	// 	cerr<<"\t\t"<<**known<<"\n";
	// }	

	if(debug)
	{
		ana.verbose = DBG_U | DBG_STATES;
	}

	ana.noU = true;
	ana.allowDeletions = true;

	ana.computeUs();

	//invalidate the beta table, set alpha=0 so only the input string
	//is allowed to occur as underlying form
	model._betaDate = 2;

	proc0->remove(outStr, rtab);
	model._alpha[0] = 0;
	model._alpha[1] = 0;

	foreach(intToProb, li, ana.localUs)
	{
		li->second = 0;
	}

	//fwd-back
	ana.forward();

	Prob alignPr = 0;
	{
		State* curr = ana.root;
		while(!curr->end())
		{
			curr = curr->path;
		}
		alignPr = expl(curr->prob);
	}

	if(debug)
	{
		cerr<<"\t\t: known states\n";
		foreach(StateSet, known, ana.states)
		{
			cerr<<"\t\t"<<**known<<"\n";
		}
	}
	ana.backward();

	SymbolString emptyStr(in.alphabet());
	ana.root->canonicalize(0, emptyStr);

	//update empiricals
	{
		State* curr = ana.root;
		while(!curr->end())
		{
			if((!curr->chars.empty() ||
				dynamic_cast<InsertState*>(curr))
			   && curr->index <= ana.surface.size())
			{
				ch->_phoneCounts[curr->lastChar()][curr->emitted()] += 1;
				if(curr->lastChar() == -1)
				{
					int prevCh = curr->chars.empty() ? 
						-1 : curr->chars[curr->chars.size() - 1];
					ch->_epsCounts[prevCh] += 1;
				}
			}

			curr = curr->path;
		}
	}

	//this code block useful for simulating what happens from a forced starting
	//state... purely debugging
	if(0 && debug)
	{
		for(int ii = 0; ii < 30; ++ii)
		{
			cerr<<"Debugging alignment "<<ii<<"\n";

			ana.decanonicalize();

			{
				StateSet keep;
				foreach(StateSet, state, ana.states)
				{
					(*state)->wipe();

					if((*state)->path != NULL || (*state)->end())
					{
						assert(dynamic_cast<MonkeyState*>(*state) == NULL);
						keep.insert(*state);
					}
					else
					{
						delete *state;
					}
				}

				ana.states = keep;

				cerr<<"\t\t: known states\n";
				foreach(StateSet, known, ana.states)
				{
					cerr<<"\t\t"<<**known<<"\n";
				}

				ana.root->prob = 0;
				ana.root->corr = 0;
			}

			ana.noU = false;
			ana.computeUs();
			ana.forward();
			ana.backward();

			SymbolString emptyStr(in.alphabet());
			ana.root->canonicalize(0, emptyStr);

			{
				StateSet keep;
				foreach(StateSet, state, ana.states)
				{
					(*state)->wipe();

					if((*state)->path != NULL || (*state)->end())
					{
						assert(dynamic_cast<MonkeyState*>(*state) == NULL);
						keep.insert(*state);
					}
					else
					{
						delete *state;
					}
				}

				ana.states = keep;
			}

			// debug print
			State* curr = ana.root;
			while(!curr->end())
			{
				cerr<<*curr<<"\n";
				curr = curr->path;
			}
		}

		cerr<<"Survived\n";
		assert(0);
	}

	//debug print
	// curr = ana.root;
	// while(!curr->end())
	// {
	// 	cerr<<*curr<<"\n";
	// 	curr = curr->path;
	// }

	if(learn)
	{
		ch->learnFrom(&ana);
	}

	//avoid destructor blowing this up, we'll need it
	model._channel = NULL;

	return alignPr;
}

ostream& operator<<(ostream& os, Analysis& ana)
{
	ana.print(os);
	return os;
}

ostream& operator<<(ostream& os, SegSampler& ss)
{
	ss.print(os);
	return os;
}
