#include "SymbolString.h"

SymbolString::SymbolString():
	_alphabet(NULL)
{
	//template constr. used in STL-- don't use
}

SymbolString::SymbolString(StrVocab& alphabet):
	_alphabet(&alphabet)
{
}

SymbolString::SymbolString(string& readFrom, StrVocab& alphabet):
	_alphabet(&alphabet)
{
	std::istringstream split(readFrom);
	char key;
	while(split>>key)
	{
		string keyStr;
		keyStr += key;
		int sym = _alphabet->get(keyStr, true);
		push_back(sym);
	}
}

SymbolString::SymbolString(istream& is, StrVocab& alphabet):
	_alphabet(&alphabet)
{
	read(is);
}

void SymbolString::print(ostream& os)
{
	if(!empty())
	{
		assert(_alphabet != NULL);
	}

	foreach(ints, ii, *this)
	{
		// if(*ii == -1)
		// {
		// 	os<<"-1";
		// }
		// else
		// {
		os<<_alphabet->inv(*ii);
		// }
	}
}

void SymbolString::write(ostream& os, int begin, int end)
{
	if(!empty())
	{
		assert(_alphabet != NULL);
	}

	foreach(ints, ii, *this)
	{
		os<<_alphabet->inv(*ii)<<" ";
	}

	if(begin == -1 && end == -1)
	{
		os<<"|| ";
	}
	else
	{
		os<<"|"<<begin<<"-"<<end<<"| ";
	}
}

void SymbolString::read(istream& is)
{
	string key;
	while(is>>key)
	{
		if(key[0] == '|')
		{
			break;
		}

		{
			int sym = _alphabet->get(key, true);
			push_back(sym);
		}
	}
}

StrVocab& SymbolString::alphabet()
{
	return *_alphabet;
}

void SymbolString::append(SymbolString& other)
{
	insert(end(), other.begin(), other.end());
}

void SymbolString::resize(int sz)
{
	ints::resize(sz);
}

void SymbolString::appendRange(SymbolString& other, int begin, int end)
{
	for(int ii = begin; ii < end; ++ii)
	{
		this->push_back(other[ii]);
	}
}

bool SymbolString::is(const string& str)
{
	std::istringstream split(str);
	string key;
	SymbolString::iterator pos = begin();
	while(split>>key)
	{
		if(pos == end())
		{
			return false;
		}

//		cerr<<"check "<<key<<" vs "<<_alphabet->inv(*pos)<<"\n";
		if(_alphabet->inv(*pos) != key)
		{
			return false;
		}
		++pos;
	}

	return (pos == end());
}

ostream& operator<<(ostream& os, SymbolString& si)
{
	si.print(os);
	return os;
}
