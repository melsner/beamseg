#include "common.h"
#include "SymbolString.h"
#include "LazySuffixTree.h"
#include "SuffixTree.h"

int main(int argc, char* argv[])
{
	appInit(DATA_PATH);

	StrVocab alpha;

	string fnordStr("F N O R D");
	SymbolString fnord(fnordStr, alpha);
	cout<<fnord<<"\n";
	cout<<fnord.size()<<" "<<fnord[0]<<" "<<alpha.inv(fnord[0])<<"\n";

	Vocab<SymbolString> lexicon;
	lexicon.get(fnord, true);

	cout<<"word from lexicon "<<lexicon.inv(0)<<"\n";

	string fordStr("F O R D");
	SymbolString ford(fordStr, alpha);

	string foreStr("F O R E");
	SymbolString fore(foreStr, alpha);

	string fStr("F");
	SymbolString f(fStr, alpha);

	string feStr("F E");
	SymbolString fe(feStr, alpha);

	SuffixTree* tr = new SuffixTree();

	tr->insert(fnord, 1.0);
	tr->insert(ford, 1.0);
	tr->insert(fore, 1.0);
	tr->insert(f, 1.0);
	tr->insert(fe, 1.0);

	tr->print(cout, alpha);

	cout<<"\n";

	LazySuffixTree* tr1 = new LazySuffixTree(0, &lexicon);

	tr1->insert(fnord, 1.0);
	tr1->insert(ford, 1.0);
	tr1->insert(fore, 1.0);
	tr1->insert(f, 1.0);
	tr1->insert(fe, 1.0);

	tr1->print(cout, alpha);
	cout<<"\n";

	foreach(intLazySuffixMap, si, tr1->successors())
	{
		si->second->successors();
		foreach(intLazySuffixMap, sj, si->second->successors())
		{
			sj->second->successors();
			foreach(intLazySuffixMap, sk, sj->second->successors())
			{
				sk->second->successors();
				foreach(intLazySuffixMap, sl, sk->second->successors())
				{
					sl->second->successors();
				}
			}
		}
	}
	tr1->print(cout, alpha);

	cout<<"\n";

}
