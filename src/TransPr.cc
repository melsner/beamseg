#include "common.h"
#include "SegSampler.h"
#include "Monkey.h"
#include "State.h"
#include "Phones.h"

#include <boost/program_options.hpp>

int main(int argc, char* argv[])
{
	appInit(DATA_PATH);

	namespace po = boost::program_options;

	po::options_description desc("Show transducer probabilities");
	desc.add_options()
		("help", "show options")
		("from", po::value<string>(),
		 "source word")
		("to", po::value<string>(),
		 "target word")
		("channel-file", po::value<string>(),
		 "file storing channel parameters")
		;

	po::positional_options_description p;

	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv).
			  options(desc).positional(p).run(), vm);
	po::notify(vm);

	if(vm.count("help"))
	{
		cout<<desc<<"\n";
		return 1;
	}

	string inputStr;
	if(!vm.count("from"))
	{
		cerr<<"We need a source word\n";
		return 1;
	}
	else
	{
		inputStr = vm["from"].as<string>();
	}

	string outputStr = "";
	if(!vm.count("to"))
	{
		cerr<<"Assuming source is target\n";
		outputStr = inputStr;
	}
	else
	{
		outputStr = vm["to"].as<string>();
	}

	StrVocab alpha;

	PhoneChannel ph(alpha);
	if(vm.count("channel-file"))
	{
		string channelFile = vm["channel-file"].as<string>();
		cerr<<"Reading channel parameters from "<<channelFile<<"\n";
		ifstream ifs(channelFile.c_str());
		assert(ifs.is_open());
		ph.read(ifs);
	}

	std::istringstream iss(inputStr);
	SymbolString input(iss, alpha);

	std::istringstream oss(outputStr);
	SymbolString output(oss, alpha);
	cout<<"Input: "<<input<<" output: "<<output<<"\n";
	input.write(cout);
	output.write(cout);
	cout<<"\n";
	forcedAlign(input, output, &ph);
}
