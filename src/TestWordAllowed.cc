#include "common.h"
#include "SegSampler.h"
#include "Monkey.h"
#include "Phones.h"

int main(int argc, char* argv[])
{
	appInit(DATA_PATH);

	StrVocab alpha;

	Monkey* simian = new Monkey(.3, false, .01, alpha);
	string empty("");
	SymbolString emptySym(empty, alpha);
	int start = simian->intern(emptySym);
	assert(start == 0);

	PhoneChannel* ph = new PhoneChannel(alpha);
	string channelFile("oracleChannel");
	ifstream ifs(channelFile.c_str());
	assert(ifs.is_open());
	ph->read(ifs);

	doubles a0(2, 2);
	doubles d0(2, 0);

	CRPLM model(2, a0, d0, simian, ph);

	string text("y uw w aa n t uw s iy ih t");
	SymbolString symbolic(alpha);
	{
		std::istringstream split(text);
		string key;
		while(split>>key)
		{
			symbolic.push_back(alpha.get(key, true));
		}
	}

	string text2("uw w aa");
	SymbolString symbolic2(alpha);
	{
		std::istringstream split(text2);
		string key;
		while(split>>key)
		{
			symbolic2.push_back(alpha.get(key, true));
		}
	}

	string text3("ah w aa");
	SymbolString symbolic3(alpha);
	{
		std::istringstream split(text3);
		string key;
		while(split>>key)
		{
			symbolic3.push_back(alpha.get(key, true));
		}
	}

	string text4("uw w aa r");
	SymbolString symbolic4(alpha);
	{
		std::istringstream split(text4);
		string key;
		while(split>>key)
		{
			symbolic4.push_back(alpha.get(key, true));
		}
	}

	string text5("aa y uw");
	SymbolString symbolic5(alpha);
	{
		std::istringstream split(text5);
		string key;
		while(split>>key)
		{
			symbolic5.push_back(alpha.get(key, true));
		}
	}

	cout<<symbolic<<"\n";

	Analysis ana(0, symbolic, &model);

	cout<<ana<<"\n";

	ana.verbose = DBG_U | DBG_STATES;

	ana.primeStates();

	ana.computeUs();

	int s2 = model.intern(symbolic2);
	cerr<<"s2 "<<symbolic2<<" allow "<<ana.wordAllowed(s2, 1)<<"\n";

	int s3 = model.intern(symbolic3);
	cerr<<"s3 "<<symbolic3<<" allow "<<ana.wordAllowed(s3, 2)<<"\n";

	int s4 = model.intern(symbolic4);
	cerr<<"s4 "<<symbolic4<<" allow "<<ana.wordAllowed(s4, 1)<<"\n";

	int s5 = model.intern(symbolic5);
	cerr<<"s5 "<<symbolic5<<" allow "<<ana.wordAllowed(s5, 0)<<"\n";

	cerr<<"ok\n";
}
