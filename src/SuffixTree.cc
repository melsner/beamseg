#include "SuffixTree.h"

SuffixTree::SuffixTree():
	_bestString(0),
	_final(0)
{
}

SuffixTree::~SuffixTree()
{
	foreach(intSuffixMap, nxt, _successors)
	{
		delete nxt->second;
	}
}

void SuffixTree::deleteChild(int ch)
{
	delete _successors[ch];
	_successors.erase(ch);
}

intSuffixMap& SuffixTree::successors()
{
	return _successors;
}

void SuffixTree::insert(SymbolString& str, Prob pr)
{
	insert1(str, 0, pr);
}

void SuffixTree::insert1(SymbolString& str, int pos, Prob pr)
{
	if(pr > _bestString)
	{
		_bestString = pr;
	}

	if(pos == str.size())
	{
		_final = pr;
		return;
	}

	int chr = str[pos];
	if(!inMap(_successors, chr))
	{
		_successors[chr] = new SuffixTree();
	}
	_successors[chr]->insert1(str, pos + 1, pr);
}

SuffixTree* SuffixTree::successor(int chr)
{
	return ifExists(_successors, chr, (SuffixTree*)NULL);
}

Prob SuffixTree::bestString()
{
	return _bestString;
}

Prob SuffixTree::final()
{
	return _final;
}

void SuffixTree::print(ostream& os, StrVocab& alphabet)
{
	print1(os, 0, alphabet);
}

// void SuffixTree::print1(ostream& os, int depth, StrVocab& alphabet)
// {
// 	if(final > 0)
// 	{
// 		os<<"*"<<final<<"*";
// 	}

// 	os<<"\n";

// 	foreach(intSuffixMap, succ, successors)
// 	{
// 		for(int ii = 0; ii < depth; ++ii)
// 		{
// 			os<<" ";
// 		}
// 		os<<alphabet.inv(succ->first);
// 		succ->second->print1(os, 1 + depth, alphabet);
// 	}
// }

void SuffixTree::print1(ostream& os, int depth, StrVocab& alphabet)
{
	if(_final > 0)
	{
		os<<"*"<<_final<<"* ";
	}

	foreach(intSuffixMap, succ, _successors)
	{
		os<<"("<<alphabet.inv(succ->first)<<" ";
		succ->second->print1(os, 1 + depth, alphabet);
		os<<")";
	}
}
