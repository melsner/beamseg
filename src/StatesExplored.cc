#include "common.h"
#include "SegSampler.h"
#include "Monkey.h"
#include "BigramMonkey.h"
#include "NGramMonkey.h"
#include "FiniteDist.h"
#include "State.h"
#include "AnnealRate.h"

#include <boost/program_options.hpp>

void addString(SegSampler& sampler, SymbolString& syms, ints& trueSplits,
			   bool cheat, bool singleChars, bool randomBreaks, 
			   double breakProb, bool lockBounds);

void addString(SegSampler& sampler, SymbolString& syms, ints& trueSplits,
			   bool cheat, bool singleChars, bool randomBreaks,
			   double breakProb, bool lockBounds)
{
	//when active, initializes at true segmentations
	sampler.add(syms);
	if(cheat)
	{
		foreach(ints, splitPt, trueSplits)
		{
			sampler.split(sampler.numSequences() - 1, *splitPt);
		}
	}
	else if(randomBreaks)
	{
		int seqSize = 
			sampler.sequence(sampler.numSequences() - 1).nSyms();
		for(int sym = 1; sym < seqSize - 1; ++sym)
		{
			if(gsl_rng_uniform(RNG) < breakProb)
			{
				sampler.split(sampler.numSequences() - 1, sym);
			}
		}
	}
	else if(singleChars)
	{
		int seqSize = 
			sampler.sequence(sampler.numSequences() - 1).nSyms();
		for(int sym = 1; sym < seqSize; ++sym)
		{
			sampler.split(sampler.numSequences() - 1, sym);
		}
	}

	if(lockBounds)
	{
		Analysis& ana = sampler.sequence(sampler.numSequences() - 1);
		foreach(intToSeg, endPos, ana.segments)
		{
			ana.allowedBounds.insert(endPos->first);
		}
		ana.lockBounds = true;
	}
}

int main(int argc, char* argv[])
{
	appInit(DATA_PATH);
	long int seed = 0;
	gsl_rng_set(RNG, seed);
	srand(seed);

	namespace po = boost::program_options;

	po::options_description desc("Beam sampling for segments/intended forms");
	desc.add_options()
		("help", "show options")
		("input-file", po::value<string>(),
		 "file to read input from")
		("init", po::value<string>(),
		 "initial breakpoints (true,random,none,single,underlying file)")
		("lock-bounds",
		 "do not resample word boundaries")
		("iters", po::value<int>(), "number of iterations")
		("grams", po::value<int>(), "ngram order")
		("alpha", po::value< std::vector<double> >(), "CRP alphas")
		("discount", po::value< std::vector<double> >(), "CRP discount")
		("break", po::value<double>(), "probability of word break")
		("output", po::value<string>(), 
		 "file stem to output to (creates output.learned.underlying, output.learned.surface)")
		("orig-temp", po::value<double>(),
		 "inverse temperature for annealing (use 1 for no annealing)")
		("brent-reader",
		 "reader for Brent format (one char per phone, space-delimited)")
		("base", po::value<string>(),
		 "base distribution (monkey,finite,bimonkey,dpmonkey)")
		("channel", po::value<string>(),
		 "channel type: noiseless, xsub, phone")
		("sample-hypers",
		 "sample hyperparameters")
		("sgd",
		 "learn channel parameters with SGD")
		("em",
		 "learn channel probabilities with EM")
		("write-channel", po::value<string>(),
		 "write final channel parameters to this file")		 
		("read-channel", po::value<string>(),
		 "read channel parameters from this file")
		;

	po::positional_options_description p;
	p.add("input-file", -1);

	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv).
			  options(desc).positional(p).run(), vm);
	po::notify(vm);

	if(vm.count("help"))
	{
		cout<<desc<<"\n";
		return 1;
	}

	int grams = 2;

	if(vm.count("grams"))
	{
		grams = vm["grams"].as<int>();
		assert(grams > 0);
		cerr<<"Selected "<<grams<<"-grams\n";
	}

	bool normalReader = true;
	double breakProb = .2;
	double pEnd = .001;
	doubles crpAlpha(grams, 0);
	crpAlpha[0] = 3000;
	crpAlpha[1] = 100; //dpseg old version had 300
	doubles crpDiscount(grams, 0);
	string output = "output";
	string baseType = "monkey";
	string channelType = "noiseless";
	bool cheat = false;
	bool lockBounds = false;
	bool randomBreaks = true;
	bool singleChars = false;
	string initUnderlying = "";
	int iters = 100;
	double origTemp = .3;
	bool sampleHypers = false;
	bool doSGD = false;
	bool doEM = false;
	string channelOut = "";

	if(vm.count("brent-reader"))
	{
		normalReader = false;
	}

	if(vm.count("break"))
	{
		breakProb = vm["break"].as<double>();
	}

	cerr<<"Break prob: "<<breakProb<<"\n";

	string input;
	if(!vm.count("input-file"))
	{
		cerr<<"Assuming input is "<<output<<".surface\n";
		input = output + ".surface";
	}
	else
	{
		input = vm["input-file"].as<string>();
	}

	if(vm.count("iters"))
	{
		iters = vm["iters"].as<int>();
	}

	if(vm.count("alpha"))
	{
		crpAlpha = vm["alpha"].as< std::vector<double> >();
	}

	for(int ii = 0; ii < crpAlpha.size(); ++ii)
	{
		cerr<<"Alpha: "<<ii<<" = "<<crpAlpha[ii]<<"\n";
	}		

	if(vm.count("discount"))
	{
		crpDiscount = vm["discount"].as< std::vector<double> >();
	}

	for(int ii = 0; ii < crpDiscount.size(); ++ii)
	{
		cerr<<"Discount: "<<ii<<" = "<<crpDiscount[ii]<<"\n";
	}

	if(vm.count("output"))
	{
		output = vm["output"].as<string>();
	}

	if(vm.count("channel"))
	{
		channelType = vm["channel"].as<string>();
	}

	if(vm.count("orig-temp"))
	{
		origTemp = vm["orig-temp"].as<double>();
	}

	if(vm.count("sample-hypers"))
	{
		sampleHypers = true;
		cerr<<"Sampling hyperparameters\n";
	}

	if(vm.count("init"))
	{
		string init = vm["init"].as<string>();
		cerr<<"Initialization: "<<init<<"\n";
		if(init == "true")
		{
			cheat = true;
		}
		else if(init == "random")
		{
			randomBreaks = true;
		}
		else if(init == "single")
		{
			singleChars = true;
			randomBreaks = false;
		}
		else if(init == "none")
		{
			randomBreaks = false;
		}
		else
		{
			randomBreaks = false;
			initUnderlying = init;
			// input = initUnderlying + ".surface";
			// cerr<<"Using "<<input<<" as input file.\n";

			// initUnderlying = initUnderlying + ".underlying";
			cerr<<"Using "<<initUnderlying<<" as initialization.\n";

			cheat = true;
		}
	}

	if(vm.count("lock-bounds"))
	{
		cerr<<"Locking word boundaries\n";
		lockBounds = true;
	}

	cerr<<"Break prob: "<<breakProb<<"\n";
	cerr<<"End prob: "<<pEnd<<"\n";

	if(vm.count("base"))
	{
		baseType = vm["base"].as<string>();
	}

	cerr<<"Base distribution: "<<baseType<<"\n";

	StrVocab alpha;

	int baseGrams = 2;
	doubles baseAlphas(baseGrams, 0);
	baseAlphas[0] = 1000;
	baseAlphas[1] = 500;
	doubles baseDiscounts(baseGrams, 0);

	Monkey* simian = NULL;
	if(baseType == "monkey")
	{
		simian = new Monkey(breakProb, false, pEnd, alpha);
	}
	else if(baseType == "bimonkey")
	{
		simian = new BigramMonkey(10, pEnd, alpha);
	}
	else if(baseType == "dpmonkey")
	{
		cerr<<"DP Monkey: grams "<<baseGrams<<" alphas ";
		writeVector(baseAlphas, cerr);
		cerr<<" discounts ";
		writeVector(baseDiscounts, cerr);

		simian = new NGramMonkey(baseGrams, baseAlphas, baseDiscounts,
								 false, pEnd, alpha);
	}
	else if(baseType == "finite")
	{
		double dummy = 0;
		simian = new FiniteDist(dummy, pEnd, alpha);

		string empty("");
		SymbolString emptySym(empty, alpha);
		int start = simian->intern(emptySym);
		assert(start == 0);
		dynamic_cast<FiniteDist*>(simian)->addPriorCount(0, 1);

		ifstream fixedV("fixed.vocab");
		assert(fixedV.is_open());
		SymbolString* word;
		while(fixedV)
		{
			word = new SymbolString(fixedV, alpha);
			if(word->empty())
			{
				delete word;
				break;
			}

			// cerr<<"word "<<*word<<"\n";
			int code = simian->intern(*word);
			dynamic_cast<FiniteDist*>(simian)->addPriorCount(code, 1000);
			delete word;
		}
	}
	else
	{
		cerr<<"Unknown base distribution "<<baseType<<"\n";
		return 1;
	}

	cerr<<"Channel type: "<<channelType<<"\n";

	Channel* channel = NULL;
	if(channelType == "noiseless")
	{
		channel = new Channel();
	}
	else if(channelType == "xsub")
	{
		channel = new XChannel(.1, alpha.get("x", true));
	}
	else if(channelType == "phone")
	{
		PhoneChannel* ph = new PhoneChannel(alpha);
		if(vm.count("read-channel"))
		{
			string channelFile = vm["read-channel"].as<string>();
			cerr<<"Reading channel parameters from "<<channelFile<<"\n";
			ifstream ifs(channelFile.c_str());
			assert(ifs.is_open());
			ph->read(ifs);
		}
		else
		{
			cerr<<"Requires a channel parameter file\n";
			return 1;
		}
		channel = ph;
	}
	else
	{
		cerr<<"Unknown channel type "<<channelType<<"\n";
		return 1;
	}

	if(vm.count("sgd"))
	{
		doSGD = true;
		if(vm.count("write-channel"))
		{
			channelOut = vm["write-channel"].as<string>();
			cerr<<"Writing channel parameters to "<<channelOut<<"\n";
		}
	}

	if(vm.count("em"))
	{
		doEM = true;
	}

	string empty("");
	SymbolString emptySym(empty, alpha);
	int start = simian->intern(emptySym);
	assert(start == 0);

	CRPLM model(grams, crpAlpha, crpDiscount, simian, channel);
	SegSampler sampler(model);

	ifstream ff(input.c_str());
	if(!ff.is_open())
	{
		cerr<<"Can't open input file '"<<input<<"'\n";
		return 1;
	}

	std::vector<SymbolString> allStrings;
	intMat allTrueSplits;

	while(ff)
	{
		string line;
		getline(ff, line);
		if(line.empty())
		{
			break;
		}

		std::istringstream split(line);

		SymbolString si(alpha);
		ints trueSplits;
		int curr = 0;

		while(split)
		{
			SymbolString* word;
			if(normalReader)
			{
				word = new SymbolString(split, alpha);
			}
			else
			{
				string key;
				split>>key;
				if(key == "||")
				{
					cerr<<"*WARNING* Is your reader setting wrong?\n";
					return 1;
				}
				word = new SymbolString(key, alpha);
			}

			if(word->empty())
			{
				delete word;
				break;
			}

			curr += word->size();
			trueSplits.push_back(curr);
			si.append(*word);
			delete word;
		}

		cerr<<"Read "<<si<<"\n";
		allStrings.push_back(si);

		//learn bigram character lm on surface strings
		//in a way that makes Frank mad
		if(dynamic_cast<BigramMonkey*>(simian) != NULL)
		{
			dynamic_cast<BigramMonkey*>(simian)->addStr(si);
		}

		trueSplits.pop_back(); //last 'split' point is end of last word
		allTrueSplits.push_back(trueSplits);
	}

	for(int si = 0; si < allStrings.size(); ++si)
	{
		addString(sampler, allStrings[si], allTrueSplits[si],
				  cheat, singleChars, randomBreaks, breakProb, lockBounds);
	}

	int nSeqs = sampler.numSequences();
	for(int seq = 0; seq < nSeqs; ++seq)
	{
		sampler.sequence(seq).primeStates();
	}

	foreach(strIntMap, ch, alpha._vocab)
	{
		if(ch->first.size() > 3 && ch->first != "<epsilon>")
		{
			cerr<<"Very long symbol detected: "<<ch->first<<"\n";
			cerr<<"*WARNING* Did you get your reader setting wrong?\n";
			return 1;
		}
	}

	cerr<<"Alphabet size: "<<alpha.size()<<"\n";
	cerr<<"Base distribution:\n"<<*simian<<"\n";

	AnnealRate* rate = NULL;
	AnnealRate* channelRate = NULL;
	AnnealRate* learningRate = NULL;
	if(origTemp == 1.0)
	{
		rate = new LinearRate(1.0, 1.0, 3);
		channelRate = new AnnealRate(.3);
		learningRate = new AnnealRate(0);
	}
	else
	{
		//useful
		// rate = new LinearRate(origTemp, 1.0, iters);
		// channelRate = new AnnealRate(1.0);

		//rate = new InvRate(new ExponentialRate(1.0/origTemp, 1.0, iters));

		// channelRate = new PiecewiseRate(
		// 	new InvRate(
		// 		new LinearRate(origTemp, 1./.5, 1./2 * iters)),
		// 	new LinearRate(.5, 1.0, 1./2 * iters));

		//useful
		// channelRate = new PiecewiseRate(
		// 	new InvRate(
		// 		new ExponentialRate(origTemp, 1./.5, 1./2 * iters)),
		// 	new LinearRate(.5, 1.0, 1./2 * iters));

		// channelRate = new PiecewiseRate(
		// 	new LinearRate(1/origTemp, 1/origTemp, 1./2 * iters),
		// 	new PiecewiseRate(
		// 		new LinearRate(.5, .5, 1./4 * iters),
		// 		new LinearRate(.5, 1.0, 1./4 * iters)));

		StepwiseRate* sr = new StepwiseRate();
		int interval = 200;
		int moment = 50;
		sr->add( new LinearRate(.3, 1, 700));
		//the 'high' rate here is usually 4, now 2
		// sr->add( new LinearRate(.3, .7, 500) );
		// // sr->add( new LinearRate(4, 4, moment) );
		// sr->add( new LinearRate(.7, 1, interval) );
		// sr->add( new LinearRate(4, 4, moment) );
		// sr->add( new LinearRate(1, 1, interval) );
		rate = sr;

		StepwiseRate* cr = new StepwiseRate();
		cr->add(new LinearRate(-3, .3, 700));

		//I think this one is the one that gets ok results
		// cr->add( new LinearRate(-3, .3, 3 * moment + interval));
		// cr->add( new LinearRate(-3, .3, moment + interval));

		// cr->add(new LinearRate(-3, .3, 500));
		// // cr->add( new LinearRate(-3, .3, 3 * moment + interval));
		// cr->add( new LinearRate(-3, .3, moment + interval));

		// cr->add(new LinearRate(-3, .5, 400));
		// cr->add(new LinearRate(.5, .5, 5));
		// cr->add(new LinearRate(.2, .2, 95));
		// cr->add( new LinearRate(-3, .3, moment + interval));

		channelRate = cr;

		// cr->add( new InvRate(
		// 			 new ExponentialRate(1./3, 1, 500)));
		// cr->add( new LinearRate(.7, .7, 3 * moment + interval));

		if(doSGD || doEM)
		{
			StepwiseRate* lr = new StepwiseRate();

			lr->add( new LinearRate(0, 0, 400));
			lr->add( new LinearRate(.0001, .0001, 50));

			// lr->add( new LinearRate(0, 0, 25));
			// lr->add( new LinearRate(.005, .005, 25));

			lr->add( new LinearRate(0, 0, interval + 2 * moment));

			learningRate = lr;
		}
		else
		{
			learningRate = new AnnealRate(0);
		}
	}

	cerr<<"Starting temperature: "<<origTemp<<"\n";
	sampler.printLexicon(cerr);

	{
		double invTemp = rate->next();
		double channelTemp = channelRate->next();
		double learnRate = learningRate->next();

		cerr<<"..."<<", inverse temp "<<invTemp<<", channel temp "<<
			channelTemp<<" ll "<<model.ll()<<"\n";
		sampler.summarize(cerr);

		int seq = 0;
		{
			Analysis& ana = sampler.sequence(seq);
			ana.invTemp = invTemp;
			ana.channelInvTemp = channelTemp;
			cerr<<":"<<seq<<" "<<ana;

			model._betaDate++;
			cerr<<"MH ";

			{
				ana.integrateOut = true;
				// ana.noU = true;
				// ana.verbose = DBG_STATES;

				Prob postOld;
				ints oldSeq;
				{
					Segment* curr = ana.segAt(0);
					while(!curr->isStartOrEnd())
					{
						oldSeq.push_back(curr->strSym);
						model._rawCounts[curr->strSym] += 10;
						curr = ana.nextSegment(*curr);
					}

					postOld = model.removeSeq(oldSeq, invTemp, 
											  ana.verbose & DBG_MHR);

					//so have to correct it here
					postOld *= invTemp;

					//compute the channel costs
					postOld += ana.root->channelCost();
				}
				model._rawCounts[0] += 10;

				intSet betasWithNoCounts;
				ana.handleLengthAlterations(betasWithNoCounts);

				ana.decanonicalize();

				//record old state seq in case we need it
				States ptrs;
				{
					State* curr = ana.root;
					while(!curr->end())
					{
						ptrs.push_back(curr->path);
						curr = curr->path;
					}
				}

				ana.unkTrees.resize(ana.surface.size() + 1);

				ana.computeUs();
				ana.forward();
			}

			cerr<<"\t"<<ana<<"\n";
			cout<<"Explored: "<<ana.states.size()<<" states\n";

			// cerr<<"\t\t: known states\n";
			// foreach(StateSet, known, ana.states)
			// {
			// 	cerr<<"\t\t"<<**known<<"\n";
			// }

		}
	}
}
