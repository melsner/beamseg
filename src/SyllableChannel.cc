#include "SyllableChannel.h"

SyllableChannel::SyllableChannel(StrVocab& alph, StrVocab& inner):
	PhoneChannel(alph),
	sylAlphabet(alph),
	innerAlph(inner),
	base(new PhoneChannel(inner))
{
	alphabet.clear();
}

SyllableChannel::~SyllableChannel()
{
	delete base;
}

void SyllableChannel::computeEps(int phPrev)
{
	int lastPh;
	if(phPrev == -1)
	{
		lastPh = -1;
	}
	else
	{
		SymbolString segments(innerAlph);
		splitPhones(phPrev, segments);
		lastPh = segments[segments.size() - 1];
	}

	prEps[phPrev] = base->epsilonAfter(lastPh);
}

void SyllableChannel::computeProbs(int phGiven)
{
	intProbMap& prTab = prGiven[phGiven];
	SymbolString segsIn(innerAlph);

	if(phGiven == -1)
	{
		foreach(strIntMap, ch, sylAlphabet._vocab)
		{
			SymbolString segsOut(innerAlph);
			splitPhones(ch->second, segsOut);
		
			Prob res = 1.0;
			foreach(SymbolString, si, segsOut)
			{
				res *= base->epsilonAfter(-1);
				res *= base->emitCh(-1, *si);
			}

			prTab[ch->second] = res;
		}

		return;
	}

	splitPhones(phGiven, segsIn);

	cerr<<"Aligning the syllable set for "<<
		(phGiven == -1 ? "-1" : sylAlphabet.inv(phGiven))<<"...";

	foreach(strIntMap, ch, sylAlphabet._vocab)
	{
		SymbolString segsOut(innerAlph);
		splitPhones(ch->second, segsOut);

		int phDiff = editDist(segsIn, segsOut);
		if(phDiff > 2)
		{
			prTab[ch->second] = 0;
		}
		else
		{
			// cerr<<"Aligning "<<segsIn<<" "<<segsOut<<"\n";
			Prob pr = forcedAlign(segsIn, segsOut, base, false);
			// cerr<<"\t\t==> "<<pr<<"\n";
			prTab[ch->second] = pr;
		}
	}

	cerr<<"...done\n";
}

void SyllableChannel::splitPhones(int sym, SymbolString& res)
{
	string& syl = sylAlphabet.inv(sym);
	int find = 0;
	int nn = syl.size();

	// cerr<<"SPLITTING "<<syl<<"\n";

	while(find <= nn)
	{
		int next = syl.find(".", find);
		if(next == string::npos)
		{
			next = nn;
		}
		string chars(syl, find, next - find);
		// cerr<<"INTO "<<chars<<"\n";
		int chSym = innerAlph.get(chars, true);
		res.push_back(chSym);

		find = next + 1;
	}
}

void SyllableChannel::learnFrom(Analysis* ana)
{
	assert(0);
}

void SyllableChannel::print(ostream& os)
{
	foreach(intIntProbMap, probe, prGiven)
	{
		IntPriVec prs;
		foreach(intProbMap, emit, probe->second)
		{
			prs.push_back(IntPriNode(emitCh(probe->first, emit->first),
									 emit->first));
		}
		sort(prs.begin(), prs.end(), IntGreater());

		if(probe->first != -1)
		{
			os<<sylAlphabet.inv(probe->first);
		}
		else
		{
			os<<"-1";
		}
		os<<":  ";
		os<<"("<<epsilonAfter(probe->first)<<") ";

		double tot = 0;
		foreach(IntPriVec, pi, prs)
		{
			if(pi->data != -1)
			{
				os<<sylAlphabet.inv(pi->data);
			}
			else
			{
				os<<"-1";
			}
			os<<" "<<pi->pri<<"   ";
			tot += pi->pri;
			if(tot > .95)
			{
				break;
			}
		}
		os<<"\n";
	}
}

void SyllableChannel::read(istream& is)
{
	base->read(is);
}

void SyllableChannel::write(ostream& os)
{
	base->write(os);
}

int SyllableChannel::editDist(SymbolString& si, SymbolString& sj)
{
	//god am I really coding this again? I can't remember
	//where the last version of it in this program went
	int n = si.size();
	int m = sj.size();
	int chart[n + 1][m + 1];
	for(int ii = 0; ii <= n; ++ii)
	{
		for(int jj = 0; jj <= m; ++jj)
		{
			chart[ii][jj] = 500;
		}
	}

	chart[0][0] = 0;

	for(int ii = 0; ii <= n; ++ii)
	{
		for(int jj = 0; jj <= m; ++jj)
		{
			if(ii == 0 && jj == 0)
			{
				continue;
			}

			{
				int prev;
				//copy
				if(ii == 0 || jj == 0)
				{
					prev = -1;
				}
				else
				{
					prev = chart[ii - 1][jj - 1];
				}

				if(prev != -1 && si[ii - 1] == sj[jj - 1])
				{
					if(prev < chart[ii][jj])
					{
						chart[ii][jj] = prev;
					}
				}
			}

			{
				int prev;
				//ins
				if(ii == 0)
				{
					prev = -1;
				}
				else
				{
					prev = chart[ii - 1][jj];
				}

				// cerr<<"Fillin "<<ii<<" "<<jj<<" pr "<<prev<<"\n";

				if(prev != -1 && prev + 1 < chart[ii][jj])
				{
					chart[ii][jj] = prev + 1;
				}
			}

			{
				int prev;
				//del
				if(jj == 0)
				{
					prev = -1;
				}
				else
				{
					prev = chart[ii][jj - 1];
				}

				// cerr<<"Fillin2 "<<ii<<" "<<jj<<" pr "<<prev<<"\n";

				if(prev != -1 && prev + 1 < chart[ii][jj])
				{
					chart[ii][jj] = prev + 1;
				}
			}
		}
	}

	// cerr<<"Edit dist "<<si<<" and "<<sj<<" is "<<chart[n][m]<<"\n";

	// for(int ii = 0; ii <= n; ++ii)
	// {
	// 	for(int jj = 0; jj <= m; ++jj)
	// 	{
	// 		cerr<<chart[ii][jj]<<" ";
	// 	}
	// 	cerr<<"\n";
	// }

	return chart[n][m];
}
