#include "common.h"
#include "SegSampler.h"
#include "Monkey.h"
#include "NGramMonkey.h"
#include "Channel.h"

#include <boost/program_options.hpp>

int main(int argc, char* argv[])
{
	appInit(DATA_PATH);

	namespace po = boost::program_options;

	po::options_description desc("Generate text");
	desc.add_options()
		("help", "show options")
		("grams", po::value<int>(), "ngram order")
		("break", po::value<double>(), "probability of word break")
		("alpha", po::value< std::vector<double> >(), "CRP alphas")
		("discount", po::value< std::vector<double> >(), "CRP discount")
		("init", po::value<string>(),
		 "initial breakpoints (true,random,none,underlying file)")
		("iters", po::value<int>(), "number of iterations")
		("output", po::value<string>(), 
		 "file stem to output to (creates output.learned.underlying, output.learned.surface)")
		("input-file", po::value<string>(),
		 "file to read input from")
		("channel", po::value<string>(),
		 "channel type: noiseless")
		("spaces", "input delimited by spaces")
		("start-syms", po::value<double>(), 
		 "temperature to start sampling underlying forms")
		("no-breaks", "don't sample breakpoints")
		("base", po::value<string>(),
		 "base distribution (monkey,nmonkey)")
		;

	po::positional_options_description p;
	p.add("input-file", -1);

	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv).
			  options(desc).positional(p).run(), vm);
	po::notify(vm);

	if(vm.count("help"))
	{
		cout<<desc<<"\n";
		return 1;
	}

	int grams = 2;

	if(vm.count("grams"))
	{
		grams = vm["grams"].as<int>();
		assert(grams > 0);
		cerr<<"Selected "<<grams<<"-grams\n";
	}

	bool normalReader = true;
	double breakProb = .2;
	doubles crpAlpha(grams, 4);
	doubles crpDiscount(grams, 0);
	string output = "output";
	string channelType = "noiseless";
	string baseType = "monkey";
	bool cheat = false;
	bool randomBreaks = true;
	string initUnderlying = "";
	double startSyms = .5;
	int iters = 20000;
	bool doBreaks = true;

	if(vm.count("break"))
	{
		breakProb = vm["break"].as<double>();
	}

	cerr<<"Break prob: "<<breakProb<<"\n";

	if(vm.count("spaces"))
	{
		normalReader = false;
	}

	if(vm.count("alpha"))
	{
		crpAlpha = vm["alpha"].as< std::vector<double> >();
	}

	for(int ii = 0; ii < crpAlpha.size(); ++ii)
	{
		cerr<<"Alpha: "<<ii<<" = "<<crpAlpha[ii]<<"\n";
	}		

	if(vm.count("discount"))
	{
		crpDiscount = vm["discount"].as< std::vector<double> >();
	}

	for(int ii = 0; ii < crpDiscount.size(); ++ii)
	{
		cerr<<"Discount: "<<ii<<" = "<<crpDiscount[ii]<<"\n";
	}		

	if(vm.count("no-breaks"))
	{
		doBreaks = false;
	}

	if(vm.count("output"))
	{
		output = vm["output"].as<string>();
	}

	if(vm.count("channel"))
	{
		channelType = vm["channel"].as<string>();
	}

	if(vm.count("iters"))
	{
		iters = vm["iters"].as<int>();
	}

	if(vm.count("start-syms"))
	{
		startSyms = vm["start-syms"].as<double>();
		startSyms -= .01;
	}

	if(vm.count("base"))
	{
		baseType = vm["base"].as<string>();
	}

	cerr<<"Base distribution: "<<baseType<<"\n";

	StrVocab alpha;
	Monkey* simian = NULL;
	if(baseType == "monkey")
	{
		simian = new Monkey(breakProb, false, alpha);
	}
	else if(baseType == "nmonkey")
	{
		int monkeyN = 2;

		doubles mAlphas(monkeyN, 1);
		doubles mDiscs(monkeyN, 0);
		simian = new NGramMonkey(monkeyN, mAlphas, mDiscs, false, alpha);
	}
	else
	{
		cerr<<"Unknown base distribution "<<baseType<<"\n";
		return 1;
	}

	string empty("");
	alpha.get(empty, true); // add empty sym
	SymbolString emptySym(empty, alpha);
	int start = simian->intern(emptySym);
	assert(start == 0);

	Channel* channel = NULL;
	if(channelType == "noiseless")
	{
		channel = new NoiselessChannel();
		//don't bother trying to find underlying forms
		startSyms = 2;
	}
	else
	{
		cerr<<"Unknown channel type "<<channelType<<"\n";
		return 1;
	}

	CRPLM model(grams, crpAlpha, crpDiscount, simian, channel);
	SegSampler sampler(model);

	string input;
	if(!vm.count("input-file"))
	{
		cerr<<"Assuming input is "<<output<<".surface\n";
		input = output + ".surface";
	}
	else
	{
		input = vm["input-file"].as<string>();
	}

	if(vm.count("init"))
	{
		string init = vm["init"].as<string>();
		cerr<<"Initialization: "<<init<<"\n";
		if(init == "true")
		{
			cheat = true;
		}
		else if(init == "random")
		{
			randomBreaks = true;
		}
		else if(init == "none")
		{
			randomBreaks = false;
		}
		else
		{
			randomBreaks = false;
			initUnderlying = init;
			input = initUnderlying + ".surface";
			cerr<<"Using "<<input<<" as input file.\n";

			initUnderlying = initUnderlying + ".underlying";
			cerr<<"Using "<<initUnderlying<<" as initialization.\n";

			cheat = true;
		}
	}

	ifstream ff(input.c_str());
	if(!ff.is_open())
	{
		cerr<<"Can't open input file '"<<input<<"'\n";
		return 1;
	}

	ifstream* underlyingF = NULL;
	if(initUnderlying != "")
	{
		underlyingF = new ifstream(initUnderlying.c_str());
		if(!underlyingF->is_open())
		{
			cerr<<"Can't open underlying forms file '"<<initUnderlying<<"'\n";
			return 1;
		}
	}

	while(ff)
	{
		string line;
		getline(ff, line);
		if(line.empty())
		{
			break;
		}

		std::istringstream split(line);

		SymbolString si(alpha);
		ints trueSplits;
		int curr = 0;

		while(split)
		{
			SymbolString* word;
			if(normalReader)
			{
				word = new SymbolString(split, alpha);
			}
			else
			{
				string key;
				split>>key;
				if(key == "||")
				{
					cerr<<"*WARNING* Is your reader setting wrong?\n";
					return 1;
				}
				word = new SymbolString(key, alpha);
			}

			if(word->empty())
			{
				break;
			}

			curr += word->size();
			trueSplits.push_back(curr);
			si.append(*word);
			delete word;
		}

		cerr<<"Read "<<si<<"\n";
		sampler.add(si);

		trueSplits.pop_back(); //last 'split' point is end of last word

		//when active, initializes at true segmentations
		if(cheat)
		{
			foreach(ints, splitPt, trueSplits)
			{
				sampler.split(sampler.numSequences() - 1, *splitPt);
			}
		}
		else if(randomBreaks)
		{
			int seqSize = 
				sampler.sequence(sampler.numSequences() - 1).nSyms();
			for(int sym = 1; sym < seqSize - 1; ++sym)
			{
				if(gsl_rng_uniform(RNG) < breakProb)
				{
					sampler.split(sampler.numSequences() - 1, sym);
				}
			}
		}

		if(underlyingF)
		{
			string line;
			getline(*underlyingF, line);
			if(line.empty())
			{
				cerr<<"Unexpected end of underlying forms file.\n";
				return 1;
			}

			std::istringstream split(line);

			Analysis* ana = &sampler.sequence(sampler.numSequences() - 1);
			intToSeg::iterator nextSeg = ana->segments.begin();
			++nextSeg; //begin marker

// 			cerr<<*ana<<"\n";
			while(split)
			{
				SymbolString* word;
				word = new SymbolString(split, alpha);

				if(word->empty())
				{
					break;
				}

// 				cerr<<*word<<" "<<nextSeg->second->str<<"\n";

				sampler._model->removeSeg(ana, nextSeg->second, false, 1);
				nextSeg->second->str = *word;
				int prevWord = nextSeg->second->strSym;
				int newWord = sampler._model->intern(*word);
				nextSeg->second->strSym = newWord;

				SiteDesc desc(ana->index, nextSeg->second->begin);
				Site site(ana, nextSeg->second);

// 				cerr<<sampler._model->repr(prevWord)<<"\n";
// 				foreach(SiteMap, si, sampler._wordSites[prevWord])
// 				{
// 					SiteDesc di = si->first;
// 					cerr<<di.first<<" aa "<<ana->index<<" , "<<
// 						di.second<<" ss "<<nextSeg->first<<"\n";
// 					cerr<<nextSeg->second->begin<<"\n";
// 				}

				sampler._wordSites[prevWord].erase(desc);
				sampler._wordSites[newWord][desc] = site;

				sampler._model->updateSeg(ana, nextSeg->second, false, 1);
				++nextSeg;
//				cerr<<*ana<<"\n";
			}
		}
	}

	foreach(strIntMap, ch, alpha._vocab)
	{
		if(ch->first.size() > 3)
		{
			cerr<<"Very long symbol detected: "<<ch->first<<"\n";
			cerr<<"*WARNING* Did you get your reader setting wrong?\n";
			return 1;
		}
	}

	cerr<<"Alphabet size: "<<alpha.size()<<"\n";
	cerr<<"Base distribution:\n"<<*simian<<"\n";

	int nSeqs = sampler.numSequences();
	double temp = 0;

	for(int ii = 0; ii < iters; ++ii)
	{
		if(ii % (iters/10) == 0)
		{
			if(temp < 1)
			{
				temp += .1;
				channel->setTemp(temp);
			}

			//very strange that I need this check...
			if(temp > 1)
			{
				temp = 1;
				channel->setTemp(temp);
			}
		}

		if(ii % 500 == 0)
		{
			cerr<<"\n";
			cerr<<sampler;
			cerr<<"... Iter "<<ii<<", temp "<<temp<<"\n";
		}

		if(ii % 100 == 0 && temp >= startSyms)
		{
			cerr<<"+";
		}
		else
		{
			cerr<<".";
		}

		if(ii % 100 == 0)
		{
			sampler.reapVocab();
			sampler.sampleHypers(false, temp);
		}

		//point Gibbs, in order, for each breakpoint
		if(doBreaks)
		{
			for(int seq = 0; seq < nSeqs; ++seq)
			{
				int seqSize = sampler.sequence(seq).nSyms();
//			cerr<<"Sampling breaks: "<<sampler.sequence(seq)<<"\n";
				for(int sym = 1; sym < seqSize - 1; ++sym)
				{
					sampler.sampleAt(seq, sym, temp, false);
				}
//			cerr<<"Done breaks: "<<sampler.sequence(seq)<<"\n";
			}
		}
	}

	cerr<<"\n***************\n\n";
	cerr<<sampler<<"\n";
	sampler.writeSeqs(cout);

	string underName = output + ".learned.underlying";
	ofstream underlying(underName.c_str());

	string surfName = output + ".learned.surface";
	ofstream surface(surfName.c_str());

	if(cheat)
	{
		underlying<<"CHEATING\n";
	}

	sampler.writeUnderlying(underlying);
	sampler.writeSurface(surface);
	sampler.summarize(cerr);
}
