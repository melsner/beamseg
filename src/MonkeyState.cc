#include "MonkeyState.h"
#include "SegSampler.h"
#include "BigramMonkey.h"

#include <climits>

MonkeyState::MonkeyState(int ind, Prob stp, Analysis& ana):
	State(ind, 0, ana),
	_stop(stp)
{
	//note: stops comparator merging this state with real state
	prevWord = INT_MAX;
}

MonkeyState::~MonkeyState()
{
}

void MonkeyState::print(ostream& os)
{
	os<<"MONKEY"<<"|"<<"..."<<chars<<" ";
	os<<prob<<" "<<corr<<" ";
	os<<"st:"<<index<<" ";
}	

int MonkeyState::sortType() const
{
	return chars.empty() ? SORT_MONKEY : SORT_EMIT;
}

bool MonkeyState::newWord() const
{
	return false;
}

Prob MonkeyState::transProb(int ch)
{
	Prob psym = (1.0 / chars.alphabet().size());
	if(chars.empty())
	{
		return psym;
	}
	else
	{
		return (1 - _stop) * psym;
	}
}

void MonkeyState::expand()
{
	bool verbose = ana.verbose & DBG_STATES;

	if(!isfinite(prob))
	{
		cerr<<"Failed monkey state "<<*this<<" "<<this<<"\n";

		assert(0);
		return;
	}

	if(chars.size() > 2) //bigram monkeys allowed an extra char
	{
		cerr<<"Monkey state with too much context "<<*this<<"\n";
		assert(0);
	}

	//always do a u reset
	{
		Prob newU = ana.getULimit(this, NULL);
		setU(newU, NULL);

		//altered XXX was prob
		if(corr < uu)
		{
			if(verbose)
			{
				cerr<<*this<<" is out of the beam\n";
			}
			return;
		}
	}

	// have to check that we don't go off the end!
	if(index < ana.surface.size() &&
	   (!ana.lockBounds || chars.empty() || !ana.boundaryAt(index)))
	{
		int currCh = chars.empty() ? -1 : chars[chars.size() - 1];

		foreach(strIntMap, ch, chars.alphabet()._vocab)
		{
			//return a monkey successor
			State* nextMonkey = ana.getMonkey(index + 1, currCh, 
											  ch->second);
			Prob localU = ana.getLocalU(nextMonkey);
			Prob emitProb = nextMonkey->emit();
			Prob transP = transProb(ch->second);
			if(emitProb > localU)
			{
				Prob uCorr = ana.pLocalU(index + 1, emitProb);

				// emitProb = powl(emitProb, ana.channelInvTemp);
				// uCorr = powl(uCorr, ana.channelInvTemp);
				Prob pr = emitProb * powl(transP, ana.invTemp);
				nextMonkey = ana.registerState(nextMonkey);
				linkTo(nextMonkey, pr, uCorr);
			}
			else
			{
				// cerr<<"fail monkey "<<emitProb<<" "<<"\n";
				delete nextMonkey;
			}
		}
	}

	//recognize the next word
	if(!chars.empty() &&
	   (!ana.lockBounds || ana.boundaryAt(index)))
	{
		if(ana.lockBounds && verbose)
		{
			cerr<<"Locked bounds and boundary permitted at "<<index<<"\n";
		}

		//always recognize the next word as UNK
		//note that G0 sometimes does generate a known word...
		//but usually with much lower probability than the adaptors
		State* recog = ana.getState(index, -1, chars, 0);
		recog = ana.registerState(recog);
		Prob pr = stop() * (1 - ana.model->_prior->_pEnd);
		linkTo(recog, pr, 1.0);
	}
}

State* MonkeyState::canonicalize(int prev, SymbolString& currStr)
{
	assert(path != NULL);

	// cerr<<"canonicalize "<<*this<<"\n";

	if(chars.empty())
	{
		State* nxt = path->canonicalize(prev, currStr);
		path = NULL;
		return nxt;
	}
	else
	{
		currStr.push_back(chars[chars.size() - 1]);
		State* replaceMe = ana.getState(index, prev,
										currStr, currStr.size());
		replaceMe = ana.registerState(replaceMe);

		if(path->newWord())
		{
			int recogWord = ana.model->intern(currStr);
			SymbolString emptyStr(currStr.alphabet());
			replaceMe->path = path->canonicalize(recogWord, emptyStr);
		}
		else
		{
			replaceMe->path = path->canonicalize(prev, currStr);
		}

		path = NULL;
		return replaceMe;
	}
}

Prob MonkeyState::stop()
{
	return _stop;
}

BiMonkeyState::BiMonkeyState(int ind, int prevCh, int addCh, Analysis& ana):
	MonkeyState(ind, 0, ana)
{
	if(prevCh != -1 || addCh != -1)
	{
		chars.resize(2);
		chars[0] = prevCh;
		chars[1] = addCh;
	}
}

BiMonkeyState::~BiMonkeyState()
{
}

Prob BiMonkeyState::transProb(int ch)
{
	int prevCh = chars.empty() ? -1 : chars[1];
	Prob res =
		dynamic_cast<BigramMonkey*>(ana.model->_prior)->biPr(prevCh, ch);
	// cerr<<"transition from "<<*this<<" on "<<
	// 	(ch == -1 ? "-1" : chars.alphabet().inv(ch))<<" is "<<
	// 	res<<"\n";
	return res;
}

Prob BiMonkeyState::stop()
{
	int ch = chars[1];
	return dynamic_cast<BigramMonkey*>(ana.model->_prior)->biPr(ch, -1);
}

void BiMonkeyState::print(ostream& os)
{
	os<<"BIMONKEY"<<"|"<<"...";
	int prevCh = chars.empty() ? -1 : chars[0];
	if(prevCh != -1)
	{
		os<<chars.alphabet().inv(prevCh);
	}
	int currCh = chars.empty() ? -1 : chars[1];
	if(currCh != -1)
	{
		os<<chars.alphabet().inv(currCh);
	}
	os<<" "<<prob<<" ";
	os<<"st:"<<index<<" ";
}	
