#include "common.h"
#include "SegSampler.h"
#include "Monkey.h"
#include "State.h"
#include "Phones.h"

#include <boost/program_options.hpp>

int main(int argc, char* argv[])
{
	appInit(DATA_PATH);

	namespace po = boost::program_options;

	po::options_description desc("Compute transducer from true mappings");
	desc.add_options()
		("help", "show options")
		("input-file", po::value<string>(),
		 "file to read input from")
		("brent-reader",
		 "reader for Brent format (one char per phone, space-delimited")
		;

	po::positional_options_description p;
	p.add("input-file", -1);

	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv).
			  options(desc).positional(p).run(), vm);
	po::notify(vm);

	bool normalReader = true;

	if(vm.count("help"))
	{
		cout<<desc<<"\n";
		return 1;
	}

	string input;
	if(!vm.count("input-file"))
	{
		cerr<<"We need an input file\n";
		return 1;
	}
	else
	{
		input = vm["input-file"].as<string>();
	}

	StrVocab alpha;

	PhoneChannel ph(alpha);

	ifstream ff(input.c_str());
	if(!ff.is_open())
	{
		cerr<<"Can't open input file '"<<input<<"'\n";
		return 1;
	}

	while(ff)
	{
		string line;
		getline(ff, line);
		if(line.empty())
		{
			break;
		}

		std::istringstream split(line);

		SymbolStrings sSurf;

		while(split)
		{
			SymbolString* word;
			if(normalReader)
			{
				word = new SymbolString(split, alpha);
			}
			else
			{
				string key;
				split>>key;
				if(key == "||")
				{
					cerr<<"*WARNING* Is your reader setting wrong?\n";
					return 1;
				}
				word = new SymbolString(key, alpha);
			}

			if(word->empty())
			{
				break;
			}
			sSurf.push_back(word);
		}

		int ln = sSurf.size();

		for(int ii = 0; ii < ln; ++ii)
		{
			{
				cerr<<*sSurf[ii]<<"\n";

				foreach(SymbolString, ch, *sSurf[ii])
				{
					ph.counts[*ch][*ch] += 1;
				}
			}
		}

		for(int ii = 0; ii < ln; ++ii)
		{
			delete sSurf[ii];
		}
	}

	cerr<<"Estimating...\n";

	foreach(intIntIntMap, ct, ph.counts)
	{
		MaxEntContext ctxt;

		{
			Feats eps;
			eps.push_back(Feat(ph.fset.num(ph.OUT, 0), 1));
			ctxt.push_back(eps);
		}

		ctxt.resize(alpha._vocab.size() + 1);

		foreach(strIntMap, ch, alpha._vocab)
		{
			Feats fts;
			fts.push_back(Feat(ph.fset.num(ph.OUT, ch->second + 1), 1));
			ctxt[ch->second + 1] = fts;
		}

		ph.learner.addCount(1 + ct->first, ctxt, (ct->second)[ct->first]);
		cerr<<"Count of "<<alpha.inv(ct->first)<<" "<<
			(ct->second)[ct->first]<<"\n";
	}

	ph.learner.estimate();

	double* wts = ph.learner.weights();

	//from initialTransducer.py used in acl paper
	wts[ph.fset.num(ph.FAITH, 0)] = -3;
	wts[ph.fset.pair(ph.fset.num(ph.FAITH, 0),
					 ph.fset.num(ph.MANNER, 0))] = -1.5;
	wts[ph.fset.pair(ph.fset.num(ph.FAITH, 0),
					 ph.fset.num(ph.PLACE, 0))] = -1.5;
	wts[ph.fset.pair(ph.fset.num(ph.FAITH, 0),
					 ph.fset.num(ph.VOICE, 0))] = -1.5;
	wts[ph.fset.pair(ph.fset.num(ph.FAITH, 0),
					 ph.fset.num(ph.GLIDE, 0))] = -1.5;
	wts[ph.fset.num(ph.OUT, 0)] = 0;

	ph.epsLearner.setDimension(ph.fset.maxFeat());
	ph.epsLearner.weights()[ph.fset.num(ph.OUT, 0)] = 3;

	ph.write(cout);
}
