#include "SyllableMonkey.h"

SyllableMonkey::SyllableMonkey(Prob stop, bool allowEmpty,
							   Prob pEnd, StrVocab& alphabet):
	Monkey(stop, allowEmpty, pEnd, alphabet)
{
	sChar = _alphabet->get("s", true);

	nuclei.insert(_alphabet->get("aa", true));
	nuclei.insert(_alphabet->get("ae", true));
	nuclei.insert(_alphabet->get("ay", true));
	nuclei.insert(_alphabet->get("aw", true));
	nuclei.insert(_alphabet->get("ao", true));
	nuclei.insert(_alphabet->get("oy", true));
	nuclei.insert(_alphabet->get("ow", true));
	nuclei.insert(_alphabet->get("eh", true));
	nuclei.insert(_alphabet->get("ey", true));
	nuclei.insert(_alphabet->get("er", true));
	nuclei.insert(_alphabet->get("ah", true));
	nuclei.insert(_alphabet->get("uw", true));
	nuclei.insert(_alphabet->get("uh", true));
	nuclei.insert(_alphabet->get("ih", true));
	nuclei.insert(_alphabet->get("iy", true));
	nuclei.insert(_alphabet->get("en", true));
	nuclei.insert(_alphabet->get("eng", true));
	nuclei.insert(_alphabet->get("em", true));
	nuclei.insert(_alphabet->get("el", true));

	liquids.insert(_alphabet->get("w", true));
	liquids.insert(_alphabet->get("y", true));
	liquids.insert(_alphabet->get("l", true));
	liquids.insert(_alphabet->get("r", true));

	fricatives.insert(_alphabet->get("f", true));
	fricatives.insert(_alphabet->get("v", true));
	fricatives.insert(_alphabet->get("dh", true));
	fricatives.insert(_alphabet->get("z", true));
	fricatives.insert(_alphabet->get("zh", true));
	fricatives.insert(_alphabet->get("jh", true));
	fricatives.insert(_alphabet->get("th", true));
	fricatives.insert(_alphabet->get("sh", true));
	fricatives.insert(_alphabet->get("ch", true));
	fricatives.insert(_alphabet->get("hh", true));

	stops.insert(_alphabet->get("g", true));
	stops.insert(_alphabet->get("k", true));
	stops.insert(_alphabet->get("t", true));
	stops.insert(_alphabet->get("d", true));
	stops.insert(_alphabet->get("b", true));
	stops.insert(_alphabet->get("p", true));

	nasals.insert(_alphabet->get("n", true));
	nasals.insert(_alphabet->get("m", true));
	nasals.insert(_alphabet->get("ng", true));
}

SyllableMonkey::~SyllableMonkey()
{
}

int SyllableMonkey::sample()
{
	assert(0);
	return -1;
}

Prob SyllableMonkey::update(int xx, double temp)
{
	return prob(xx);
}

Prob SyllableMonkey::remove(int xx, double temp)
{
	return prob(xx);
}

Prob SyllableMonkey::prob(int sample)
{
	SymbolString& unintern = _lexicon.inv(sample);
	int nSyms = unintern.size();
	if(nSyms == 0) //the empty string
	{
		if(_allowEmpty) //as an actual string
		{
			return _stop;
		}
		else //as start-end (no! even as end token this causes issues)
		{
			return _pEnd;
		}
	}

	Prob pSym = (1 - _stop) * (1.0 / _alphabet->size());

	//oddly, I get an include error when using the macro here...
	assert(isProb(pSym));

	Prob res;
	if(_allowEmpty)
	{
		res = powl(pSym, nSyms) * _stop;
	}
	else
	{
		//if empty isn't allowed, first char is guaranteed
		res = powl(pSym, nSyms - 1) * (1.0 / _alphabet->size()) * _stop;
	}

	res *= (1 - _pEnd); //not the empty str
	assert(isProb(res));

	//can underflow quite easily, actually
	if(res == 0)
	{
		res = EPSILON;
	}

	//eventually we'd want to overhaul this
	//but for now, let's just apply a large penalty
	//if the proposed word has an invalid syllable
	if(invalid(unintern))
	{
		// cerr<<unintern<<" contains invalid syllables\n";
		res = 0;
	}

	// cerr<<" alpha size "<<_alphabet->size()<<" ps "<<pSym<<"\n";
	// cerr<<"Monkey: prob of "<<unintern<<" = "<<res<<"\n";

	return res;
}

bool SyllableMonkey::invalid(SymbolString& unintern)
{
	// cerr<<"CHECKING "<<unintern<<"\n";

	typedef std::pair<int, int> range;
	typedef std::vector<range> Ranges;

	Ranges sylls;
	int nn = unintern.size();

	for(int ii = 0; ii < nn; ++ii)
	{
		if(contains(nuclei, unintern[ii]))
		{
			sylls.push_back(range(ii, ii+1));
		}
	}

	if(sylls.empty())
	{
		return true;
	}

	int ns = sylls.size();
	for(int si = 0; si < ns - 1; ++si)
	{
		range& syll = sylls[si];
		int prev = (si == 0)?0:sylls[si - 1].second;

		// cerr<<"Located nucleus at "<<syll.first<<" "<<
		// 	_alphabet->inv(unintern[syll.first])<<"\n";

		int endOnset = syll.first;
		while(syll.first > prev)
		{
			// cerr<<"Trying onset from "<<(syll.first - 1)<<
			// 	" to "<<endOnset<<"\n";

			if(onsetOk(unintern, syll.first - 1, endOnset))
			{
				syll.first -= 1;
			}
			else
			{
				break;
			}
		}
	}

	for(int si = 0; si < ns - 1; ++si)
	{
		if(!codaOk(unintern, sylls[si].second, sylls[si + 1].first))
		{
			return true;
		}
		sylls[si].second = sylls[si + 1].first;
	}

	if(!codaOk(unintern, sylls[ns - 1].second, nn))
	{
		return true;
	}
	sylls[ns - 1].second = nn;

	return false;
}

bool SyllableMonkey::onsetOk(SymbolString& str, int begin, int end)
{
	// cerr<<"Onset "; for(int qq = begin; qq < end; ++qq) { 
	// 	cerr<<_alphabet->inv(str[qq]); } cerr<<"\n";

	int pri = -1;
	for(int ii = begin; ii < end; ++ii)
	{
		int currPri = 0;
		if(str[ii] == sChar)
		{
			currPri = 0;
		}
		else if(contains(stops, str[ii]) || contains(fricatives, str[ii])
				|| contains(nasals, str[ii]))
		{
			currPri = 1;
		}
		else if(contains(liquids, str[ii]))
		{
			currPri = 2;
		}
		else if(contains(nuclei, str[ii]))
		{
			// cerr<<"Onset "; for(int qq = begin; qq < end; ++qq) { 
			// 	cerr<<_alphabet->inv(str[qq]); } 
			// cerr<<" is invalid (nucleus)\n";

			return false;
		}
		else
		{
			cerr<<"WARN unknown segment type "<<str<<" "<<ii<<"\n";
		}

		if(currPri <= pri)
		{
			// cerr<<"Onset "; for(int qq = begin; qq < end; ++qq) { 
			// 	cerr<<_alphabet->inv(str[qq]); } 
			// cerr<<" is invalid (sonority)\n";

			return false;
		}

		pri = currPri;
	}

	return true;
}

bool SyllableMonkey::codaOk(SymbolString& str, int begin, int end)
{
	int pri = 4;
	for(int ii = begin; ii < end; ++ii)
	{
		int currPri = 0;
		if(str[ii] == sChar)
		{
			continue;
		}
		else if(contains(stops, str[ii]) || contains(fricatives, str[ii]))
		{
			currPri = 1;
		}
		else if(contains(nasals, str[ii]))
		{
			currPri = 2;
		}
		else if(contains(liquids, str[ii]))
		{
			currPri = 3;
		}
		else if(contains(nuclei, str[ii]))
		{
			// cerr<<"Coda "; for(int qq = begin; qq < end; ++qq) { 
			// 	cerr<<_alphabet->inv(str[qq]); } 
			// cerr<<" is invalid (nucleus)\n";

			return false;
		}
		else
		{
			cerr<<"WARN unknown segment type "<<str<<" "<<ii<<"\n";
		}

		if(currPri >= pri)
		{
			// cerr<<"Coda "; for(int qq = begin; qq < end; ++qq) { 
			// 	cerr<<_alphabet->inv(str[qq]); } 
			// cerr<<" is invalid (sonority)\n";

			return false;
		}

		pri = currPri;
	}

	return true;
}
