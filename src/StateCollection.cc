#include "StateCollection.h"

StateCollection::StateCollection(int sz):
	origStates(SORT_MONKEY * sz),
	allStates(SORT_MONKEY * sz),
	orig(NULL)
{
}

void StateCollection::clear(State* root)
{
	for(int tier = 0; tier < tiers(); ++tier)
	{
		origStates[tier] = NULL;
		foreach(States, state, allStates[tier])
		{
			(*state)->wipe();

			if((*state)->path != NULL || (*state)->end())
			{
				assert(dynamic_cast<MonkeyState*>(*state) == NULL);
				assert(origStates[tier] == NULL);
				origStates[tier] = state;
			}
			else
			{
				delete *state;
			}
		}
		allStates[tier].clear();
		allStates[tier].push_back(origStates[tier]);
	}
}

StateSet& StateCollection::tier(int ti, Prob threshold)
{
	clearTier(ti, threshold);
	mergeTier(tier);
	return currTier;
}

State* StateCollection::origState()
{
	assert(orig != NULL);
	return orig;
}

void StateCollection::add(State* si)
{
	int bucket = si->index * SORT_MONKEY + si->sortType();
	allStates[bucket].push_back(si);
}

void StateCollection::clearTier(
