#include "common.h"
#include "SegSampler.h"
#include "Monkey.h"
#include "Channel.h"

int main(int argc, char* argv[])
{
	appInit(DATA_PATH);

	StrVocab alpha;
	//vocab is a-z
	for(char aa = 'a'; aa <= 'z'; ++aa)
	{
		string si;
		si += aa;
		alpha.get(si, true);
	}

	Monkey simian(.3, alpha);
	string empty("");
	SymbolString emptySym(empty, alpha);
	int start = simian.intern(emptySym);
	assert(start == 0);

	CRPLM model(2, 4, .3, &simian, new NoiselessChannel());

	string text("a b c d e f g h i j k l m");
	SymbolString symbolic(text, alpha);
	cout<<symbolic<<"\n";

	Analysis ana(symbolic, &model);

	int size = symbolic.size();

	cout<<ana<<"\n";

	for(int ii = 2; ii < size; ii += 2)
	{
		ana.split(ii);
		cout<<ana<<"\n";
	}

	for(int ii = 2; ii < size; ii += 2)
	{
		ana.merge(ii);
		cout<<ana<<"\n";
	}

	// SegSampler sampler(model);
	// sampler.add(symbolic);

	// cout<<sampler<<"\n";

	// sampler.sampleAt(0, 5, true);
	// cout<<sampler<<"\n";

	// for(int ii = 0; ii < 10; ++ii)
	// {
	// 	for(int jj = 1; jj < size - 1; ++jj)
	// 	{
	// 		sampler.sampleAt(0, jj, true);
	// 		cout<<sampler<<"\n";
	// 	}
	// }
}
