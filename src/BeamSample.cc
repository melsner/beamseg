#include "common.h"
#include "SegSampler.h"
#include "Monkey.h"
#include "BigramMonkey.h"
#include "NGramMonkey.h"
#include "CollocationMonkey.h"
#include "SyllableMonkey.h"
#include "FiniteDist.h"
#include "State.h"
#include "AnnealRate.h"
#include "SyllableChannel.h"

#include <boost/program_options.hpp>

void addString(SegSampler& sampler, SymbolString& syms, ints& trueSplits,
			   bool cheat, bool singleChars, bool randomBreaks, 
			   double breakProb, bool lockBounds);

void addString(SegSampler& sampler, SymbolString& syms, ints& trueSplits,
			   bool cheat, bool singleChars, bool randomBreaks,
			   double breakProb, bool lockBounds)
{
	//when active, initializes at true segmentations
	sampler.add(syms);
	if(cheat)
	{
		foreach(ints, splitPt, trueSplits)
		{
			sampler.split(sampler.numSequences() - 1, *splitPt);
		}
	}
	else if(randomBreaks)
	{
		int seqSize = 
			sampler.sequence(sampler.numSequences() - 1).nSyms();
		for(int sym = 1; sym < seqSize - 1; ++sym)
		{
			if(gsl_rng_uniform(RNG) < breakProb)
			{
				sampler.split(sampler.numSequences() - 1, sym);
			}
		}
	}
	else if(singleChars)
	{
		int seqSize = 
			sampler.sequence(sampler.numSequences() - 1).nSyms();
		for(int sym = 1; sym < seqSize; ++sym)
		{
			sampler.split(sampler.numSequences() - 1, sym);
		}
	}

	if(lockBounds)
	{
		Analysis& ana = sampler.sequence(sampler.numSequences() - 1);
		foreach(intToSeg, endPos, ana.segments)
		{
			ana.allowedBounds.insert(endPos->first);
		}
		ana.lockBounds = true;
	}
}

int main(int argc, char* argv[])
{
	appInit(DATA_PATH);

	namespace po = boost::program_options;

	po::options_description desc("Beam sampling for segments/intended forms");
	desc.add_options()
		("help", "show options")
		("input-file", po::value<string>(),
		 "file to read input from")
		("init", po::value<string>(),
		 "initial breakpoints (true,random,none,single,underlying file)")
		("lock-bounds",
		 "do not resample word boundaries")
		("iters", po::value<int>(), "number of iterations")
		("grams", po::value<int>(), "ngram order")
		("alpha", po::value< std::vector<double> >(), "CRP alphas")
		("discount", po::value< std::vector<double> >(), "PY discount")
		("break", po::value<double>(), "probability of word break")
		("output", po::value<string>(), 
		 "file stem to output to (creates output.learned.underlying, output.learned.surface)")
		("linear-rate",
		 "anneal linearly to 1 (otherwise custom schedule)")
		("const-rate",
		 "annealing temperature locked at 1 (otherwise custom schedule)")
		("brent-reader",
		 "reader for Brent format (one char per phone, space-delimited)")
		("base", po::value<string>(),
		 "base distribution (monkey,finite,bimonkey,dpmonkey,colloc,syl)")
		("channel", po::value<string>(),
		 "channel type: noiseless, xsub, phone,syl")
		("sample-hypers",
		 "sample hyperparameters")
		("no-staging",
		 "no staged init")
		("sgd",
		 "learn channel parameters with SGD")
		("em",
		 "learn channel probabilities with EM")
		("write-channel", po::value<string>(),
		 "write final channel parameters to this file")		 
		("read-channel", po::value<string>(),
		 "read channel parameters from this file")
		;

	po::positional_options_description p;
	p.add("input-file", -1);

	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv).
			  options(desc).positional(p).run(), vm);
	po::notify(vm);

	if(vm.count("help"))
	{
		cout<<desc<<"\n";
		return 1;
	}

	int grams = 2;

	if(vm.count("grams"))
	{
		grams = vm["grams"].as<int>();
		assert(grams > 0);
		cerr<<"Selected "<<grams<<"-grams\n";
	}

	bool normalReader = true;
	double breakProb = .2;
//	double pEnd = .001;
	double pEnd = .5;
	doubles crpAlpha(grams, 0);
	crpAlpha[0] = 3000;
	crpAlpha[1] = 100; //dpseg old version had 300
	doubles crpDiscount(grams, 0);
	string output = "output";
	string baseType = "monkey";
	string channelType = "noiseless";
	bool cheat = false;
	bool lockBounds = false;
	bool randomBreaks = true;
	bool singleChars = false;
	string initUnderlying = "";
	int iters = 100;
	bool linearRate = false;
	bool constRate = false;
	bool staging = true;
	bool sampleHypers = false;
	bool doSGD = false;
	bool doEM = false;
	string channelOut = "";

	if(vm.count("brent-reader"))
	{
		normalReader = false;
	}

	if(vm.count("break"))
	{
		breakProb = vm["break"].as<double>();
	}

	cerr<<"Break prob: "<<breakProb<<"\n";

	string input;
	if(!vm.count("input-file"))
	{
		cerr<<"Assuming input is "<<output<<".surface\n";
		input = output + ".surface";
	}
	else
	{
		input = vm["input-file"].as<string>();
	}

	if(vm.count("iters"))
	{
		iters = vm["iters"].as<int>();
	}

	if(vm.count("alpha"))
	{
		crpAlpha = vm["alpha"].as< std::vector<double> >();
	}

	for(int ii = 0; ii < crpAlpha.size(); ++ii)
	{
		cerr<<"Alpha: "<<ii<<" = "<<crpAlpha[ii]<<"\n";
	}		

	if(vm.count("discount"))
	{
		crpDiscount = vm["discount"].as< std::vector<double> >();
	}

	for(int ii = 0; ii < crpDiscount.size(); ++ii)
	{
		cerr<<"Discount: "<<ii<<" = "<<crpDiscount[ii]<<"\n";
	}

	if(vm.count("output"))
	{
		output = vm["output"].as<string>();
	}

	if(vm.count("channel"))
	{
		channelType = vm["channel"].as<string>();
	}

	if(vm.count("linear-rate"))
	{
		linearRate = true;
	}

	if(vm.count("const-rate"))
	{
		constRate = true;
	}

	if(vm.count("no-staging"))
	{
		staging = false;
	}

	if(vm.count("sample-hypers"))
	{
		sampleHypers = true;
		cerr<<"Sampling hyperparameters\n";
	}

	if(vm.count("init"))
	{
		string init = vm["init"].as<string>();
		cerr<<"Initialization: "<<init<<"\n";
		if(init == "true")
		{
			cheat = true;
		}
		else if(init == "random")
		{
			randomBreaks = true;
		}
		else if(init == "single")
		{
			singleChars = true;
			randomBreaks = false;
		}
		else if(init == "none")
		{
			randomBreaks = false;
		}
		else
		{
			randomBreaks = false;
			initUnderlying = init;
			// input = initUnderlying + ".surface";
			// cerr<<"Using "<<input<<" as input file.\n";

			// initUnderlying = initUnderlying + ".underlying";
			cerr<<"Using "<<initUnderlying<<" as initialization.\n";

			cheat = true;
		}
	}

	if(vm.count("lock-bounds"))
	{
		cerr<<"Locking word boundaries\n";
		lockBounds = true;
	}

	cerr<<"Break prob: "<<breakProb<<"\n";
	cerr<<"End prob: "<<pEnd<<"\n";

	if(vm.count("base"))
	{
		baseType = vm["base"].as<string>();
	}

	cerr<<"Base distribution: "<<baseType<<"\n";

	StrVocab alpha;
	StrVocab secondary;

	int baseGrams = 2;
	doubles baseAlphas(baseGrams, 0);
	baseAlphas[0] = 1000;
	baseAlphas[1] = 500;
	doubles baseDiscounts(baseGrams, 0);

	Monkey* simian = NULL;
	if(baseType == "monkey")
	{
		simian = new Monkey(breakProb, false, pEnd, alpha);
	}
	else if(baseType == "bimonkey")
	{
		simian = new BigramMonkey(10, breakProb, pEnd, alpha);
	}
	else if(baseType == "dpmonkey")
	{
		cerr<<"DP Monkey: grams "<<baseGrams<<" alphas ";
		writeVector(baseAlphas, cerr);
		cerr<<" discounts ";
		writeVector(baseDiscounts, cerr);

		simian = new NGramMonkey(baseGrams, baseAlphas, baseDiscounts,
								 false, pEnd, alpha);
	}
	else if(baseType == "colloc")
	{
		double a1 = 100;
		cerr<<"Collocations of Monkey: alpha "<<a1<<"\n";
		simian = new CollocationMonkey(a1, breakProb, pEnd, alpha);
	}
	else if(baseType == "syl")
	{
		cerr<<"Monkey with syllable penalties\n";
		simian = new SyllableMonkey(breakProb, false, pEnd, alpha);
	}
	else if(baseType == "finite")
	{
		double dummy = 0;
		simian = new FiniteDist(dummy, pEnd, alpha);

		string empty("");
		SymbolString emptySym(empty, alpha);
		int start = simian->intern(emptySym);
		assert(start == 0);
		dynamic_cast<FiniteDist*>(simian)->addPriorCount(0, 1);

		ifstream fixedV("fixed.vocab");
		assert(fixedV.is_open());
		SymbolString* word;
		while(fixedV)
		{
			word = new SymbolString(fixedV, alpha);
			if(word->empty())
			{
				delete word;
				break;
			}

			// cerr<<"word "<<*word<<"\n";
			int code = simian->intern(*word);
			dynamic_cast<FiniteDist*>(simian)->addPriorCount(code, 1000);
			delete word;
		}
	}
	else
	{
		cerr<<"Unknown base distribution "<<baseType<<"\n";
		return 1;
	}

	cerr<<"Channel type: "<<channelType<<"\n";

	Channel* channel = NULL;
	if(channelType == "noiseless" || channelType == "none")
	{
		channel = new Channel();
	}
	else if(channelType == "xsub")
	{
		channel = new XChannel(.1, alpha.get("x", true));
	}
	else if(channelType == "phone")
	{
		PhoneChannel* ph = new PhoneChannel(alpha);
		if(vm.count("read-channel"))
		{
			string channelFile = vm["read-channel"].as<string>();
			cerr<<"Reading channel parameters from "<<channelFile<<"\n";
			ifstream ifs(channelFile.c_str());
			assert(ifs.is_open());
			ph->read(ifs);
		}
		else
		{
			cerr<<"Requires a channel parameter file\n";
			return 1;
		}
		channel = ph;
	}
	else if(channelType == "syl")
	{
		SyllableChannel* ph = new SyllableChannel(alpha, secondary);
		if(vm.count("read-channel"))
		{
			string channelFile = vm["read-channel"].as<string>();
			cerr<<"Reading channel parameters from "<<channelFile<<"\n";
			ifstream ifs(channelFile.c_str());
			assert(ifs.is_open());
			ph->read(ifs);
		}
		else
		{
			cerr<<"Requires a channel parameter file\n";
			return 1;
		}
		channel = ph;
	}
	else
	{
		cerr<<"Unknown channel type "<<channelType<<"\n";
		return 1;
	}

	if(vm.count("sgd"))
	{
		doSGD = true;
	}

	if(vm.count("em"))
	{
		doEM = true;
	}

	if(doSGD || doEM)
	{
		if(vm.count("write-channel"))
		{
			channelOut = vm["write-channel"].as<string>();
			cerr<<"Writing channel parameters to "<<channelOut<<"\n";
		}
	}

	string empty("");
	SymbolString emptySym(empty, alpha);
	int start = simian->intern(emptySym);
	assert(start == 0);

	CRPLM model(grams, crpAlpha, crpDiscount, simian, channel);
	SegSampler sampler(model);

	ifstream ff(input.c_str());
	if(!ff.is_open())
	{
		cerr<<"Can't open input file '"<<input<<"'\n";
		return 1;
	}

	std::vector<SymbolString> allStrings;
	intMat allTrueSplits;

	while(ff)
	{
		string line;
		getline(ff, line);
		if(line.empty())
		{
			break;
		}

		std::istringstream split(line);

		SymbolString si(alpha);
		ints trueSplits;
		int curr = 0;

		while(split)
		{
			SymbolString* word;
			if(normalReader)
			{
				word = new SymbolString(split, alpha);
			}
			else
			{
				string key;
				split>>key;
				if(key == "||")
				{
					cerr<<"*WARNING* Is your reader setting wrong?\n";
					return 1;
				}
				word = new SymbolString(key, alpha);
			}

			if(word->empty())
			{
				delete word;
				break;
			}

			curr += word->size();
			trueSplits.push_back(curr);
			si.append(*word);
			delete word;
		}

		cerr<<"Read "<<si<<"\n";
		allStrings.push_back(si);

		//learn bigram character lm on surface strings
		//in a way that makes Frank mad
		if(dynamic_cast<BigramMonkey*>(simian) != NULL)
		{
			dynamic_cast<BigramMonkey*>(simian)->addStr(si);
		}

		trueSplits.pop_back(); //last 'split' point is end of last word
		allTrueSplits.push_back(trueSplits);
	}

	if(staging)
	{
		for(int si = 0; si < 1 || si < .05 * allStrings.size(); ++si)
		{
			addString(sampler, allStrings[si], allTrueSplits[si],
					  cheat, singleChars, randomBreaks, breakProb, lockBounds);
		}
	}
	else
	{
		for(int si = 0; si < allStrings.size(); ++si)
		{
			addString(sampler, allStrings[si], allTrueSplits[si],
					  cheat, singleChars, randomBreaks, breakProb, lockBounds);
		}
	}

	int nSeqs = sampler.numSequences();
	for(int seq = 0; seq < nSeqs; ++seq)
	{
		sampler.sequence(seq).primeStates();
	}

	foreach(strIntMap, ch, alpha._vocab)
	{
		if(ch->first.size() > 3 && ch->first != "<epsilon>")
		{
			cerr<<"Very long symbol detected: "<<ch->first<<"\n";
			cerr<<"*WARNING* Did you get your reader setting wrong?\n";
			// return 1;
		}
	}

	cerr<<"Alphabet size: "<<alpha.size()<<"\n";
	cerr<<"Base distribution:\n"<<*simian<<"\n";

	AnnealRate* rate = NULL;
	AnnealRate* channelRate = NULL;
	AnnealRate* learningRate = NULL;
	if(linearRate)
	{
		rate = new LinearRate(.1, 1.0, 1000);
		channelRate = new LinearRate(0, .3, 1000);

		if(doSGD || doEM)
		{
			StepwiseRate* lr = new StepwiseRate();

			lr->add( new LinearRate(0, 0, 800));

			lr->add( new LinearRate(.0001, .0001, 100));

			lr->add( new LinearRate(0, 0, 1000));

			// lr->add( new LinearRate(.0001, .0001, 10));

			// lr->add( new LinearRate(0, 0, 1000));

			learningRate = lr;
		}
		else
		{
			learningRate = new AnnealRate(0);
		}
	}
	else if(constRate)
	{
		rate = new LinearRate(1.0, 1.0, 1000);
		channelRate = new AnnealRate(.3);
		learningRate = new AnnealRate(0);
	}		
	else
	{
		StepwiseRate* sr = new StepwiseRate();
		StepwiseRate* cr = new StepwiseRate();
		// sr->add( new LinearRate(.3, .7, 500) );
		// cr->add(new LinearRate(0, 0, 400));
		// cr->add(new LinearRate(0, .1, 100));

		// sr->add( new LinearRate(4, 4, 100) );
		// cr->add(new LinearRate(.3, .3, 100));

		// sr->add( new LinearRate(.7, 1, 200) );
		// cr->add(new LinearRate(.3, .3, 200));

		sr->add( new LinearRate(.1, 1.0, 800));
		sr->add( new LinearRate(1.0, 2.0, 200));

		cr->add(new LinearRate(.0, .3, 800));
		cr->add(new LinearRate(.3, .3, 200));

		rate = sr;
		channelRate = cr;

		if(doSGD || doEM)
		{
			StepwiseRate* lr = new StepwiseRate();

			lr->add( new LinearRate(0, 0, 800));

			lr->add( new LinearRate(.0001, .0001, 100));

			lr->add( new LinearRate(0, 0, 1000));

			learningRate = lr;
		}
		else
		{
			learningRate = new AnnealRate(0);
		}
	}

	sampler.printLexicon(cerr);
	if(dynamic_cast<CollocationMonkey*>(simian))
	{
		dynamic_cast<CollocationMonkey*>(simian)->printMetaLexicon(cerr);
	}

	if(channelType == "phone" || channelType == "syl")
	{
		dynamic_cast<PhoneChannel*>(channel)->print(cerr);
	}

//	for(int ii = 0; ii <= iters; ++ii)
	for(int ii = 0; !rate->done(); ++ii)
	{
		double invTemp = rate->next();
		double channelTemp = channelRate->next();
		double learnRate = learningRate->next();

		cerr<<"... Iter "<<ii<<", inverse temp "<<invTemp<<", channel temp "<<
			channelTemp<<" ll "<<model.ll()<<"\n";
		sampler.summarize(cerr);

		if(ii == 10)
		{
			cerr<<"Staging up to 10% data\n";
			for(int si = nSeqs; si < .1 * allStrings.size(); ++si)
			{
				addString(sampler, allStrings[si], allTrueSplits[si],
						  cheat, singleChars, randomBreaks, breakProb, 
						  lockBounds);
				Analysis& ana = sampler.sequence(si);
				ana.primeStates();

				ana.invTemp = invTemp;
				ana.channelInvTemp = channelTemp;
				cerr<<"Added: "<<ana;
				model._betaDate++;
				ana.mhrSweep();
				cerr<<"\t"<<ana<<"\n";
			}
			nSeqs = sampler.numSequences();
		}
		else if(ii == 50)
		{
			cerr<<"Staging up to 25% data\n";
			for(int si = nSeqs; si < .25 * allStrings.size(); ++si)
			{
				addString(sampler, allStrings[si], allTrueSplits[si],
						  cheat, singleChars, randomBreaks, breakProb,
						  lockBounds);
				Analysis& ana = sampler.sequence(si);
				ana.primeStates();

				ana.invTemp = invTemp;
				ana.channelInvTemp = channelTemp;
				cerr<<"Added: "<<ana;
				model._betaDate++;
				ana.mhrSweep();
				cerr<<"\t"<<ana<<"\n";
			}
			nSeqs = sampler.numSequences();
		}
		else if(ii == 100)
		{
			cerr<<"Staging up to 50% data\n";
			for(int si = nSeqs; si < .50 * allStrings.size(); ++si)
			{
				addString(sampler, allStrings[si], allTrueSplits[si],
						  cheat, singleChars, randomBreaks, breakProb, 
						  lockBounds);
				Analysis& ana = sampler.sequence(si);
				ana.primeStates();

				ana.invTemp = invTemp;
				ana.channelInvTemp = channelTemp;
				cerr<<"Added: "<<ana;
				model._betaDate++;
				ana.mhrSweep();
				cerr<<"\t"<<ana<<"\n";
			}
			nSeqs = sampler.numSequences();
		}
		else if(ii == 150)
		{
			cerr<<"Staging up to 75% data\n";
			for(int si = nSeqs; si < .75 * allStrings.size(); ++si)
			{
				addString(sampler, allStrings[si], allTrueSplits[si],
						  cheat, singleChars, randomBreaks, breakProb,
						  lockBounds);
				Analysis& ana = sampler.sequence(si);
				ana.primeStates();

				ana.invTemp = invTemp;
				ana.channelInvTemp = channelTemp;
				cerr<<"Added: "<<ana;
				model._betaDate++;
				ana.mhrSweep();
				cerr<<"\t"<<ana<<"\n";
			}
			nSeqs = sampler.numSequences();
		}
		else if(ii == 200)
		{
			cerr<<"Staging up to 100% data\n";
			for(int si = nSeqs; si < allStrings.size(); ++si)
			{
				addString(sampler, allStrings[si], allTrueSplits[si],
						  cheat, singleChars, randomBreaks, breakProb,
						  lockBounds);
				Analysis& ana = sampler.sequence(si);
				ana.primeStates();

				ana.invTemp = invTemp;
				ana.channelInvTemp = channelTemp;
				cerr<<"Added: "<<ana;
				model._betaDate++;
				ana.mhrSweep();
				cerr<<"\t"<<ana<<"\n";
			}
			nSeqs = sampler.numSequences();
		}

		ints perm;
		randPermutation(perm, nSeqs);

		LogProb sgwTerm = sampler.sgwStyleResample(invTemp);
		cerr<<"SGW style estimate "<<sgwTerm<<"\n";

		for(int seqa = 0; seqa < nSeqs; ++seqa)
		{
			int seq = perm[seqa];
			Analysis& ana = sampler.sequence(seq);
			ana.invTemp = invTemp;
			ana.channelInvTemp = channelTemp;
			cerr<<ii<<":"<<seq<<" "<<ana;

			// ana.verbose = DBG_U | DBG_STATES;

			model._betaDate++;

			// if(seq == 2){ ana.verbose = DBG_U | DBG_STATES | DBG_MHR; }

			{
				cerr<<"MH ";
				ana.mhrSweep();

				if((doSGD || doEM) && learnRate > 0 && channelType == "phone")
				{
					dynamic_cast<PhoneChannel*>(channel)->learnFrom(&ana);
				}
			}

			cerr<<"\t"<<ana<<"\n";

			if(doSGD && seq % 100 == 0 && learnRate > 0 &&
			   channelType == "phone")
			{
				dynamic_cast<PhoneChannel*>(channel)->sgd(learnRate);
			}

			// writeHashMap(model.stats, cerr);
		}

		if(sampleHypers)
		{
			sampler.sampleHypers(false, invTemp);
		}

		if(doEM && learnRate > 0 && channelType == "phone")
		{
			PhoneChannel* pch = dynamic_cast<PhoneChannel*>(channel);
			pch->estimate();
		}

		if((doSGD || doEM) && channelType == "phone")
		{
			dynamic_cast<PhoneChannel*>(channel)->print(cerr);
		}

		sampler.printLexicon(cerr);
		if(dynamic_cast<CollocationMonkey*>(simian))
		{
			dynamic_cast<CollocationMonkey*>(simian)->printMetaLexicon(cerr);
		}
		sampler.printPhoneDist(cerr);
	}

	cerr<<"\n***************\n\n";
	string underName = output + ".learned.underlying";
	ofstream underlying(underName.c_str());

	string surfName = output + ".learned.surface";
	ofstream surface(surfName.c_str());

	if(cheat)
	{
		underlying<<"CHEATING\n";
	}

	if(channelOut != "" && channelType == "phone")
	{
		ofstream ofs(channelOut.c_str());
		dynamic_cast<PhoneChannel*>(channel)->write(ofs);
	}

	sampler.writeUnderlying(underlying);
	sampler.writeSurface(surface);
}
