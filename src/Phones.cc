#include "Phones.h"
#include "MonkeyState.h"

//copied from charFeats.py

void initPhones(StrVocab& alphabet, intPhoneMap& table)
{
	new Phone(table, alphabet, "aa", "midlow back vowel");
	new Phone(table, alphabet, "ae", "midlow front vowel");
	new Phone(table, alphabet, "ay", "low back vowel offglidey");
	new Phone(table, alphabet, "aw", "low back vowel offglidew");
	new Phone(table, alphabet, "ao", "midlow back vowel");
	new Phone(table, alphabet, "oy", "mid back vowel offglidey");
	new Phone(table, alphabet, "ow", "mid back vowel offglidew");
	new Phone(table, alphabet, "eh", "mid front vowel");
	new Phone(table, alphabet, "ey", "mid front vowel offglidey");
	new Phone(table, alphabet, "er", "rhotic mid front vowel");
	new Phone(table, alphabet, "y", "high palatal approximant glidey");
	new Phone(table, alphabet, "w", "high labial approximant glidew");
	new Phone(table, alphabet, "ah", "mid central vowel");
	new Phone(table, alphabet, "uw", "high back vowel");
	new Phone(table, alphabet, "uh", "midhigh back vowel");
	new Phone(table, alphabet, "ih", "midhigh front vowel");
	new Phone(table, alphabet, "iy", "high front vowel");
	new Phone(table, alphabet, "en", "mid central nasal vowel");
	new Phone(table, alphabet, "eng", "mid back nasal vowel");
	new Phone(table, alphabet, "ng", "velar nasal");
	new Phone(table, alphabet, "n", "alveolar nasal");
	new Phone(table, alphabet, "m", "labial nasal");
	new Phone(table, alphabet, "em", "mid front nasal vowel");
	new Phone(table, alphabet, "p", "voiceless labial stop");
	new Phone(table, alphabet, "b", "voiced labial stop");
	new Phone(table, alphabet, "f", "voiceless labial fricative");
	new Phone(table, alphabet, "v", "voiced labial fricative");
	new Phone(table, alphabet, "d", "voiced alveolar stop");
	new Phone(table, alphabet, "dh", "voiced dental fricative");
	new Phone(table, alphabet, "z", "voiced alveolar fricative");
	new Phone(table, alphabet, "zh", "voiced postalveolar fricative");
	new Phone(table, alphabet, "jh", "voiced alveolar affricate");
	new Phone(table, alphabet, "t", "voiceless alveolar stop");
	new Phone(table, alphabet, "s", "voiceless alveolar fricative");
	new Phone(table, alphabet, "th", "voiceless dental fricative");
	new Phone(table, alphabet, "sh", "voiceless postalveolar fricative");
	new Phone(table, alphabet, "ch", "voiceless alveolar affricate");
	new Phone(table, alphabet, "l", "voiced palatal approximant glidel");
	new Phone(table, alphabet, "r", "voiced alveolar approximant");
	new Phone(table, alphabet, "el", "palatal vowel glidel");
	new Phone(table, alphabet, "k", "voiceless velar stop");
	new Phone(table, alphabet, "g", "voiced velar stop");
	new Phone(table, alphabet, "hh", "voiceless glottal fricative");
	new Phone(table, alphabet, "<epsilon>", "epsilon");
}

Phone::Phone(intPhoneMap& table, StrVocab& alphabet, 
			 string nm, string features)
{
	name = nm;
	int myInd = -1;
	if(name != "<epsilon>")
	{
		myInd = alphabet.get(nm, true);
	}
	table[myInd] = this;
	// cerr<<"construct "<<nm<<" and install at "<<alphabet.get(nm, true)<<
	// 	" "<<this<<"\n";

	vowel = features.find("vowel") != string::npos;
	epsilon = (features == "epsilon");
	voice = (vowel ||
				  (features.find("nasal") != string::npos) ||
				  (features.find("voiced") != string::npos));

	sonority = -1;
	string manners[] = {"stop", "fricative", "affricate",
						"approximant", "vowel"};
	for(int ii = 0; ii < 5; ++ii)
	{
		if(features.find(manners[ii]) != string::npos)
		{
			sonority = ii;
			break;
		}
	}

	//merge fricative/affricate
	if(sonority >= 2)
	{
		sonority -= 1;
	}

	// I can't believe this code actually was in here, this is awful
	// sonority -= 1;
	// if(sonority <= 0)
	// {
	// 	sonority += 1;
	// }

	position = -1;
	height = -1;
	string poss[] = {"labial", "dental", "alveolar",
					 "postalveolar", "palatal", "velar", "glottal"};

	if(!vowel)
	{
		for(int ii = 0; ii < 7; ++ii)
		{
			if(features.find(poss[ii]) != string::npos)
			{
				position = ii;
				break;
			}
		}
	}
	else
	{
		if(features.find("back") != string::npos)
		{
			position = 5;
		}
		else if(features.find("central") != string::npos)
		{
			position = 2;
		}
		else if(features.find("front") != string::npos)
		{
			position = 1;
		}

		string heights[] = {"high", "midhigh", "mid", "midlow", "low"};
		for(int ii = 0; ii < 5; ++ii)
		{
			if(features.find(heights[ii]) != string::npos)
			{
				height = ii;
				break;
			}
		}
	}

	nasal = features.find("nasal") != string::npos;
	rhotic = features.find("rhotic") != string::npos;
	glide = rhotic || nasal || (features.find("glide") != string::npos);
}

PhoneChannel::PhoneChannel(StrVocab& alph):
	greatestEpsAfter(-1),
	alphabet(alph),
	learner(0, 0.0),
	epsLearner(50, 0.0)
{
	initPhones(alphabet, table);
	epsPh = table[-1];
	assert(epsPh != NULL);

	FAITH = fset.addType("faith", 1);

	MANNER = fset.addType("manner", 5);
	GIVMANNER = fset.addType("given-manner", 5);

	PLACE = fset.addType("place", 8);
	GIVPLACE = fset.addType("given-place", 8);

	VOICE = fset.addType("voice", 3);
	GIVVOICE = fset.addType("given-voice", 3);

	GLIDE = fset.addType("glide", 3);
	GIVGLIDE = fset.addType("given-glide", 3);

	OUT = fset.addType("output", alph.size() + 1);

	learner.setDimension(fset.maxFeat()); //maybe replace with vocab?
	epsLearner.setDimension(fset.maxFeat()); //maybe replace with vocab?
}

PhoneChannel::~PhoneChannel()
{
}

Channel* PhoneChannel::copy()
{
	assert(0); //don't use this
}

int PhoneChannel::statePrevChar(State* st)
{
	int sz = st->chars.size();
	if(st->lastChar() == -1)
	{
		if(sz < 1)
		{
			return -1;
		}
		return st->chars[sz - 1];
	}
	else
	{
		if(sz < 2)
		{
			return -1;
		}
		return st->chars[sz - 2];
	}
}

Prob PhoneChannel::emitCh(int given, int produced)
{
	if(!inMap(prGiven, given))
	{
		computeProbs(given);
	}
	Prob res = prGiven[given][produced];

	// assertProb(res);
	// if(res == 0)
	// {
	// 	cerr<<"for "<<given<<" "<<produced<<"\n";
	// 	cerr<<(given == -1 ? "-1" : alphabet.inv(given))<<"\n";
	// 	cerr<<(produced == -1 ? "-1" : alphabet.inv(produced))<<"\n";
	// }
 	// assert(res != 0);
	return res;
}

Prob PhoneChannel::epsilonAfter(int prevCh)
{
	if(!inMap(prEps, prevCh))
	{
		computeEps(prevCh);
	}

	return prEps[prevCh];
}

Prob PhoneChannel::greatestEpsilonAfter()
{
	if(greatestEpsAfter != -1)
	{
		return greatestEpsAfter;
	}

	for(int ii = -1; ii < alphabet.size(); ++ii)
	{
		Prob term = epsilonAfter(ii);
		if(term > greatestEpsAfter)
		{
			greatestEpsAfter = term;
		}
	}

	return greatestEpsAfter;
}

Prob PhoneChannel::emit(State* st, int ch)
{
	int given = st->lastChar();

	Prob res = 1.0;

	int prevCh = statePrevChar(st);

	// cerr<<"Probability of epsilon following "<<
	// 	(prevCh == -1 ? "-1" : alphabet.inv(prevCh))<<" is "<<
	// 	prEps[prevCh]<<"\n";

	if(given == -1)
	{
		res *= epsilonAfter(prevCh);
	}
	else
	{
		res *= 1 - epsilonAfter(prevCh);
	}

	// cerr<<"prob of "<<(ch == -1 ? "-1" : alphabet.inv(ch))
	// 	<<" given "<<(given == -1 ? "-1" : alphabet.inv(given))<<
	// 	" is "<<prGiven[given][ch]<<"\n";

	res *= emitCh(given, ch);
	assertProb(res);

	return res;
}

void PhoneChannel::computeEps(int phPrev)
{
	MaxEntContext ctxt;

	Feats noEps;
	noEps.push_back(Feat(fset.num(OUT, 1), 1));
	ctxt.push_back(noEps);

	Feats nextEps;
	pointFeats(phPrev, -1, nextEps);
	ctxt.push_back(nextEps);

	// prEps[phPrev] = smoothExtremes(epsLearner.probToken(1, ctxt));
	prEps[phPrev] = epsLearner.probToken(1, ctxt);
}

void PhoneChannel::computeProbs(int phGiven)
{
	intProbMap& prTab = prGiven[phGiven];

	MaxEntContext ctxt;

	{
		Feats eps;
		pointFeats(phGiven, -1, eps);
		ctxt.push_back(eps);
	}

	ctxt.resize(alphabet._vocab.size() + 1);

	foreach(strIntMap, ch, alphabet._vocab)
	{
		Feats fts;
		pointFeats(phGiven, ch->second, fts);
		ctxt[ch->second + 1] = fts;
	}

	// prTab[-1] = smoothExtremes(learner.probToken(0, ctxt));
	prTab[-1] = learner.probToken(0, ctxt);
	assert(prTab[-1] > 0);
	foreach(strIntMap, ch, alphabet._vocab)
	{
		// Prob pr = smoothExtremes(learner.probToken(1 + ch->second, ctxt));
		Prob pr = learner.probToken(1 + ch->second, ctxt);

		assert(pr != 0);
		prTab[ch->second] = pr;

		// learner.printMarginals(ctxt, cerr);
		// cerr<<"Prob of "<<
		// 	ch->first<<" given "<<
		// 	(phGiven == -1 ? "-1" : alphabet.inv(phGiven))<<" = "<<
		// 	pr<<"\n";
	}
}

Phone* PhoneChannel::getPhone(int ph)
{
	if(ph == -1)
	{
		return epsPh;
	}
	intPhoneMap::iterator entry = table.find(ph);
	if(entry == table.end())
	{
		cerr<<"Unknown phone "<<alphabet.inv(ph)<<"\n";
		assert(0);
	}
	return entry->second;
}

void PhoneChannel::pointFeats(int phGiven, int phProd, Feats& res)
{
	Phone* given = getPhone(phGiven);
	Phone* prod = getPhone(phProd);

	// cerr<<"Feats for "<<given->name<<" -> "<<prod->name<<"\n";

	if(phGiven == phProd)
	{
		// cerr<<"FAITH is on "<<fset.num(FAITH, 0)<<" "<<
		// 	learner.weights()[fset.num(FAITH, 0)]<<"\n";
		res.push_back(Feat(fset.num(FAITH, 0), 1));
	}

	if(given->sonority == prod->sonority)
	{
		res.push_back(Feat(fset.pair(fset.num(FAITH, 0),
									 fset.num(MANNER, 0)), 1));
	}

	// res.push_back(Feat(fset.num(MANNER, 1 + prod->sonority), 1));
	res.push_back(Feat(fset.pair(fset.num(GIVMANNER, 1 + given->sonority),
								 fset.num(MANNER, 1 + prod->sonority)), 1));

	if(given->voice == prod->voice)
	{
		res.push_back(Feat(fset.pair(fset.num(FAITH, 0),
									 fset.num(VOICE, 0)), 1));
	}

	// res.push_back(Feat(fset.num(VOICE, 1 + prod->voice), 1));
	res.push_back(Feat(fset.pair(fset.num(GIVVOICE, 1 + given->voice),
								 fset.num(VOICE, 1 + prod->voice)), 1));

	if(given->position == prod->position)
	{
		res.push_back(Feat(fset.pair(fset.num(FAITH, 0),
									 fset.num(PLACE, 0)), 1));
	}

	// res.push_back(Feat(fset.num(PLACE, 1 + prod->position), 1));
	res.push_back(Feat(fset.pair(fset.num(GIVPLACE, 1 + given->position),
								 fset.num(PLACE, 1 + prod->position)), 1));


	//the "glide" symbol is used for a bunch of different glides,
	//so faith isn't violated only if it's the same one
	if(given->glide == prod->glide &&
	   (given->glide == 0 || phGiven == phProd))
	{
		res.push_back(Feat(fset.pair(fset.num(FAITH, 0),
									 fset.num(GLIDE, 0)), 1));
	}

	res.push_back(Feat(fset.pair(fset.num(GIVGLIDE, 1 + given->glide),
								 fset.num(GLIDE, 1 + prod->glide)), 1));

	res.push_back(Feat(fset.num(OUT, 1 + phProd), 1));
}

void PhoneChannel::learnFrom(Analysis* ana)
{
	Prob total = 1;
	State* curr = ana->root;

	if(curr->path->lastChar() == -1)
	{
		// cerr<<*curr<<" begins with epenthetic\n";
		epsCounts[-1][1] += 1;
	}
	else
	{
		// cerr<<*curr<<" does not begin with epenthetic\n";
		epsCounts[-1][0] += 1;
	}

	while(!curr->end())
	{
		if(!curr->newWord())
		{
			// cerr<<*curr<<"\n";
			int given = curr->lastChar();
			int prevCh = statePrevChar(curr);

			// cerr<<"giv "<<given<<" prev "<<prevCh<<"\n";

			if(given == -1)
			{
				epsCounts[prevCh][1] += 1;
			}
			else
			{
				epsCounts[prevCh][0] += 1;
			}

			int ch = curr->emitted();

			// cerr<<(given == -1 ? "-1" : alphabet.inv(given))<<" -> "<<
			// 	(ch == -1 ? "-1" : alphabet.inv(ch))<<" ";
			if(!inMap(prGiven, given))
			{
				computeProbs(given);
			}
			total *= prGiven[given][ch];
			// cerr<<prGiven[given][ch]<<"\n";

			counts[given][ch] += 1;			
		}

		curr = curr->path;
	}

	// cerr<<"Pr: "<<total<<" LPR: "<<logl(total)<<"\n";
}

void PhoneChannel::estimate()
{
	//not currently cmd-line configurable: pseudocount for x->x bias in EM
	double lambda = 200;

	foreach(intIntIntMap, given, counts)
	{
		MaxEntContext ctxt;

		{
			Feats eps;
			pointFeats(given->first, -1, eps);
			ctxt.push_back(eps);
		}

		ctxt.resize(alphabet._vocab.size() + 1);

		foreach(strIntMap, ch, alphabet._vocab)
		{
			Feats fts;
			// cerr<<"adding features for "<<ch->first<<" at "<<
			// 	(ch->second + 1)<<"\n";
			pointFeats(given->first, ch->second, fts);
			ctxt[ch->second + 1] = fts;
		}

		foreach(intIntMap, prod, given->second)
		{
			// cerr<<"given "<<alphabet.inv(given->first)<<" produce "<<
			// 	alphabet.inv(prod->first)<<" count "<<prod->second<<
			// 	" at "<<(1 + prod->first)<<"\n";

			int count = prod->second;
			if(given->first == prod->first)
			{
				count += lambda;
			}

			learner.addCount(1 + prod->first, ctxt, count);
		}
	}

	learner.estimate();
	learner.clear();
	counts.clear();

	foreach(intIntIntMap, given, epsCounts)
	{
		MaxEntContext ctxt;

		Feats noEps;
		noEps.push_back(Feat(fset.num(OUT, 1), 1));
		ctxt.push_back(noEps);

		Feats nextEps;
		pointFeats(given->first, -1, nextEps);
		ctxt.push_back(nextEps);

		foreach(intIntMap, prod, given->second)
		{
			int count = prod->second;
			if(given->first == 0)
			{
				count += lambda;
			}
			else
			{
				count += lambda * .05;
			}

			epsLearner.addCount(prod->first, ctxt, count);
		}
	}

	epsLearner.estimate();
	epsLearner.clear();
	epsCounts.clear();

	prGiven.clear();
	prEps.clear();

	cerr.unsetf(std::ios_base::scientific);
}

void PhoneChannel::mlEstimate()
{
	cerr<<"ML\n";

	prGiven.clear();
	prEps.clear();

	intIntMap unigrams;
	double unigramTotal = 0;
	foreach(intIntIntMap, given, counts)
	{
		foreach(intIntMap, prod, given->second)
		{
			unigrams[prod->first] += prod->second;
			unigramTotal += prod->second;
		}
	}

	double lambda = 200;

	foreach(intIntIntMap, given, counts)
	{
		double total = 0;
		foreach(intIntMap, prod, given->second)
		{
			total += prod->second;
		}

		intProbMap& prTab = prGiven[given->first];
		foreach(strIntMap, ch, alphabet._vocab)
		{
			Prob backoff = unigrams[ch->second] / unigramTotal;
			prTab[ch->second] = given->second[ch->second] / (total + lambda)
				+ (lambda * backoff) / (total + lambda);
		}

		{
			Prob backoff = unigrams[-1] / unigramTotal;
			prTab[-1] = given->second[-1] / (total + lambda)
				+ lambda * backoff;
		}
	}

	counts.clear();

	foreach(intIntIntMap, given, epsCounts)
	{
		double total = 0;
		foreach(intIntMap, prod, given->second)
		{
			total += prod->second;
		}

		prEps[given->first] = (given->second[1] + 10) / (total + 20);
	}

	prEps[-1] = 0;

	epsCounts.clear();
}

void PhoneChannel::sgd(double rate)
{
	cerr<<"SGD: rate "<<rate<<"\n";

	foreach(intIntIntMap, given, counts)
	{
		// if(given->first != -1)
		// {
		// 	cerr<<"Features for "<<alphabet.inv(given->first)<<"\n";
		// }

		MaxEntContext ctxt;

		{
			Feats eps;
			pointFeats(given->first, -1, eps);
			ctxt.push_back(eps);
		}

		ctxt.resize(alphabet._vocab.size() + 1);

		foreach(strIntMap, ch, alphabet._vocab)
		{
			Feats fts;
			// cerr<<"adding features for "<<ch->first<<" at "<<
			// 	(ch->second + 1)<<"\n";
			pointFeats(given->first, ch->second, fts);
			ctxt[ch->second + 1] = fts;

			// foreach(Feats, fi, fts)
			// {
			// 	cerr<<"\t"<<fset.inv(fi->first)<<" "<<fi->second<<"\n";
			// }
		}

		foreach(intIntMap, prod, given->second)
		{
			// if(given->first != -1)
			// {
			// 	cerr<<"given "<<alphabet.inv(given->first);
			// }
			// else
			// {
			// 	cerr<<"given -1";
			// }
			// cerr<<" produce ";
			// if(prod->first != -1)
			// {
			// 	cerr<<alphabet.inv(prod->first);
			// }
			// else
			// {
			// 	cerr<<"-1";
			// }
			// cerr<<" count "<<prod->second<<" at "<<(1 + prod->first)<<"\n";

			learner.addCount(1 + prod->first, ctxt, prod->second);
		}
	}

	// {
	// 	MaxEntLikelihood fn(0, learner._examples);

	// 	double grad[learner._dim];
	// 	fn(learner._dim, learner._weights, grad);

	// 	for(int ii = 0; ii < learner._dim; ++ii)
	// 	{
	// 		if(learner._weights[ii] != 0 || grad[ii] != 0)
	// 		{
	// 			cerr<<fset.inv(ii)<<" ";
	// 			cerr<<" wt "<<learner._weights[ii]<<" grad "<<grad[ii]<<
	// 				" out "<<(learner._weights[ii] - rate * grad[ii])<<"\n";
	// 		}
	// 	}
	// }

	learner.sgd(rate, false);
	learner.clear();
	counts.clear();

	foreach(intIntIntMap, given, epsCounts)
	{
		MaxEntContext ctxt;

		Feats noEps;
		noEps.push_back(Feat(fset.num(OUT, 1), 1));
		ctxt.push_back(noEps);

		Feats nextEps;
		pointFeats(given->first, -1, nextEps);
		ctxt.push_back(nextEps);

		foreach(intIntMap, prod, given->second)
		{
			epsLearner.addCount(prod->first, ctxt, prod->second);
		}
	}

	epsLearner.sgd(rate, false);
	epsLearner.clear();
	epsCounts.clear();

	prGiven.clear();
	prEps.clear();
}

LogProb PhoneChannel::ll()
{
	LogProb result = 0;

	//there's already code that does this... why am I doing this?

	foreach(intIntIntMap, given, _phoneCounts)
	{
		foreach(intIntMap, prod, given->second)
		{
			result += prod->second * logl(emitCh(given->first, prod->first));
		}
	}

	foreach(intIntMap, given, _epsCounts)
	{
		result += given->second * logl(epsilonAfter(given->first));
	}

	// assertLogProb(result);

	return result;
}

void PhoneChannel::print(ostream& os)
{
	foreach(intPhoneMap, probe, table)
	{
		IntPriVec prs;
		foreach(intPhoneMap, emit, table)
		{
			prs.push_back(IntPriNode(emitCh(probe->first, emit->first),
									 emit->first));
		}
		sort(prs.begin(), prs.end(), IntGreater());

		if(probe->first != -1)
		{
			os<<alphabet.inv(probe->first);
		}
		else
		{
			os<<"-1";
		}
		os<<":  ";
		os<<"("<<epsilonAfter(probe->first)<<") ";

		double tot = 0;
		foreach(IntPriVec, pi, prs)
		{
			if(pi->data != -1)
			{
				os<<alphabet.inv(pi->data);
			}
			else
			{
				os<<"-1";
			}
			os<<" "<<pi->pri<<"   ";
			tot += pi->pri;
			if(tot > .95)
			{
				break;
			}
		}
		os<<"\n";
	}
}

void PhoneChannel::write(ostream& os)
{
	learner.write(os);
	epsLearner.write(os);
}

void PhoneChannel::read(istream& is)
{
	learner.read(is);
	epsLearner.read(is);
}
