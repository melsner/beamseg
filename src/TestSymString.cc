#include "common.h"
#include "SymbolString.h"

int main(int argc, char* argv[])
{
	appInit(DATA_PATH);

	StrVocab alpha;

	string fnordStr("F N O R D");
	SymbolString fnord(fnordStr, alpha);
	cout<<fnord<<"\n";
	cout<<fnord.size()<<" "<<fnord[0]<<" "<<alpha.inv(fnord[0])<<"\n";

	Vocab<SymbolString> lexicon;
	lexicon.get(fnord, true);

	cout<<"word from lexicon "<<lexicon.inv(0)<<"\n";

	cout<<fnord.is(fnordStr)<<" "<<fnord.is("FN O")<<" "<<
		fnord.is("F N O R D A")<<" "<<fnord.is("A A A A A")<<"\n";
}
