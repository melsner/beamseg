#include "CRP.h"
#include "Monkey.h"

CRP::CRP(double& alpha, Generator* prior, double& discount):
	_alpha(alpha),
	_total(0),
	_nTables(0),
	_fakeCounts(0),
	_prior(prior),
	_discount(discount),
	_betaDate(NULL),
	_myBetaDate(0)
{
}

CRP::~CRP()
{
	foreach(intToTableLst, tabLst, _tables)
	{
		foreach(TableLst, tab, tabLst->second)
		{
			delete *tab;
		}
	}

	foreach(intCRPMap, child, _children)
	{
		delete child->second;
	}
}

int CRP::sample()
{
	Prob sm = gsl_rng_uniform(RNG);

	Prob denom = _total + _alpha;

	//mass devoted to items with counts
	Prob total = 0;
	foreach(intIntMap, labelCt, _counts)
	{
		//PY: discount each table by D
		Prob discountFactor = _nTablesFor[labelCt->first] * _discount;
		total += labelCt->second - discountFactor;
		if(sm <= total / denom)
		{
			return labelCt->first;
		}
	}

	//remaining mass for new items
	return _prior->sample();
}

Prob CRP::update(int sample, double temp)
{
	Table* tab;
	return update(sample, tab, temp);
}

Prob CRP::update(int sample, Table*& rtab, double temp)
{
	_beta.clear();

	// Prob pRes = prob(sample);
	Prob res = 0;

	// cerr<<"Updating table for "<<sample<<" at temp "<<temp<<"\n";
	// cerr<<*this;

	Prob discountFactor = _nTablesFor[sample] * _discount;
	Prob total = 0;
	TableLst& tables = _tables[sample];
	foreach(TableLst, tab, tables)
	{
		Prob term = (*tab)->count - _discount;
		term = powl(term, temp);
		total += term;
		// cerr<<"term for "<<**tab<<" "<<term<<"\n";
	}

	{
		Prob term = _prior->prob(sample) * (_alpha + discountFactor);
		term = powl(term, temp);
		total += term;
		// cerr<<"sample "<<sample<<" a "<<_alpha<<" dc "<<discountFactor<<"\n";
		// cerr<<"term for prior "<<term<<" "<<_prior->prob(sample)<<"\n";
	}

	Prob sm = gsl_rng_uniform(RNG);
	Prob progress = 0;

	// cerr<<"sampled a "<<sm<<"\n";

	foreach(TableLst, tab, tables)
	{
		Prob term = (*tab)->count - _discount;
		term = powl(term, temp);
		progress += term;

		if(sm <= progress / total)
		{
			res = ((*tab)->count - _discount) / (_alpha + _total);

			// cerr<<"num "<<((*tab)->count - _discount)<<" den "<<
			// 	(_alpha + _total)<<" res "<<res<<"\n";

			(*tab)->count += 1;
			rtab = *tab;

			progress = -1;
			break;
		}
	}

	if(progress != -1)
	{
		//new table
		res = (_alpha + (_nTables * _discount)) / (_alpha + _total);

		TableLst& tables = _tables[sample];
		Table* nt = new Table(sample); //auto-inits count=1
		tables.push_back(nt);
		rtab = nt;

		_nTablesFor[sample] += 1;
		_nTables += 1;
 
		assert(_tables[sample].size() == _nTablesFor[sample]);

		Prob term = _prior->prob(sample);

		// cerr<<"New table term "<<res<<" prior term "<<term<<"\n";

		CRP* crpPrior = dynamic_cast<CRP*>(_prior);
		if(crpPrior)
		{
			Table* pt;
			crpPrior->update(sample, pt, temp);
			nt->parTab = pt;
			// cerr<<"prior sends table "<<pt<<"\n";
		}
		else
		{
			_prior->update(sample, temp);
		}

		res *= term;
		// cerr<<"Sample from prior: "<<term<<"\n";
	}

	_total += 1;
	_counts[sample] += 1; //items with label X

	return res;
//XXX	return pRes;
}

void CRP::addPriorCount(int sample, int count)
{
	TableLst& tables = _tables[sample];
	Table* nt = new Table(sample); //auto-inits count=1
	nt->count = count;
	tables.push_back(nt);
	_nTablesFor[sample] += 1;
	_nTables += 1;
	_total += count;
	_fakeCounts += count;
	_counts[sample] += count;
}

Prob CRP::remove(int sample, Table* tab)
{
	_beta.clear();

	_total -= 1;
	_counts[sample] -= 1;
	tab->count -= 1;

	Prob res;

	if(tab->count == 0)
	{
		TableLst& tables = _tables[sample];
		tables.remove(tab);
		_nTablesFor[sample] -= 1;
		_nTables -= 1;
		CRP* crpPrior = dynamic_cast<CRP*>(_prior);
		if(crpPrior)
		{
			// cerr<<"removing from prior table "<<tab->parTab<<"\n";
			assert(tab->parTab != NULL);
			res = crpPrior->remove(sample, tab->parTab);
		}
		else
		{
			res = _prior->remove(sample);
		}
		res *= _alpha / (_total + _alpha);

		delete tab;
	}
	else
	{
		res = tab->count / (_total + _alpha);
	}

	if(_counts[sample] == 0)
	{
		_counts.erase(sample);
		_tables.erase(sample);
		_nTablesFor.erase(sample);
	}

	return res;
//	return prob(sample);
}


Prob CRP::remove(int sample, double temp)
{
	assert(0);
	_beta.clear();

	int count = _counts[sample];
	assert(count > 0);

	_total -= 1;
	_counts[sample] -= 1;

	Prob total = 0;
	TableLst& tables = _tables[sample];

	// cerr<<"prior pr "<<_prior->prob(sample)<<" "<<_alpha<<"\n";
	LogProb prior = logl(_prior->prob(sample) * (_alpha));
	LogProb mx = -INFINITY;
	LogProb terms[tables.size()];
	int ctr = 0;

	foreach(TableLst, tab, tables)
	{
		Prob term = gsl_sf_lngamma((*tab)->count) + prior;
		// cerr<<"term for "<<(*tab)->count<<" prior "<<prior<<" "<<term<<"\n";
		if(term > mx)
		{
			mx = term;
		}
		terms[ctr] = term;
		++ctr;
	}

	for(ctr = 0; ctr < tables.size(); ++ctr)
	{
		// cerr<<"norming "<<terms[ctr]<<" mx "<<mx<<"\n";
		terms[ctr] = expl(terms[ctr] - mx);
		total += terms[ctr];
	}

	Prob sm = gsl_rng_uniform(RNG);
	Prob res = 0;

	//prev table-- which one? (no prior sampling here) (no?)
	Prob progress = 0;
	ctr = 0;
	foreach(TableLst, tab, tables)
	{
		Prob term = terms[ctr];
		// cerr<<term<<" pr "<<progress/total<<" "<<sm<<"\n";
		++ctr;
		progress += term;

		if(sm <= progress / total)
		{
			(*tab)->count -= 1;
			if((*tab)->count == 0)
			{
				delete *tab;
				tables.erase(tab);
				_nTablesFor[sample] -= 1;
				_nTables -= 1;

				res = (_alpha + (_nTables * _discount)) / (_alpha + _total);
				res *= _prior->remove(sample, temp);
				// assertProb(res);
			}
			else
			{
				res = ((*tab)->count - _discount) / (_alpha + _total);
				// assertProb(res);
			}

			// if(_tables[sample].size() != _nTablesFor[sample])
			// {
			// 	cerr<<"Error: "<<sample<<" has "<<
			// 		_tables[sample].size()<<" real tables, recorded "<<
			// 		_nTablesFor[sample]<<" tables.\n";
			// }

			assert(_tables[sample].size() == _nTablesFor[sample]);

			progress = -1;
			break;
		}
	}

	if(progress != -1)
	{
		cerr<<"Removal: failed to find a table!\n";
		assert(0);
	}

	if(_counts[sample] == 0)
	{
		_counts.erase(sample);
		_tables.erase(sample);
		_nTablesFor.erase(sample);
	}

//	return res;
	return prob(sample);
}

Prob CRP::prob(int sample)
{
	Prob denom = _total + _alpha;

	Prob overallDiscountFactor = _nTables * _discount;
	Prob fromPrior =  _prior->prob(sample) * 
		(_alpha + overallDiscountFactor) / denom;
	assertProb(fromPrior);
	if(fromPrior == 0)
	{
		return 0;
	}

	int count = ifExists(_counts, sample, 0);
	int tables = ifExists(_nTablesFor, sample, 0);
	Prob discountFactor = tables * _discount;
	Prob fromCounts = (count - discountFactor) / (_total + _alpha);
	assertProb(fromCounts);

	Prob res = fromCounts + fromPrior;
	// assert(_alpha == 0 || res > 0);

	return res;
}

Prob CRP::probNoG0(int sample)
{
	Prob denom = _total + _alpha;

	Prob overallDiscountFactor = _nTables * _discount;

	CRP* par = dynamic_cast<CRP*>(_prior);

	Prob fromPrior = 0;
	if(par != NULL)
	{
		fromPrior = par->probNoG0(sample) * 
			(_alpha + overallDiscountFactor) / denom;
	}

	int count = ifExists(_counts, sample, 0);
	int tables = ifExists(_nTablesFor, sample, 0);
	Prob discountFactor = tables * _discount;
	Prob fromCounts = (count - discountFactor) / (_total + _alpha);
	assertProb(fromCounts);

	Prob res = fromCounts + fromPrior;
	assertProb(res);

	return res;
}

void CRP::noNewTables()
{
	_alpha = 0;
	_discount = 0;
}

intIntMap& CRP::labels()
{
	return _counts;
}

LogProb CRP::ll(bool verbose)
{
	//configuration for 0 or 1 is determined
	//this special-casing is needed b/c floating-point prec. produces
	//invalid results
	if(_total <= 1)
	{
		return 0;
	}

	LogProb res = 0;
	Prob currDiscFactor = 0;
	foreach(intToTableLst, tabLst, _tables)
	{
		foreach(TableLst, tab, tabLst->second)
		{
			// cerr<<"Computing for "<<**tab<<"\n";

			//numerator terms 1..n associated with seating at table
			//can't figure out the correct generalization for PY
			// res += gsl_sf_lngamma((*tab)->count);

			res += gsl_sf_lngamma((*tab)->count - _discount);
			res -= gsl_sf_lngamma(1 - _discount);
			// iterative factorial fn
			// Prob check = 0;
			// for(int ii = 1; ii < (*tab)->count; ++ii)
			// {
			// 	check += logl(ii - _discount);
			// }
			// cerr<<"fn for count "<<(*tab)->count<<"\n";

			//term for table creation (probably more succinct way to do this)
			res += logl(_alpha + currDiscFactor);

			//now that this table was created, disc factor increases
			currDiscFactor += _discount;
		}
	}

	//denom terms 1..N associated with all customers
	res -= gsl_sf_lngamma(_alpha + _total);
	res += gsl_sf_lngamma(_alpha);

	//remember to compute prior terms associated with labels somewhere else!
	// assertLogProb(res);

	return res;
}

void CRP::print(ostream& os)
{
	foreach(intToTableLst, tabLst, _tables)
	{
		os<<":: "<<tabLst->first<<" x"<<tabLst->second.size()<<" :: ";
		foreach(TableLst, tab, tabLst->second)
		{
			os<<**tab<<" ";
		}
		os<<"\n";
	}
	os<<"Alpha: "<<_alpha<<" discount "<<_discount<<"\n";
	os<<"Total: "<<_total<<" Config LL: "<<ll()<<"\n";
}

bool CRP::canRemove(int sample)
{
	return inMap(_counts, sample);
}

int CRP::total()
{
	return _total - _fakeCounts;
}

intProbMap& CRP::beta()
{
	if(*_betaDate != _myBetaDate)
	{
		_myBetaDate = *_betaDate;
		sampleBeta();
	}
	return _beta;
}

intProbMap& CRP::meanBeta(bool uni, double pc)
{
	if(*_betaDate != _myBetaDate)
	{
		_myBetaDate = *_betaDate;
		getMeanBeta(uni, pc);
	}
	return _beta;
}

void CRP::sampleBeta()
{
	assert(_discount == 0);
	_beta.clear();

	CRP* par = dynamic_cast<CRP*>(_prior);
	if(par)
	{
		intProbMap& parBeta = par->beta();
		size_t lexSize = par->_counts.size();
		Prob mpar[lexSize + 1];
		{
			intIntMap::iterator word = par->_counts.begin();
			for(size_t ii = 0; ii < lexSize; ++ii,++word)
			{
				int count = ifExists(_counts, word->first, 0);
				mpar[ii] = count + _alpha * parBeta[word->first];

				// cerr<<"fetch mpar for "<<word->first<<" count "<<count<<
				// 	" other "<<(_alpha * parBeta[word->first])<<" total "<<
				// 	mpar[ii]<<"\n";
			}
		}
		mpar[lexSize] = _alpha * parBeta[-1];

		// cerr<<"child mpar\n";
		// for(int ii = 0; ii <= lexSize; ++ii)
		// {
		// 	cerr<<"mp "<<mpar[ii]<<"\n";
		// }

		//check
		// Prob total = 0;
		// for(size_t ii = 0; ii < lexSize + 1; ++ii)
		// {
		// 	total += mpar[ii];
		// }
		// cerr<<"TOTAL MPAR "<<total<<"\n";

		Prob bs[lexSize + 1];

		// gsl_ran_dirichlet(RNG, lexSize + 1, mpar, bs);
		sample_dirichlet(lexSize + 1, mpar, bs);

		{
			intIntMap::iterator word = par->_counts.begin();
			for(size_t ii = 0; ii < lexSize; ++ii,++word)
			{
				_beta[word->first] = bs[ii];
			}
		}
		_beta[-1] = bs[lexSize];
	}
	else
	{
		size_t lexSize = _counts.size();
		Prob priWt = 0;
		Prob mpar[lexSize + 1];
		{
			intIntMap::iterator word = _counts.begin();
			for(size_t ii = 0; ii < lexSize; ++ii,++word)
			{
				int count = word->second;
				assert(count > 0);
				Prob term = _prior->prob(word->first);
				priWt += term;

				// cerr<<"root mpar for "<<word->first<<" count "<<count<<
				// 	" other "<<(_alpha * term)<<" total "<<
				// 	(count + _alpha * term)<<"\n";

				mpar[ii] = count + _alpha * term;
			}
		}
		mpar[lexSize] = _alpha * (1 - priWt);

		// cerr<<"root mpar\n";
		// for(int ii = 0; ii <= lexSize; ++ii)
		// {
		// 	cerr<<"mp "<<mpar[ii]<<"\n";
		// }

		// //check
		// Prob total = 0;
		// for(size_t ii = 0; ii < lexSize + 1; ++ii)
		// {
		// 	total += mpar[ii];
		// }
		// cerr<<"TOTAL ROOT "<<total<<"\n";

		Prob bs[lexSize + 1];

		// gsl_ran_dirichlet(RNG, lexSize + 1, mpar, bs);
		sample_dirichlet(lexSize + 1, mpar, bs);

		// {
		// 	size_t sz = lexSize + 1;
		// 	double gi[sz];
		// 	for(int ii = 0; ii < sz; ++ii)
		// 	{
		// 		gi[ii] = gsl_ran_gamma(RNG, mpar[ii], 1);
		// 	}

		// 	Prob total = 0;
		// 	for(int ii = 0; ii < sz; ++ii)
		// 	{
		// 		total += gi[ii];
		// 	}

		// 	intIntMap::iterator word = _counts.begin();
		// 	for(size_t ii = 0; ii < lexSize; ++ii,++word)
		// 	{
		// 		cerr<<"gi for "<<word->first<<" "<<gi[ii]<<" alpha "<<
		// 			mpar[ii]<<" total "<<total<<" res "<<(gi[ii]/total)<<"\n";
		// 	}

		// 	for(int ii = 0; ii < sz; ++ii)
		// 	{
		// 		bs[ii] = gi[ii] / total;
		// 	}
		// }


		{
			intIntMap::iterator word = _counts.begin();
			for(size_t ii = 0; ii < lexSize; ++ii,++word)
			{
				_beta[word->first] = bs[ii];
			}
		}
		_beta[-1] = bs[lexSize];
	}
}

void CRP::getMeanBeta(bool uni, double pc)
{
	{
		CRP* par = this;
		while(par != NULL)
		{
			CRP* par2 = dynamic_cast<CRP*>(par->_prior);
			if(!par2)
			{
				break;
			}
			par = par2;
		}
		if(par == this)
		{
			_beta.clear();
		}
		else
		{
			_beta = par->meanBeta(uni, pc);
		}
	}

	Prob denom = _total + _alpha;
	Prob overallDiscountFactor = _nTables * _discount;

	Prob ntf = (_alpha + overallDiscountFactor) / denom;
	foreach(intProbMap, bi, _beta)
	{
		bi->second *= ntf;
	}

	foreach(intIntMap, word, _counts)
	{
		//open-code the prob function so we can memo the repeated parts
		//and the count
		{
			Prob res;
			Prob fromPrior =  _prior->prob(word->first) * ntf;
			assertProb(fromPrior);
			int count = word->second;
			int tables = (count == 0) ? 0 : _nTablesFor[word->first];
			Prob discountFactor = tables * _discount;
			Prob fromCounts = (count - discountFactor) / denom;
			assertProb(fromCounts);

			res = fromCounts + fromPrior;

			_beta[word->first] = res;
		}
	}

	_beta[-1] = newTableProb();
	assertProb(_beta[-1]);

	if(uni)
	{
		//unigram case
		foreach(intProbMap, pi, _beta)
		{
			pi->second *= pc;
		}
		_beta[0] = 1 - pc;
	}
}

// void CRP::getMeanBeta()
// {
// 	_beta.clear();

// 	intIntMap* lexicon;
// 	{
// 		CRP* par = this;
// 		while(par != NULL)
// 		{
// 			lexicon = &par->_counts;
// 			par = dynamic_cast<CRP*>(par->_prior);
// 		}
// 	}

// 	foreach(intIntMap, word, *lexicon)
// 	{
// 		//assert(word->second > 0); //note: untrue for finite distr subclass
// 		//makes dealing with deletions hard
// 		//Prob pr = probNoG0(word->first);
// 		Prob pr = prob(word->first);
// 		//assert(pr > 0); //XXX using a hack to deal with deletions
// 		_beta[word->first] = pr;
// 	}

// 	_beta[-1] = newTableProb();
// 	assertProb(_beta[-1]);
// }

Prob CRP::newTableProb()
{
	Prob pnew = (_alpha + (_nTables * _discount)) / (_alpha + _total);
	CRP* par = dynamic_cast<CRP*>(_prior);
	if(par)
	{
		pnew *= par->newTableProb();
	}
	if(_alpha == 0)
	{
		return 0;
	}
	else
	{
		assertProb(pnew);
	}
	return pnew;
}

void Table::print(ostream& os)
{
	os<<"("<<label<<") "<<count;
}

ostream& operator<<(ostream& os, CRP& crp)
{
	crp.print(os);
	return os;
}

ostream& operator<<(ostream& os, Table& tab)
{
	tab.print(os);
	return os;
}
