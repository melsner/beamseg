#include "DPEnumerator.h"

DPEnumerator::DPEnumerator(CRP& dis):
	distr(dis),
	_countPtr(0),
	_remaining(1)
{
	_cached.reserve(distr->_counts.size());
	_sortedCounts.reserve(distr->_counts.size());
	assert(distr->_discount == 0);

	foreach(intIntMap, ii, distr->_counts)
	{
		_sortedCounts.push_back(IntPriNode(ii->second, ii->first));
	}
	sort(_sortedCounts.begin(), _sortedCounts.end(), IntGreater());
}

DPEnumerator::~DPEnumerator()
{
}

DPIterator DPEnumerator::iter(Prob pr)
{
	return DPIterator(this, pr);
}

bool DPEnumerator::next()
{
	if(_countPtr < _sortedCounts.size())
	{
		IntPriNode& item = _sortedCounts[_countPtr];

		//break the next stick
		double bi = gsl_ran_beta(RNG, 1, distr->_alpha);
		Prob pr = bi * _remaining;
		_remaining *= (1 - pr);

		_cached.push_back(IntProb(item.first, pr));

		++_countPtr;

		return true;
	}

	return false;
}

DPIterator::DPIterator(DPEnumerator* dp, Prob st):
	dpen(dp),
	stop(st),
	cached(0),
	weight(1.0)
{
}

DPIterator::DPIterator(DPEnumerator* dp, Prob st, Prob wt):
	dpen(dp),
	stop(st),
	cached(0),
	weight(wt)
{
}

void DPIterator::next()
{
	do
	{
		step();
	}
	while(value().second < stop);
}

void DPIterator::step()
{
	total += value().second;

	if(1 - total < stop)
	{
		return;
	}

	if(cached != -1)
	{
		if(cached >= dpen->cached.size())
		{
			if(!dpen->next())
			{
				cached = -1;
				prior = dpen->priorIterator();
			}
			else
			{
				++cached;
			}
		}
		else
		{
			++cached;
		}
	}

	if(cached == -1 && prior != NULL)
	{
		prior->next();
	}
}

bool DPIterator::done()
{
	return ((1 - total) < stop &&
			(cached == -1 &&
			 (prior == NULL || prior->done())));
}

IntProb DPIterator::value()
{
	IntProb it(-1, -1);
	if(cached != -1)
	{
		it = dpen->cached[cached];
	}
	else if(prior != NULL)
	{
		it = prior->value();
	}
	else
	{
		assert(done());
		assert(0); //dereference of invalid iterator
	}

	return IntProb(it.first, weight * it.second);
}


