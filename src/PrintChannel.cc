#include "common.h"
#include "SegSampler.h"
#include "Monkey.h"
#include "BigramMonkey.h"
#include "NGramMonkey.h"
#include "FiniteDist.h"
#include "State.h"
#include "AnnealRate.h"

#include <boost/program_options.hpp>

int main(int argc, char* argv[])
{
	appInit(DATA_PATH);

	namespace po = boost::program_options;

	po::options_description desc("Print a channel transducer");
	desc.add_options()
		("help", "show options")
		("channel", po::value<string>(),
		 "channel type: noiseless, xsub, phone")
		("read-channel", po::value<string>(),
		 "read channel parameters from this file")
		;

	po::positional_options_description p;
	p.add("read-channel", -1);

	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv).
			  options(desc).positional(p).run(), vm);
	po::notify(vm);

	if(vm.count("help"))
	{
		cout<<desc<<"\n";
		return 1;
	}

	string channelType = "phone";

	if(vm.count("channel"))
	{
		channelType = vm["channel"].as<string>();
	}

	cerr<<"Channel type: "<<channelType<<"\n";

	StrVocab alpha;

	Channel* channel = NULL;
	if(channelType == "noiseless")
	{
		channel = new Channel();
	}
	else if(channelType == "xsub")
	{
		channel = new XChannel(.1, alpha.get("x", true));
	}
	else if(channelType == "phone")
	{
		PhoneChannel* ph = new PhoneChannel(alpha);
		if(vm.count("read-channel"))
		{
			string channelFile = vm["read-channel"].as<string>();
			cerr<<"Reading channel parameters from "<<channelFile<<"\n";
			ifstream ifs(channelFile.c_str());
			assert(ifs.is_open());
			ph->read(ifs);
		}
		else
		{
			cerr<<"Requires a channel parameter file\n";
			return 1;
		}
		channel = ph;
	}
	else
	{
		cerr<<"Unknown channel type "<<channelType<<"\n";
		return 1;
	}

	if(channelType == "phone")
	{
		PhoneChannel* ph = dynamic_cast<PhoneChannel*>(channel);
		ph->print(cout);

		double* wts = ph->learner.weights();
		for(int ii = 0; ii < ph->learner.dimension(); ++ii)
		{
			if(wts[ii] != 0)
			{
				cout<<wts[ii]<<" "<<ph->fset.inv(ii)<<"\n";
			}
		}
	}
}
