#include "common.h"
#include "Monkey.h"

int main(int argc, char* argv[])
{
	appInit(DATA_PATH);

	StrVocab alpha;
	for(char aa = 'a'; aa <= 'z'; ++aa)
	{
		string si;
		si += aa;
		alpha.get(si, true);
	}

	Monkey simian(.1, alpha);
	for(int ii = 0; ii < 25; ++ii)
	{
		int str = simian.draw();
		cerr<<simian.repr(str)<<" p= "<<simian.prob(str)<<"\n";
	}
}
