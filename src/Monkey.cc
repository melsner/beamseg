#include "Monkey.h"

Monkey::Monkey(Prob stop, bool allowEmpty, Prob pEnd,
			   StrVocab& alphabet):
	_stop(stop),
	_allowEmpty(allowEmpty),
	_pEnd(pEnd),
	_alphabet(&alphabet)
{
}

Monkey::~Monkey()
{
}

Monkey* Monkey::copy()
{
	return new Monkey(*this);
}

int Monkey::intern(SymbolString& ss)
{
	return _lexicon.get(ss, true);
}

int Monkey::sample()
{
	SymbolString res(*_alphabet);
	bool canMakeEmpty = false;

	while(!canMakeEmpty && res.empty()) //check we've generated some symbols
	{
		while(true) //generate some chars
		{
			Prob sm = gsl_rng_uniform(RNG);
			if(sm <= _stop)
			{
				break;
			}
			res.push_back(randomSym());
		}

		if(_allowEmpty)
		{
			canMakeEmpty = true;
		}
	}

	int intern = _lexicon.get(res, true);
	return intern;
}

Prob Monkey::update(int sample, double temp)
{
	return prob(sample);
}

Prob Monkey::remove(int sample, double temp)
{
	return prob(sample);
}

Prob Monkey::prob(int sample)
{
	SymbolString& unintern = _lexicon.inv(sample);
	int nSyms = unintern.size();
	if(nSyms == 0) //the empty string
	{
		if(_allowEmpty) //as an actual string
		{
			return _stop;
		}
		else //as start-end (no! even as end token this causes issues)
		{
			// cerr<<"empty str "<<_pEnd<<"\n";
			return _pEnd;
		}
	}

	Prob pSym = (1 - _stop) * (1.0 / _alphabet->size());

	//oddly, I get an include error when using the macro here...
	assert(isProb(pSym));

	Prob res;
	if(_allowEmpty)
	{
		res = powl(pSym, nSyms) * _stop;
	}
	else
	{
		//if empty isn't allowed, first char is guaranteed
		res = powl(pSym, nSyms - 1) * (1.0 / _alphabet->size()) * _stop;
	}

	res *= (1 - _pEnd); //not the empty str
	assert(isProb(res));

	//can underflow quite easily, actually
	if(res == 0)
	{
		res = EPSILON;
	}

	// cerr<<" alpha size "<<_alphabet->size()<<" ps "<<pSym<<"\n";
	// cerr<<"Monkey: prob of "<<unintern<<" = "<<res<<" "<<logl(res)<<"\n";

	return res;
}

SymbolString& Monkey::repr(int sample)
{
	return _lexicon.inv(sample);
}

int Monkey::randomSym()
{
	Prob sm = gsl_rng_uniform(RNG);
	Prob total = _alphabet->size();

	int progress = 0;
	//breaks abstraction... whatever
	foreach(strIntMap, ch, _alphabet->_vocab)
	{
		progress += 1;

		if(sm <= progress / total)
		{
			return ch->second;
		}
	}

	assert(0);
}
