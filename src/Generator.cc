#include "Generator.h"

Generator::Generator()
{
}

Generator::~Generator()
{
}

int Generator::draw()
{
	int chosen = sample();
	update(chosen);
	return chosen;
}

Prob Generator::update(int sample, double temp)
{
	return 1.0;
}

Prob Generator::remove(int sample, double temp)
{
	return 1.0;
}

Prob Generator::prob(int sample)
{
	return 1.0;
}

int Generator::sample()
{
	return 0;
}

void Generator::print(ostream& os)
{
}

bool Generator::canRemove(int sample)
{
	return true;
}

LogProb Generator::ll(bool verbose)
{
	return 0;
}

ostream& operator<<(ostream& os, Generator& gen)
{
	gen.print(os);
	return os;
}

Counter::Counter(int start):
	_next(start)
{
}

Counter::~Counter()
{
}

int Counter::sample()
{
	return _next;
}

Prob Counter::update(int sample, double temp)
{
	_next = sample + 1;
	return 1;
}

Prob Counter::remove(int sample, double temp)
{
	return 1;
}

Prob Counter::prob(int sample)
{
	return 1.0;
}

Uniform::Uniform(int start, int end):
	_start(start),
	_end(end),
	_n(end - start)
{
}

Uniform::~Uniform()
{
}

int Uniform::sample()
{
	Prob sm = gsl_rng_uniform(RNG);

	int progress = 0;
	for(int ii = _start; ii < _end; ++ii)
	{
		progress += 1;

		if(sm <= progress / (double)_n)
		{
			return ii;
		}
	}

	assert(0);
}

Prob Uniform::update(int sample, double temp)
{
	return prob(sample);
}

Prob Uniform::remove(int sample, double temp)
{
	return prob(sample);
}

Prob Uniform::prob(int sample)
{
	if(sample < _start || sample >= _end)
	{
		return 0;
	}
	return 1.0 / (double)_n;
}
