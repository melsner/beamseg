#include "common.h"
#include "Monkey.h"
#include "CRPLM.h"

#include <boost/program_options.hpp>

int main(int argc, char* argv[])
{
	appInit(DATA_PATH);

	namespace po = boost::program_options;

	po::options_description desc("Generate text");
	desc.add_options()
		("help", "show options")
		("grams", po::value<int>(), "ngram order")
		("break", po::value<double>(), "probability of word break")
		("alpha", po::value< std::vector<double> >(), "CRP alphas")
		("discount", po::value< std::vector<double> >(), "CRP discount")
		("num", po::value<int>(), "number of sequences")
		("output", po::value<string>(), 
		 "file stem to output to (creates output.underlying, output.surface)")
		;

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if(vm.count("help"))
	{
		cout<<desc<<"\n";
		return 1;
	}

	int grams = 1;

	if(vm.count("grams"))
	{
		grams = vm["grams"].as<int>();
		assert(grams > 0);
		cerr<<"Selected "<<grams<<"-grams\n";
	}

	double breakProb = .3;
	doubles crpAlpha(grams, 4);
	doubles crpDiscount(grams, 0);
	int numSeqs = 25;
	string output = "output";
	string baseType = "monkey";

	if(vm.count("break"))
	{
		breakProb = vm["break"].as<double>();
	}

	if(vm.count("alpha"))
	{
		crpAlpha = vm["alpha"].as< std::vector<double> >();
	}

	for(int ii = 0; ii < crpAlpha.size(); ++ii)
	{
		cerr<<"Alpha: "<<ii<<" = "<<crpAlpha[ii]<<"\n";
	}		

	if(vm.count("discount"))
	{
		crpDiscount = vm["discount"].as< std::vector<double> >();
	}

	for(int ii = 0; ii < crpDiscount.size(); ++ii)
	{
		cerr<<"Discount: "<<ii<<" = "<<crpDiscount[ii]<<"\n";
	}		

	if(vm.count("num"))
	{
		numSeqs = vm["num"].as<int>();
	}

	if(vm.count("output"))
	{
		output = vm["output"].as<string>();
	}

	StrVocab alpha;
	//vocab is a-z
	for(char aa = 'a'; aa <= 'z'; ++aa)
	{
		string si;
		si += aa;
		alpha.get(si, true);
	}

	Monkey* simian = NULL;
	if(baseType == "monkey")
	{
		simian = new Monkey(breakProb, false, alpha);
	}
	else
	{
		cerr<<"Unknown base distribution "<<baseType<<"\n";
		return 1;
	}

	SymbolString startSym(alpha);
	int start = simian->intern(startSym);
	assert(start == 0);

	CRPLM model(grams, crpAlpha, crpDiscount, simian);

	string underName = output + ".underlying";
	ofstream underlying(underName.c_str());

	string surfName = output + ".surface";
	ofstream surface(surfName.c_str());

	SegSampler sampler(model);

	for(int seqs = 0; seqs < numSeqs; ++seqs)
	{
		ints seq;
		model.drawSeq(seq);

		SymbolString allSurf(alpha);
		int nWords = seq.size();
		ints breaks;
		int total = 0;
		for(int ii = 0; ii < nWords; ++ii)
		{
			FST under;
			// cerr<<model.repr(seq[ii])<<" ";

			kit.stringToFst(model.repr(seq[ii]), under);

			FST lattice;
			Compose(under, misspeller, &lattice);

			FST sample;
			RandGen(lattice, &sample,
					RandGenOptions<LogProbArcSelector<FSTArc> >(
						selector));
			// lattice.Write("lattice.fst");
			// sample.Write("sample.fst");
			SymbolString* surfStr = kit.fstToString(sample, false);

			allSurf.appendRange(*surfStr, 0, surfStr->size());
			total += surfStr->size();
			delete surfStr;
			if(ii + 1 < nWords)
			{
				breaks.push_back(total);
			}
		}

		// cerr<<"\n";
		// cerr<<"Surf: "<<allSurf<<"\n";

		Analysis* ana = new Analysis(seqs, allSurf, &model);
		int ctr = 0;
		foreach(ints, breakp, breaks)
		{
			ana->split(*breakp);
			ana->segAt(*breakp - 1)->str = model.repr(seq[ctr]);
			ana->segAt(*breakp - 1)->strSym = seq[ctr];
			++ctr;
		}
		ana->segAt(ana->nSyms() - 1)->str = model.repr(seq[ctr]);
		ana->segAt(ana->nSyms() - 1)->strSym = seq[ctr];

		cerr<<*ana;

		sampler.add(ana);
	}

	sampler.reapVocab();

	cerr<<"--- generated lexicon --- \n";
	foreach(intIntMap, word, model.lexicon())
	{
		cerr<<model.repr(word->first)<<"\n";
	}

	sampler.writeUnderlying(underlying);
	sampler.writeSurface(surface);
}
