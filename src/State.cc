#include "State.h"
#include "SegSampler.h"
#include "MonkeyState.h"

State::State(int ind, int pv, Analysis& analysis):
	index(ind),
	prevWord(pv),
	chars(*analysis.model->_prior->_alphabet),
	uu(0),
	prob(-INFINITY),
	corr(-INFINITY),
	suffix(NULL),
	path(NULL),
	ana(analysis)
{
}

State::~State()
{
	//if prevword = -1, analysis owns suffixtree
	if(newWord() && suffix != NULL && prevWord != -1)
	{
		delete suffix;
	}
}

void State::wipe()
{
	// cerr<<"wiping "<<*this<<" "<<this<<"\n";

	if(newWord() && suffix != NULL && prevWord != -1)
	{
		delete suffix;
	}

	uu = 0;
	prob = -INFINITY;
	corr = -INFINITY;
	suffix = NULL;
	successors.clear();
	predecessors.clear();
}

State* State::canonicalize(int prev, SymbolString& currStr)
{
	assert(end() || path != NULL);
	if(!end())
	{
		if(prevWord == -1)
		{
			State* replaceMe = ana.getState(index, prev, chars, chars.size());
			replaceMe = ana.registerState(replaceMe);
			replaceMe->path = path->canonicalize(prev, currStr);
			path = NULL;
			return replaceMe;
		}
		else
		{
			State* nxt = path->canonicalize(prevWord, currStr);
			assert(nxt != this);
			path = nxt;
		}
	}

	return this;
}

bool State::less(const State& other) const
{
	if(index < other.index)
	{
		return true;
	}
	else if(index > other.index)
	{
		return false;
	}
	else if(sortType() < other.sortType())
	{
		return true;
	}
	else if(other.sortType() < sortType())
	{
		return false;
	}

		else if(newWord() && !other.newWord())
	{
		//make sure recognize state is shuffled to last position
		return false;
	}
	else if(other.newWord() && !newWord())
	{
		//make sure recognize state is shuffled to last position
		return true;
	}
	else if(chars.size() < other.chars.size())
	{
		return true;
	}
	else if(other.chars.size() < chars.size())
	{
		return false;
	}
	else if(chars < other.chars)
	{
		return true;
	}
	else if(chars > other.chars) //could be computed alongside <
	{
		return false;
	}
	else if(prevWord < other.prevWord)
	{
		return true;
	}
	else if(prevWord > other.prevWord)
	{
		return false;
	}

	{
		const InsertState* ptr = dynamic_cast<const InsertState*>(this);
		const InsertState* ptr2 = dynamic_cast<const InsertState*>(&other);
		if(ptr && !ptr2)
		{
			return false;
		}
		else if(ptr2 && !ptr)
		{
			return true;
		}
	}

	return false;
}

int State::sortType() const
{
	return newWord() ? SORT_REC : SORT_EMIT;
}

void State::print(ostream& os)
{
	if(prevWord != -1)
	{
		os<<ana.model->repr(prevWord);
	}
	else
	{
		os<<"UNK";
	}
	os<<"|"<<chars<<" ";
	os<<prob<<" "<<corr<<" ";
	os<<"st:"<<index<<" ";
	os<<"["<<uu<<"] ";

	// os<<this<<" ";
	// if(suffix != NULL)
	// {
	// 	suffix->print(cerr, *ana.model->_prior->_alphabet);
	// }
}

void State::printLattice(ostream& os)
{
	printLattice1(os, 0);
}

void State::printLattice1(ostream& os, int depth)
{
	for(int di = 0; di < depth; ++di)
	{
		os<<" ";
	}
	print(os);
	os<<"\n";

	foreach(StateToProb, nxt, successors)
	{
		nxt->first->printLattice1(os, depth + 1);
	}
}

void State::linkTo(State* st, Prob pr, Prob ucorr)
{
	if(!isProb(pr) || pr <= 0)
	{
		cerr<<"Failed link from "<<*this<<" to "<<*st<<" pr "<<pr<<"\n";
	}
	assert(isProb(pr) && pr > 0);
	if(!isfinite(ucorr) || ucorr <= 0)
	{
		cerr<<"Failed link from "<<*this<<" to "<<*st<<" corr "<<ucorr<<"\n";
	}
	assert(isfinite(ucorr) && ucorr > 0);

	assert(this != st);
	successors[st] = pr * ucorr;
	st->predecessors[this] = pr * ucorr;

	if(!std::isfinite(st->prob))
	{
		st->prob = logl(pr) + prob;
		st->corr = logl(ucorr * pr) + corr;
	}
	else
	{
		logplusequals(st->prob, logl(pr) + prob);
		logplusequals(st->corr, logl(ucorr * pr) + corr);
	}
}

void State::setU(Prob uuNew, CRP* distr)
{
	uu = uuNew;
}

bool State::newWord() const
{
	return chars.empty();
}

bool State::end()
{
	return index == ana.surface.size() + 1;
}

void State::expand()
{
	bool verbose = ana.verbose & DBG_STATES;

	if(!isfinite(prob))
	{
		if(ana.model->_alpha[0] > 0)
		{
			cerr<<"Failed state "<<*this<<"\n";
			assert(0);
		}

		return;
	}

	//always do a u reset
	{
		Prob newU = ana.getULimit(this, NULL);
		setU(newU, NULL);

		Prob suffPr = (suffix ? suffix->bestString() : 1.0);
//XXX allowed?		assert(suffPr > 0);
		Prob heurPr = prob + logl(suffPr);
		//altered XXX was prob
		if(corr < uu)
		{
			if(verbose)
			{
				cerr<<*this<<" (suffix "<<suffPr<<" heur "<<heurPr<<") "<<
					"is out of the beam\n";
			}
			return;
		}
	}

	if(newWord())
	{
		//if I am a new word state X|
		if(verbose)
		{
			cerr<<"Recognized word: ";
			if(prevWord != -1)
			{
				cerr<<ana.model->repr(prevWord);
			}
			else
			{
				cerr<<"UNK";
			}
			cerr<<"\n";
		}

		intLst ctxt;
		if(ana.model->_ngrams == 2)
		{
			ctxt.push_back(prevWord);
		}
		CRP* proc = ana.model->getProcessOrEmpty(ctxt);
		if(prevWord == -1 && ana.model->_ngrams > 1)
		{
			assert(proc->_total == 0);
		}

		//set a U
		// used to have: if(uu == 0)
		// {
		// 	Prob newU = ana.getULimit(this, proc);
		// 	setU(newU, proc);
		// }

		if(verbose)
		{
			cerr<<"U: "<<uu<<"\n";
		}

		intProbMap& beta = (ana.integrateOut ? 
							proc->meanBeta(ana.model->_ngrams == 1,
										   ana.model->posteriorPCont()) :
							proc->beta());
		Prob wordU = ana.wordUs[index];

		//build the suffix tree at this point
		if(prevWord != -1)
		{
			suffix = new LazySuffixTree(chars.alphabet().size(),
										0, &ana.model->_prior->_lexicon);
//			suffix = new SuffixTree();

			int insertCt = 0;
			intSet& allowed = ana.wordsAllowed[index];
			foreach(intSet, wi, allowed)
			{
				Prob pr = beta[*wi];
				if(pr == 0)
				{
					if(ana.model->_alpha[0] > 0)
					{
						cerr<<"WARN zeroprob "<<ana.model->repr(*wi)<<
							" index "<<*wi<<
							" after "<<ana.model->repr(prevWord)<<" "<<
							pr<<"\n";
						cerr<<proc->_total<<" entries in rst "<<ctxt.size()<<"\n";
						// assert(0);
					}
					else
					{
						continue;
					}
				}

				if(pr > wordU)
				{
					Prob uCorr = ana.pWordU(index, pr);
					pr = powl(pr, ana.invTemp);
					suffix->insert(*wi, pr, uCorr);
					++insertCt;

					// SymbolString& word = ana.model->repr(bi->first);
					// suffix->insert(word, pr);

					if(verbose)
					{
						SymbolString& word = ana.model->repr(*wi);
						cerr<<"inserting "<<word<<" pr "<<pr<<"\n";
						if(!inMap(ana.model->lexicon(), *wi) && *wi != 0)
						{
							cerr<<"in beta/not in lexicon "<<word<<"\n";
							assert(0);
						}
					}
				}
			}
			// cerr<<"Inserted "<<insertCt<<" strings\n";
		}
		else
		{
			//this tree is pre-built without the cutoff
			//but the successor constructor will take care of cutting
			//them off
			suffix = ana.unkTree(index);
		}

		// if(verbose)
		// {
		// 	suffix->print(cerr, chars.alphabet());
		// }

		//check if I have a monkey successor
		// (this method of handling g0 is inexact and should
		// not be used if a new state is likely to ocurr more than one
		// in the same sequence)

		//*don't* charge the stop cost right off
		//b/c different monkey impls. might do it differently
		Prob pr = beta[-1];
		if(pr > wordU)
		{
			Prob uCorr = ana.pWordU(index, pr);
			pr = powl(pr, ana.invTemp);
			State* succ = ana.getMonkey(index, -1, -1);
			assert(succ->prevWord != prevWord);
			assert(less(*succ));
			State* regSucc = ana.registerState(succ);
			linkTo(regSucc, pr, uCorr);
			// cerr<<"after link "<<*succ<<" "<<succ<<"\n";
		}
	}

	if(index == ana.surface.size() && newWord() && suffix->final())
	{
		//link all end states to a single sink state
		State* final = ana.getState(index + 1, 0, chars, 0);
		final = ana.registerState(final);
		Prob pr = suffix->final();
		Prob corr = suffix->finalCorr();
		linkTo(final, pr, corr);

		if(verbose)
		{
			cerr<<"link to final state "<<pr<<"\n";
		}

		return;
	}

	//enumerate a successor state for each item in the suffix tree
	assert(suffix != NULL);
	LazySuffixTrees& succs = suffix->successors();
	int sz = succs.size();
	int prevCh = chars.empty() ? -1 : chars[chars.size() - 1];
	Prob eps = ana.model->_channel->epsilonAfter(prevCh);
	for(int nxt = 0; nxt < sz; ++nxt)
	{
		if(succs[nxt] == NULL)
		{
			continue;
		}

		if(index < ana.surface.size())
		{
			Prob charProb = ana.model->_channel->emitCh(nxt, 
													   ana.surface[index]);
			Prob emitProb = charProb * (1 - eps);
			Prob localU = ana.getLocalU(index + 1);

			if(verbose)
			{
				cerr<<"emit "<<emitProb<<" local u "<<localU<<"\n";
			}

			if(emitProb > localU && 
			   (!ana.lockBounds || newWord() || !ana.boundaryAt(index)))
			{
				Prob uCorr = ana.pLocalU(index + 1, emitProb);
				// emitProb = powl(emitProb, ana.channelInvTemp);
				// uCorr = powl(uCorr, ana.channelInvTemp);
				State* succ = ana.getState(index + 1, prevWord, chars, 
										   chars.size(), nxt);
				assert(less(*succ));

				succ = ana.registerState(succ);
				succ->suffix = succs[nxt];
				linkTo(succ, emitProb, uCorr);

				if(verbose)
				{
					Prob succSuff = (succ->suffix ? 
									 succ->suffix->bestString() : 1.0);
					Prob succHeur = succ->prob + logl(succSuff);

					cerr<<"produced state "<<*succ<<" suff "<<succSuff<<
						" heur "<<succHeur<<"\n";
				}
				// ++produced;
			}
		}
		// cerr<<"Stree branch factor "<<produced<<"\n";

		//create a successor in which the next underlying char is deleted
		//////// no deletions? add 0 to condition below
		if(ana.allowDeletions)
		{
			bool isPost = newWord() || sortType() == SORT_POSTDELETE;

			Prob charProb = ana.model->_channel->emitCh(nxt, -1);
			Prob emitProb = charProb * (1 - eps);
			Prob localU = ana.getLocalU(index);

			if(verbose)
			{
				cerr<<"emit "<<emitProb<<" local u "<<localU<<"\n";
			}

			if(emitProb > localU)
			{
				Prob uCorr = ana.pLocalU(index, emitProb);
				// emitProb = powl(emitProb, ana.channelInvTemp);
				// uCorr = powl(uCorr, ana.channelInvTemp);
				State* succ = ana.getDeleteState(index, prevWord, chars,
												 chars.size(), nxt,
												 isPost);
				assert(less(*succ));
				succ = ana.registerState(succ);
				succ->suffix = succs[nxt];
				linkTo(succ, emitProb, uCorr);
//				if(verbose){cerr<<"Pass local u "<<succ<<" "<<*succ<<"\n";}
			}
		}
	}

	//create a successor in which the next surface char is inserted
	if(index < ana.surface.size() &&
	   (!ana.lockBounds || newWord() || !ana.boundaryAt(index)))
	{
		Prob charProb = ana.model->_channel->emitCh(-1,
												   ana.surface[index]);
		Prob emitProb = eps * charProb;
		Prob localU = ana.getLocalU(index + 1);

		if(verbose)
		{
			cerr<<"emit "<<emitProb<<" local u "<<localU<<"\n";
		}

		if(emitProb > localU)
		{
			Prob uCorr = ana.pLocalU(index + 1, emitProb);
//			emitProb = powl(emitProb, ana.channelInvTemp);
			// uCorr = powl(uCorr, ana.channelInvTemp);
			State* succ = ana.getInsertState(index + 1, prevWord, chars, 
											 chars.size());
			assert(less(*succ));
			succ = ana.registerState(succ);
			succ->suffix = suffix;
			linkTo(succ, emitProb, uCorr);
//			if(verbose){cerr<<"Pass local u "<<succ<<" "<<*succ<<"\n";}
		}
	}

	//check if the suffix tree licenses me to end
	if((suffix->final() > 0 && !chars.empty() && sortType() != SORT_POSTDELETE)
	   && (!ana.lockBounds || ana.boundaryAt(index)))
	{
		int recogWord = ana.model->intern(chars);
		if(!inMap(ana.model->lexicon(), recogWord))
		{
			cerr<<"recognized unk word "<<chars<<"\n";
			cerr<<"in beta "<<inMap(
				ana.model->_root->meanBeta(ana.model->_ngrams == 1,
										   ana.model->posteriorPCont()), 
									recogWord)<<"\n";
			assert(0); //can't happen b/c monkey handles these
			recogWord = -1;
		}
		State* succ = ana.getState(index, recogWord, chars, 0);
		assert(less(*succ));
		succ = ana.registerState(succ);
		Prob pr = suffix->final();
		linkTo(succ, pr, 1.0);
	}
}

Prob State::emit()
{
	// int chr = emitted();
	// cerr<<"emitting "<<(chr == -1 ? "-1" : chars.alphabet().inv(chr))<<
	// 	" given "<<chars.alphabet().inv(chars[chars.size() - 1])<<" = ";
	// cerr<<ana.model->_channel->emit(this, chr)<<"\n";
	return ana.model->_channel->emit(this, emitted());
}

int State::emitted()
{
	return ana.surface[index - 1];
}

int State::lastChar()
{
	return chars[chars.size() - 1];
}

State* State::sample()
{
	// assertLogProb(prob);

	Prob mx = -INFINITY;
	int sz = predecessors.size();
	Prob pi[sz];
	State* states[sz];
	int ctr = 0;
	foreach(StateToProb, pred, predecessors)
	{
		LogProb term = pred->first->corr + logl(pred->second);
		pi[ctr] = term;
		states[ctr] = pred->first;
		++ctr;

		if(term > mx)
		{
			mx = term;
		}
	}
	assert(isfinite(mx));

	// cerr<<*this<<"sampling back\n";

	Prob total = 0;
	for(int ii = 0; ii < sz; ++ii)
	{
		pi[ii] = expl(pi[ii] - mx);
		// cerr<<"\tpredecessor "<<*states[ii]<<" pr "<<pi[ii]<<"\n";
		total += pi[ii];
	}

	double sample = gsl_ran_flat(RNG, 0, total);
	Prob progress = 0;
	for(int ii = 0; ii < sz; ++ii)
	{
		progress += pi[ii];
		if(progress >= sample)
		{
			assert(states[ii] != this);
			states[ii]->path = this;
			return states[ii];
		}
	}

	assert(0);
	return NULL;
}

LogProb State::pathProb(State* from)
{
//	assertLogProb(prob);

	// {
	// 	cerr<<"All the predecessors for "<<*this<<"\n";
	// 	foreach(StateToProb, pred, predecessors)
	// 	{
	// 		cerr<<"\t"<<*(pred->first)<<"\n";
	// 	}
	// }

	Prob mx = -INFINITY;
	int sz = predecessors.size();
	Prob pi[sz];
	int chosen = -1;
	int ctr = 0;
	foreach(StateToProb, pred, predecessors)
	{
		LogProb term = pred->first->corr + logl(pred->second);
		pi[ctr] = term;
		if(pred->first == from)
		{
			chosen = ctr;
		}
		++ctr;

		if(term > mx)
		{
			mx = term;
		}
	}
	assert(isfinite(mx));

	if(chosen == -1)
	{
		cerr<<"Can't find link between "<<*from<<" and "<<*this<<"\n";
	}
	assert(chosen != -1);

	// cerr<<"chosen max "<<mx<<"\n";

	Prob total = 0;
	for(int ii = 0; ii < sz; ++ii)
	{
		pi[ii] = expl(pi[ii] - mx);
		total += pi[ii];
	}

	// {
	// 	StateToProb::iterator pred = predecessors.begin();
	// 	for(int ii = 0; ii < sz; ++ii)
	// 	{
	// 		if(ii == chosen)
	// 		{
	// 			cerr<<"*** ";
	// 		}
	// 		cerr<<"pr "<<ii<<" "<<(pi[ii]/total)<<" "<<
	// 			*(pred->first)<<" "<<logl(pred->second)<<"\n";
	// 		++pred;
	// 	}
	// 	cerr<<"Computing path prob "<<*from<<" -> "<<*this<<" is "<<
	// 		pi[chosen] / total<<"\n";
	// }

	return logl(pi[chosen] / total);
}

LogProb State::channelCost()
{
	if(end())
	{
		return 0;
	}
	else if(newWord())
	{
		return path->channelCost();
	}
	else
	{
		if(ana.verbose & DBG_MHR)
		{
			Prob pEm = emit();
			cerr<<"\t"<<*this<<" emit "<<pEm<<" log "<<logl(pEm)<<"\n";
		}

		return logl(emit()) + path->channelCost();
	}
}

DeleteState::DeleteState(int ind, int pv, bool post, Analysis& analysis):
	State(ind, pv, analysis),
	postDelete(post)
{
}

DeleteState::~DeleteState()
{
}

void DeleteState::print(ostream& os)
{
	if(prevWord != -1)
	{
		os<<ana.model->repr(prevWord);
	}
	else
	{
		os<<"UNK";
	}
	os<<"|"<<chars<<"* ";
	os<<prob<<" "<<corr<<" ";
	os<<"st:"<<index<<" ";
	os<<"["<<uu<<"] ";
	// if(postDelete)
	// {
	// 	cerr<<"POST ";
	// }
	// else
	// {
	// 	cerr<<"PRE ";
	// }
}

int DeleteState::sortType() const
{
	return postDelete ? SORT_POSTDELETE : SORT_DELETE;
}

int DeleteState::emitted()
{
	return -1;
}

State* DeleteState::canonicalize(int prev, SymbolString& currStr)
{
	assert(end() || path != NULL);
	if(!end())
	{
		if(prevWord == -1)
		{
			State* replaceMe = ana.getDeleteState(
				index, prev, chars, chars.size(), -1, postDelete);
			replaceMe = ana.registerState(replaceMe);
			replaceMe->path = path->canonicalize(prev, currStr);
			path = NULL;
			return replaceMe;
		}
		else
		{
			State* nxt = path->canonicalize(prevWord, currStr);
			assert(nxt != this);
			path = nxt;
		}
	}

	return this;
}

InsertState::InsertState(int ind, int pv, Analysis& analysis):
	State(ind, pv, analysis)
{
}

InsertState::~InsertState()
{
}

void InsertState::print(ostream& os)
{
	if(prevWord != -1)
	{
		os<<ana.model->repr(prevWord);
	}
	else
	{
		os<<"UNK";
	}
	os<<"|"<<chars<<"$ ";
	os<<prob<<" "<<corr<<" ";
	os<<"st:"<<index<<" ";
	os<<"["<<uu<<"] ";
}

int InsertState::lastChar()
{
	return -1;
}

bool InsertState::newWord() const
{
	return false;
}

State* InsertState::canonicalize(int prev, SymbolString& currStr)
{
	assert(end() || path != NULL);
	if(!end())
	{
		if(prevWord == -1)
		{
			State* replaceMe = ana.getInsertState(
				index, prev, chars, chars.size());
			replaceMe = ana.registerState(replaceMe);
			replaceMe->path = path->canonicalize(prev, currStr);
			path = NULL;
			return replaceMe;
		}
		else
		{
			State* nxt = path->canonicalize(prevWord, currStr);
			assert(nxt != this);
			path = nxt;
		}
	}

	return this;
}

ostream& operator<<(ostream& os, State& st)
{
	st.print(os);
	return os;
}

bool operator<(State& aa, State& bb)
{
	return aa.less(bb);
}

bool operator<(const State& aa, const State& bb)
{
	return aa.less(bb);
}

bool deleteType(int xx)
{
	return xx == SORT_DELETE || xx == SORT_POSTDELETE;
}
