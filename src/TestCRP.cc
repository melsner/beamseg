#include "common.h"
#include "CRP.h"

int main(int argc, char* argv[])
{
	appInit(DATA_PATH);

	// CRP myCRP(1, new Counter());

	// for(int ii = 0; ii < 25; ++ii)
	// {
	// 	cout<<"Draw: "<<myCRP.draw()<<"\n";
	// 	cout<<myCRP<<"\n";
	// }

	// cout<<"---------------------\n";

	// myCRP.noNewTables();

	// for(int ii = 0; ii < 25; ++ii)
	// {
	// 	int si = myCRP.sample();
	// 	cout<<"Removing token: "<<si<<"\n";
	// 	myCRP.remove(si);
	// 	cout<<myCRP<<"\n";
	// }


	double dummy = 0;
	double d1 = 1;
	double d2 = 5;
	CRP upper(d1, new Counter(), dummy);
	CRP lower(d2, &upper, dummy);
	ints draws;

	for(int ii = 0; ii < 25; ++ii)
	{
		int di = lower.draw();
		draws.push_back(di);
		cout<<"Draw: "<<di<<"\n";
		cout<<lower;
		cout<<"Upper distr:\n"<<upper<<"\n";
	}

	cout<<"---------------------\n";

	// upper.noNewTables();

	random_shuffle(draws.begin(), draws.end());

	for(int ii = 0; ii < 25; ++ii)
	{
		int si = draws[ii];
		cout<<"Removing token: "<<si<<"\n";
		lower.remove(si);
		cout<<lower;
		cout<<"Upper distr:\n";
		cout<<upper<<"\n";
	}
}
