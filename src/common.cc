#include "common.h"

gsl_rng* RNG = NULL;
intStrStrMap STEM_CACHE;

void appInit(const string& data)
{
	RNG = gsl_rng_alloc(gsl_rng_taus);
	long int seed = time(NULL);

	// cerr<<"WARNING FIXED RNG!\n";
	// seed = 1;

	cerr<<"Seeding RNG: "<<seed<<"\n";
	gsl_rng_set(RNG, seed);
	srand(seed);
}

//downcase/upcase a string
string lc(const string& str)
{
	char* buf = new char[str.length()];
	str.copy(buf, str.length());
	for(int i = 0; i < str.length(); i++)
		buf[i] = tolower(buf[i]);
	string copy(buf, str.length());
	delete[] buf;
	return copy;
}

string uc(const string& str)
{
	char* buf = new char[str.length()];
	str.copy(buf, str.length());
	for(int i = 0; i < str.length(); i++)
		buf[i] = toupper(buf[i]);
	string copy(buf, str.length());
	delete[] buf;
	return copy;
}

bool endswith(const string& check, const string& suffix)
{
	return check.substr(check.size() - suffix.size(), suffix.size()) ==
		suffix;
}

int posToInt(const string& type)
{
#ifdef USE_WORDNET
	if(type[0] == 'N')
	{
		return NOUN;
	}
	else if(type[0] == 'J')
	{
		return ADJ;
	}
	else if(type[0] == 'R')
	{
		return ADV;
	}
	else if(type[0] == 'V' || type == "MD" || type == "AUX")
	{
		return VERB;
	}
	else
	{
		return NOUN; //stemmer won't work, closed-class word
	}
#else
	cerr<<"Warning: this function doesn't work without WORDNET.\n";
	return -1;
#endif
}

string oldStem(const string& str, int tag)
{
#ifdef USE_WORDNET
	char* sense = morphword(const_cast<char*>(str.c_str()), tag);
	if(sense != NULL)
	{
		string copy(sense);
		return copy;
	}
#else
	cerr<<"Warning: this function doesn't work without WORDNET.\n";
#endif
	return str;
}

string& stem(const string& str)
{
#ifdef USE_WORDNET
	strStrMap nounCache = STEM_CACHE[NOUN];
	strStrMap::iterator entry = nounCache.find(str);
	if(entry != nounCache.end())
	{
		return entry->second;
	}

	char* sense = morphword(const_cast<char*>(str.c_str()), NOUN);
	if(sense != NULL)
	{
		string copy(sense);
		nounCache[str] = copy;
	}
	else
	{
		nounCache[str] = str;
	}
	return nounCache[str];

#else
	cerr<<"Warning: this function doesn't work without WORDNET.\n";
	STEM_CACHE[0][str] = str;
	return STEM_CACHE[0][str];
#endif
}

bool isNum(const string& word)
{
	for(int i=0; i<word.length(); i++)
	{
		if(!isdigit(word[i]))
		{
			return false;
		}
	}
	return true;
}

bool isAlphaNum(const string& word)
{
	for(int i=0; i<word.length(); i++)
	{
		if(isdigit(word[i]))
		{
			return true;
		}
	}
	return false;
}

void randPermutation(ints& iperm, int size)
{
	iperm.clear();
	for(int i=0; i<size; i++)
	{
		iperm.push_back(i);
	}

	random_shuffle(iperm.begin(), iperm.end()); 	//STL random shuffle
}

void checkToken(istream& is, const string& str)
{
	string key;
	is>>key;
	if(key != str)
	{
		cerr<<"Expected "<<str<<", got "<<key<<"\n";
		assert(0);
	}
}

bool isIdentity(ints& perm)
{
	int size = perm.size();
	for(int i = 0; i < size; i++)
	{
		if(perm[i] != i)
		{
			return false;
		}
	}
	return true;
}

void sample_dirichlet(int sz, Prob* alpha, Prob* rv)
{
	//marginal beta method
	Prob remaining = 0;
	for(int ii = 0; ii < sz; ++ii)
	{
		remaining += alpha[ii];
	}

	Prob soFar = 0;
	for(int ii = 0; ii < sz - 1; ++ii)
	{
		remaining -= alpha[ii];
		double bi = gsl_ran_beta(RNG, alpha[ii], remaining);

		rv[ii] = bi * (1 - soFar);
		soFar += rv[ii];
	}

	rv[sz - 1] = (1 - soFar);
}

//normalized gammas method
// {
// 	double gi[sz];
// 	for(int ii = 0; ii < sz; ++ii)
// 	{
// 		gi[ii] = gsl_ran_gamma(RNG, alpha[ii], 1);
// 	}

// 	Prob total = 0;
// 	for(int ii = 0; ii < sz; ++ii)
// 	{
// 		total += gi[ii];
// 	}

// 	for(int ii = 0; ii < sz; ++ii)
// 	{
// 		rv[ii] = gi[ii] / total;
// 	}
// }
