#include "CRPLM.h"

CRPLM::CRPLM(int ngrams, doubles& alpha, doubles& discount, 
			 Monkey* makeWords, Channel* ch):
	_ngrams(ngrams),
	_alpha(alpha),
	_discount(discount),
	_prior(makeWords),
	_channel(ch),
	_nWords(0),
	_nUtts(0),
	_betaDate(0)
{
	assert(_ngrams >= 1);
	assert(_alpha.size() == _ngrams);
	assert(_discount.size() == _ngrams);

	_root = new CRP(_alpha[0], _prior, _discount[0]);
	_root->_betaDate = &_betaDate;
	// if(_ngrams > 1)
	// {
	// 	_root->update(0); //start/end symbol-- make sure we end sometimes
	// }

	_empty.resize(_ngrams, NULL);
}

CRPLM::~CRPLM()
{
	delete _root;
	delete _prior;
	if(_channel != NULL)
	{
		delete _channel;
	}

	foreach(CRPs, em, _empty)
	{
		if(*em != NULL)
		{
			delete *em;
		}
	}
}

CRPLM* CRPLM::copy()
{
	CRPLM* copy = new CRPLM(_ngrams, _alpha, _discount, _prior->copy(),
							_channel->copy());
	return copy;
}

void CRPLM::sampleSeq(ints& seq)
{
	seq.clear();

	while(seq.empty()) //keep going till we generate at least one word
	{
		intLst context;
		for(int ii = 1; ii < _ngrams; ++ii)
		{
			context.push_back(0); //kind of hacky? start sym is 0?
		}

		while(true)
		{
			CRP* givenPrev = getProcess(context);
			int nextWord = givenPrev->sample();

			if(nextWord == 0)
			{
				break;
			}

			if(_ngrams == 1)
			{
				double sm = gsl_rng_uniform(RNG);
				Prob posteriorPCont = 
					(_nWords - _nUtts + 2.0) / (_nWords + 3.0);
				if(sm > posteriorPCont)
				{
					break;
				}
			}

			seq.push_back(nextWord);

			context.push_back(nextWord);
			context.pop_front();
		}
	}
}

LogProb CRPLM::updateSeq(ints& seq, double temp, bool verbose)
{
	TableLst dummy;
	return updateSeq(seq, dummy, temp, verbose);
}

LogProb CRPLM::updateSeq(ints& seq, TableLst& tabs, double temp, bool verbose)
{
	LogProb res = 0;

	intLst context;
	for(int ii = 1; ii < _ngrams; ++ii)
	{
		context.push_back(0); //kind of hacky? start sym is 0?
	}

	int word = 0;

	foreach(ints, sym, seq)
	{
		if(verbose)
		{
			cerr<<"Updating production of "<<repr(*sym)<<" given ["<<
				context.size();
			foreach(intLst, ct, context)
			{
				cerr<<" "<<repr(*ct);
			}
			cerr<<"]";
		}

		CRP* givenPrev = getProcess(context);
		Table* rtab;
		LogProb term = logl(givenPrev->update(*sym, rtab, temp));
		if(_ngrams == 1)
		{
			Prob ppc = (_nWords + word - _nUtts + 1.0) / 
				(_nWords + word + 2.0);
			assertProb(ppc);
			term *= ppc;
		}
		tabs.push_back(rtab);
		res += term;

		if(verbose)
		{
			cerr<<"= "<<term<<"\n";
		}

		context.push_back(*sym);
		context.pop_front();

		word += 1;
	}

	if(_ngrams > 1)
	{
		CRP* givenPrev = getProcess(context);
		Table* rtab;
		LogProb term = logl(givenPrev->update(0, rtab, temp));
		tabs.push_back(rtab);
		res += term;

		if(verbose)
		{
			cerr<<"P(end sym) = "<<term<<"\n";
		}
	}
	else
	{
		Prob ppc = (_nWords + word - _nUtts + 1.0) / 
			(_nWords + word + 2.0);
		assertProb(ppc);
		res += logl(1 - ppc);
	}

	if(verbose)
	{
		cerr<<"Total: "<<res<<"\n";
	}

	_nWords += seq.size();
	_nUtts += 1;
	_uttLengths[seq.size()] += 1;

	return res;
}

void CRPLM::resampleTables(ints& seq, TableLst& tabs, 
						   double temp, bool verbose)
{
	for(int pass = 0; pass < 30; ++pass)
	{
		TableLst newTabs;
		TableLst::iterator ctr = tabs.begin();

		intLst context;
		for(int ii = 1; ii < _ngrams; ++ii)
		{
			context.push_back(0); //kind of hacky? start sym is 0?
		}

		foreach(ints, sym, seq)
		{
			if(verbose)
			{
				cerr<<"Resampling production of "<<repr(*sym)<<" given ["<<
					context.size();
				foreach(intLst, ct, context)
				{
					cerr<<" "<<repr(*ct);
				}
				cerr<<"]\n";
			}

			CRP* givenPrev = getProcess(context);
			Table* rtab;
			Table* tab = *ctr;
			++ctr;
			givenPrev->remove(*sym, tab);
			givenPrev->update(*sym, rtab, temp);
			newTabs.push_back(rtab);

			context.push_back(*sym);
			context.pop_front();
		}

		if(_ngrams > 1)
		{
			if(verbose)
			{
				cerr<<"Resampling production of end symbol given [";
				foreach(intLst, ct, context)
				{
					cerr<<" "<<repr(*ct);
				}
				cerr<<"]\n";
			}

			CRP* givenPrev = getProcess(context);
			Table* tab = *ctr;
			givenPrev->remove(0, tab);
			Table* rtab;
			givenPrev->update(0, rtab, temp);
			newTabs.push_back(rtab);
		}

		tabs = newTabs;
	}
}

void CRPLM::drawSeq(ints& seq)
{
	//slightly more efficient implementation possible
	sampleSeq(seq);
	updateSeq(seq, 1.0, false);
}

LogProb CRPLM::removeSeq(ints& seq, TableLst& tabs, double temp, bool verbose)
{
	LogProb res = 0;

	intLst context;
	for(int ii = 1; ii < _ngrams; ++ii)
	{
		context.push_back(0); //kind of hacky? start sym is 0?
	}

	_nWords -= seq.size();
	_nUtts -= 1;
	_uttLengths[seq.size()] -= 1;

	int word = 0;
	TableLst::iterator ctr = tabs.begin();
	foreach(ints, sym, seq)
	{
		if(verbose)
		{
			cerr<<"Removing production of "<<repr(*sym)<<" given ["<<
				context.size();
			foreach(intLst, ct, context)
			{
				cerr<<" "<<repr(*ct);
			}
			cerr<<"]  = ";
		}

		CRP* givenPrev = getProcess(context);
		// cerr<<givenPrev<<" proc\n";
		Table* tab = *ctr;
		++ctr;
		Prob term = givenPrev->remove(*sym, tab);
		if(_ngrams == 1)
		{
			Prob ppc = (_nWords + word - _nUtts + 1.0) / 
				(_nWords + word + 2.0);
			assertProb(ppc);
			assert(ppc != 0 && ppc != 1);
			term *= ppc;
		}
		res += logl(term);

		if(verbose)
		{
			cerr<<logl(term)<<"\n";
		}

		context.push_back(*sym);
		context.pop_front();

		word += 1;
	}

	if(_ngrams > 1)
	{
		CRP* givenPrev = getProcess(context);
		Table* tab = *ctr;
		LogProb term = logl(givenPrev->remove(0, tab));
		res += term;

		if(verbose)
		{
			cerr<<"P(end sym) = "<<term<<"\n";
		}
	}
	else
	{
		Prob ppc = (_nWords + word - _nUtts + 1.0) / 
			(_nWords + word + 2.0);
		assertProb(ppc);
		assert(ppc != 0 && ppc != 1);
		res += logl(1 - ppc);
	}

	if(verbose)
	{
		cerr<<"Total: "<<res<<"\n";
	}

	return res;	
}

LogProb CRPLM::removeSeq(ints& seq, double temp, bool verbose)
{
	assert(0);
	LogProb res = 0;

	intLst context;
	for(int ii = 1; ii < _ngrams; ++ii)
	{
		context.push_back(0); //kind of hacky? start sym is 0?
	}

	_nWords -= seq.size();
	_nUtts -= 1;
	_uttLengths[seq.size()] -= 1;

	foreach(ints, sym, seq)
	{
		if(verbose)
		{
			cerr<<"Removing production of "<<repr(*sym)<<" given ["<<
				context.size();
			foreach(intLst, ct, context)
			{
				cerr<<" "<<repr(*ct);
			}
			cerr<<"]  = ";
		}

		CRP* givenPrev = getProcess(context);
		// cerr<<givenPrev<<" proc\n";
		Prob term = givenPrev->remove(*sym, temp);
		res += logl(term);

		if(verbose)
		{
			cerr<<logl(term)<<"\n";
		}

		context.push_back(*sym);
		context.pop_front();
	}

	if(_ngrams > 1)
	{
		CRP* givenPrev = getProcess(context);
		LogProb term = logl(givenPrev->remove(0, temp));
		res += term;

		if(verbose)
		{
			cerr<<"P(end sym) = "<<term<<"\n";
		}
	}

	if(verbose)
	{
		cerr<<"Total: "<<res<<"\n";
	}

	return res;
}

intIntMap& CRPLM::lexicon()
{
	//useful for finitedist prior?
	if(dynamic_cast<CRP*>(_prior))
	{
		return dynamic_cast<CRP*>(_prior)->labels();
	}

	intLst empty;
	CRP* unigrams = getProcess(empty);
	return unigrams->labels();
}

CRP* CRPLM::getProcess(intLst& context)
{
	if(context.empty())
	{
		return _root;
	}
	intLst::reverse_iterator ct = context.rbegin();
	intLst::reverse_iterator rend = context.rend();
	return getProcess(_root, ct, rend, true, 0, false);
}

void CRPLM::rmProcess(intLst& context)
{
	if(context.empty())
	{
		return; //don't actually rm root
	}
	intLst::reverse_iterator ct = context.rbegin();
	intLst::reverse_iterator rend = context.rend();
	getProcess(_root, ct, rend, false, 0, false);
}

CRP* CRPLM::getProcessOrEmpty(intLst& context)
{
	if(context.empty())
	{
		return _root;
	}
	intLst::reverse_iterator ct = context.rbegin();
	intLst::reverse_iterator rend = context.rend();
	return getProcess(_root, ct, rend, true, 0, true);
}

CRP* CRPLM::getProcess(CRP* curr, intLst::reverse_iterator& context, 
					   intLst::reverse_iterator& rend, bool get, 
					   int level, bool empty)
{
	if(context == rend)
	{
		return curr;
	}

	int firstPart = *context;
	++context;

	intCRPMap::iterator entry = curr->_children.find(firstPart);
	if(context == rend && !get)
	{
		//rm case
		assert(entry != curr->_children.end());
		delete entry->second;
		curr->_children.erase(entry);
		return NULL;
	}

	if(entry != curr->_children.end())
	{
		return getProcess(entry->second, context, rend, get, 
						  level + 1, empty);
	}
	else if(empty && context == rend)
	{
		if(_empty[level + 1] == NULL)
		{
			CRP* created = new CRP(_alpha[level + 1], curr, 
								   _discount[level + 1]);
			created->_betaDate = &_betaDate;
			_empty[level + 1] = created;
		}
		assert(_empty[level + 1]->_total == 0);

		return _empty[level + 1];
	}
	else
	{
		CRP* created = new CRP(_alpha[level + 1], curr, 
							   _discount[level + 1]);
		created->_betaDate = &_betaDate;
		curr->_children[firstPart] = created;
		return getProcess(created, context, rend, get, level + 1, empty);
	}
}

LogProb CRPLM::probWithBreak(Analysis* ana, int pos)
{
	assert(ana->posIsBreak(pos));

	//note: ... -2 | -1 || 00a * 00b || 1 | 2
	// so return P(00a | -1, -2...) * P(00b | 00a, -1...) * P(1 | 00b, 00a...)

	Segment* atPos = ana->segAt(pos); //00b, begins at pos
	assert(atPos->begin == pos);

	Segment* bfrPos = ana->prevSegment(*atPos); //00a, ends at pos
	assert(bfrPos->end == pos);

	LogProb p0 = logl(segProb(ana, bfrPos, false));
	LogProb p1 = logl(segProb(ana, atPos, false));

	LogProb pRest = 0;
	Segment* curr = ana->nextSegment(*atPos);
	for(int ii = 1; ii < _ngrams; ++ii)
	{
		pRest += logl(segProb(ana, curr, false));
		curr = ana->nextSegment(*curr);
	}

	LogProb res = p0 + p1 + pRest;
//	assert(isLogProb(res));
	return res;
}

LogProb CRPLM::probNoBreak(Analysis* ana, int pos)
{
	assert(!ana->posIsBreak(pos));

	//note: ... -2 | -1 || 00*00 || 1 | 2
	// so return P(00 | -1, -2...) * P(1 | 00...)

	Segment* atPos = ana->segAt(pos); //00, begins before pos

	LogProb p0 = logl(segProb(ana, atPos, false));

	LogProb pRest = 0;
	Segment* curr = ana->nextSegment(*atPos);
	for(int ii = 1; ii < _ngrams; ++ii)
	{
		pRest += logl(segProb(ana, curr, false));
		curr = ana->nextSegment(*curr);
	}

	LogProb res = p0 + pRest;
//	assert(isLogProb(res));
	return res;
}

Prob CRPLM::segProbFull(Analysis* ana, Segment* seg, bool verbose)
{
	Prob init = segProb(ana, seg, verbose);
	Prob pRest = 1.0;
	for(int ii = 1; ii < _ngrams; ++ii)
	{
		seg = ana->nextSegment(*seg);

		intLst context;
		Segment* curr = seg;
		for(int ii = 1; ii < _ngrams; ++ii)
		{
			curr = ana->prevSegment(*seg);
			int segSym = curr->strSym;
			context.push_front(segSym);
		}

		CRP* distr = getProcess(context);
		int thisSym = seg->strSym;
		Prob nextPr = distr->prob(thisSym);

		if(verbose)
		{
			cerr<<"target for next segment is "<<*seg->targetSubstr()<<"\n";
			cerr<<"underlying string is "<<seg->str<<"\n";
		}

		pRest *= nextPr;
	}

	Prob res = init * pRest;
	return res;
}

Prob CRPLM::segProb(Analysis* ana, Segment* seg, bool verbose)
{
	if(verbose)
	{
		cerr<<"target for this segment is "<<*seg->targetSubstr()<<"\n";
		cerr<<"underlying string is "<<seg->str<<"\n";
	}

	intLst context;
	Segment* curr = seg;
	for(int ii = 1; ii < _ngrams; ++ii)
	{
		curr = ana->prevSegment(*seg);
		int segSym = curr->strSym;
		context.push_front(segSym);
	}

	// cerr<<"context ";
	// foreach(intLst, ct, context)
	// {
	// 	cerr<<*ct<<" ";
	// }
	// cerr<<"\n";

	CRP* distr = getProcess(context);
	int thisSym = seg->strSym;

	// cerr<<"gen "<<thisSym<<"\n";
	// cerr<<*distr<<"\n";

	Prob res = distr->prob(thisSym);
	assert(isProb(res));

	if(verbose)
	{
		cerr<<"Symbol prob "<<res<<"\n";
	}

	assert(isProb(res));

	if(_ngrams == 1)
	{
		Prob uttProb = 1.0;
		for(int word = 1; word < ana->nSegments(); ++word)
		{
			Prob posteriorPCont = (_nWords + word - _nUtts + 1.0) / 
				(_nWords + word + 2.0);
			uttProb *= posteriorPCont;
			assert(isProb(uttProb));
		}
		Prob posteriorPCont = (_nWords + ana->nSegments() - _nUtts + 2.0) / 
			(_nWords + ana->nSegments() + 3.0);
		uttProb *= (1 - posteriorPCont);

		if(verbose)
		{
			cerr<<"Utt length prob "<<uttProb<<"\n";
		}
		res *= uttProb;
	}

	return res;
}

Prob CRPLM::posteriorPCont()
{
	Prob posteriorPCont = (_nWords - _nUtts + 1.0) / 
				(_nWords + 2.0);
	assertProb(posteriorPCont);
	return posteriorPCont;
}

Prob CRPLM::updateSegProduction(Analysis* ana, Segment* seg, 
								intLst& context, bool verbose, double temp)
{
	Prob res = 1;

	Segment* curr = seg;
	for(int ii = 1; ii < _ngrams; ++ii)
	{
		curr = ana->prevSegment(*curr);
		int segSym = curr->strSym;
		context.push_front(segSym);
	}

	CRP* distr = getProcess(context);
	int thisSym = seg->strSym;

	if(verbose)
	{
		cerr<<"Adding counts for "<<seg->str<<" given ["<<context.size()<<" ";
		foreach(intLst, sym, context)
		{
			cerr<<repr(*sym)<<", ";
		}
		cerr<<"]\n"; // to "<<distr<<"\n";
	}

	//add count corresponding to production of curr symbol from previous ones
	Table* tab;
	Prob distP = distr->update(thisSym, tab, temp);
	seg->tab = tab;

	if(verbose)
	{
		cerr<<"Count "<<ifExists(distr->_counts, thisSym, 0)<<
			" prior count "<<ifExists(_root->_counts, thisSym, 0)<<"\n";
		cerr<<"Lexical: "<<distP<<"\n";
	}

	res *= distP;

	return res;
}

Prob CRPLM::updateSeg(Analysis* ana, Segment* seg,
					  bool verbose, double temp)
{
	intLst context;
	Prob res = updateSegProduction(ana, seg, context, verbose, temp);

	//add counts for productions of subsequent symbols given curr one
	Segment* curr = seg;
	for(int ii = 1; ii < _ngrams; ++ii)
	{
		context.pop_front();
		context.push_back(curr->strSym);

		CRP* distr = getProcess(context);
		curr = ana->nextSegment(*curr);
		int currSym = curr->strSym;

		if(verbose)
		{
			cerr<<"Adding counts for subsequent "<<curr->str<<" given ";
			foreach(intLst, sym, context)
			{
				cerr<<repr(*sym)<<", ";
			}
			cerr<<"\n";
		}

		Table* tab;
		res *= distr->update(currSym, tab, temp);
		curr->tab = tab;
	}

	if(_ngrams == 1)
	{
		Prob posteriorPCont = (_nWords + ana->nSegments() - _nUtts + 2.0) / 
			(_nWords + ana->nSegments() + 3.0);
		if(ana->nextSegment(*seg)->isStartOrEnd())
		{
			res *= 1 - posteriorPCont;
		}
		else
		{
			res *= posteriorPCont;
		}
	}

	_nWords += ana->nSegments();
	_nUtts += 1;
	assert(_nUtts > 0);
	assert(_nWords > 0);
	assert(_nWords >= _nUtts);
	_uttLengths[ana->nSegments()] += 1;

	return res;
}

Prob CRPLM::removeSegProduction(Analysis* ana, Segment* seg,
								intLst& context, bool verbose, double temp)
{
	Segment* curr = seg;
	for(int ii = 1; ii < _ngrams; ++ii)
	{
		curr = ana->prevSegment(*curr);
		int segSym = curr->strSym;
		context.push_front(segSym);
	}

	CRP* distr = getProcess(context);
	int thisSym = seg->strSym;

	if(verbose)
	{
		cerr<<"Removing counts for "<<seg->str<<" given ";
		foreach(intLst, sym, context)
		{
			cerr<<repr(*sym)<<" ";
		}
		cerr<<"\n";
	}

	//rm count corresponding to production of curr symbol from previous ones
	Prob distP = distr->remove(thisSym, seg->tab);
	if(distr->labels().empty())
	{
		rmProcess(context);
	}

	if(verbose)
	{
		cerr<<"Lexical: "<<distP<<"\n";
	}

	Prob res = distP;
	assert(isProb(res));

	return res;
}

Prob CRPLM::removeSeg(Analysis* ana, Segment* seg,
					  bool verbose, double temp)
{
	intLst context;
	Prob res = removeSegProduction(ana, seg, context, verbose, temp);

	//rm counts for productions of subsequent symbols given curr one
	Segment* curr = seg;
	for(int ii = 1; ii < _ngrams; ++ii)
	{
		context.pop_front();
		context.push_back(curr->strSym);

		CRP* distr = getProcess(context);
		curr = ana->nextSegment(*curr);
		int currSym = curr->strSym;

		if(verbose)
		{
			cerr<<"Removing counts for "<<curr->str<<" given ";
			foreach(intLst, sym, context)
			{
				cerr<<repr(*sym)<<", ";
			}
			cerr<<"\n";
		}

		res *= distr->remove(currSym, curr->tab);
		if(distr->labels().empty())
		{
			rmProcess(context);
		}
	}

	if(_ngrams == 1)
	{
		Prob posteriorPCont = (_nWords + ana->nSegments() - _nUtts + 2.0) / 
			(_nWords + ana->nSegments() + 3.0);
		assert(isProb(posteriorPCont));
		if(ana->nextSegment(*seg)->isStartOrEnd())
		{
			res *= 1 - posteriorPCont;
		}
		else
		{
			res *= posteriorPCont;
		}
	}

	_nWords -= ana->nSegments();
	_nUtts -= 1;
	assert(_nUtts >= 0);
	assert(_nWords >= 0);
	assert(_nWords >= _nUtts);
	_uttLengths[ana->nSegments()] -= 1;

	assert(isProb(res));

	return res;
}

LogProb CRPLM::ll(bool verbose)
{
	LogProb vocabLL = 0;
	foreach(intIntMap, word, _root->_nTablesFor)
	{
		if(word->first == 0) //start token
		{
//			continue;
		}

		//generation of vocabulary
		Prob wProb = _prior->prob(word->first);
		//word with k tables was generated k times
		vocabLL += logl(wProb) * word->second;
		// cerr<<"Prob of "<<repr(word->first)<<" with "<<word->second<<
		// 	" is "<<wProb<<"\n";
	}

	LogProb crpLL = treeLL(_root);

	LogProb uttLL = 0;
	if(_ngrams == 1)
	{
		int utts = 0;
		int words = 0;

		foreach(intIntMap, len, _uttLengths)
		{
			for(int ctr = 0; ctr < len->second; ++ctr)
			{
				for(int word = 1; word < len->first; ++word)
				{
					Prob posteriorPCont = (words - utts + 1.0) / (words + 2.0);
//					cerr<<"p continue "<<posteriorPCont<<"\n";
					assert(isProb(posteriorPCont) && posteriorPCont > 0.0);
					uttLL += logl(posteriorPCont);
					words += 1;
				}

//				cerr<<"at this point "<<words<<" words "<<utts<<" utts\n";
				Prob posteriorPCont = (words - utts + 1.0) / (words + 2.0);
//				cerr<<"p word boundary "<<(1 - posteriorPCont)<<"\n";
				assert(isProb(posteriorPCont) && posteriorPCont < 1.0);
				uttLL += logl(1 - posteriorPCont);
				utts += 1;
				words += 1;
			}
		}
	}

	//properly, we should compute prior values of the hyperparameters...
	//but since these priors are meant to be vague anyway, let's just
	//say they cancel
	//(if we use uniform priors, they *do* cancel, since we only
	//ever see a single sample from each one)

	LogProb channelP = _channel->ll();

	if(verbose)
	{
		cerr<<"Vocab term "<<vocabLL<<"\n";
		cerr<<"Unigram term "<<_root->ll()<<"\n";
		cerr<<"Bigrams "<<(crpLL - _root->ll())<<"\n";
		// cerr<<"Channel "<<(channelP)<<"\n";
		if(_ngrams == 1)
		{
			cerr<<"Utterance lengths "<<uttLL<<"\n";
		}
	}

	// cerr<<"ROOT:\n";
	// cerr<<*_root;

	LogProb res = vocabLL + crpLL + uttLL + channelP;
	return res;
}

LogProb CRPLM::treeLL(CRP* node)
{
	LogProb res = node->ll();
	// cerr<<*node;
	// cerr<<"Local ll "<<res<<"\n\n";
	foreach(intCRPMap, child, node->_children)
	{
		res += treeLL(child->second);
	}
	assert(isLogProb(res));
	return res;
}

int CRPLM::totalTables(CRP* node)
{
	int res = node->_nTables;
	foreach(intCRPMap, child, node->_children)
	{
		res += totalTables(child->second);
	}
	return res;
}

SymbolString& CRPLM::repr(int word)
{
	return _prior->repr(word);
}

int CRPLM::intern(SymbolString& word)
{
	return _prior->intern(word);
}
