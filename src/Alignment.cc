#include "Alignment.h"

Alignment::Alignment():
	ints(0),
	_begin(-1),
	_srcSize(-1),
	_valid(false)
{
}

Alignment::Alignment(int begin, int size):
	ints(size),
	_begin(begin),
	_srcSize(size),
	_valid(true)
{
	makeOneToOne();
}

Alignment::Alignment(const Alignment& other):
	ints(other),
	_begin(other._begin),
	_srcSize(other._srcSize),
	_valid(other._valid)
{
}

void Alignment::makeOneToOne()
{
	int sz = size();
	for(int ii = 0; ii < sz; ++ii)
	{
		(*this)[ii] = ii;
	}
}

void Alignment::checkOneToOne()
{
	int sz = size();
	for(int ii = 0; ii < sz; ++ii)
	{
		if((*this)[ii] != ii)
		{
			assert(0);
		}
	}
}

void Alignment::checkMonotonic()
{
	int sz = size();
	int prev = -1;
	for(int ii = 0; ii < sz; ++ii)
	{
		if((*this)[ii] < 0)
		{
			assert(0);
		}

		if((*this)[ii] < prev)
		{
			assert(0);
		}
		prev = (*this)[ii];
	}
}

int Alignment::split(int targPos, Alignment& onRight)
{
	int sz = size();

	targPos -= _begin;

	assert(targPos < sz);

	//first, scan produced characters that will be split to the left
	//and find the last source string character that produces one
	//...this character will go left
	int srcLeftPos = (*this)[targPos - 1];

	int firstRightPos = srcLeftPos + 1;

	//if this would cause the left side to get all the chars
	if(firstRightPos >= _srcSize)
	{
		firstRightPos -= 1; //just steal a char

		for(int ii = 0; ii < targPos; ++ii)
		{
			if(firstRightPos > 0 && (*this)[ii] == firstRightPos)
			{
				(*this)[ii] -= 1;
			}
		}
	}

	// cerr<<"Splitting (begin is "<<_begin<<")\n";
	// foreach(ints, ii, *this)
	// {
	// 	cerr<<*ii<<" ";
	// }
	// cerr<<"\n";
	// cerr<<"at "<<targPos<<
	// 	" underlying string split at "<<firstRightPos<<"\n";

	//next, copy all the target chars
	//if any of them were sourced to the (now leftward) src character,
	//they become sourced to the first rightward char
	int targSize = sz - targPos;
	onRight.resize(targSize);
	for(int ii = 0; ii < targSize; ++ii)
	{
		int srcPos = (*this)[ii + targPos] - firstRightPos;
		if(srcPos >= 0)
		{
			onRight[ii] = srcPos;
		}
		else
		{
			onRight[ii] = 0;
			onRight._valid = false;
		}
	}

	onRight._begin = _begin + targPos;
	onRight._srcSize = _srcSize - firstRightPos;

	onRight.checkMonotonic();
	checkMonotonic();

	// cerr<<"Results in this:\n";
	// foreach(ints, ii, *this)
	// {
	// 	cerr<<*ii<<" ";
	// }
	// cerr<<"\n";
	// cerr<<"On right:\n";
	// foreach(ints, ii, onRight)
	// {
	// 	cerr<<*ii<<" ";
	// }
	// cerr<<"\n";

	return firstRightPos;
}

void Alignment::merge(Alignment& onRight)
{
	// checkOneToOne();
	// cerr<<"merger of\n";
	// foreach(ints, ii, *this)
	// {
	// 	cerr<<*ii<<" ";
	// }
	// cerr<<"\n";

	// cerr<<"with\n";
	// foreach(ints, ii, onRight)
	// {
	// 	cerr<<*ii<<" ";
	// }
	// cerr<<"\n";
	// cerr<<_srcSize<<" original source chars, "<<
	// 	onRight._srcSize<<" on right\n";

	//comparatively simple
	int oldSize = size();
	int targSize = onRight.size();
	int newSize = oldSize + targSize;
	resize(newSize);

	for(int ii = 0; ii < targSize; ++ii)
	{
		(*this)[oldSize + ii] = onRight[ii] + _srcSize;
	}

	// foreach(ints, ii, *this)
	// {
	// 	cerr<<*ii<<" ";
	// }
	// cerr<<"\n";

	_srcSize += onRight._srcSize;

	checkMonotonic();
}

bool Alignment::valid()
{
	return _valid == 1;
}

bool Alignment::cleared()
{
	return _valid == -1;
}

void Alignment::setValid(int state)
{
	_valid = state;
}
