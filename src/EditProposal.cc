#include "EditProposal.h"

EditProposal::EditProposal(CRPLM& model, intIntMap* surf):
	_model(&model),
	_surf(surf),
	_pIns(.1),
	_pDel(.1),
	_pSub(.7),
	_totalChars(0)
{
	foreach(intIntMap, lex, _model->lexicon())
	{
		SymbolString& si = _model->repr(lex->first);
		foreach(SymbolString, ch, si)
		{
			_charCt[*ch] += 1;
			_totalChars += 1;
		}
	}
}

EditProposal::~EditProposal()
{
}

int EditProposal::justEd(int wordSym, Prob& proposal, Prob& reverse,
						 double temp, bool verbose)
{
	SymbolString& word = _model->repr(wordSym);

	Probs unscaled;
	unscaled.reserve(_model->lexicon().size());
	ints words;
	words.reserve(_model->lexicon().size());
	Prob norm = 0;

	foreach(intIntMap, lex, _model->lexicon())
	{
		if(lex->first == 0 || lex->first == wordSym)
		{
			continue;
		}

		Prob pEd = editDist(word, _model->repr(lex->first));
		pEd = powl(pEd, temp);
		unscaled.push_back(pEd);
		words.push_back(lex->first);

		if(verbose)
		{
			cerr<<_model->repr(lex->first)<<" pEd "<<pEd<<"\n";
		}

		norm += pEd;
	}

	if(verbose)
	{
		int size = unscaled.size();
		for(int ii = 0; ii < size; ++ii)
		{
			double pr = (unscaled[ii]/norm);
			cerr<<_model->repr(words[ii])<<" scaled "<<pr<<"\n";
		}
	}

	double sm = gsl_rng_uniform(RNG);
	double progress = 0;
	int chosen = -1;
	int size = unscaled.size();
	for(int ii = 0; ii < size; ++ii)
	{
		progress += unscaled[ii];
		if(sm <= progress / norm)
		{
			chosen = words[ii];
			proposal = unscaled[ii] / norm;

			Prob ed = editDist(_model->repr(chosen), word);
			ed = powl(ed, temp);
			reverse = ed / norm;

			break;
		}
	}
	assert(chosen != -1);
	assert(!_model->repr(chosen).empty());

	return chosen;
}

int EditProposal::proposeMerge(int wordSym,
							   Prob& proposal, Prob& reverse, bool verbose)
{
	intIntMap& surface = *_surf;
	SymbolString& word = _model->repr(wordSym);

	Probs unscaled;
	unscaled.reserve(_model->lexicon().size());
	ints words;
	words.reserve(_model->lexicon().size());
	Prob norm = 0;

	foreach(intIntMap, lex, _model->lexicon())
	{
		if(lex->first == 0 || lex->first == wordSym)
		{
			continue;
		}

		Prob pW = ifExists(_model->_root->_counts, lex->first, 0);
		assert(pW > 0);
		Prob pEd = editDist(word, _model->repr(lex->first));
		unscaled.push_back(pW * pEd);
		words.push_back(lex->first);

		if(verbose)
		{
			cerr<<_model->repr(lex->first)<<" pW "<<pW<<" pEd "<<pEd<<"\n";
		}

		norm += pW * pEd;
	}

	if(verbose)
	{
		int size = unscaled.size();
		for(int ii = 0; ii < size; ++ii)
		{
			cerr<<_model->repr(words[ii])<<" scaled "<<
				(unscaled[ii]/norm)<<"\n";
		}
	}

	double sm = gsl_rng_uniform(RNG);
	double progress = 0;
	int chosen = -1;
	int size = unscaled.size();
	for(int ii = 0; ii < size; ++ii)
	{
		progress += unscaled[ii];
		if(sm <= progress / norm)
		{
			chosen = words[ii];
			proposal = unscaled[ii] / norm;

			int surfCt = ifExists(surface, chosen, 0);
			Prob ed = editDist(_model->repr(chosen), word);
			Prob surfNorm = 0;
			foreach(intIntMap, si, surface)
			{
				if(!ifExists(_model->_root->_counts, si->first, 0))
				{
					surfNorm += si->second * 
						editDist(_model->repr(si->first), word);
				}
			}
			surfNorm += surfCt * ed;
			reverse = (surfCt * ed) / surfNorm;

			break;
		}
	}
	assert(chosen != -1);
	assert(!_model->repr(chosen).empty());

	return chosen;
}

Prob EditProposal::splitReverse(int mergeFrom, int mergeTo, 
								bool verbose)
{
	SymbolString& word = _model->repr(mergeFrom);

	Prob norm = 0;
	Prob numerator = 0;

	foreach(intIntMap, lex, _model->lexicon())
	{
		if(lex->first == 0 || lex->first == mergeFrom)
		{
			continue;
		}

		Prob pW = ifExists(_model->_root->_counts, lex->first, 0);
		assert(pW > 0);
		Prob pEd = editDist(word, _model->repr(lex->first));

		Prob term = pW * pEd;

		if(lex->first == mergeTo)
		{
			numerator = term;
		}

		if(verbose)
		{
			cerr<<_model->repr(lex->first)<<" pW "<<pW<<" pEd "<<pEd<<"\n";
		}

		norm += term;
	}

	return numerator / norm;
}

int EditProposal::proposeSplit(int wordSym,
							   Prob& proposal, bool verbose)
{
	intIntMap& surface = *_surf;
	SymbolString& word = _model->repr(wordSym);

	Probs unscaled;
	unscaled.reserve(surface.size());
	ints words;
	words.reserve(surface.size());
	Prob norm = 0;

	foreach(intIntMap, lex, surface)
	{
		assert(lex->first != 0);

		if(ifExists(_model->_root->_counts, lex->first, 0))
		{
			continue;
		}
		Prob pW = ifExists(surface, lex->first, 0);
		assert(pW > 0);
		Prob pEd = editDist(word, _model->repr(lex->first));
		unscaled.push_back(pW * pEd);
		words.push_back(lex->first);

		if(verbose)
		{
			cerr<<_model->repr(lex->first)<<" pW "<<pW<<" pEd "<<pEd<<"\n";
		}

		norm += pW * pEd;
	}

	if(verbose)
	{
		int size = unscaled.size();
		for(int ii = 0; ii < size; ++ii)
		{
			cerr<<_model->repr(words[ii])<<" scaled "<<
				(unscaled[ii]/norm)<<"\n";
		}
	}

	double sm = gsl_rng_uniform(RNG);
	double progress = 0;
	int chosen = -1;
	int size = unscaled.size();
	for(int ii = 0; ii < size; ++ii)
	{
		progress += unscaled[ii];
		if(sm <= progress / norm)
		{
			chosen = words[ii];
			proposal = unscaled[ii] / norm;
			break;
		}
	}

	return chosen;
}

Prob EditProposal::editDist(SymbolString& in, SymbolString& out)
{
	if(inMap(_editCache[in], out))
	{
		return _editCache[in][out];
	}

	// cerr<<"Aligning "<<in<<" with "<<out<<"\n";

	int m = out.size();
	int n = in.size();

	ProbMat chart;
	chart.resize(n + 1);
	foreach(ProbMat, row, chart)
	{
		row->resize(m + 1);
	}

	for(int inInd = 0; inInd < n + 1; ++inInd)
	{
		for(int outInd = 0; outInd < m + 1; ++outInd)
		{
			if(inInd == 0 && outInd == 0)
			{
				chart[0][0] = 1;
				continue;
			}

			// cerr<<"Computing alignment at "<<inInd<<" "<<outInd<<"\n";

			Prob cell = 0;

			if(outInd - 1 >= 0)
			{
				Prob prevCell = chart[inInd][outInd - 1];
				// cerr<<"Insertion : "<<inInd<<" "<<(outInd - 1)<<" "<<
				// 	prevCell<<"\n";

				Prob ins = prevCell * _pIns * pSymbol(out[outInd - 1],
													  out.alphabet());
				cell += ins;
			}

			if(inInd - 1 >= 0)
			{
				Prob prevCell = chart[inInd - 1][outInd];
				Prob del = prevCell * _pDel;
				cell += del;
			}

			if(inInd - 1 >= 0 && outInd - 1 >= 0)
			{
				Prob prevCell = chart[inInd - 1][outInd - 1];
				Prob sub = prevCell * (1 - _pIns);
				if(in[inInd - 1] != out[outInd - 1])
				{
					sub *= _pSub * pSymbol(out[outInd - 1], out.alphabet());
				}
				else
				{
					sub *= 1 - (_pSub + _pDel);
				}
				cell += sub;
			}

			// cerr<<"Chars here are ";
			// if(inInd - 1 >= 0)
			// {
			// 	cerr<<in[inInd - 1]<<" ";
			// }
			// cerr<<" and ";
			// if(outInd - 1 >= 0)
			// {
			// 	cerr<<out[outInd - 1];
			// }
			// cerr<<" p= "<<cell<<"\n";

			chart[inInd][outInd] = cell;
		}
	}

	Prob pSeq = chart[n][m];

//	cerr<<"Edit dist "<<in<<" "<<out<<" is "<<pSeq<<"\n";

	_editCache[in][out] = pSeq;

	return pSeq;
}

int EditProposal::sampleEdit(SymbolString& word, Prob& proposal)
{
	SymbolString result(word.alphabet());
	Prob pSym = 1.0 / word.alphabet().size();
	result.reserve(word.size() + 2);

	proposal = 1;

	int size = word.size();
	for(int ii = 0; ii < size; ++ii)
	{
		while(true)
		{
			double sm = gsl_rng_uniform(RNG);
			if(sm > _pIns)
			{
				break;
			}

			result.push_back(randomSym(word.alphabet()));
			proposal *= _pIns * pSym;
		}
		proposal *= 1 - _pIns;

		{
			double sm = gsl_rng_uniform(RNG);
			if(sm < _pDel)
			{
				proposal *= _pDel;
			}
			else if(sm < _pDel + _pSub)
			{
				result.push_back(randomSym(word.alphabet()));
				proposal *= _pSub * pSym;
			}
			else
			{
				result.push_back(word[ii]);
				proposal *= 1 - (_pSub + _pDel);
			}
		}
	}

	while(true)
	{
		double sm = gsl_rng_uniform(RNG);
		if(sm > _pIns)
		{
			break;
		}

		result.push_back(randomSym(word.alphabet()));
		proposal *= _pIns * pSym;
	}
	proposal *= 1 - _pIns;

	int resSym = _model->intern(result);
	return resSym;
}

int EditProposal::randomSym(StrVocab& alphabet)
{
	Prob sm = gsl_rng_uniform(RNG);
	Prob total = alphabet.size();

	int progress = 0;
	//breaks abstraction... whatever
	foreach(strIntMap, ch, alphabet._vocab)
	{
		progress += 1;

		if(sm <= progress / total)
		{
			return ch->second;
		}
	}

	assert(0);
}

Prob EditProposal::pSymbol(int sym, StrVocab& alphabet)
{
//	return 1.0 / alphabet.size();
	return _charCt[sym] / (double)_totalChars;
}

SurfaceProposal::SurfaceProposal(CRPLM& model, intIntIntMap* real):
	EditProposal(model, NULL),
	_real(real)
{
}

SurfaceProposal::~SurfaceProposal()
{
}

int SurfaceProposal::proposeSplit(int word, Prob& proposal, bool verbose)
{
	intIntMap& realizations = (*_real)[word];

	int progress = 0;
	int chosen = -1;
	int total = realizations.size();
	if(inMap(realizations, word))
	{
		total -= 1;
	}

	//proposing the original word (a null split) is 
	//not a real proposal but a failure, and will abort the MH move
	if(total == 0)
	{
		return -1;
	}

	double sm = gsl_rng_uniform(RNG);

	foreach(intIntMap, surf, realizations)
	{
		if(surf->first != word)
		{
			progress += 1;
		}

		if(sm <= progress / (double) total)
		{
			chosen = surf->first;
		}
	}

	assert(chosen != -1);
	proposal = 1 / (double) total;
	return chosen;
}

int SurfaceProposal::proposeMerge(int word, Prob& proposal, Prob& reverse,
								  bool verbose)
{
	//we need to choose a word that appears as itself,
	//or appears as a surface string under the target word
	intIntIntMap& realizations = *_real;

	int total = 0;
	foreach(intIntIntMap, under, realizations)
	{
		if(under->first != word &&
		   inMap(under->second, under->first))
		{
			total += 1;
		}
	}
	intIntMap& wordReals = realizations[word];
	foreach(intIntMap, surf, wordReals)
	{
		if(surf->first != word)
		{
			intIntIntMap::iterator foundBefore = 
				realizations.find(surf->first);
			if(foundBefore != realizations.end())
			{
				if(!inMap(foundBefore->second, surf->first))
				{
					total += 1;
				}
			}
		}
	}

	int progress = 0;
	int chosen = -1;
	double sm = gsl_rng_uniform(RNG);
	foreach(intIntIntMap, under, realizations)
	{
		if(under->first != word &&
		   inMap(under->second, under->first))
		{
			progress += 1;
		}

		if(sm <= progress / (double)total)
		{
			chosen = under->first;
			break;
		}
	}

	if(chosen == -1)
	{
		foreach(intIntMap, surf, wordReals)
		{
			if(surf->first != word)
			{
				intIntIntMap::iterator foundBefore = 
					realizations.find(surf->first);
				if(foundBefore != realizations.end())
				{
					if(!inMap(foundBefore->second, surf->first))
					{
						progress += 1;
					}

					if(sm <= progress / (double)total)
					{
						chosen = surf->first;
						break;
					}
				}
			}
		}
	}

	assert(chosen != -1);

	proposal = 1 / (double) total;
	int oldRealizations = wordReals.size();
	int newRealizations = 0;
	intIntMap& mergeReals = realizations[chosen];
	foreach(intIntMap, merge, mergeReals)
	{
		if(!inMap(wordReals, merge->first))
		{
			newRealizations += 1;
		}
	}

	reverse = 1 / (double)(oldRealizations + newRealizations);

	return chosen;
}

Prob SurfaceProposal::splitReverse(int mergeFrom, int mergeTo, bool verbose)
{
	intIntIntMap& realizations = *_real;

	int total = 0;
	foreach(intIntIntMap, under, realizations)
	{
		if(under->first != mergeFrom &&
		   inMap(under->second, under->first))
		{
			total += 1;
		}
	}
	bool isSurf = false;
	intIntMap& wordReals = realizations[mergeFrom];
	foreach(intIntMap, surf, wordReals)
	{
		if(surf->first != mergeFrom)
		{
			intIntIntMap::iterator foundBefore = 
				realizations.find(surf->first);
			if(foundBefore != realizations.end())
			{
				if(!inMap(foundBefore->second, surf->first))
				{
					total += 1;

					if(surf->first == mergeTo)
					{
						isSurf = true;
					}
				}
			}
		}
	}

	if(mergeTo != mergeFrom &&
	   (isSurf || inMap(realizations[mergeTo], mergeTo)))
	{
		return 1 / (double)total;
	}
	return 0;
}

RecorderProposal::RecorderProposal(CRPLM& model):
	EditProposal(model, NULL)
{
}

RecorderProposal::~RecorderProposal()
{
}

int RecorderProposal::proposeMerge(int word, Prob& proposal, Prob& reverse,
								   bool verbose)
{
	proposal = 1;
	reverse = 1;

	SymbolString& wordStr = _model->repr(word);
	SymStrToSymStr::iterator entry = _merges.find(wordStr);
	if(entry == _merges.end())
	{
		return word;
	}
	return _model->intern(entry->second);
}

int RecorderProposal::proposeSplit(int word, Prob& proposal,
								   bool verbose)
{
	proposal = 1;

	SymbolString& wordStr = _model->repr(word);
	SymStrToSymStr::iterator entry = _splits.find(wordStr);
	if(entry == _splits.end())
	{
		return word;
	}
	return _model->intern(entry->second);
}

Prob RecorderProposal::splitReverse(int mergeFrom, int mergeTo, bool verbose)
{
	return 1;
}

void RecorderProposal::addMerge(int word, int newWord)
{
	_merges[_model->repr(word)] = _model->repr(newWord);
}

void RecorderProposal::addSplit(int word, int newWord)
{
	_splits[_model->repr(word)] = _model->repr(newWord);
}
