#include "Channel.h"

Channel::Channel()
{
}

Channel::~Channel()
{
}

Channel* Channel::copy()
{
	return new Channel();
}

Prob Channel::emitCh(int given, int produced)
{
	if(given == produced)
	{
		return 1;
	}
	return 0;
}

Prob Channel::epsilonAfter(int given)
{
	return 0;
}

Prob Channel::emit(State* st, int produced)
{
	int given = st->lastChar();
	return emitCh(given, produced);
}

LogProb Channel::ll()
{
	return 0;
}

XChannel::XChannel(Prob misspell, int xx):
	Channel(),
	_misspell(misspell),
	_xx(xx)
{
}

XChannel::~XChannel()
{
}

Channel* XChannel::copy()
{
	return new XChannel(_misspell, _xx);
}

Prob XChannel::emitCh(int given, int produced)
{
	if(produced == _xx)
	{
		if(given == _xx)
		{
			return 1; //an underlying x is always a surface x
		}
		return _misspell; //any character emits x
	}
	else if(given == produced)
	{
		return 1 - _misspell; //a character emits itself
	}
	return 0; //the only misspellings are those with surface char x
}

Prob XChannel::emit(State* st, int produced)
{
	int given = st->lastChar();

	return emitCh(given, produced);
}
