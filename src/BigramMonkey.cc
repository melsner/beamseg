#include "BigramMonkey.h"

BigramMonkey::BigramMonkey(double alpha, Prob pBreak, 
						   Prob pEnd, StrVocab& alphabet):
	Monkey(pBreak, false, pEnd, alphabet),
	_total(0),
	_alpha(alpha)
{
}

BigramMonkey::~BigramMonkey()
{
}

int BigramMonkey::sample()
{
	assert(0);
	return -1;
}

Prob BigramMonkey::update(int xx, double temp)
{
	return prob(xx);
}

Prob BigramMonkey::remove(int xx, double temp)
{
	return prob(xx);
}

Prob BigramMonkey::prob(int ww)
{
	SymbolString& word = repr(ww);
	int sz = word.size();

	if(sz == 0)
	{
		return _pEnd;
	}

	Prob res = (1 - _pEnd);
	int prev = -1;
	for(int ii = 0; ii < sz; ++ii)
	{
		res *= biPr(prev, word[ii]);
		prev = word[ii];
	}
	res *= biPr(prev, -1);

	assertProb(res);

	if(res == 0)
	{
		res = EPSILON;
	}

	// cerr<<"BiMonkey: prob of "<<word<<" is "<<res<<"\n";
	// cerr<<"(Monkey: prob of "<<word<<" is "<<Monkey::prob(ww)<<")\n";

	return res;
}

Prob BigramMonkey::biPr(int pr, int curr)
{
	if(curr == -1)
	{
		return _stop;
	}

	int ct = _counts[pr][curr];
	int tot = _totals[pr];
	int backoffCt = _totals[curr];

	Prob p1 = ct / ((Prob)tot + _alpha);
	Prob l1 = _alpha / ((Prob)tot + _alpha);
	Prob p2 = backoffCt / ((Prob)_total + _alpha);
	Prob l2 = _alpha / ((Prob)_total + _alpha);
	Prob p3 = (1.0 / _alphabet->size());

	Prob res = p1 + l1 * (p2 + l2 * p3);

	// cerr<<"Bigram prob "<<(pr == -1 ? "-1" : _alphabet->inv(pr))<<" "<<
	// 	(curr == -1 ? "-1" : _alphabet->inv(curr))<<" is "<<res<<"\n";

	res *= (1 - _stop);

	assertProb(res);
	assert(res > 0);

	return res;
}

void BigramMonkey::addStr(SymbolString& str)
{
	int prev = -1;
	int sz = str.size();
	for(int ii = 0; ii < sz; ++ii)
	{
		addCount(prev, str[ii]);
		prev = str[ii];
	}
	//stopping now constant
	// addCount(prev, -1);

	// cerr<<"add str\n";
}

void BigramMonkey::addCount(int prev, int curr)
{
	_counts[prev][curr] += 1;
	_totals[prev] += 1;
	_total += 1;
}
