#include "common.h"
#include "SegSampler.h"
#include "Monkey.h"
#include "State.h"
#include "Phones.h"

#include <boost/program_options.hpp>

int main(int argc, char* argv[])
{
	appInit(DATA_PATH);

	namespace po = boost::program_options;

	po::options_description desc("Compute transducer from true mappings");
	desc.add_options()
		("help", "show options")
		("underlying", po::value<string>(),
		 "file to read underlying forms from")
		("input-file", po::value<string>(),
		 "file to read input from")
		("channel-file", po::value<string>(),
		 "file storing channel parameters for initial channel")
		("brent-reader",
		 "reader for Brent format (one char per phone, space-delimited")
		;

	po::positional_options_description p;
	p.add("input-file", -1);

	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv).
			  options(desc).positional(p).run(), vm);
	po::notify(vm);

	bool normalReader = true;

	if(vm.count("help"))
	{
		cout<<desc<<"\n";
		return 1;
	}

	string input;
	if(!vm.count("input-file"))
	{
		cerr<<"We need an input file\n";
		return 1;
	}
	else
	{
		input = vm["input-file"].as<string>();
	}

	string initUnderlying = "";
	if(!vm.count("underlying"))
	{
		cerr<<"Assuming underlying is surface\n";
	}
	else
	{
		initUnderlying = vm["underlying"].as<string>();
	}

	StrVocab alpha;

	// XChannel xsub(.1, alpha.get("x", true));
	PhoneChannel ph(alpha);
	if(vm.count("channel-file"))
	{
		string channelFile = vm["channel-file"].as<string>();
		cerr<<"Reading channel parameters from "<<channelFile<<"\n";
		ifstream ifs(channelFile.c_str());
		assert(ifs.is_open());
		ph.read(ifs);
	}

	ifstream ff(input.c_str());
	if(!ff.is_open())
	{
		cerr<<"Can't open input file '"<<input<<"'\n";
		return 1;
	}

	ifstream* underlyingF = NULL;
	if(initUnderlying != "")
	{
		underlyingF = new ifstream(initUnderlying.c_str());
		if(!underlyingF->is_open())
		{
			cerr<<"Can't open underlying forms file '"<<initUnderlying<<"'\n";
			return 1;
		}
	}

	while(ff)
	{
		string line;
		getline(ff, line);
		if(line.empty())
		{
			break;
		}

		std::istringstream split(line);

		SymbolStrings sSurf;

		while(split)
		{
			SymbolString* word;
			if(normalReader)
			{
				word = new SymbolString(split, alpha);
			}
			else
			{
				string key;
				split>>key;
				if(key == "||")
				{
					cerr<<"*WARNING* Is your reader setting wrong?\n";
					return 1;
				}
				word = new SymbolString(key, alpha);
			}

			if(word->empty())
			{
				break;
			}
			sSurf.push_back(word);
		}

		SymbolStrings sUnder;
		if(underlyingF)
		{
			string line;
			getline(*underlyingF, line);
			if(line.empty())
			{
				cerr<<"Unexpected end of underlying forms file.\n";
				return 1;
			}

			std::istringstream split(line);

			while(split)
			{
				SymbolString* word;
				word = new SymbolString(split, alpha);

				if(word->empty())
				{
					break;
				}
				sUnder.push_back(word);
			}
		}
		else
		{
			sUnder = sSurf;
		}

		int ln = sSurf.size();
		assert(ln == sUnder.size());

		for(int ii = 0; ii < ln; ++ii)
		{
			//required only in old builds, should now work for everything
			//if(sUnder[ii]->size() == sSurf[ii]->size())
			{
				cerr<<*sUnder[ii]<<" -> "<<*sSurf[ii]<<"\n";
				forcedAlign(*sUnder[ii], *sSurf[ii], &ph, true);
			}
		}

		for(int ii = 0; ii < ln; ++ii)
		{
			delete sSurf[ii];
			if(underlyingF)
			{
				delete sUnder[ii];
			}
		}
	}

	{
		ostream& os = cerr;
		os<<"--- empirical phone-to-phone counts ---\n";

		int nonzeros = 0;

		foreach(intIntIntMap, pho, ph._phoneCounts)
		{
			IntPriVec sorter;
			foreach(intIntMap, prod, pho->second)
			{
				if(prod->second > 0)
				{
					if(prod->first != pho->first)
					{
						nonzeros += prod->second;
					}

					sorter.push_back(IntPriNode(prod->second, prod->first));
				}
			}
			sort(sorter.begin(), sorter.end(), IntGreater());

			if(pho->first != -1)
			{
				os<<alpha.inv(pho->first)<<"\n";
			}
			else
			{
				os<<"-1\n";
			}

			foreach(IntPriVec, node, sorter)
			{
				if(node->data != -1)
				{
					os<<"\t"<<alpha.inv(node->data);
				}
				else
				{
					os<<"\t-1";
				}

				os<<"\t"<<node->pri<<"\n";
			}
			os<<"\n";
		}

		os<<"Total "<<nonzeros<<" changes\n";
	}

	cerr<<"Estimating...\n";
	ph.estimate();
	double* wts = ph.learner.weights();
	for(int ii = 0; ii < ph.learner.dimension(); ++ii)
	{
		if(wts[ii] != 0)
		{
			cerr<<wts[ii]<<" "<<ph.fset.inv(ii)<<"\n";
		}
	}

	ph.write(cout);
}
