#include "FSTKit.h"
#include "Monkey.h"
#include <fst/shortest-distance.h>
#include <fst/matcher.h>

FSTKit::FSTKit(StrVocab& alphabet):
	_alphabet(alphabet)
{
}

void FSTKit::stringToFst(SymbolString& str, FST& fst)
{
	fst.AddState();
	fst.SetStart(0);

	int lastState = 0;

	for(lastState = 0; lastState < str.size(); ++lastState)
	{
		int charName = str[lastState];
		fst.AddState();
		fst.AddArc(lastState, FSTArc(charName + 1, charName + 1, 0.0,
									 lastState + 1));
	}

	fst.SetFinal(lastState, 0);
}

LogProb FSTKit::fstProb(FST& fst)
{
	assert(fst.NumStates() > 0);

	vector<FSTArc::Weight> dist;
	ShortestDistance(fst, &dist, true);
	StateId start = fst.Start();
	return -dist[start].Value();
}

SymbolString* FSTKit::fstToString(FST& fst, bool input)
{
	SymbolString* str = new SymbolString(_alphabet);

	StateId state = fst.Start();
	// cerr<<"start state is "<<state<<"\n";

	while(fst.Final(state) == FST::Weight::Zero())
	{
		ArcIterator<FST> aiter(fst, state);
		const FSTArc& arc = aiter.Value();

		//throw out epsilon
		if(input && arc.ilabel != 0)
		{
			str->push_back(arc.ilabel - 1);
		}
		else if(!input && arc.olabel != 0)
		{
			str->push_back(arc.olabel - 1);
		}

		state = arc.nextstate;

		aiter.Next();
		assert(aiter.Done());
	}

	return str;
}

void FSTKit::monkeyFst(Prob stop, bool allowEmpty, FST& out)
{
	StateId start = out.AddState();
	LogProb pSym = logl((1 - stop) * (1.0 / _alphabet.size()));

	if(out.NumStates() == 1)
	{
		out.SetStart(0);
	}
	else
	{
		for(int chr = 0; chr < _alphabet.size(); ++chr)
		{
			vector<FSTArc>::iterator findArc = 
				out.GetImpl()->states_[0]->arcs.begin();
			while(findArc != out.GetImpl()->states_[0]->arcs.end())
			{
				if(findArc->ilabel > chr + 1)
				{
					break;
				}

				findArc++;
			}

			out.GetImpl()->states_[0]->arcs.insert(
				findArc,
				FSTArc(chr + 1, chr + 1, -pSym, start));
		}
	}

	if(allowEmpty)
	{
		out.SetFinal(0, -logl(stop));
	}

	for(int unroll = 0; unroll < MAX_WORD_LEN; ++unroll)
	{
		out.SetFinal(start, -logl(stop));
		StateId nxt = out.AddState();

		for(int chr = 0; chr < _alphabet.size(); ++chr)
		{
			out.AddArc(start, FSTArc(chr + 1, chr + 1, -pSym, nxt));
		}
		start = nxt;
	}
	out.SetFinal(start, -logl(stop));
}

// void FSTKit::vocabFst(CRP* distr, FST& g0, SymVocab& vocab, FST& out)
// {
// 	Prob denom = distr->_total + distr->_alpha;

// 	//mass devoted to items with counts

// 	Prob remaining = distr->_alpha;
// 	foreach(intIntMap, labelCt, distr->_counts)
// 	{
// 		//PY: discount each table by D
// 		Prob discountFactor = distr->_nTablesFor[labelCt->first] * 
// 			distr->_discount;
// 		Prob pr = (labelCt->second - discountFactor) / denom;
// 		remaining += discountFactor;

// 		FST strFst;
// 		stringToFst(vocab.inv(labelCt->first), strFst);
// 		probUnion(out, strFst, logl(pr));
// 	}

// 	remaining /= denom;
// 	probUnion(out, g0, logl(remaining));
// }

//build a deterministic, epsilon-free fst in place
void FSTKit::vocabFst(CRP* distr, FST& g0, SymVocab& vocab,
					  intIntMap& endStates, FST& out)
{
	Prob denom = distr->_total + distr->_alpha;

	out = *g0.Copy(true);
	if(out.NumStates() == 0)
	{
		out.AddState();
		out.SetStart(0);
	}

	Prob remaining = distr->_alpha + distr->_nTables * distr->_discount;
	remaining /= denom;

	scaleWeights(out, -logl(remaining));

	// cerr<<"Scaled prior weights\n";

	//mass devoted to items with counts
	foreach(intIntMap, labelCt, distr->_counts)
	{
		//PY: discount each table by D
		Prob discountFactor = distr->_nTablesFor[labelCt->first] * 
			distr->_discount;
		Prob pr = (labelCt->second - discountFactor) / denom;

		SymbolString& str = vocab.inv(labelCt->first);
		StateId idn = insertIntoTrie(str, 0, out, 0, -logl(pr));
		endStates[labelCt->first] = idn;
	}

	uint64 props = out.Properties(kFstProperties, false);
	out.SetProperties(props | kILabelSorted | kOLabelSorted, 
					  kFstProperties);

	// cerr<<"Inserted "<<distr->_counts.size()<<" items\n";
}

void FSTKit::updateFst(CRP* distr, SymVocab& vocab, intIntMap& endStates, 
					   FST& out)
{
	// out.Write("original.fst");

	Prob denom = distr->_total + distr->_alpha;
	Prob remaining = distr->_alpha + distr->_nTables * distr->_discount;
	remaining /= denom;

	Prob priorWt = 0;
	{
		Monkey* mo = dynamic_cast<Monkey*>(distr->_prior);
		assert(mo != NULL);
		priorWt = remaining * mo->_stop;
	}

	//deal with all monkey states; note assumption of layout
	for(int unroll = 0; unroll < MAX_WORD_LEN; ++unroll)
	{
		out.SetFinal(1 + unroll, -logl(priorWt));
	}

	foreach(intIntMap, labelCt, distr->_counts)
	{
		Prob discountFactor = distr->_nTablesFor[labelCt->first] * 
			distr->_discount;
		Prob pr = (labelCt->second - discountFactor) / denom;
		if(endStates.find(labelCt->first) != endStates.end())
		{
			out.SetFinal(endStates[labelCt->first], -logl(pr));
		}
		else
		{
			SymbolString& str = vocab.inv(labelCt->first);
			StateId idn = insertIntoTrie(str, 0, out, 0, -logl(pr));
			endStates[labelCt->first] = idn;
		}
	}

	int dels = 0;
	foreach(intIntMap, es, endStates)
	{
		if(distr->_counts.find(es->first) == distr->_counts.end())
		{
			out.SetFinal(es->second, FSTArc::Weight::Zero());
			++dels;
		}
	}

	// cerr<<"Updated "<<lexicon->size()<<" items and deleted "<<dels<<"\n";
	// out.Write("updated.fst");
}

void FSTKit::scaleWeights(FST& out, LogProb wt)
{
	int ns = out.NumStates();
	int scaled = 0;
	for(int ii = 0; ii < ns; ++ii)
	{
		if(out.Final(ii) != FSTArc::Weight::Zero())
		{
			out.SetFinal(ii, wt + out.Final(ii).Value());
			++scaled;
		}
	}

	// cerr<<"Scaled "<<scaled<<" weights of "<<ns<<" states\n";
}

StateId FSTKit::insertIntoTrie(SymbolString& str, int ctr, FST& out, 
							   int state, LogProb pr)
{
	if(ctr == str.size())
	{
		if(out.Final(state) != FSTArc::Weight::Zero())
		{
			// cerr<<"final increase from "<<out.Final(state).Value()<<" by "<<
			// 	pr<<" to "<<-logplus(-pr, -out.Final(state).Value())<<"\n";

			out.SetFinal(state, -logplus(-pr, -out.Final(state).Value()));
		}
		else
		{
			out.SetFinal(state, pr);
		}

		return state;
	}

	int chr = str[ctr];
	SortedMatcher<FST> matcher(out, MATCH_INPUT);
	if(out.NumStates() > 0)
	{
		matcher.SetState(state);
		if(matcher.Find(chr + 1))
		{
			FSTArc& arc = *const_cast<FSTArc*>(&matcher.Value());
			//avoid monkey (one piece of permitted nondeterminism)
			if(arc.weight == FSTArc::Weight::One())
			{
				// cerr<<"going to old stt "<<arc.nextstate<<"\n";
				return insertIntoTrie(str, ctr + 1, out, arc.nextstate, pr);
			}
		}
	}

	//if we can't find, add new state
	{
		StateId nxt = out.AddState();
		vector<FSTArc>::iterator findArc = 
			out.GetImpl()->states_[state]->arcs.begin();
		while(findArc != out.GetImpl()->states_[state]->arcs.end())
		{
			if(findArc->ilabel >= chr + 1)
			{
				break;
			}

			findArc++;
		}

		// cerr<<"going to new stt "<<nxt<<"\n";
		out.GetImpl()->states_[state]->arcs.insert(
			findArc,
			FSTArc(chr + 1, chr + 1, 0, nxt));

		return insertIntoTrie(str, ctr + 1, out, nxt, pr);
	}
}

void FSTKit::bigramVocabFst(CRP* distr, FST& g0, SymVocab& vocab, FST& out)
{
	intProbMap probTab;

	Prob remaining = distr->_alpha;
	{
		Prob denom = distr->_total + distr->_alpha;

		//mass devoted to items with counts
		foreach(intIntMap, labelCt, distr->_counts)
		{
			//PY: discount each table by D
			Prob discountFactor = distr->_nTablesFor[labelCt->first] * 
				distr->_discount;
			Prob pr = (labelCt->second - discountFactor) / denom;
			remaining += discountFactor;

			probTab[labelCt->first] = logl(pr);
		}

		remaining /= denom;
	}

	{
		distr = dynamic_cast<CRP*>(distr->_prior);
		assert(distr != NULL);
		Prob parRemaining = distr->_alpha;
		Prob denom = distr->_total + distr->_alpha;
		foreach(intIntMap, labelCt, distr->_counts)
		{
			//PY: discount each table by D
			Prob discountFactor = distr->_nTablesFor[labelCt->first] * 
				distr->_discount;
			Prob pr = (labelCt->second - discountFactor) / denom;
			parRemaining += discountFactor;

			probTab[labelCt->first] += logl(pr);
		}

		parRemaining /= denom;
	}

	foreach(intProbMap, item, probTab)
	{
		FST strFst;
		stringToFst(vocab.inv(item->first), strFst);
		probUnion(out, strFst, item->second);
	}

	probUnion(out, g0, logl(remaining));
}

void FSTKit::probUnion(FST& out, FST& other, LogProb weight)
{
	//copied from union.h
	StateId numstates1 = out.NumStates();
	if(numstates1 == 0)
	{
		out.AddState();
		out.SetStart(0);
		numstates1 = 1;
	}

	for(StateIterator<FST> siter(other);
		!siter.Done();
		siter.Next())
	{
		StateId s1 = out.AddState();
		StateId s2 = siter.Value();
		out.SetFinal(s1, other.Final(s2));

		for(ArcIterator<FST> aiter(other, s2);
			!aiter.Done();
			aiter.Next())
		{
			FSTArc arc = aiter.Value();
			arc.nextstate += numstates1;
			out.AddArc(s1, arc);
		}
	}

	StateId start1 = out.Start();
	StateId start2 = other.Start();
	uint64 props1 = out.Properties(kFstProperties, false);
	uint64 props2 = other.Properties(kFstProperties, false);

	out.AddArc(start1, FSTArc(0, 0, -weight, start2 + numstates1));

	out.SetProperties(UnionProperties(props1, props2), kFstProperties);
}

void FSTKit::identityMisspell(FST& out)
{
	out.AddState();
	out.SetStart(0);
	out.SetFinal(0, 0);

	for(int chr = 0; chr < _alphabet.size(); ++chr)
	{
		out.AddArc(0, FSTArc(chr + 1, chr + 1, 0, 0));
	}
}

void FSTKit::xMisspell(FST& out, Prob error)
{
	out.AddState();
	out.SetStart(0);
	out.SetFinal(0, 0);

	LogProb prN = logl(1 - error);
	LogProb prE = logl(error);
	int xchr = _alphabet.get("x", false);
	assert(xchr != -1);

	for(int chr = 0; chr < _alphabet.size(); ++chr)
	{
		out.AddArc(0, FSTArc(chr + 1, chr + 1, -prN, 0));
		out.AddArc(0, FSTArc(chr + 1, xchr + 1, -prE, 0));
	}
}

int FSTKit::depth(FST& fst)
{
	return depth1(fst, fst.Start());
}

int FSTKit::depth1(FST& fst, StateId state)
{
	if(fst.Final(state) != FSTArc::Weight::Zero())
	{
		return 1;
	}
	ArcIterator<FST> aiter(fst, state);
	if(aiter.Done())
	{
		return 1;
	}
	const FSTArc& arc = aiter.Value();
	return 1 + depth1(fst, arc.nextstate);
}

StateId FSTKit::sampleState(Probs& prevProbs, StateId currState,
							FST& fst, const FSTArc*& arc)
{
	double total = 0;
	int sz = prevProbs.size();
	for(int prev = 0; prev < sz; ++prev)
	{
		// cerr<<"Prev state: "<<prev->first<<" "<<prev->second<<"\n";
		ArcIterator<FST> ai(fst, prev);
		for(; !ai.Done(); ai.Next())
		{
			const FSTArc& aival = ai.Value();
			if(aival.nextstate == currState)
			{
				double pr = expl(-aival.weight.Value()) * prevProbs[prev];
				total += pr;
			}
		}
	}

	double sample = gsl_ran_flat(RNG, 0, total);
	double progress = 0;
	for(int prev = 0; prev < sz; ++prev)
	{
		// cerr<<"Prev state: "<<prev->first<<" "<<prev->second<<"\n";
		ArcIterator<FST> ai(fst, prev);
		for(; !ai.Done(); ai.Next())
		{
			const FSTArc& aival = ai.Value();
			if(aival.nextstate == currState)
			{
				double pr = expl(-aival.weight.Value()) * prevProbs[prev];
				progress += pr;
				if(progress >= sample)
				{
					arc = &aival;
					return prev;
				}
			}
		}
	}

	assert(0);

	// double total = 0;
	// typedef std::map<Prob, const FSTArc*> probToArc;
	// probToArc prs;
	// typedef std::map<const FSTArc*, int> arcToInt;
	// arcToInt arcTab;

	// int sz = prevProbs.size();
	// for(int prev = 0; prev < sz; ++prev)
	// {
	// 	// cerr<<"Prev state: "<<prev->first<<" "<<prev->second<<"\n";
	// 	ArcIterator<FST> ai(fst, prev);
	// 	for(; !ai.Done(); ai.Next())
	// 	{
	// 		const FSTArc& aival = ai.Value();
	// 		if(aival.nextstate == currState)
	// 		{
	// 			double pr = expl(-aival.weight.Value()) * prevProbs[prev];
	// 			if(pr > 0)
	// 			{
	// 				prs[pr + total] = &aival;
	// 				total += pr;
	// 			}
	// 			arcTab[&aival] = prev;
	// 		}
	// 	}
	// }
	// assert(total > 0);

	// double sample = gsl_ran_flat(RNG, 0, total);
	// arc = prs.lower_bound(sample)->second;
	// StateId state = arcTab[arc];
	// return state;
}

LogProb FSTKit::sampleSeq(FST& fst, FST& out, int verbose)
{
	// used to determinize and rm epsilon here: now suspect it
	// can be done earlier
	// machine has to be acyclic so depth works?
	assert(fst.Properties(kNoEpsilons, true) &&
		   fst.Properties(kAcyclic, true));

	// FST fst;
	// Determinize(input, &fst);
	// RmEpsilon(&fst);

	// if(verbose)
	// {
	// 	fst.Write("operate.fst");
	// }

	StateId start = 0;

	int nn = depth(fst);
	int nStates = fst.NumStates();

	if(verbose)
	{
		cerr<<"detected n "<<nn<<"\n";
	}

	//forward pass
	// cerr<<"start state: "<<start<<"\n";

	ProbMat table(nn);
	for(int tt = 0; tt < nn; ++tt)
	{
		table[tt].resize(nStates);
	}
	Probs totals(nn);
	table[0][0] = 1.0;
	totals[0] = 1.0;

	for(int tt = 1; tt < nn; ++tt)
	{
		if(verbose)
		{
			cerr<<"t="<<tt<<"...\n";
		}

		Probs& prevTimeStep = table[tt - 1];
		Probs& timeStep = table[tt];
		Prob& total = totals[tt];

		for(int prev = 0; prev < nStates; ++prev)
		{
			double pHere = prevTimeStep[prev];
			if(verbose)
			{
				cerr<<"\tprev="<<prev<<" pr "<<pHere<<"\n";
			}

			if(pHere == 0)
			{
				continue;
			}

			for(ArcIterator<FST> ai(fst, prev); !ai.Done(); ai.Next())
			{
				const FSTArc& arc = ai.Value();
				double pTrans = expl(-arc.weight.Value());
				double pr = pTrans * pHere;
				if(fst.Final(arc.nextstate) != FSTArc::Weight::Zero())
				{
					pr *= expl(-fst.Final(arc.nextstate).Value());
				}

				if(verbose)
				{
					cerr<<"\t\t :"<<prev<<"->"<<arc.nextstate<<" "<<
						pTrans<<" = "<<pr<<"\n";
				}

				if(pr > 0)
				{
					timeStep[arc.nextstate] += pr;
					total += pr;
				}
			}
		}

		foreach(Probs, nxt, timeStep)
		{
			*nxt /= total;
		}

		if(verbose)
		{
			writeVector(timeStep, cerr);
		}
	}

	if(verbose)
	{
		cerr<<"ok, sampling back\n";
	}

	//backward (sampling) pass
	StateId end = -1;
	Prob total = 0;
	LogProb seqPr = 0;
	for(int st = 0; st < nStates; ++st)
	{
		total += table[nn - 1][st];
	}
	assert(total > 0);
	if(verbose)
	{
		cerr<<"final probs\n";
		writeVector(table[nn - 1], cerr);
		cerr<<"totals\n";
		writeVector(totals, cerr);
	}

	double sample = gsl_ran_flat(RNG, 0, total);
	Prob progress = 0;
	for(int st = 0; st < nStates; ++st)
	{
		progress += table[nn - 1][st];
		if(progress > sample)
		{
			end = st;

			// cerr<<(nn - 1)<<":: "<<table[nn - 1][st]<<" of "<<
			// 	totals[nn - 1]<<" "<<
			// 	(logl(table[nn - 1][st]) + logl(totals[nn - 1]))<<"\n";

			seqPr += logl(table[nn - 1][st]) + logl(totals[nn - 1]);
			break;
		}
	}
	assert(end != -1);

	if(verbose)
	{
		cerr<<"end state is "<<end<<"\n";
	}

	for(int tt = 0; tt < nn; ++tt)
	{
		out.AddState();
	}
	out.SetStart(0);

	StateId nextState = end;
	for(int tt = nn - 2; tt >= 0; --tt)
	{
		// cerr<<"t="<<tt<<"...\n";
		Probs& timeStep = table[tt];
		const FSTArc* arc;
		StateId prev = sampleState(timeStep, nextState, fst, arc);
		seqPr += logl(table[tt][prev]) + logl(totals[tt]);

		// cerr<<tt<<":: "<<table[tt][prev]<<" of "<<totals[tt]<<" "<<
		// 	(logl(table[tt][prev]) + logl(totals[tt]))<<"\n";

		// cerr<<"t="<<tt<<" prev "<<prev<<" "<<arc->olabel<<"...\n";

		out.AddArc(tt, FSTArc(arc->ilabel, arc->olabel,
							  0, tt + 1));

		nextState = prev;
	}
	assert(nextState == start);
	out.SetFinal(nn - 1, 0.0);
	assertLogProb(seqPr);
	// cerr<<"done\n";
	return seqPr;
}

void FSTKit::composeWithString(FST& in, SymbolString& str, FST& out)
{
	assert(out.NumStates() == 0);
	StateId start = in.Start();
	int ptr = 0;
	StateId copyTo = out.AddState();
	if(out.NumStates() == 1)
	{
		out.SetStart(0);
	}
	if(str.size() == 0)
	{
		out.SetFinal(copyTo, in.Final(start));
	}

	compose1(in, start, out, str, ptr);
}

StateId FSTKit::compose1(FST& in, StateId state, FST& out,
						 SymbolString& str, int ptr)
{
	if(ptr == str.size())
	{
		if(in.Final(state) != FSTArc::Weight::Zero())
		{
			StateId nxt = out.AddState();
			out.SetFinal(nxt, in.Final(state));
			return nxt;
		}
		return kNoStateId;
	}

	int chr = str[ptr];

	StateId copyTo = (ptr == 0) ? 0 : kNoStateId;
	intIntMap arcTab;
	for(ArcIterator<FST> ai(in, state); !ai.Done(); ai.Next())
	{
		const FSTArc& arc = ai.Value();
		if(arc.olabel == chr + 1)
		{
			if(inMap(arcTab, arc.nextstate))
			{
				out.AddArc(copyTo, FSTArc(arc.ilabel, arc.olabel,
										  arc.weight, arcTab[arc.nextstate]));
			}
			else
			{
				StateId nxt = compose1(in, arc.nextstate, out, str, ptr + 1);
				if(nxt != kNoStateId)
				{
					if(copyTo == kNoStateId)
					{
						copyTo = out.AddState();
					}

					out.AddArc(copyTo, FSTArc(arc.ilabel, arc.olabel,
											  arc.weight, nxt));
					arcTab[arc.nextstate] = nxt;
				}
			}
		}
		assert(arc.olabel != 0); //handle epsilon sometime
	}

	return copyTo;
}

StateId FSTKit::sampleState(intProbMap& prevProbs, StateId currState,
							intIntProbMap& transitions, int chr,
							FST& fst, const FSTArc*& arc, bool verbose)
{
	double total = 0;
	foreach(intProbMap, transOut, transitions[currState])
	{
		// cerr<<"transition "<<prev<<" "<<currState<<" "<<
		// 	transitions[prev][currState]<<"\n";
		total += transOut->second;
	}

	// cerr<<"total "<<total<<"\n";
	assert(total > 0);

	double sample = gsl_ran_flat(RNG, 0, total);
	double progress = 0;
	StateId chosen = -1;
	foreach(intProbMap, transOut, transitions[currState])
	{
		progress += transOut->second;
		if(progress >= sample)
		{
			chosen = transOut->first;
			break;
		}
	}
	assert(chosen != -1);

	// cerr<<"chose prev state "<<chosen<<"\n";

	total = transitions[currState][chosen];
	sample = gsl_ran_flat(RNG, 0, total);
	progress = 0;

	assert(total > 0);

	ArcIterator<FST> aiter(fst, chosen);
	for(; !aiter.Done(); aiter.Next())
	{
		const FSTArc& aival = aiter.Value();

		if(aival.olabel == chr + 1 && aival.nextstate == currState)
		{
			double pr = expl(-aival.weight.Value());
			progress += pr;

			if(progress >= sample)
			{
				arc = &aival;
				return chosen;
			}
		}
	}

	assert(0);
}

LogProb FSTKit::sampleComposedWithString(FST& fst, FST& out, 
										 SymbolString& str,
										 int verbose)
{
	// if(verbose)
	// {
	// 	fst.Write("operate.fst");
	// }

	StateId start = fst.Start();

	int nn = str.size();

	if(verbose)
	{
		cerr<<"detected n "<<nn<<"\n";
	}

	//forward pass
	// cerr<<"start state: "<<start<<"\n";

	intIntProbMap table;
	intProbMap totals(nn + 1);
	table[0][0] = 1.0;
	totals[0] = 1.0;

	intIntIntProbMap transitions;

	for(int tt = 1; tt < nn + 1; ++tt)
	{
		if(verbose)
		{
			cerr<<"t="<<tt<<"...\n";
		}

		intProbMap& prevTimeStep = table[tt - 1];
		intProbMap& timeStep = table[tt];
		Prob& total = totals[tt];

		int chr = str[tt - 1];

		foreach(intProbMap, prev, prevTimeStep)
		{
			intIntProbMap& transHere = transitions[tt];
			double pHere = prev->second;
			// if(verbose)
			// {
			// 	cerr<<"\tprev="<<prev<<" pr "<<pHere<<"\n";
			// }

			if(pHere == 0)
			{
				continue;
			}

			//use for sorted fst
			// SortedMatcher<FST> matcher(fst, MATCH_OUTPUT);
			// matcher.SetState(prev->first);
			// if(matcher.Find(chr + 1))
			// {
			// 	for(; !matcher.Done(); matcher.Next())

			ArcIterator<FST> aiter(fst, prev->first);
			for(; !aiter.Done(); aiter.Next())
			{
				const FSTArc& arc = aiter.Value();

				if(arc.olabel == chr + 1)
				{
					double pTrans = expl(-arc.weight.Value());
					double pr = pTrans * pHere;

					if(verbose)
					{
						cerr<<"\t\t :"<<prev->first<<
							"->"<<arc.nextstate<<" "<<
							pTrans<<" = "<<pr<<"\n";
					}

					if(pr > 0)
					{
						timeStep[arc.nextstate] += pr;
						total += pr;
						// cerr<<"incr trans "<<tt<<" "<<prev<<" "<<
						// 	arc.nextstate<<"\n";
						transHere[arc.nextstate][prev->first] += pTrans;
					}
				}
			}
		}

		if(verbose)
		{
			cerr<<"total: "<<total<<"\n";
		}
		assert(total > 0);

		foreach(intProbMap, nxt, timeStep)
		{
			nxt->second /= total;
		}

		// if(verbose)
		// {
		// 	writeVector(timeStep, cerr);
		// }
	}

	if(verbose)
	{
		cerr<<"ok, sampling back\n";
	}

	intProbMap& finalProbs = table[nn];
	totals[nn] = 0;

	//final weights
	foreach(intProbMap, st, finalProbs)
	{
		if(fst.Final(st->first) != FSTArc::Weight::Zero())
		{
			if(verbose)
			{
				cerr<<"acceptable final state "<<st->first<<" curr "<<
					st->second<<" "<<
					expl(-fst.Final(st->first).Value())<<"\n";
			}

			st->second *= expl(-fst.Final(st->first).Value());
		}
		else
		{
			st->second = 0;
		}
		totals[nn] += st->second;
	}
	foreach(intProbMap, st, finalProbs)
	{
		st->second /= totals[nn];
	}

	//backward (sampling) pass

	//sample the last state
	StateId end = -1;
	Prob total = totals[nn];
	LogProb seqPr = 0;

	if(!(total > 0))
	{
		cerr<<"failed to find any path: "<<str<<"\n";
		cerr<<"total computed: "<<total<<"\n";
	}
	assert(total > 0);
	if(verbose)
	{
		cerr<<"final probs\n";
		foreach(intProbMap, st, finalProbs)
		{
			if(st->second > 0)
			{
				cerr<<st->first<<"="<<st->second<<" ";
			}
			cerr<<"\n";
		}
	}

	//don't divide by total, already normed!
	double sample = gsl_rng_uniform(RNG);
	Prob progress = 0;
	foreach(intProbMap, st, finalProbs)
	{
		progress += st->second;
		if(progress > sample) //don't divide by total, already normed!
		{
			end = st->first;
			seqPr += -fst.Final(st->first).Value();

			if(verbose)
			{
				cerr<<"Seq prob: "<<-fst.Final(st->first).Value()<<
					" (final)"<<"\n";
			}

			break;
		}
	}
	assert(end != -1);

	if(verbose)
	{
		cerr<<"sample "<<sample<<"\n";
		cerr<<"end state is "<<end<<"\n";
	}

	for(int tt = 0; tt <= nn; ++tt)
	{
		out.AddState();
	}
	out.SetStart(0);

	StateId nextState = end;
	for(int tt = nn - 1; tt >= 0; --tt)
	{
		intProbMap& timeStep = table[tt];
		const FSTArc* arc;
		StateId prev = sampleState(timeStep, nextState, 
								   transitions[tt + 1], str[tt], fst, arc,
								   verbose);
		if(verbose)
		{
			cerr<<"t="<<tt<<" chose "<<prev<<"\n";
		}

		seqPr += -arc->weight.Value();

		if(verbose)
		{
			cerr<<"Seq prob: "<<-arc->weight.Value()<<" (arc)"<<"\n";
		}

		out.AddArc(tt, FSTArc(arc->ilabel, arc->olabel,
							  0, tt + 1));

		nextState = prev;
	}
	assert(nextState == start);
	out.SetFinal(nn, 0.0);
	assertLogProb(seqPr);
	// cerr<<"done\n";
	return seqPr;
}
