#include "NGramMonkey.h"

NGramMonkey::NGramMonkey(int ngrams, doubles& alpha, doubles& discount,
						 bool allowEmpty, Prob pEnd, StrVocab& alphabet):
	BigramMonkey(alpha[0], .2, pEnd, alphabet),
	_ngrams(ngrams),
	_alpha(alpha),
	_discount(discount),
	_g0(-1, alphabet.size()),
	_updates(0)
{
	assert(_alpha.size() == _ngrams);
	assert(_discount.size() == _ngrams);

	_root = new CRP(_alpha[0], &_g0, _discount[0]);
}

NGramMonkey::~NGramMonkey()
{
	delete _root;
}

int NGramMonkey::sample()
{
	SymbolString res(*_alphabet);
	bool canMakeEmpty = false;

	while(!canMakeEmpty && res.empty()) //check we've generated some symbols
	{
		intLst context;
		for(int ii = 1; ii < _ngrams; ++ii)
		{
			context.push_back(0);
		}

		while(true) //generate some chars
		{
			CRP* dist = getProcess(context);
			int sym = dist->sample();

			if(sym == -1)
			{
				break;
			}

			res.push_back(sym);
			context.push_back(sym);

			context.pop_front();
		}

		if(_allowEmpty)
		{
			canMakeEmpty = true;
		}
	}

	int intern = _lexicon.get(res, true);
	return intern;
}

Prob NGramMonkey::update(int sample, double temp)
{
	++_updates;
	if(_updates > 10000)
	{
		_cache.clear();
		_updates = 0;
	}

	_g0._end = _alphabet->size();

	Prob res = 1.0;

	SymbolString& out = _lexicon.inv(sample);
	intLst context;
	for(int ii = 1; ii < _ngrams; ++ii)
	{
		context.push_back(-1);
	}

	foreach(SymbolString, chr, out)
	{
		CRP* dist = getProcess(context);
		res *= dist->update(*chr, temp);
		context.push_back(*chr);

		context.pop_front();
	}

	CRP* dist = getProcess(context);
	res *= dist->update(-1, temp);
	return res;
}

Prob NGramMonkey::remove(int sample, double temp)
{
	++_updates;
	if(_updates > 10000)
	{
		_cache.clear();
		_updates = 0;
	}

	_g0._end = _alphabet->size();

	Prob res = 1.0;

	SymbolString& out = _lexicon.inv(sample);
	intLst context;
	for(int ii = 1; ii < _ngrams; ++ii)
	{
		context.push_back(-1);
	}

	foreach(SymbolString, chr, out)
	{
		CRP* dist = getProcess(context);
		res *= dist->remove(*chr, temp);
		if(dist->labels().empty())
		{
			rmProcess(context);
		}
		context.push_back(*chr);

		context.pop_front();
	}

	CRP* dist = getProcess(context);
	res *= dist->remove(-1, temp);
	if(dist->labels().empty())
	{
		rmProcess(context);
	}
	return res;
}

Prob NGramMonkey::prob(int sample)
{
	if(inMap(_cache, sample))
	{
		return _cache[sample];
	}

	SymbolString& out = _lexicon.inv(sample);
	int nSyms = out.size();
	if(nSyms == 0) //the empty string
	{
		if(!_allowEmpty) //as an actual string
		{
			return _pEnd;
		}
	}

	_g0._end = _alphabet->size();

	Prob res = 1.0 - _pEnd;

	intLst context;
	for(int ii = 1; ii < _ngrams; ++ii)
	{
		context.push_back(-1);
	}

	foreach(SymbolString, chr, out)
	{
		CRP* dist = getProcess(context);
		res *= dist->prob(*chr);
		context.push_back(*chr);

		context.pop_front();
	}

	CRP* dist = getProcess(context);
	res *= dist->prob(-1);

	_cache[sample] = res;

	return res;
}

Prob NGramMonkey::biPr(int pr, int curr)
{
	intLst context;
	context.push_back(pr);
	CRP* dist = getProcess(context);
	return dist->prob(curr);
}

void NGramMonkey::print(ostream& os)
{
	SymbolString empty(*_alphabet);
	printTree(os, _root, empty);
}

void NGramMonkey::printTree(ostream& os, CRP* node, SymbolString& context)
{
	if(node->_children.empty())
	{
		foreach(SymbolString, ii, context)
		{
			if(*ii != -1)
			{
				os<<_alphabet->inv(*ii);
			}
			else
			{
				os<<"<s>";
			}
		}
		os<<"\n";

		foreach(intIntMap, ci, node->labels())
		{
			if(ci->first != -1)
			{
				os<<"\t"<<_alphabet->inv(ci->first)<<"\t"<<ci->second<<"\n";
			}
			else
			{
				os<<"\t</s>\t"<<ci->second<<"\n";
			}
		}
	}

	foreach(intCRPMap, child, node->_children)
	{
		context.push_back(child->first);
		printTree(os, child->second, context);
		context.pop_back();
	}
}

CRP* NGramMonkey::getProcess(intLst& context)
{
	if(context.empty())
	{
		return _root;
	}
	intLst::reverse_iterator ct = context.rbegin();
	intLst::reverse_iterator rend = context.rend();
	return getProcess(_root, ct, rend, true, 0);
}

void NGramMonkey::rmProcess(intLst& context)
{
	if(context.empty())
	{
		return; //don't actually rm root
	}
	intLst::reverse_iterator ct = context.rbegin();
	intLst::reverse_iterator rend = context.rend();
	getProcess(_root, ct, rend, false, 0);
}

CRP* NGramMonkey::getProcess(CRP* curr, intLst::reverse_iterator& context, 
					   intLst::reverse_iterator& rend, bool get, 
					   int level)
{
	if(context == rend)
	{
		return curr;
	}

	int firstPart = *context;
	++context;

	intCRPMap::iterator entry = curr->_children.find(firstPart);
	if(context == rend && !get)
	{
		//rm case
		assert(entry != curr->_children.end());
		delete entry->second;
		curr->_children.erase(entry);
		return NULL;
	}

	if(entry != curr->_children.end())
	{
		return getProcess(entry->second, context, rend, get, 
						  level + 1);
	}
	else
	{
		CRP* created = new CRP(_alpha[level], curr, _discount[level]);
		curr->_children[firstPart] = created;
		return getProcess(created, context, rend, get, level + 1);
	}
}
