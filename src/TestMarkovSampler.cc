#include "common.h"
#include "SegSampler.h"
#include "Monkey.h"
#include "MarkovSampler.h"

#include <boost/program_options.hpp>

int main(int argc, char* argv[])
{
	appInit(DATA_PATH);

	namespace po = boost::program_options;

	po::options_description desc("Generate text");
	desc.add_options()
		("input-file", po::value<string>(),
		 "file to read input from")
		("init", po::value<string>(),
		 "initial breakpoints (true,random,none,underlying file)")
		("iters", po::value<int>(), "number of iterations")
		("grams", po::value<int>(), "ngram order")
		("alpha", po::value< std::vector<double> >(), "CRP alphas")
		("discount", po::value< std::vector<double> >(), "CRP discount")
		;

	po::positional_options_description p;
	p.add("input-file", -1);

	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv).
			  options(desc).positional(p).run(), vm);
	po::notify(vm);

	if(vm.count("help"))
	{
		cout<<desc<<"\n";
		return 1;
	}

	int grams = 1;

	if(vm.count("grams"))
	{
		grams = vm["grams"].as<int>();
		assert(grams > 0);
		cerr<<"Selected "<<grams<<"-grams\n";
	}

	bool normalReader = true;
	double breakProb = .3;
	doubles crpAlpha(grams, 4);
	doubles crpDiscount(grams, 0);
	string output = "output";
	string baseType = "monkey";
	bool cheat = false;
	bool randomBreaks = true;
	string initUnderlying = "";
	int iters = 100;

	string input;
	if(!vm.count("input-file"))
	{
		cerr<<"Assuming input is "<<output<<".surface\n";
		input = output + ".surface";
	}
	else
	{
		input = vm["input-file"].as<string>();
	}

	if(vm.count("iters"))
	{
		iters = vm["iters"].as<int>();
	}

	if(vm.count("alpha"))
	{
		crpAlpha = vm["alpha"].as< std::vector<double> >();
	}

	for(int ii = 0; ii < crpAlpha.size(); ++ii)
	{
		cerr<<"Alpha: "<<ii<<" = "<<crpAlpha[ii]<<"\n";
	}		

	if(vm.count("discount"))
	{
		crpDiscount = vm["discount"].as< std::vector<double> >();
	}

	for(int ii = 0; ii < crpDiscount.size(); ++ii)
	{
		cerr<<"Discount: "<<ii<<" = "<<crpDiscount[ii]<<"\n";
	}

	if(vm.count("init"))
	{
		string init = vm["init"].as<string>();
		cerr<<"Initialization: "<<init<<"\n";
		if(init == "true")
		{
			cheat = true;
		}
		else if(init == "random")
		{
			randomBreaks = true;
		}
		else if(init == "none")
		{
			randomBreaks = false;
		}
		else
		{
			randomBreaks = false;
			initUnderlying = init;
			input = initUnderlying + ".surface";
			cerr<<"Using "<<input<<" as input file.\n";

			initUnderlying = initUnderlying + ".underlying";
			cerr<<"Using "<<initUnderlying<<" as initialization.\n";

			cheat = true;
		}
	}

	cerr<<"Break prob: "<<breakProb<<"\n";

	cerr<<"Base distribution: "<<baseType<<"\n";

	StrVocab alpha;

	//XXX only for alpha-strings
	for(char aa = 'a'; aa <= 'z'; ++aa)
	{
		string si;
		si += aa;
		alpha.get(si, true);
	}

	Monkey* simian = NULL;
	if(baseType == "monkey")
	{
		simian = new Monkey(breakProb, false, alpha);
	}
	else
	{
		cerr<<"Unknown base distribution "<<baseType<<"\n";
		return 1;
	}

	string empty("");
	SymbolString emptySym(empty, alpha);
	int start = simian->intern(emptySym);
	assert(start == 0);

	CRPLM model(grams, crpAlpha, crpDiscount, simian);
	SegSampler sampler(model);

	LogProbArcSelector<FSTArc> selector;

	ifstream ff(input.c_str());
	if(!ff.is_open())
	{
		cerr<<"Can't open input file '"<<input<<"'\n";
		return 1;
	}

	while(ff)
	{
		string line;
		getline(ff, line);
		if(line.empty())
		{
			break;
		}

		std::istringstream split(line);

		SymbolString si(alpha);
		ints trueSplits;
		int curr = 0;

		while(split)
		{
			SymbolString* word;
			if(normalReader)
			{
				word = new SymbolString(split, alpha);
			}
			else
			{
				string key;
				split>>key;
				if(key == "||")
				{
					cerr<<"*WARNING* Is your reader setting wrong?\n";
					return 1;
				}
				word = new SymbolString(key, alpha);
			}

			if(word->empty())
			{
				break;
			}

			curr += word->size();
			trueSplits.push_back(curr);
			si.append(*word);
			delete word;
		}

		cerr<<"Read "<<si<<"\n";
		sampler.add(si);

		trueSplits.pop_back(); //last 'split' point is end of last word

		//when active, initializes at true segmentations
		if(cheat)
		{
			foreach(ints, splitPt, trueSplits)
			{
				sampler.split(sampler.numSequences() - 1, *splitPt);
			}
		}
		else if(randomBreaks)
		{
			int seqSize = 
				sampler.sequence(sampler.numSequences() - 1).nSyms();
			for(int sym = 1; sym < seqSize - 1; ++sym)
			{
				if(gsl_rng_uniform(RNG) < breakProb)
				{
					sampler.split(sampler.numSequences() - 1, sym);
				}
			}
		}
	}

	foreach(strIntMap, ch, alpha._vocab)
	{
		if(ch->first.size() > 3)
		{
			cerr<<"Very long symbol detected: "<<ch->first<<"\n";
			cerr<<"*WARNING* Did you get your reader setting wrong?\n";
			return 1;
		}
	}

	FSTKit kit(alpha);
	FST misspeller;
	kit.xMisspell(misspeller, .1);
//	kit.identityMisspell(misspeller);

	cerr<<"Alphabet size: "<<alpha.size()<<"\n";
	cerr<<"Base distribution:\n"<<*simian<<"\n";

	int nSeqs = sampler.numSequences();
	double temp = 1.0;

	for(int ii = 0; ii < iters; ++ii)
	{
		for(int seq = 0; seq < nSeqs; ++seq)
		{
			Analysis& ana = sampler.sequence(seq);
			cerr<<ii<<":"<<seq<<" "<<ana;
			MarkovSampler mk(ana, model, misspeller, selector,
				MHR | CUTOFF );

			mk.sample(temp);
		}

		cerr<<"--- generated lexicon ("<<
			model.lexicon().size()<<" words) --- \n";
		foreach(intIntMap, word, model.lexicon())
		{
			cerr<<model.repr(word->first)<<" "<<word->second<<"\n";
		}
	}

	cerr<<"\n***************\n\n";
	// cerr<<sampler<<"\n";
	// sampler.writeSeqs(cout);
	// sampler.summarize(cerr);
}
