#!/usr/bin/python
from __future__ import division

import sys
from path import path
from collections import defaultdict
import re
from itertools import izip

import PrecRec
from waterworks.ClusterMetrics import ConfusionMatrix
from waterworks.iterextras import pairwise

def formatLines(fn, charBounds=False):
    melsnerReader = False
    for line in file(fn):
        line = line.strip()

        if line.endswith("|") or "||" in line:
            melsnerReader = True
            break

    for line in file(fn):
        line = line.strip()

        if melsnerReader:
            if not line.endswith("|") and "||" not in line:
                #only one word
                chars = [tuple(line.strip().split(" ")),]
                chars = [tuple([x.split("?")[0] for x in word]) 
                         for word in chars]

                yield chars
            else:
                #melsner format: spaces separate chars, || separates words
                words = re.split("\|(\d+-\d+)?\|", line)
                chars = [tuple(x.strip().split(" ")) for x in words if x]
                chars = [tuple([x.split("?")[0] for x in word]) 
                         for word in chars]

                if charBounds:
                    progress = 0
                    res = []
                    for item in chars:
                        res.append(item)
                        res.append(("%d-%d" % 
                                    (progress, progress + len(item)),))
                        progress += len(item)
                    yield res
                else:
                    yield chars
        else:
            #auh20 format: char-for-char, spaces separate words
            words = line.split()
            chars = [tuple(x) for x in words]
            if charBounds:
                progress = 0
                res = []
                for item in chars:
                    res.append(item)
                    res.append(("%d-%d" % (progress, progress + len(item)),))
                    progress += len(item)
                yield res
            else:
                yield chars

def cat(strs):
    return reduce(lambda x,y: x + y, strs)

def getBreaks(words):
    breaks = []
    soFar = 0
    for word in words:
        soFar += len(word)
        breaks.append(soFar)
    breaks.pop(-1) #end-of-string isn't a break
    return breaks

def scoreBreaks(surface, found):
    matched = 0
    proposed = 0
    actual = 0

    warned = 0

    for wReal,wFound in izip(formatLines(surface), formatLines(found)):
        cat1 = cat([cat(x) for x in wReal])
        cat2 = cat([cat(x) for x in wFound])
        if cat1 != cat2:
            if not warned:
                print "Warning: surface string mismatch:", cat1, cat2
                warned = 1
            elif warned == 1:
                print "Warning: more mismatches"
                warned += 1
            #sys.exit(1)

        trueBreaks = set(getBreaks(wReal))

        foundBreaks = set(getBreaks(wFound))

        # if trueBreaks != foundBreaks:
        #     print "Error!"
        #     print "Real words", wReal
        #     print "Found words", wFound
        #     print trueBreaks
        #     print foundBreaks

        actual += len(trueBreaks)
        proposed += len(foundBreaks)
        matched += len(trueBreaks.intersection(foundBreaks))

    return PrecRec.precision_recall_f(matched, actual, proposed)

def getWords(wordLst):
    words = []
    bounds = []

    for ii in range(len(wordLst)):
        if ii % 2 == 0:
            words.append(wordLst[ii])
        else:
            (b1,b2) = wordLst[ii][0].split("-")
            bounds.append((int(b1), int(b2)))
    return words, bounds

def scoreWords(underlying, found, labeled=True, surface=False,
               foundSurface=False):
    matched = 0
    proposed = 0
    actual = 0

    for wReal,wFound in izip(formatLines(underlying, charBounds=surface),
                             formatLines(found, charBounds=foundSurface)):
        realWords, realBounds = getWords(wReal)
        #print realWords, realBounds

        foundWords, foundBounds = getWords(wFound)
        #print foundWords, foundBounds

        if labeled:
            realSeq = zip(realWords, realBounds)
            foundSeq = zip(foundWords, foundBounds)
        else:
            realSeq = realBounds
            foundSeq = foundBounds

        actual += len(realSeq)
        proposed += len(foundSeq)
        matched += len(set(realSeq).intersection(foundSeq))

    return PrecRec.precision_recall_f(matched, actual, proposed)

def scoreMappedWords(underlying, found, surface=False, foundSurface=False):
    conf = ConfusionMatrix()

    for wReal,wFound in izip(formatLines(underlying, charBounds=surface),
                             formatLines(found, charBounds=foundSurface)):
        realWords, realBounds = getWords(wReal)
        #print realWords, realBounds

        foundWords, foundBounds = getWords(wFound)
        #print foundWords, foundBounds

        boundToWord = dict(zip(foundBounds, foundWords))
        for bi,real in zip(realBounds, realWords):
            if bi in boundToWord:
                conf.add(real, boundToWord[bi])
    mapping = conf.one_to_one_greedy_mapping()

    # for k,v in mapping.items():
    #     if k != v:
    #         print "".join(v), "->", "".join(k)

    matched = 0
    proposed = 0
    actual = 0

    for wReal,wFound in izip(formatLines(underlying, charBounds=surface),
                             formatLines(found, charBounds=foundSurface)):
        realWords, realBounds = getWords(wReal)
        #print realWords, realBounds

        foundWords, foundBounds = getWords(wFound)
        #print foundWords, foundBounds

        def mapWord(word):
            if word in mapping:
                return mapping[word]
            return "not-mapped-%s" % "".join(word)

        mappedWords = [mapWord(x) for x in foundWords]

        realSeq = zip(realWords, realBounds)
        foundSeq = zip(mappedWords, foundBounds)

        actual += len(realSeq)
        proposed += len(foundSeq)
        matched += len(set(realSeq).intersection(foundSeq))

    return PrecRec.precision_recall_f(matched, actual, proposed)

def scoreLexicon(underlying, found, surface=False, foundSurface=False):
    realLex = set()
    foundLex = set()

    for wReal,wFound in izip(formatLines(underlying, charBounds=surface),
                             formatLines(found, charBounds=foundSurface)):
        realWords, realBounds = getWords(wReal)
        #print realWords, realBounds

        foundWords, foundBounds = getWords(wFound)
        #print foundWords, foundBounds

        for word in realWords:
            realLex.add(word)

        for word in foundWords:
            foundLex.add(word)

    actual = len(realLex)
    proposed = len(foundLex)
    matched = len(realLex.intersection(foundLex))

    # print "-- real lexicon --"

    # for xx in realLex:
    #     print cat(xx)

    # print

    # print "-- found lexicon --"

    # for xx in foundLex:
    #     print cat(xx)

    # print actual, "true words", proposed, "found words", matched, "matched"

    return PrecRec.precision_recall_f(matched, actual, proposed)

def scoreMappedLexicon(underlying, found, surface=False, foundSurface=False,
                       metric="one-to-one"):
    conf = ConfusionMatrix()

    for wReal,wFound in izip(formatLines(underlying, charBounds=surface),
                             formatLines(found, charBounds=foundSurface)):
        realWords, realBounds = getWords(wReal)
        #print realWords, realBounds

        foundWords, foundBounds = getWords(wFound)
        #print foundWords, foundBounds

        boundToWord = dict(zip(foundBounds, foundWords))
        for bi,real in zip(realBounds, realWords):
            if bi in boundToWord:
                conf.add(real, boundToWord[bi])
    mapping = conf.one_to_one_greedy_mapping()

    if metric == "v-beta":
        return conf.v_beta()
    elif metric == "v-measure":
        return conf.v_measure()
    else:
        assert(metric == "one-to-one")

    realLex = set()
    foundLex = set()

    for wReal,wFound in izip(formatLines(underlying, charBounds=surface),
                             formatLines(found, charBounds=foundSurface)):
        realWords, realBounds = getWords(wReal)
        #print realWords, realBounds

        foundWords, foundBounds = getWords(wFound)
        #print foundWords, foundBounds

        for word in realWords:
            realLex.add(word)

        for word in foundWords:
            if word in mapping:
                foundLex.add(mapping[word])
            else:
                foundLex.add("no-mapping-for-%s" % "".join(word))

    actual = len(realLex)
    proposed = len(foundLex)
    matched = len(realLex.intersection(foundLex))

    print actual, "true words", proposed, "found mapped words",\
        matched, "matched"

    return PrecRec.precision_recall_f(matched, actual, proposed)

if __name__ == "__main__":
    stem = sys.argv[1]

    underlying = path(stem + ".underlying")
    surface = path(stem + ".surface")
    surfaceIsUnderlying = False
    foundSurfaceIsUnderlying = False
    requireLearned = False
    if not surface.exists():
        print "No surface forms found. Trying", stem
        surface = path(stem)
        if not surface.exists():
            print "No gold-standard forms found."
            sys.exit(1)
    if not underlying.exists():
        print "Failed to detect underlying forms."
        print "Assuming they match the surface forms."
        surfaceIsUnderlying = True
        underlying = surface

    if len(sys.argv) > 2:
        learnedStem = sys.argv[2]
    else:
        learnedStem = stem
        requireLearned = True

    foundUnderlying = path(learnedStem + ".learned.underlying")
    foundSurface = path(learnedStem + ".learned.surface")

    if not foundSurface.exists():
        print "No surface forms found."
        if not requireLearned:
            print "Trying", learnedStem
            foundSurface = path(learnedStem)
            if not foundSurface.exists():
                print "No learned forms found."
                sys.exit(1)
        else:
            sys.exit(1)
    if not foundUnderlying.exists():
        print "Failed to detect learned underlying forms."
        print "Assuming they match the surface forms."
        foundSurfaceIsUnderlying = True
        foundUnderlying = foundSurface

    nLines = None
    for fn in [underlying, surface, foundUnderlying, foundSurface]:
        lines = len(file(fn).readlines())
        if nLines is None:
            nLines = lines
        else:
            if nLines != lines:
                print "Number of lines mismatches,", nLines, lines
                sys.exit(1)

    (bp,br,bf) = scoreBreaks(surface, foundSurface)
    (wp,wr,wf) = scoreWords(underlying, foundUnderlying, 
                            surface=surfaceIsUnderlying,
                            foundSurface=foundSurfaceIsUnderlying)
    (swp,swr,swf) = scoreWords(underlying, foundUnderlying, labeled=False,
                               surface=surfaceIsUnderlying,
                               foundSurface=foundSurfaceIsUnderlying)
    (mwp, mwr, mwf) = scoreMappedWords(underlying, foundUnderlying,
                                       surface=surfaceIsUnderlying,
                                       foundSurface=foundSurfaceIsUnderlying)
    (lp,lr,lf) = scoreLexicon(underlying, foundUnderlying, 
                              surface=surfaceIsUnderlying,
                               foundSurface=foundSurfaceIsUnderlying)
    (mlp,mlr,mlf) = scoreMappedLexicon(underlying, foundUnderlying, 
                                       surface=surfaceIsUnderlying,
                                       foundSurface=foundSurfaceIsUnderlying)
    # (lexV) = scoreMappedLexicon(underlying, foundUnderlying, 
    #                             surface=surfaceIsUnderlying,
    #                             foundSurface=
    #                             foundSurfaceIsUnderlying,
    #                             metric="v-measure")

    print "UP %4.2f UR %4.2f UF %4.2f" % (100 * wp, 100 * wr, 100 * wf)
    print "MP %4.2f MR %4.2f MF %4.2f" % (100 * mwp, 100 * mwr, 100 * mwf)
    print "SP %4.2f SR %4.2f SF %4.2f" % (100 * swp, 100 * swr, 100 * swf)
    print "BP %4.2f BR %4.2f BF %4.2f" % (100 * bp, 100 * br, 100 * bf)
    print "LP %4.2f LR %4.2f LF %4.2f" % (100 * lp, 100 * lr, 100 * lf)
    print "MLP %4.2f MLR %4.2f MLF %4.2f" % (100 * mlp, 100 * mlr, 100 * mlf)
    # print "LVM %4.2f" % (100 * lexV)

