#!/usr/bin/python
from __future__ import division

import sys
from path import path
from collections import defaultdict
import re
from itertools import izip

import PrecRec
from waterworks.Probably import xlog2x
from waterworks.ClusterMetrics import ConfusionMatrix
from waterworks.iterextras import pairwise

from scoreSeg import formatLines, getWords

from clusterAnalysis import reportForAllRealTokens, reportForAllFoundTokens
from AlignTokens import Aligner

def countUp(stat, counts):
    if stat["left"] and stat["right"]:
        if stat["internal"]:
            if stat["found"]:
                counts["corrsplit"] += 1
            else:
                counts["wrongsplit"] += 1
        else:
            if stat["found"]:
                counts["corr"] += 1
            else:
                counts["wrongform"] += 1
    else:
        if stat["inword"]:
            counts["inword"] += 1
        elif stat["colloc"]:
            if stat["found"]:
                counts["corrcoll"] += 1
            else:
                counts["colloc"] += 1
        else:
            if stat["left"]:
                if stat["found"]:
                    counts["foundleft"] += 1
                else:
                    counts["left"] += 1
            else:
                if stat["right"]:
                    if stat["found"]:
                        counts["foundright"] += 1
                    else:
                        counts["right"] += 1
                else:
                    counts["neither"] += 1

if __name__ == "__main__":
    stem = sys.argv[1]

    underlying = path(stem + ".underlying")
    surface = path(stem + ".surface")
    surfaceIsUnderlying = False
    foundSurfaceIsUnderlying = False
    requireLearned = False
    if not surface.exists():
        print "No surface forms found. Trying", stem
        surface = path(stem)
        if not surface.exists():
            print "No gold-standard forms found."
            sys.exit(1)
    if not underlying.exists():
        print "Failed to detect underlying forms."
        print "Assuming they match the surface forms."
        surfaceIsUnderlying = True
        underlying = surface

    if len(sys.argv) > 2:
        learnedStem = sys.argv[2]
    else:
        learnedStem = stem
        requireLearned = True

    foundUnderlying = path(learnedStem + ".learned.underlying")
    foundSurface = path(learnedStem + ".learned.surface")

    if not foundSurface.exists():
        print "No surface forms found."
        if not requireLearned:
            print "Trying", learnedStem
            foundSurface = path(learnedStem)
            if not foundSurface.exists():
                print "No learned forms found."
                sys.exit(1)
        else:
            sys.exit(1)
    if not foundUnderlying.exists():
        print "Failed to detect learned underlying forms."
        print "Assuming they match the surface forms."
        foundSurfaceIsUnderlying = True
        foundUnderlying = foundSurface

    nLines = None
    for fn in [underlying, surface, foundUnderlying, foundSurface]:
        lines = len(file(fn).readlines())
        if nLines is None:
            nLines = lines
        else:
            if nLines != lines:
                print "Number of lines mismatches,", nLines, lines
                sys.exit(1)

    stats = reportForAllRealTokens(underlying, foundUnderlying,
                                surface=surfaceIsUnderlying,
                                foundSurface=foundSurfaceIsUnderlying,
                                   surf=foundSurface,
                                verbose=False)

    vTypes = 0
    cTypes = 0
    vToks = 0
    cToks = 0

    vCounts = defaultdict(int)
    cCounts = defaultdict(int)

    for word, stts in stats.items():
        if word[0][0] in ["a", "e", "i", "o", "u"]:
            vTypes += 1

            vToks += len(stts)

            for stat in stts:
                countUp(stat, vCounts)

        else:
            cTypes += 1

            cToks += len(stts)

            for stat in stts:
                countUp(stat, cCounts)

    print "Real vocab:", vTypes, "vowel init", cTypes, "cons init"
    print "Real vocab:", vToks, "vowel init toks", cToks, "cons init toks"

    print "------- vow -----------"

    den = sum(vCounts.values())
    for (kk,vv) in sorted(vCounts.items(), key=lambda xx: xx[-1], reverse=True):
        print kk, vv, vv/den

    print "------- cons -----------"

    den = sum(cCounts.values())
    for (kk,vv) in sorted(cCounts.items(), key=lambda xx: xx[-1], reverse=True):
        print kk, vv, vv/den


    stats = reportForAllFoundTokens(underlying, foundUnderlying,
                                surface=surfaceIsUnderlying,
                                foundSurface=foundSurfaceIsUnderlying,
                                verbose=False)

    vTypes = 0
    cTypes = 0
    vToks = 0
    cToks = 0

    for word, stts in stats.items():
        if word[0][0] in ["a", "e", "i", "o", "u"]:
            vTypes += 1

            vToks += len(stts)

        else:
            cTypes += 1

            cToks += len(stts)

    print "Found vocab:", vTypes, "vowel init", cTypes, "cons init"
    print "Found vocab:", vToks, "vowel init toks", cToks, "cons init toks"
