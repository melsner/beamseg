#!/usr/bin/python
from __future__ import division

import sys
from path import path
from collections import defaultdict
import re
from itertools import izip

import PrecRec
from waterworks.Probably import xlog2x
from waterworks.ClusterMetrics import ConfusionMatrix
from waterworks.iterextras import pairwise

from scoreSeg import formatLines, getWords

from clusterAnalysis import reportForAllRealTokens
from AlignTokens import Aligner

def editCheck(case):
    (real, adjRealBounds, seq, charseq) = case
    allSeq = reduce(lambda x, y: list(x) + list(y), seq, [])

    aligner = Aligner(allSeq, charseq)
    acts = aligner.recover_actions()

    # print real
    # print "allsq", allSeq
    # print "csq", charseq
    # print acts

    furthestLeftCopy = None
    furthestRightCopy = None

    charSeqInd = 0
    for act in acts:
        if act == "copy":
            if furthestLeftCopy == None:
                furthestLeftCopy = charSeqInd
            furthestRightCopy = charSeqInd

            charSeqInd += 1

        if act == "ins":
            charSeqInd += 1


    # print furthestLeftCopy, furthestRightCopy

    realBegin, realEnd = adjRealBounds
    realEnd -= 1

    if len(allSeq) < 3:
        return

    if realBegin > furthestRightCopy:
        # print "real word", real
        # print "sequence of found chars", allSeq
        # print "surface reflex of found chars", charseq
        # # print acts
        # print furthestLeftCopy, furthestRightCopy, realBegin, realEnd

        # print "off the right edge"

        return (real, tuple(seq), charseq)
    elif realEnd < furthestLeftCopy:
        # print real
        # print allSeq
        # print charseq
        # print realBegin, realEnd, furthestLeftCopy, furthestRightCopy
        # print "off the left edge"

        return (real, tuple(seq), charseq)

    return None

if __name__ == "__main__":
    stem = sys.argv[1]

    underlying = path(stem + ".underlying")
    surface = path(stem + ".surface")
    surfaceIsUnderlying = False
    foundSurfaceIsUnderlying = False
    requireLearned = False
    if not surface.exists():
        print "No surface forms found. Trying", stem
        surface = path(stem)
        if not surface.exists():
            print "No gold-standard forms found."
            sys.exit(1)
    if not underlying.exists():
        print "Failed to detect underlying forms."
        print "Assuming they match the surface forms."
        surfaceIsUnderlying = True
        underlying = surface

    if len(sys.argv) > 2:
        learnedStem = sys.argv[2]
    else:
        learnedStem = stem
        requireLearned = True

    foundUnderlying = path(learnedStem + ".learned.underlying")
    foundSurface = path(learnedStem + ".learned.surface")

    if not foundSurface.exists():
        print "No surface forms found."
        if not requireLearned:
            print "Trying", learnedStem
            foundSurface = path(learnedStem)
            if not foundSurface.exists():
                print "No learned forms found."
                sys.exit(1)
        else:
            sys.exit(1)
    if not foundUnderlying.exists():
        print "Failed to detect learned underlying forms."
        print "Assuming they match the surface forms."
        foundSurfaceIsUnderlying = True
        foundUnderlying = foundSurface

    nLines = None
    for fn in [underlying, surface, foundUnderlying, foundSurface]:
        lines = len(file(fn).readlines())
        if nLines is None:
            nLines = lines
        else:
            if nLines != lines:
                print "Number of lines mismatches,", nLines, lines
                sys.exit(1)

    stats = reportForAllRealTokens(underlying, foundUnderlying,
                                surface=surfaceIsUnderlying,
                                foundSurface=foundSurfaceIsUnderlying,
                                   surf=foundSurface,
                                verbose=False)

    gCounts = defaultdict(int)
    targWord = ("y", "uw")
    for si in stats[targWord]:
        tc = si["case"]
        tc = (tc[0], tuple(tc[1]))
        gCounts[tc] += 1

    print
    print "------------- %s -----------" % " ".join(targWord)
    print

    for pair,ct in sorted(gCounts.items(), key=lambda xx: xx[-1], 
                          reverse=True):
        if ct < 3:
            break

        print pair, ct

    agg = reduce(lambda x, y: x + y, stats.values(), [])
    print len(agg), "agg"

    edCounts = defaultdict(int)
    confCounts = defaultdict(int)
    confCountsWrong = defaultdict(int)

    counts = defaultdict(int)
    for stat in agg:
        ed = editCheck(stat["data"])
        if ed != None:
            edCounts[ed] += 1

        if "confuse" in stat:
            confCounts[stat["confuse"]] += 1
        if "confuseWrong" in stat:
            confCountsWrong[stat["confuseWrong"]] += 1

        if stat["left"] and stat["right"]:
            if stat["internal"]:
                if stat["found"]:
                    counts["corrsplit"] += 1
                else:
                    counts["wrongsplit"] += 1
            else:
                if stat["found"]:
                    counts["corr"] += 1
                else:
                    counts["wrongform"] += 1
        else:
            # if stat["inword"]:
            #     counts["inword"] += 1
            if stat["colloc"]:
                if stat["found"]:
                    counts["corrcoll"] += 1
                else:
                    counts["colloc"] += 1
            else:
                if stat["left"]:
                    # if stat["found"]:
                    #     counts["foundleft"] += 1
                    # else:
                    #     counts["left"] += 1
                    counts["left"] += 1
                else:
                    if stat["right"]:
                        # if stat["found"]:
                        #     counts["foundright"] += 1
                        # else:
                        #     counts["right"] += 1
                        counts["right"] += 1
                    else:
                        counts["neither"] += 1

    den = sum(counts.values())
    for (kk,vv) in sorted(counts.items(), key=lambda xx: xx[-1], reverse=True):
        print kk, vv, vv/den

    for word,statLst in stats.items():
        counts = defaultdict(int)
        for stat in statLst:

            if stat["left"] and stat["right"]:
                if stat["internal"]:
                    if stat["found"]:
                        counts["corrsplit"] += 1
                    else:
                        counts["wrongsplit"] += 1
                else:
                    if stat["found"]:
                        counts["corr"] += 1
                    else:
                        counts["wrongform"] += 1
            else:
                if stat["inword"]:
                    counts["inword"] += 1
                elif stat["colloc"]:
                    if stat["found"]:
                        counts["corrcoll"] += 1
                    else:
                        counts["colloc"] += 1
                else:
                    if stat["left"]:
                        if stat["found"]:
                            counts["foundleft"] += 1
                        else:
                            counts["left"] += 1
                    else:
                        if stat["right"]:
                            if stat["found"]:
                                counts["foundright"] += 1
                            else:
                                counts["right"] += 1
                        else:
                            counts["neither"] += 1

        if len(statLst) > 100:
            print word,
            den = sum(counts.values())
            for (kk,vv) in sorted(counts.items(), 
                                  key=lambda xx: xx[-1], reverse=True):
                print kk, vv/den, 
            print

    print
    print "------------- CONFUSIONS DELETING A WORD -----------"
    print

    for pair,ct in sorted(edCounts.items(), key=lambda xx: xx[-1], 
                          reverse=True):
        if ct < 3:
            break

        print pair, ct

    print
    print "------------- CONFUSIONS WITH CORRECT BOUNDS -----------"
    print

    for pair,ct in sorted(confCounts.items(), key=lambda xx: xx[-1], 
                          reverse=True):
        if ct < 3:
            break

        print pair, ct

    print
    print "------------- MISC CONFUSIONS --------------"
    print

    for pair,ct in sorted(confCountsWrong.items(), key=lambda xx: xx[-1], 
                          reverse=True):
        if ct < 3:
            break

        print pair, ct
