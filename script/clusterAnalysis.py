#!/usr/bin/python
from __future__ import division

import sys
from path import path
from collections import defaultdict
import re
from itertools import izip

import PrecRec
from waterworks.Probably import xlog2x
from waterworks.ClusterMetrics import ConfusionMatrix
from waterworks.iterextras import pairwise

from scoreSeg import formatLines, getWords, cat

# import pylab
# from numpy import array

def isAnyBound(searchFor, realBounds, left):
    for bi in realBounds:
        if left:
            if bi[0] == searchFor:
                return True
        else:
            if bi[1] == searchFor:
                return True

    return False

def createMapping(underlying, found, surface=False, foundSurface=False):
    conf = ConfusionMatrix()

    for wReal,wFound in izip(formatLines(underlying, charBounds=surface),
                             formatLines(found, charBounds=foundSurface)):
        realWords, realBounds = getWords(wReal)

        foundWords, foundBounds = getWords(wFound)
        #print foundWords, foundBounds

        boundToWord = dict(zip(foundBounds, foundWords))
        for bi,real in zip(realBounds, realWords):
            if bi in boundToWord:
                conf.add(real, boundToWord[bi])
    mapping = conf.one_to_one_greedy_mapping()

    return mapping

def reportForAllFoundTokens(underlying, found, surface=False,
                            foundSurface=False, verbose=False):
    stats = defaultdict(list)

    mapping = createMapping(underlying, found, surface, foundSurface)
    def mapWord(word):
        if word in mapping:
            return mapping[word]
        return "not-mapped-%s" % "".join(word)

    for wReal,wFound in izip(formatLines(underlying, charBounds=surface),
                             formatLines(found, charBounds=foundSurface)):
        realWords, realBounds = getWords(wReal)
        #print realWords, realBounds

        foundWords, foundBounds = getWords(wFound)
        #print foundWords, foundBounds

        for foundWord, foundBound in zip(foundWords, foundBounds):
            targBound = foundBound

            realSeq = []
            realBoundSeq = []
            take = 0

            for ind,bi in enumerate(realBounds):
                if take == 0 and (bi[0] <= targBound[0] < bi[1]):
                    take = 1

                if take == 1:
                    realSeq.append(ind)
                    realBoundSeq.append(bi)

                if bi[1] >= targBound[1]:
                    take = 2

            if take < 2:
                realSeq = None

            if realSeq:
                realWordSeq = []
                for ind in realSeq:
                    realWordSeq.append(realWords[ind])
            else:
                #can't happen
                print foundWord, foundBound
                print realBounds
                print realSeq
                print take
                assert(0)

            #questions to answer:
            #did we get the left bound?
            #did we get the right bound?
            #is there a true bound internal to this word?
            #did we detect the true word correctly somewhere in here?
            #is the whole sequence a collocation?
            #are we entirely contained within a true word?

            leftBoundOk = (targBound[0] == realBoundSeq[0][0])
            rightBoundOk = (targBound[1] == realBoundSeq[-1][1])

            leftBoundThisWord = (targBound[0] == realBoundSeq[0][0] and
                                 realWordSeq[0] == mapWord(foundWord))
            rightBoundThisWord = (targBound[1] == realBoundSeq[-1][1] and
                                  realWordSeq[-1] == mapWord(foundWord))

            internalBound = (len(realSeq) > 1)

            leftBoundIsAnyBound = isAnyBound(targBound[0], realBounds, 
                                             left=True)
            rightBoundIsAnyBound = isAnyBound(targBound[1], realBounds,
                                              left=False)
            colloc = (leftBoundIsAnyBound and rightBoundIsAnyBound and
                      internalBound)

            insideWord = (not leftBoundOk and not rightBoundOk \
                              and not internalBound)

            foundIt = any([ri == mapWord(foundWord) for ri in realWordSeq])

            isCorrect = (leftBoundOk and rightBoundOk and \
                             not internalBound and foundIt)

            corrColloc = (leftBoundIsAnyBound and rightBoundIsAnyBound and
                          internalBound and foundIt)

            stats[foundWord].append({"left": leftBoundOk,
                                     "right": rightBoundOk,
                                     "leftword": leftBoundThisWord,
                                     "rightword": rightBoundThisWord,
                                     "internal": internalBound,
                                     "colloc": colloc,
                                     "inword": insideWord,
                                     "found": foundIt,
                                     "correct": isCorrect,
                                     "corrcolloc": corrColloc,
                                     "case": (foundWord, realWordSeq)
                                     })

            if verbose:
                print foundWord, foundBound
                print realSeq, realBoundSeq, realWordSeq

                print "left", leftBoundOk, "right", rightBoundOk
                print "true bound inside the word", internalBound
                print "collocation", colloc, "correct collocation", corrColloc
                print "inside a real word", insideWord
                print "mapped to", mapWord(foundWord)
                print "word actually present", foundIt
                print "completely right", isCorrect

    return stats

def reportForAllRealTokens(underlying, found, surface=False,
                        foundSurface=False, surf=None, verbose=False):
    stats = defaultdict(list)

    mapping = createMapping(underlying, found, surface, foundSurface)
    def mapWord(word):
        if word in mapping:
            return mapping[word]
        return "not-mapped-%s" % "".join(word)

    for wReal,wFound,wSurfC in izip(
        formatLines(underlying, charBounds=surface),
        formatLines(found, charBounds=foundSurface),
        formatLines(surf, charBounds=True)):

        surfWords, surfBounds = getWords(wSurfC)
        surfChars = cat(surfWords)
        # print surfChars

        realWords, realBounds = getWords(wReal)
        #print realWords, realBounds

        foundWords, foundBounds = getWords(wFound)
        #print foundWords, foundBounds

        for realWord, realBound in zip(realWords, realBounds):
            targBound = realBound

            foundSeq = []
            foundBoundSeq = []
            take = 0

            for ind,bi in enumerate(foundBounds):
                if take == 0 and (bi[0] <= targBound[0] < bi[1]):
                    take = 1

                if take == 1:
                    foundSeq.append(ind)
                    foundBoundSeq.append(bi)

                if bi[1] >= targBound[1]:
                    take = 2

            if take < 2:
                foundSeq = None

            if foundSeq:
                foundWordSeq = []
                for ind in foundSeq:
                    foundWordSeq.append(foundWords[ind])
            else:
                #can't happen
                print realWord, realBound
                print foundBounds
                print foundSeq
                print take
                assert(0)

            #questions to answer:
            #did we get the left bound?
            #did we get the right bound?
            #is there a proposed bound internal to this word?
            #did we detect this word correctly somewhere in the proposed seq
            #is the found sequence a collocation?
            #are we entirely contained within a proposed word

            leftBoundOk = (targBound[0] == foundBoundSeq[0][0])
            rightBoundOk = (targBound[1] == foundBoundSeq[-1][1])
            internalBound = (len(foundSeq) > 1)

            #inside a collocation:
            #no word boundary was proposed in this word
            #and both boundaries of the found word are real boundaries
            #and the proposed bounds are not correct
            colloc = (not internalBound and
                      isAnyBound(foundBoundSeq[0][0], realBounds, left=True)
                      and
                      isAnyBound(foundBoundSeq[0][1], realBounds, left=False)
                      and (not leftBoundOk or not rightBoundOk))

            insideWord = (not leftBoundOk and not rightBoundOk \
                              and not internalBound)
            foundIt = any([realWord == mapWord(fi) for fi in foundWordSeq])
            isCorrect = (leftBoundOk and rightBoundOk and \
                             not internalBound and foundIt)

            relIndBound = (realBound[0] - foundBoundSeq[0][0],
                           realBound[1] - foundBoundSeq[0][0])

            stat = {"left": leftBoundOk,
                    "right": rightBoundOk,
                    "internal": internalBound,
                    "colloc": colloc,
                    "inword": insideWord,
                    "found": foundIt,
                    "correct": isCorrect,
                    "case": (realWord, foundWordSeq),
                    "data": (realWord,
                             relIndBound,
                             foundWordSeq,
                             surfChars[foundBoundSeq[0][0]:
                                           foundBoundSeq[-1][1]])
                    }

            if not foundIt and leftBoundOk and rightBoundOk \
                    and not internalBound:
                if any([fi in mapping for fi in foundWordSeq]):
                    # print foundWordSeq[0], "maps to", mapWord(foundWordSeq[0])
                    stat["confuse"] = (realWord, mapWord(foundWordSeq[0]))

            if not foundIt and not internalBound and \
                    not (leftBoundOk and rightBoundOk):
                if any([fi in mapping for fi in foundWordSeq]):
                    stat["confuseWrong"] = (realWord, mapWord(foundWordSeq[0]))
                    # if realWord == ("ih", "t") and  mapWord(foundWordSeq[0]) == ("d", "ah", "z"):
                    #     print realWord, foundWordSeq[0], wReal, wFound

            stats[realWord].append(stat)

            if verbose:
                print realWord, realBound
                print foundSeq, foundBoundSeq, foundWordSeq

                print "left", leftBoundOk, "right", rightBoundOk
                print "proposed bound inside the word", internalBound
                print "collocation", colloc
                print "inside a real word", insideWord
                print "word actually present", foundIt
                print "completely right", isCorrect

    return stats

def sortedVocab(underlying, surface=False):
    vocab = defaultdict(int)
    for wReal in formatLines(underlying, charBounds=surface):
        realWords, realBounds = getWords(wReal)

        for wi in realWords:
            vocab[wi] += 1

    return sorted(vocab.keys(), key=lambda xx: vocab[xx], reverse=True)

def wordFreqs(underlying, surface=False):
    vocab = defaultdict(int)
    for wReal in formatLines(underlying, charBounds=surface):
        realWords, realBounds = getWords(wReal)

        for wi in realWords:
            vocab[wi] += 1

    return vocab

def describeByLength(underlying, found, surface=False,
               foundSurface=False):
    matchedWordLengths = defaultdict(int)
    actualWordLengths = defaultdict(int)
    proposedWordLengths = defaultdict(int)

    matched = 0
    proposed = 0
    actual = 0

    for wReal,wFound in izip(formatLines(underlying, charBounds=surface),
                             formatLines(found, charBounds=foundSurface)):
        realWords, realBounds = getWords(wReal)
        #print realWords, realBounds

        foundWords, foundBounds = getWords(wFound)
        #print foundWords, foundBounds

        realSeq = realBounds
        foundSeq = foundBounds

        for item in set(realSeq):
            actualWordLengths[item[1] - item[0]] += 1
            actual += 1

        for item in set(foundSeq):
            proposedWordLengths[item[1] - item[0]] += 1
            proposed += 1

        for item in set(realSeq).intersection(foundSeq):
            matchedWordLengths[item[1] - item[0]] += 1
            matched += 1

    allLengths = set(actualWordLengths.keys() +
                     proposedWordLengths.keys() +
                     matchedWordLengths.keys())

    for le in sorted(allLengths):
        print "\tLength:", le, "\t",
        print "mtch:", matchedWordLengths[le], "\t",
        print "prop:", proposedWordLengths[le], "\t",
        print "act:", actualWordLengths[le], "\t",
        (pp, rr, ff) = PrecRec.precision_recall_f(matchedWordLengths[le],
                                                  actualWordLengths[le],
                                                  proposedWordLengths[le])
        print "\tP: %.3g" % pp, "\tR: %.3g" % rr, "\tF: %.3f" % ff

    print "Overall, mtch:", matched, "prop:", proposed, "act:", actual,
    (pp, rr, ff) = PrecRec.precision_recall_f(matched, actual, proposed)
    print "\tP: %.3g" % pp, "\tR: %.3g" % rr, "\tF: %.3f" % ff


if __name__ == "__main__":
    stem = sys.argv[1]

    underlying = path(stem + ".underlying")
    surface = path(stem + ".surface")
    surfaceIsUnderlying = False
    foundSurfaceIsUnderlying = False
    requireLearned = False
    if not surface.exists():
        print "No surface forms found. Trying", stem
        surface = path(stem)
        if not surface.exists():
            print "No gold-standard forms found."
            sys.exit(1)
    if not underlying.exists():
        print "Failed to detect underlying forms."
        print "Assuming they match the surface forms."
        surfaceIsUnderlying = True
        underlying = surface

    if len(sys.argv) > 2:
        learnedStem = sys.argv[2]
    else:
        learnedStem = stem
        requireLearned = True

    foundUnderlying = path(learnedStem + ".learned.underlying")
    foundSurface = path(learnedStem + ".learned.surface")

    if not foundSurface.exists():
        print "No surface forms found."
        if not requireLearned:
            print "Trying", learnedStem
            foundSurface = path(learnedStem)
            if not foundSurface.exists():
                print "No learned forms found."
                sys.exit(1)
        else:
            sys.exit(1)
    if not foundUnderlying.exists():
        print "Failed to detect learned underlying forms."
        print "Assuming they match the surface forms."
        foundSurfaceIsUnderlying = True
        foundUnderlying = foundSurface

    nLines = None
    for fn in [underlying, surface, foundUnderlying, foundSurface]:
        lines = len(file(fn).readlines())
        if nLines is None:
            nLines = lines
        else:
            if nLines != lines:
                print "Number of lines mismatches,", nLines, lines
                sys.exit(1)

    describeByLength(underlying, foundUnderlying,
                     surface=surfaceIsUnderlying,
                     foundSurface=foundSurfaceIsUnderlying)

    # hByLen = defaultdict(list)
    # Hs = reportForAllFoundTokens(underlying, foundUnderlying,
    #                             surface=surfaceIsUnderlying,
    #                             foundSurface=foundSurfaceIsUnderlying,
    #                             verbose=False)

    # xs = []
    # ys = []

    # for wi,H in Hs.items():
    #     # print " ".join(vi)
    #     # print
    #     hByLen[len(wi)].append(H)
    #     xs.append(len(wi))
    #     ys.append(H)

    # pylab.scatter(array(xs), array(ys), c='r', s=10)

    # print "Entropy of found words:"

    # for li in sorted(hByLen.keys()):
    #     vals = hByLen[li]
    #     print li, len(vals), "values\tH:", sum(vals)/len(vals)

    # hByLen = defaultdict(list)
    # Hs = reportForAllRealTokens(underlying, foundUnderlying,
    #                             surface=surfaceIsUnderlying,
    #                             foundSurface=foundSurfaceIsUnderlying,
    #                             verbose=False)

    # xs = []
    # ys = []

    # for wi,H in Hs.items():
    #     # print " ".join(vi)
    #     # print
    #     hByLen[len(wi)].append(H)
    #     xs.append(len(wi))
    #     ys.append(H)

    # pylab.scatter(array(xs), array(ys), c='b', s=10)

    # print "Entropy of real words:"

    # for li in sorted(hByLen.keys()):
    #     vals = hByLen[li]
    #     print li, len(vals), "values\tH:", sum(vals)/len(vals)

    # ################

    # freq = wordFreqs(foundUnderlying)
    # hByFreq = defaultdict(list)
    # Hs = reportForAllFoundTokens(underlying, foundUnderlying,
    #                             surface=surfaceIsUnderlying,
    #                             foundSurface=foundSurfaceIsUnderlying,
    #                             verbose=False)
    # for wi,H in Hs.items():
    #     # print " ".join(vi)
    #     # print
    #     hByFreq[freq[wi]].append(H)

    # print "Entropy of found words:"

    # xs = []
    # ys = []

    # for li in sorted(hByFreq.keys()):
    #     vals = hByFreq[li]
    #     print li, len(vals), "values\tH:", sum(vals)/len(vals)

    #     for vi in vals:
    #         ys.append(vi)
    #         xs.append(li)
    #         assert(isinstance(vi, float))
    #         assert(isinstance(li, int))

    # #pylab.scatter(array(xs), array(ys), c='r', s=10)

    freq = wordFreqs(underlying)
    hByFreq = defaultdict(list)
    Hs = reportForAllRealTokens(underlying, foundUnderlying,
                                surface=surfaceIsUnderlying,
                                foundSurface=foundSurfaceIsUnderlying,
                                verbose=False)
    for wi,H in Hs.items():
        # print " ".join(vi)
        # print
        hByFreq[1].append(H)

    print "Entropy of real words:"
    for li in sorted(hByFreq.keys()):
        vals = hByFreq[li]
        print li, len(vals), "values\tH:", sum(vals)/len(vals)


    # xs = []
    # ys = []

    # for li in sorted(hByFreq.keys()):
    #     vals = hByFreq[li]
    #     print li, len(vals), "values\tH:", sum(vals)/len(vals)
    #     for vi in vals:
    #         ys.append(vi)
    #         xs.append(li)
    #         assert(isinstance(li, int))

    # #pylab.scatter(array(xs), array(ys), c='b', s=10)

    # pylab.show()
