#!/usr/bin/python
from __future__ import division

import sys
from path import path
import re
from itertools import izip
from collections import defaultdict

import PrecRec

from scoreSeg import formatLines, getWords, cat

if __name__ == "__main__":
    ff = sys.argv[1]
    under = path(ff + ".underlying")
    surf = path(ff + ".surface")

    assert(under.exists() and surf.exists())

    target = tuple(sys.argv[2:])

    res = defaultdict(int)

    for under,surf in izip(formatLines(under), formatLines(surf)):
        words,bounds = getWords(under)
        for wi,si in izip(words, surf):
            if si == target:
                res[wi] += 1

    print "Underlying forms of", target, ":"
    for si,ct in reversed(sorted(res.items(), key=lambda x: x[1])):
        print si, ct
