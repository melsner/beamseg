#!/usr/bin/python
from __future__ import division

import sys
from path import path
import re
from itertools import izip
from collections import defaultdict

import PrecRec

from scoreSeg import formatLines, getWords, cat

if __name__ == "__main__":
    stem = sys.argv[1]

    learnedStem = sys.argv[2]

    underlying = path(stem + ".underlying")
    surface = path(stem + ".surface")

    foundUnderlying = path(learnedStem + ".learned.underlying")
    foundSurface = path(learnedStem + ".learned.surface")

    for fn in [underlying, surface, foundUnderlying, foundSurface]:
        if not fn.exists():
            print fn, "not found"
            sys.exit(0)

    realizations = defaultdict(lambda: defaultdict(int))
    mistakes = defaultdict(lambda: defaultdict(int))
    changes = defaultdict(lambda: defaultdict(int))
    corrections = defaultdict(lambda: defaultdict(int))
    miscorrections = defaultdict(lambda: defaultdict(int))

    for surf,under,surfF,underF in izip(formatLines(surface), 
                                        formatLines(underlying),
                                        formatLines(foundSurface),
                                        formatLines(foundUnderlying)):
        realWords,realBounds = getWords(under)
        foundWords,foundBounds = getWords(underF)

        boundToWord = dict(zip(foundBounds,foundWords))

        for wi,bi in izip(realWords, realBounds):
            if bi in boundToWord:
                foundWord = boundToWord[bi]

                if foundWord != wi:
                    # print "Real", "".join(wi), "Found", "".join(foundWord)
                    mistakes["".join(wi)]["".join(foundWord)] += 1

        surfChars = cat(surf)
        for wi,bi in izip(realWords, realBounds):
            surfSub = surfChars[bi[0]:bi[1]]
            changes["".join(wi)]["".join(surfSub)] += 1
            if wi != surfSub:
                if bi in boundToWord:
                    foundWord = boundToWord[bi]

                    realizations["".join(foundWord)]["".join(surfSub)] += 1

                    if foundWord == wi:
                        # print "Found", foundWord, "surface was", surfSub
                        corrections["".join(wi)]["".join(surfSub)] += 1
            elif wi == surfSub:
                if bi in boundToWord:
                    foundWord = boundToWord[bi]

                    realizations["".join(foundWord)]["".join(surfSub)] += 1

                    if foundWord != wi:
                        miscorrections["".join(wi)]["".join(foundWord)] += 1

    print "--- changes from underlying to surf ---"
    total = 0
    for baseWord,tab in sorted(changes.items(), key=lambda x: x[0]):
        if any([x != baseWord for x in tab]):
            print baseWord
            for change,ct in tab.items():
                print "\t%s:\t%d" % (change, ct)
                if change != baseWord:
                    total += ct
            print
    print total, "total changes"

    print

    print "--- mistaken underlying forms ---"
    total = 0
    for baseWord,tab in mistakes.items():
        print baseWord
        for mistake,ct in tab.items():
            print "\t%s:\t%d" % (mistake, ct)
            total += ct
        print
    print total, "total mistakes"

    print

    print "--- altered underlying forms ---"
    total = 0
    for baseWord,tab in realizations.items():
        if any([x != baseWord for x in tab]):
            for change,ct in tab.items():
                if change != baseWord:
                    total += ct

    for baseWord,tab in sorted(realizations.items(), key=lambda x: x[0]):
        if any([x != baseWord for x in tab]):
            print baseWord
            for change,ct in tab.items():
                if change != baseWord:
                    sign = "!" * int(100 * (ct / total))
                else:
                    sign = ""
                print "\t%s:\t%d %s" % (change, ct, sign)
            print
    print total, "total alterations"

    print

    print "--- corrected underlying forms ---"
    total = 0
    for baseWord,tab in corrections.items():
        print baseWord
        for corr,ct in tab.items():
            print "\t%s:\t%d" % (corr, ct)
            total += ct
        print
    print total, "total corrections"

    print "--- miscorrected underlying forms ---"
    total = 0
    for baseWord,tab in miscorrections.items():
        print baseWord
        for corr,ct in tab.items():
            print "\t%s:\t%d" % (corr, ct)
            total += ct
        print
    print total, "total miscorrections"
